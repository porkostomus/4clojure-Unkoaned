{:_id 38 :restricted ["max" "max-key"], :title "Maximum value"
:tests ["(= (__ 1 8 3 4) 8)" "(= (__ 30 20) 30)" "(= (__ 45 67 11) 67)"]
:description "Write a function which takes a variable number of parameters and returns the maximum value."
:tags ["easy" "core-functions"]}

(defn get-highest [& all]
  (last
    (sort all)))
1 8 3 4
;;=> 8

(#(last (sort %&)) 30 20) ;;=> 30	
(#(last (sort %&)) 45 67 11) ;;=> 67


(fn [& args] (last (sort args)))

adereth's solution:

(fn [ & xs ]
  (reduce #(if (> %1 %2) %1 %2) xs))
mfikes's solution:

(fn m [& args] (reduce #(if (< %1 %2) %2 %1) args))

chouser's solution:

#(reduce (fn [a b] (if (> a b) a b)) %&)

aengelberg's solution:

(fn [& stuff]
  (- (apply min (map - stuff))))

chunchangshao's solution:

(fn [x & more] (reduce #(if (> %2 %) %2 %) x more))

hypirion's solution:

#(last (sort %&))

jafingerhut's solution:

(fn [& args] (last (sort args)))

balint's solution:

(fn [& xs] (reduce (fn [a b] (if (> a b) a b)) xs))

amcnamara's solution:

#(last (sort %&))

stuarth's solution:

(fn [& ls] 
  (reduce #(if (> %1 %2) %1 %2) ls))