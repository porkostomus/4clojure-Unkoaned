{:_id 42 :title "Factorial Fun"
:tests ["(= (__ 1) 1)" "(= (__ 3) 6)" "(= (__ 5) 120)" "(= (__ 8) 40320)"]
:description "Write a function which calculates factorials."
:tags ["easy" "math"]}

(defn bang-it [n]
  (reduce * (range 1 (inc n))))
#'user/bang-it
user> (bang-it 8)
40320

#(apply * (range 1 (+ 1 %)))

adereth's solution:

#(reduce * (range 1 (inc %)))

mfikes's solution:

(fn f [n] (if (zero? n) 1 (* n (f (dec n)))))

chouser's solution:

#(apply * (range 1 (inc %)))

aengelberg's solution:

(fn fac [n]
  (if (= n 0) 1 (* n (fac (dec n)))))

chunchangshao's solution:

#(reduce * (map inc (range %)))

hypirion's solution:

(fn ! [n]
  (if (zero? n) 1
      (* n (! (- n 1)))))

jafingerhut's solution:

#(reduce * (range 1 (inc %)))

balint's solution:

#(last
  (take (inc %) 
    (map last
      (iterate
        (fn [fact]
          (conj fact (* (last fact) (count fact))))
        [1]))))

amcnamara's solution:

#(apply * (range 1 (inc %)))

amalloy's solution:

#(apply * (range 1 (inc %)))

stuarth's solution:

(fn [acc n] (if (zero? n) acc (recur (* n acc) (dec n)))) 1

dbyrne's solution:

#(apply * (range 1 (inc %)))