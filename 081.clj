;; 81 Set Intersection

{:_id 81 :restricted ["intersection"]
:title "Set Intersection"
:tests [
"(= (__ #{0 1 2 3} #{2 3 4 5}) #{2 3})"
"(= (__ #{0 1 2} #{3 4 5}) #{})"
"(= (__ #{:a :b :c :d} #{:c :e :a :f :d}) #{:a :c :d})"]
:description "Write a function which returns the intersection of two sets.
The intersection is the sub-set of items that each set has in common."
:tags ["easy" "set-theory"]}

user=> (find-doc " intersection")
-------------------------
clojure.set/intersection
([s1] [s1 s2] [s1 s2 & sets])
  Return a set that is the intersection of the input sets

((fn [a b] (set (filter #(contains? b %) a)))
#{0 1 2 3} #{2 3 4 5})
;;=> #{2 3}
((fn [a b] (set (filter #(contains? b %) a)))
#{0 1 2} #{3 4 5})
;;=> #{}
((fn [a b] (set (filter #(contains? b %) a)))
#{:a :b :c :d} #{:c :e :a :f :d})
;;=> #{:a :c :d}


(fn [a b] (set (filter #(contains? b %) a)))

adereth's solution:

(fn [s1 s2] (set (filter s1 s2)))

mfikes's solution:

(fn inter [a b] (apply hash-set (filter #(b %1) a)))

chouser's solution:

#(set (filter % %2))

aengelberg's solution:

#(set (filter %2 %))

chunchangshao's solution:

#(let [d clojure.set/difference](d % (d % %2)))

hypirion's solution:

(fn [a b]
  (into #{} (filter a b)))

jafingerhut's solution:

(fn [x y]
  (set (filter #(contains? x %) y)))

balint's solution:

(fn [s1 s2] (into #{} (filter #(s2 %) s1)))

borkdude's solution:

(fn [s1 s2]
  (clojure.set/difference s1 
     (clojure.set/difference s1 s2)))

amcnamara's solution:

#(set (filter (fn [i] (some % #{i})) %2))

amalloy's solution:

(comp set keep)

stuarth's solution:

(fn [s1 s2] (into #{} (keep s2 s1)))