;; 121 Universal Computation Engine [m]

Takes mathematical formula in prefix notation,
returns a function that calculates the value of the formula.
The formula can contain nested calculations
using the four basic mathematical operators,
numeric constants, and symbols representing variables.
The returned function has to accept a single parameter
containing the map of variable names to their values. 
Special Restrictions: eval, resolve

(= 2 ((__ '(/ a b))
      '{b 8 a 16}))
(= 8 ((__ '(+ a b 2))
      '{a 2 b 4}))
(= [6 0 -4]
     (map (__ '(* (+ 2 a)
                  (- 10 b)))
            '[{a 1 b 8}
              {b 5 a -2}
              {a 2 b 11}]))
(= 1 ((__ '(/ (+ x 2)
              (* 3 (+ y 1))))
      '{x 4 y 1}))
 
(fn [form]
  (fn [values]
    (let [env (merge {'+ + '- - '* * '/ /} values)] 
     ((fn eval- [f]
        (if (seq? f)
            (let [[op & args] f] (apply (env op) (map eval- args)))
            (get env f f)))
       form))))

(fn [form] (fn [values] (let [env (merge {'+ + '- - '* * '/ /} values)] 
((fn eval- [f] (if (seq? f) (let [[op & args] f] (apply (env op) (map eval- 
args))) (get env f f))) form))))

adereth's solution:

#(fn [m] ((fn v [x] (if (number? x) x
                        (let [[op & r] x]
                          (apply ({'/ / '* * '+ + '- -} op)
                                 (map v r)))))
           (clojure.walk/prewalk-replace m %)))
       
mfikes's solution:

(fn [symbolic-expression]
  (fn [values-map]
    (let [
          ops-map {'+ + '- - '* * '/ /}
          substitute (fn [coll] (map #(get values-map % %) coll))
          evaluate (fn evaluate [expression]
                     (if (number? expression)
                       expression
                       (let [[op & args] (substitute expression)]
                         (apply (ops-map op) (map evaluate args)))))]
      (evaluate symbolic-expression))))
  
chouser's solution:

(fn [x]
  (fn [m] ((fn e [x m]
    (if (seq? x)
      (apply ({'+ + '- - '* * '/ /} (first x))
             (map #(e % m) (rest x)))
      (m x x)))
    x m)))

aengelberg's solution:

(fn [l]
  (fn [m]
    ((fn f [l]
       (cond
        (number? l) l
        (seq? l) (apply (f (first l))(map f (rest l)))
        :else ((assoc m '+ + '- - '* * '/ /) l)))
     l)))
 
chunchangshao's solution:

(fn
  [str-fn]
  (fn
  [x]
  (letfn [(sym-2-str [[k v]] {(str k) v})
          (fn-choose [f-sym] (case (str f-sym) "+" + "-" - "*" * "/" /))
          (eval-fn [f m] (if ((complement seq?) f)
                           (if (number? f) f (m (str f)))
                           (apply (fn-choose (first f)) (map eval-fn (next f) (repeat m)))
               ))]
          (eval-fn str-fn (into {} (map sym-2-str x))))))
      
hypirion's solution:

(fn [expr]
  (fn [hm] 
    (letfn [(evl [x]
              (if (list? x)
                (let [[f & r] (map evl x)]
                  (apply f r))
                ({'+ +, '- -, '/ /, '* *} x x)))]
      (evl (clojure.walk/prewalk-replace hm expr)))))
  
jafingerhut's solution:

(fn [expr]
  (let [my-eval (fn my-eval [expr]
                  (if (coll? expr)
                    (apply ({'+ + '- - '* * '/ /} (first expr))
                           (map my-eval (rest expr)))
                    expr))]
    (fn [vals]
      (my-eval (clojure.walk/prewalk-replace vals expr)))))
  
balint's solution:

(fn [form]
  (fn [values]
    (let [ops-vars (merge {'+ + '- - '* * '/ /} values)]
      ((fn eval- [f]
        (if (seq? f)
          (let [[f & args] f]
            (apply (ops-vars f) (map eval- args)))
          (get ops-vars f f)))
        form)
      )))
  
amcnamara's solution:

(fn b [c]
  (fn [m]
    (apply (fn [f & a] (apply f a))
           (map #(if (seq? %) ((b %) m) %)
            (replace (into m {'+ + '- - '/ / '* *}) c)))))
        
amalloy's solution:

(fn [formula]
  (fn [values]
    ((fn compute [x]
       (if (seq? x)
         (let [[op & args] x]
           (apply ({'+ + '/ / '- - '* *} op)
                  (map compute args)))
         (get values x x)))
     formula)))