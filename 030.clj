{:_id 30 :title "Compress a Sequence"
:tests ["(= (apply str (__ \"Leeeeeerrroyyy\")) \"Leroy\")"
"(= (__ [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))"
"(= (__ [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2]))"]
:description "Write a function which removes consecutive duplicates from a sequence."
:tags ["easy" "seqs"]}

(defn compress [s]
  (map first
    (partition-by identity s)))
#'user/compress
user=> (compress [1 1 2 3 3 2 2 3])
(1 2 3 2 3)

#(map first (partition-by identity %))

adereth's solution:

#(map first (partition-by identity %))

mfikes's solution:

reduce #(if (= (last %1) %2) %1 (concat %1 [%2])) []

chouser's solution:

#(map first (partition-by identity %))

aengelberg's solution:

(fn f [l]
  (if (empty? l)()
    (if (empty? (rest l)) l
      (if (= (first l)(second l))(f (rest l))
        (lazy-seq (cons (first l)(f (rest l))))))))

chunchangshao's solution:

#(loop [s %,res []] (if (empty? s) res (recur (rest s) (if (= (first s) (last res)) res (conj res (first s))))))

hypirion's solution:

(fn [a]
  (let [partitioned 
          (->> (partition 2 1 a)
               (remove (partial apply =)))]
    (concat (first partitioned)
          (map second (rest partitioned)))))

jafingerhut's solution:

(fn [s] (map first (partition-by identity s)))

balint's solution:

(fn [stream]
  (reverse
    (reduce
      #(if
        (or (empty? %1) (not= (first %1) %2))
          (conj %1 %2)
          %1)
      '()
      stream)))

amcnamara's solution:

#(mapcat distinct (partition-by identity %))

amalloy's solution:

#(map last (partition-by max %))

stuarth's solution:

(fn [s] (reduce (fn [agg l] (if (= (last agg) l) agg (conj agg l))) [] (seq s)))

dbyrne's solution:

(fn f [x]
  (if-let [y (first x)]
    (cons y (f (drop-while #(= y %) x)))))