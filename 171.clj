Intervals
Difficulty:	Medium
Topics:	

Write a function that takes a sequence of integers and returns a sequence of "intervals".
Each interval is a a vector of two integers, start and end,
such that all integers between start and end (inclusive) are contained in the input sequence.

(= (__ [1 2 3]) [[1 3]])

(= (__ [10 9 8 1 2 3]) [[1 3] [8 10]])

(= (__ [1 1 1 1 1 1 1]) [[1 1]])

(= (__ []) [])

(= (__ [19 4 17 1 3 10 2 13 13 2 16 4 2 15 13 9 6 14 2 11])
       [[1 4] [6 6] [9 11] [13 17] [19 19]])
 
(fn intervals [s]
  (reduce (fn [memo e]
            (let [memo (if (empty? memo) [[e e]] memo)
                  [l h] (nth memo (dec (count memo)))]
              (cond
               (= e h) memo
               (= e (inc h)) (assoc-in memo [(dec (count memo)) 1] e)
               :else (conj memo [e e])))) 
          [] (distinct (sort s))))

(fn intervals [s]
  (reduce (fn [memo e]
            (let [memo (if (empty? memo) [[e e]] memo)
                  [l h] (nth memo (dec (count memo)))]
              (cond
               (= e h) memo
               (= e (inc h)) (assoc-in memo [(dec (count memo)) 1] e)
               :else (conj memo [e e])))) 
          [] (distinct (sort s))))
      
adereth's solution:

(fn [s]
  (let [n (reduce conj #{} s)]
    (partition 2
    (reduce (fn [a v]
              (cond (and (n (dec v)) (n (inc v))) a
                    (or (n (dec v)) (n (inc v))) (conj a v)
                    :e (-> a (conj v) (conj v))))
            []
            (sort n)))))
        
mfikes's solution:

(fn [coll]
  (reverse (reduce (fn [[[a b] & r :as is] n]
                     (if (and a (= (inc b) n))
                       (cons [a n] r)
                       (cons [n n] is)))
                   ()
                   (distinct (sort coll)))))
               
chouser's solution:

#(->> % distinct sort
  (reduce
    (fn [[[l h] & im] x]
      (if (= x (+ 1 h))
        (cons [l x] im)
        (list* [x x] [l h] im)))
    [[0 -1]])
  reverse
  rest)

aengelberg's solution:

(fn [l]
  (let [next-seq (fn [l]
                   (let [c (count (first (partition-by identity (map - l (range)))))
                         s (take c l)]
                     [[(first s)(last s)] (drop c l)]))
        s (fn s [l]
            (if (empty? l)
              ()
              (let [[f r] (next-seq l)]
                (cons f (s r)))))]
    (s (distinct (sort l)))))

chunchangshao's solution:

#(case (count %) 
   0 [] 3 [[1 3]] 
   6 [[1 3] [8 10]]
   7 [[1 1]]
   [[1 4] [6 6] [9 11] [13 17] [19 19]])

hypirion's solution:

(fn [a]
  (let [[f & b] (distinct (sort a))]
    (loop [f f l f [n & r] b a []]
      (cond (nil? f) a
            (= (+ 1 l) n) (recur f n r a)
            :else (recur n n r (conj a [f l]))))))
        
jafingerhut's solution:

(fn [c]
  (let [sorted (apply sorted-set c)]
    (if (seq sorted)
      (loop [ret []
             minval (first sorted)
             maxval minval
             sorted (rest sorted)]
        (if-let [x (first sorted)]
          (if (= x (inc maxval))
            (recur ret minval x (rest sorted))
            (recur (conj ret [minval maxval]) x x (rest sorted)))
          (conj ret [minval maxval])))
      [])))
  
balint's solution:

(fn [args]
  ((fn make-interval [c]
    (let [adj (take-while (fn [[a b]] (= (inc a) b))
                               (partition 2 1 c))]
      (cond
        (empty? c) []
        (empty? adj)
          (cons
            [(first c) (first c)]
            (make-interval (next c)))
        :else
          (cons
            [(ffirst adj) (last (last adj))]
            (make-interval (drop (inc (count adj)) c))))))
    (sort (set args))))

amcnamara's solution:

(fn [v]
  (#(loop [[a & [b & _ :as e]] % i (first %) r []]
      (if b
        (if (= b (inc a))
          (recur e i r)
          (recur e b (into r [[i a]])))
        (if i
          (into r [[i a]])
          [])))
    (sort (distinct v))))

amalloy's solution:

(fn [coll]
  (if (empty? coll)
    (),
    ((fn step [nums]
       (lazy-seq
        (let [start (first nums)]
          (loop [end start, nums (rest nums)]
            (if (empty? nums)
              [[start end]]
              (let [x (first nums)]
                (if (= x (inc end))
                  (recur x (rest nums))
                  (cons [start end] (step nums)))))))))
     (into (sorted-set) coll))))