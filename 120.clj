;; 120 Sum of squares

Takes coll of ints
Returns count of how many elements
are smaller than the sum
of their squared component digits.
Example:
10 is larger than 1 squared plus 0 squared
15 is smaller than 1 squared plus 5 squared
	
((fn sum-square [coll]
   (let [digits (fn [n] (map #(- (int %) 48) (str n)))
         square #(* % %)
         sum-digits (fn [n] (reduce + (map square (digits n))))]
     (count (filter #(< % (sum-digits %)) coll))))
  (range 10))
;;=> 8

((fn sum-square [coll] (let [digits (fn [n] (map #(- (int %) 48) (str n))) square #(* % %) sum-digits (fn [n] (reduce + (map square (digits n))))] (count (filter #(< % (sum-digits %)) coll)))) (range 30)) ;;=> 19
((fn sum-square [coll] (let [digits (fn [n] (map #(- (int %) 48) (str n))) square #(* % %) sum-digits (fn [n] (reduce + (map square (digits n))))] (count (filter #(< % (sum-digits %)) coll)))) (range 100))  ;;=> 50
((fn sum-square [coll] (let [digits (fn [n] (map #(- (int %) 48) (str n))) square #(* % %) sum-digits (fn [n] (reduce + (map square (digits n))))] (count (filter #(< % (sum-digits %)) coll)))) (range 1000)) ;;=> 50

(fn sum-square [coll] (let [digits (fn [n] (map #(- (int %) 48) (str n))) square #(* % %) sum-digits (fn [n] (reduce + (map square (digits n))))] (count (filter #(< % (sum-digits %)) coll))))

adereth's solution:

(fn [s]
  (let [squared-digits (fn [n]
                 (map second       
                      (rest
                       (take-while #(not= % [0 0])
                                   (iterate
                                    (fn [[q r]] [(quot q 10) (* (rem q 10) (rem q 10))])
                                    [n 0])))))
        squared-digits-sum (->> s
                            (map squared-digits)
                            (map #(reduce + %)))
        is-smaller (map #(< %1 %2) s squared-digits-sum)
        ]
    (count (filter identity is-smaller))
   ))

mfikes's solution:

(fn [coll]
    (let [digits (fn digits [n]
                     (if (< n 10)
                       (list n)
                       (cons (rem n 10) (digits (quot n 10)))))]
         (count (filter
                  #(< % (apply + (map * (digits %) (digits %))))
                  coll))))
              
chouser's solution:

reduce
#(+ %
    (if (< %2 (apply + (for [c (str %2)]
                         (Math/pow (- (int c) 48) 2))))
      1
      0))
0

aengelberg's solution:

#({9 8 29 19 99 50 999 50}(last %))

chunchangshao's solution:

#(case (count %) 10 8 30 19 50)

hypirion's solution:

(fn [coll]
  (let [n->d (fn [n] (map #(Character/digit % 10) (str n)))
        d-sum (fn [n] (reduce + (map #(* % %) (n->d n))))]
    (count (filter #(< % (d-sum %)) coll))))

jafingerhut's solution:

(fn [coll]
  (count
   (filter (fn [n]
             (< n (->> (str n)
                       (map #(read-string (str %)))
                       (map #(* % %))
                       (reduce +))))
           coll)))
       
balint's solution:

(fn [is]
  (->
    (filter
      #(let [sumsq (reduce +
                      (map (fn [d] (* (read-string d) (read-string d)))
                           (re-seq #"." (str %))))]
         (< % sumsq))
      is)
    count))

amcnamara's solution:

(fn [r]
  (count
    (filter
      (fn [i]
        (< i (reduce + (map #(Math/pow (- (int %) 48) 2) (str i)))))
      r)))
  
amalloy's solution:

(fn [coll]
  (count
   (for [x coll
         :let [digits (map (comp read-string str) (str x))]
         :when (< x (reduce + (map #(* % %) digits)))]
     x)))
 
stuarth's solution:

(fn [c]
    (letfn [(sqs [n] (reduce + (for [s (str n) :let [x (Integer/parseInt (str s))]]
                         (* x x))))]
      (count (filter #(< % (sqs %)) c))))
  
dbyrne's solution:

(fn [x]
  (count
    (filter
      (fn [y]
        (< y
          (reduce
            #(let [z (- (int %2) 48)]
               (+ % (* z z)))
            0
            (str y))))           
      x)))