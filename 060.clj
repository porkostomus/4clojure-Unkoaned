;; 60 Sequence Reductions
{:_id 60 :restricted ["reductions"]
:tests [
"(= (take 5 (__ + (range))) [0 1 3 6 10])"
"(= (__ conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])"
"(= (last (__ * 2 [3 4 5])) (reduce * 2 [3 4 5]) 120)"]
:description "Write a function which behaves like reduce, but returns each intermediate value of the reduction.
Your function must accept either two or three arguments, and the return sequence must be lazy."
:tags ["medium" "seqs" "core-functions"]}

(= (take 5 (__ + (range))) [0 1 3 6 10])	
(= (__ conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])
(= (last (__ * 2 [3 4 5])) (reduce * 2 [3 4 5]) 120)

(fn g
  ([f [x & s]] (g f x s))
  ([f a [x & s]] 
    (lazy-seq       
        (cons a (if x (g f (f a x) s))))))
        
user=> (take 5 ((fn g
  ([f [x & s]] (g f x s))
  ([f a [x & s]] 
    (lazy-seq       
        (cons a (if x (g f (f a x) s)))))) + (range)))
(0 1 3 6 10)

user=> ((fn g
  ([f [x & s]] (g f x s))
  ([f a [x & s]] 
    (lazy-seq       
        (cons a (if x (g f (f a x) s)))))) conj [1] [2 3 4])
([1] [1 2] [1 2 3] [1 2 3 4])

user=> (last ((fn g
  ([f [x & s]] (g f x s))
  ([f a [x & s]] 
    (lazy-seq       
        (cons a (if x (g f (f a x) s)))))) * 2 [3 4 5]))
120

(fn my-reduce
  ([op input] (my-reduce op (first input) (rest input)))
  ([op result input]
  (lazy-seq
    (if (empty? input) (list result)
      (cons result
            (my-reduce op
                 (op result (first input))
(rest input)))))))

adereth's solution:

(fn r ([f v s]
           (lazy-seq
            (cons v
                  (if (seq s)
                    (r f (f v (first s)) (rest s))))))
  ([f s] (r f (first s) (rest s))))

mfikes's solution:

(fn red [f b & c]
  (let [int-red (fn int-red [f coll acc]
                  (lazy-seq
                    (if (seq coll)
                      (let [next-val (f acc (first coll))]
                        (cons next-val (int-red f (rest coll) next-val))))))]
    (if c
      (cons b (int-red f (first c) b))
      (cons (first b) (int-red f (rest b) (first b))))))

chouser's solution:

(fn r
  ([f [a & b]] (r f a b))
  ([f a b]
    (let [m (atom a)]
      (cons a (map #(swap! m f %) b)))))

aengelberg's solution:

(fn r 
  ([f o l]
  (lazy-seq
    (if
      (= l ())
      (list o)
      (cons o (r f (f o (first l)) (rest l))))))
  ([f l]
    (r f (first l)(rest l))))

chunchangshao's solution:

(fn a ([f x xs]
  (lazy-seq (if-let [t (first xs)] (cons x (a f (f x t) (rest xs))) [x]))) ([f xs] (a f (first xs) (next xs))))

hypirion's solution:

#(letfn [(rdct [fun init [f & r :as lst]]
          (lazy-seq
            (if (empty? lst)
              [init]
              (let [res (fun init f)]
                (cons init (rdct fun res r))))))]
   (if (= 3 (count %&))
    (apply rdct %&)
    (let [[fun elts] %&]
      (rdct fun (first elts) (rest elts)))))

jafingerhut's solution:

(fn r
  ([f c]
     (r f (first c) (rest c)))
  ([f i c]
     (lazy-seq
      (cons i
            (if-let [c (seq c)]
              (r f (f i (first c)) (next c)))))))
          
balint's solution:

(fn reds
  ([f [x & xs]]
   (reds f x xs))
  ([f red coll]
    (lazy-seq
      (cons red
        (when-let [s (seq coll)]
          (reds f (f red (first s)) (rest s)))))))

amcnamara's solution:

(fn r
  ([f c]
    (if (empty? c)
      (f)
      (r f (first c) (rest c))))
  ([f i c]
    (cons i
      (if (not-empty c)
        (lazy-seq (r f (f i (first c)) (rest c)))))))

amalloy's solution:

(fn r
  ([f [x & coll]] (r f x coll))
  ([f acc coll]
     (lazy-seq
      (if-let [[x & coll] (seq coll)]
        (cons acc
              (r f (f acc x) coll))
        [acc]))))
    
stuarth's solution:

(fn r*
    ([f c] (r* f (first c) (rest c)))
    ([f n c]
       (cons n (lazy-seq
          (when-let [c (seq c)]
            (r* f (f n (first c)) (rest c)))))))