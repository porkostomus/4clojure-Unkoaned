{:_id 82 :title "Word Chains"
:tests ["(= true (__ #{\"hat\" \"coat\" \"dog\" \"cat\" \"oat\" \"cot\" \"hot\" \"hog\"}))"
"(= false (__ #{\"cot\" \"hot\" \"bat\" \"fat\"}))"
"(= false (__ #{\"to\" \"top\" \"stop\" \"tops\" \"toss\"}))"
"(= true (__ #{\"spout\" \"do\" \"pot\" \"pout\" \"spot\" \"dot\"}))"
"(= true (__ #{\"share\" \"hares\" \"shares\" \"hare\" \"are\"}))"
"(= false (__ #{\"share\" \"hares\" \"hare\" \"are\"}))"],
:description "A word chain consists of a set of words ordered
so that each word differs by only one letter from the words directly before and after it.
The one letter difference can be either an insertion, a deletion, or a substitution.
Here is an example word chain:<br/><br/>cat -> cot -> coat -> oat -> hat -> hot -> hog -> dog<br/><br/>
Write a function which takes a sequence of words, and returns true if they can be arranged into one continous word chain,
and false if they cannot."
:tags ["hard" "seqs"]}

(fn [t c f r s]
  (> 2 (c (t #{1 2}
    (map #(c (t (fn [x] 
                  (loop [i x j %]
                    (if (not= (f i) (f j))
                      (or (= (r i) (r j)) (= (r i) (seq j)) (= (seq i) (r j)))
                      (or (empty? i) (recur (r i) (r j)))))) s)) s)))))
filter count first rest

(fn [s]
  (or (some (fn [w]
              ((fn f [a s]
                 (or (empty? s)
                     (some #(if (loop [[a & b :as c] (seq a) [d & e :as g] (seq %)]
                                  (if (= a d)
                                    (recur b e)
                                    (or (= b e) (= b g) (= c e))))
                              (f % (disj s %)))
                           s)))
               w (disj s w)))
            s)
      false))

(fn find-chain [words]
  (let [lev (fn lev [s1 s2]
              (cond (empty? s1) (count s2)
                    (empty? s2) (count s1)
                    (= (first s1) (first s2)) (lev (rest s1) (rest s2))
                    :else (inc (min (lev (rest s1) s2)
                                    (lev s1 (rest s2))
                                    (lev (rest s1) (rest s2))))))
        neighbors (fn [words word] (filter #(= (lev word %) 1) words))
        chain (fn chain [graph visited root]
                (let [visited (conj visited root)
                      neigh (remove visited (graph root))]
                  (if (= visited words) 
                    true
                    (some (partial chain graph visited) neigh))))
        graph (into {} (for [w words] [w (neighbors words w)]))]
    (true? (some (partial chain graph #{}) words))))

adereth's solution:

(fn wc [w]
  (let [ld (fn [a b] ((reduce (fn [acc [i j]]
                                (assoc acc [i j]
                                       (if (zero? (min i j))
                                         (max i j)
                                         (min (inc (acc [(dec i) j]))
                                              (inc (acc [i (dec j)]))
                                              (+ (acc [(dec i) (dec j)])
                                                 (if (= (nth a (dec i))
                                                        (nth b (dec j)))
                                                   0 1))))))
                              {}
                              (sort-by #(apply + %)
                                       (for [i (range (inc (count a)))
                                             j (range (inc (count b)))]
                                         [i j])))
                      [(count a) (count b)]))
        adjacent? (fn [w1 w2] (= 1 (ld w1 w2)))
        adjacencies (zipmap (vec w) (for [word (vec w)] (apply hash-set (filter #(adjacent? word %) (vec w)))))
        wc? (fn [ws] (every? identity (map adjacent? ws (rest ws))))
        adjacent? (fn [w1 w2] ((adjacencies w1) w2))
        perms (fn perms [things prev]
                (lazy-seq
                 (if (= 1 (count things))
                   (list things)
                   (for [head things
                         tail (perms (disj things head) head)
                         :when (or (nil? prev)
                                   (adjacent? head prev))]
                     (cons head tail)))))
        ]
 
    (not (nil? (first (filter wc? (perms w nil)))))
 
    ))

mfikes's solution:

(fn chain-exists? [coll]
  (letfn [(chainable? [w1 w2]
                      (if (= (.length w1) (.length w2))
                        (= 1 (apply + (map (fn [c1 c2]
                                             (if (= c1 c2)
                                               0
                                               1))
                                           w1 w2)))
                        (if (< (.length w1) (.length w2))
                          (some #(= (seq w1) (concat (take % w2) (drop (inc %) w2)))
                                (range (.length w2)))
                          (some #(= (seq w2) (concat (take % w1) (drop (inc %) w1)))
                                (range (.length w1))))))
 
          (chains-through? [w0 coll]
                           (if (empty? coll)
                             true
                             (some (fn [w]
                                     (and (chainable? w0 w)
                                          (chains-through? w (disj coll w))))
                                   coll)))]
    (boolean (some (fn [w]
                     (chains-through? w (disj coll w)))
                   coll))))
               
chouser's solution:

(fn [s]
  (or (some (fn [w]
              ((fn f [a s]
                 (or (empty? s)
                     (some #(if (loop [[a & b :as c] (seq a) [d & e :as g] (seq %)]
                                  (if (= a d)
                                    (recur b e)
                                    (or (= b e) (= b g) (= c e))))
                              (f % (disj s %)))
                           s)))
               w (disj s w)))
            s)
      false))
  
aengelberg's solution:

(let [adj? (fn [x y]
             (or (and (empty? x)
                      (empty? y))
                 (= x (rest y))
                 (= (rest x) y)
                 (= (rest x) (rest y))
                 (and (= (first x) (first y))
                      (recur (rest x) (rest y)))))
      f (fn f [s]
          (cond
            (empty? s) '(())
            :else (for [i s
                        :let [s (disj s i)]
                        p (f s)
                        :when (or (empty? p) (adj? (seq (first p)) (seq i)))]
                    (cons i p))))]
  #(>= (count (f %)) 1))

chunchangshao's solution:

#(case (count %) 
   (8 6) true 
   5 (if (some #{"share"} %) true false)
   false)

hypirion's solution:

(letfn [(leven [[fa & ra :as a] [fb & rb :as b]]
          (cond (nil? a) (count b)
                (nil? b) (count a)
                (= fa fb) (leven ra rb)
                :else (+ 1 
                        (min (leven ra rb)
                             (leven a rb)
                             (leven ra b)))))
        (rem-disj [ht e]
          [(dissoc ht e) (ht e)])
        (walkable? [[ht elts]]
          (if (empty? ht) 
            true
            (let [walks (for [n-e elts :when (ht n-e)] 
                          (walkable? (rem-disj ht n-e)))]
              (some true? walks))))]
  (fn [st]
    (let [ht (apply merge-with concat
                (for [a st, b st :when (= 1 (leven a b))] {a [b]}))]
      (or (some #(walkable? (rem-disj ht %)) st)
          false))))
      
jafingerhut's solution:

;; This isn't necessarily an efficient way to solve the problem for
;; large word sets, but should be fast enough for small ones.
 
;; Calculate a map next-words where (next-words w) is a set of all
;; words in the input set that are chainable to the word w.  As a bit
;; of a hack, add a special entry (next-words nil)=word-set to get
;; things started.
 
;; For all possible first words in the set, check all remaining
;; possible second words that are chainable to the first, and from
;; each of those all possible third words that are chainable to the
;; second, etc., until either we exhaust the set of remaining words
;; (and there is thus a solution), or we can find no next words to
;; chain among the remaining ones (and there is no solution).
 
(fn [word-set]
  (letfn [(any? [c]
            (true? (some true? c)))
          (word-except-letter [word i]
            (if (< i (count word))
              (vec (concat (subvec word 0 i) (subvec word (inc i))))))
          (one-letter-different-or-added? [word1 word2]
            (any? (for [i (range (count word2))]
                    (or (= (word-except-letter word1 i)
                           (word-except-letter word2 i))
                        (= word1 (word-except-letter word2 i))))))
          (chainable? [str1 str2]
            (let [word1 (vec str1)
                  word2 (vec str2)]
              (or (one-letter-different-or-added? word1 word2)
                  (one-letter-different-or-added? word2 word1))))]
    (let [next-words (into {nil word-set}
                           (for [word word-set]
                             [word (set (filter #(chainable? word %) word-set))]))
          finishable? (fn f? [cur-word remaining-words]
                        (or (= remaining-words #{})
                            (any? (map #(f? % (disj remaining-words %))
                                       (filter #((next-words cur-word) %)
                                               remaining-words)))))]
      (finishable? nil word-set))))
  
balint's solution:

(fn [words]
    (let [chainable?
          (fn [w1 w2]
            (let [lw (if (>= (count w1) (count w2)) w1 w2)
                 sw (if (>= (count w1) (count w2)) w2 w1)
                 clw (count lw)
                 csw (count sw)
                 letters (partial re-seq #"\w")
                 diff (fn [c1 c2 d]
                        (cond
                          (nil? c1) (+ (count c2) d)
                          (nil? c2) (+ (count c1) d)
                          (not= (first c1) (first c2)) (recur (next c1) c2 (inc d))
                          :else (recur (next c1) (next c2) d)))]
            (cond
              (= clw csw)
                (= 1 (apply + (map #(if (= %1 %2) 0 1) (letters w1) (letters w2))))
              (= 1 (- clw csw))
                (= 1 (diff (letters lw) (letters sw) 0))
              :else false)))]
    (letfn [(word-chain
            ([words] (word-chain words [] (seq words)))
            ([words chain [w & rnwords :as next-words]]
      (if-not (seq words)
        chain
        (when (seq next-words)
          (let [[nw & rninchain :as next-in-chain] (filter #(chainable? w %) words)]
            (or (word-chain (disj words w) (cons w chain) next-in-chain)
                (word-chain words chain rnwords)))))))]
    (if (word-chain words) true false)
    )))

amcnamara's solution:

(fn [t c f r s]
  (> 2 (c (t #{1 2}
    (map #(c (t (fn [x] 
                  (loop [i x j %]
                    (if (not= (f i) (f j))
                      (or (= (r i) (r j)) (= (r i) (seq j)) (= (seq i) (r j)))
                      (or (empty? i) (recur (r i) (r j)))))) s)) s)))))
filter count first rest

amalloy's solution:

(letfn [(subst? [x y]
          (and (= (count x) (count y))
               (not (next (filter false? (map = x y))))))
        (deletion? [x y]
          (let [diff (apply - (map count [x y]))]
            (cond (neg? diff) (deletion? y x)
                  (not= 1 diff) false
                  :else (loop [x x, y y]
                          (if-let [[y & ys :as all-ys] (seq y)]
                            (let [[x & xs] (seq x)]
                              (if (= x y)
                                (recur xs ys)
                                (= all-ys xs)))
                            true)))))
        (link? [x y]
          (or (subst? x y)
              (deletion? x y)))]
  (fn [words]
    (let [links (for [a words]
                  (dec (count (filter #(link? a %)
                                      words))))]
      (> 3 (count (filter #(<= % 1) links))))))
  
dbyrne's solution:

(fn [t c f r s]
  (> 2 (c (t #{1 2}
    (map #(c (t (fn [x] 
                  (loop [i x j %]
                    (if (not= (f i) (f j))
                      (or (= (r i) (r j)) (= (r i) (seq j)) (= (seq i) (r j)))
                      (or (empty? i) (recur (r i) (r j)))))) s)) s)))))
filter count first rest