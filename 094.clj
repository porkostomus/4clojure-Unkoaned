;; 94 Game of Life [h]

The game of life is a cellular automaton
devised by mathematician John Conway. 

The 'board' consists of both live (#) and dead ( ) cells.
Each cell interacts with its eight neighbours
(horizontal, vertical, diagonal),
and its next state is dependent on the following rules:

1) Any live cell with fewer than two live neighbours dies,
as if caused by under-population.
2) Any live cell with two or three live neighbours
lives on to the next generation.
3) Any live cell with more than three live neighbours dies,
as if by overcrowding.
4) Any dead cell with exactly three live neighbours
becomes a live cell, as if by reproduction.

Given a board, returns a board representing
the next generation of cells.

(= (__ ["      "  
        " ##   "
        " ##   "
        "   ## "
        "   ## "
        "      "])
   ["      "  
    " ##   "
    " #    "
    "    # "
    "   ## "
    "      "])
(= (__ ["     "
        "     "
        " ### "
        "     "
        "     "])
   ["     "
    "  #  "
    "  #  "
    "  #  "
    "     "])
(= (__ ["      "
        "      "
        "  ### "
        " ###  "
        "      "
        "      "])
   ["      "
    "   #  "
    " #  # "
    " #  # "
    "  #   "
    "      "])


(fn [d]
    (for [i (range (count d))]
      (apply
       str
       (for [j (range (count (d i)))]
         (let [z (= \# (get-in d [i j]))
               v [-1 0 1]
               u (count (filter #(= \# (get-in d %)) (for [a v, b v] [(+ i a) (+ j b)])))]
           (if (or (== 3 u) (and z (== 4 u))) \# " "))))))

(fn conway [board] 
  (let [cells (set (for [y (range (count board))
                         x (range (count (get board y)))
                         :when (not= \space (get-in board [y x]))]
                     [x y]))
        width (count (first board))
        height (count board)
        neighbors (fn [[x y]]
                    (for [dx [-1 0 1] dy (if (zero? dx) [-1 1] [-1 0 1])]
                      [(+ x dx) (+ y dy)]))
        step (fn [cells]
               (set (for [[loc n] (frequencies (mapcat neighbors cells))
                          :when (or (= n 3) (and (= n 2) (cells loc)))]
                      loc)))
        serialize (fn [alive] 
                    (mapv #(apply str %) 
                          (partition width
                                     (for [y (range height) x (range width) 
                                           :let [sym (if (alive [x y]) \# \space)]]
                                       sym))))]
    (-> cells step serialize)))

adereth's solution:

(fn [b]
  (let [a #(= \# (get-in b %))
        c count
        z range
        s (c b)
        r (z s)
        g #(z (max (- % 1) 0) (min (+ 2 %) s))
        o not=
        v #(let [n (c
                     (filter a (for [m (g %)
                                     n (g %2)
                                     :when (or (o m %) (o n %2))]
                                 [m n])))]
              (if (a [% %2])
                (cond
                 (< n 2) \ 
                 (< 1 n 4) \#
                 :e \ )
                (if (= n 3) \# \ )))]
    (for [x r] (apply str (for [y r] (v x y))))))

mfikes's solution:

(fn next-board [board]
  (let [board-matrix (mapv #(vec %) board)]
    (map-indexed
      (fn [r row]
        (apply str
               (map-indexed
                 (fn [c cell]
                   (let [neighbor-indexes (for [ri (range (dec r) (+ 2 r))
                                                ci (range (dec c) (+ 2 c))
                                                :when (not (and (= ri r) (= ci c)))]
                                            [ri ci])
                         live-neighbor-count (apply + (map #(if (= \# (get-in board-matrix %)) 1 0)
                                                           neighbor-indexes))
                         cell-alive (= \# cell)]
                     (if cell-alive
                       (if (< 1 live-neighbor-count 4) \# \space)
                       (if (= 3 live-neighbor-count) \# \space))))
                 row)))
      board-matrix)))
  
chouser's solution:

#(let [r (range (count %))
       v [-1 0 1]
       a \#]
   (for [y r]
     (apply str (for [x r c [(count
                              (for [j v
                                    k v
                                    :when (= a (get-in % [(+ y j) (+ x k)]))]
                                1))]]
                  (if (or (= c 3) (and (= c 4) (= a (get-in % [y x]))))
                    a
                    \ )))))
                
aengelberg's solution:

#(vec (map (partial apply str)
           (for [i (range (count %))]
             (for [j (range (count (first %)))]
               (let [c (count (filter (partial = \#) (map (partial get-in %) (vec (map vec
                                                                                       (for [a [(dec i) i (inc i)]
                                                                                             b [(dec j) j (inc j)]
                                                                                             :when (not (and (= a i)
                                                                                                             (= b j)))]
                                                                                           [a b]))))))]
                 (case (get-in % [i j])
                   \  (case c 3 \# \ )
                   (case c
                     2 \#
                     3 \#
                     \ ))))))
               )
           
chunchangshao's solution:

(fn [chessboard]
    (let [nrow (count chessboard)
          ncol (count (nth chessboard 0))
          ; 根据下标获取元素
          get-coord (fn [x y] (nth (nth chessboard x) y))
          ; 相邻生命的坐标
          coords (fn [x y] (filter 
                              #(let [row-num (first %)
                                     col-num (last %)] 
                                ; 过滤越界的坐标
                                (and (>= row-num 0) 
                                     (>= col-num 0)
                                     (< row-num nrow) 
                                     (< col-num ncol)))
                              ; 当前坐标周围的八个坐标（笛卡尔积）
                              (for [xx [(dec x) x (inc x)]
                                    yy [(dec y) y (inc y)] 
                                        :when (not (and (= xx x) (= yy y)))]
                                    [xx yy])))
          ; 相邻的生命
          neighbors (fn [x y] (map (partial apply get-coord) (coords x y)))
          ; 相邻生命状态是live（#）的个数
          nlive (fn [x y] (count (filter #(= \# %) (neighbors x y))))
          ; 下一轮的状态（一维数组）
          result (for [x (range 0 nrow)
                       y (range 0 ncol)] 
                    (let [element (get-coord x y)
                          live-num (nlive x y)]
                        (if (= \# element)
                            ; live状态的生命周围如果有2个或3个生命是live的，则下轮继续存活，否则死亡
                            (if (or (< live-num 2) (> live-num 3)) \space \#)
                            ; die状态的生命周围如果有刚好3个生命是live的，则下轮复活，否则死亡
                            (if (= live-num 3) \# \space))))]
          ; 把结果转成4Clojure需要的形式
          (map (partial apply str) (partition ncol result))))
      
hypirion's solution:

(fn [board]
  (let [w (count (first board))
        h (count board)
        at (fn [x y] (.charAt (board y) x))
        tree (->>
              (for [x (range w), y (range h)]
                (zipmap (map #(map + [x y] %)
                             [[1 0] [0 1] [-1 0] [0 -1]
                              [1 1] [-1 -1] [-1 1] [1 -1]])
                        (repeat (if (= \# (at x y)) 
                                      [\#] nil))))
              (apply merge-with concat))]
    (for [y (range h)]
      (apply str
        (for [x (range w)]
          (condp = (count (tree [x y]))
                 2 (at x y)
                 3 \#
                 \space))))))
             
jafingerhut's solution:

(fn [b]
  (let [w (count (b 0))
        h (count b)
        l (fn [r c]
            (if (and (< -1 r h) (< -1 c h))
              (if (= \# (.charAt (b r) c)) 1 0)
              0))
        n (fn [r c]
            (apply + (map #(l (+ r %) (+ c %2))
                          [-1 -1 -1  0  0  1  1  1]
                          [-1  0  1 -1  1 -1  0  1])))]
    (for [r (range 0 h)]
      (apply str (for [c (range 0 w)]
                   (let [n (n r c)]
                     (if (or (= 3 n)
                             (and (= 1 (l r c)) (= 2 n)))
                       \# " ")))))))
                   
balint's solution:

(fn [board]
  (letfn [(to-living [brd]
            (into #{}
              (for [x (range) :while (< x (count brd))
                    y (range) :while (< y (count (brd x)))
                    :when (= \# (get (brd x) y))]
                [x y])))
          (to-board [n living]
            (for [r (range n)]
              (apply str
                (for [c (range n)] (if (living [r c]) \# \space)))))
          ;; neighbors and cells are copied verbatim from C. Legrand's solution
          ;; http://clj-me.cgrand.net/2011/08/19/conways-game-of-life/
          (neighbours [[x y]]
            (for [dx [-1 0 1] dy (if (zero? dx) [-1 1] [-1 0 1])]
                  [(+ dx x) (+ dy y)]))
          (step [cells]
            (set (for [[loc n] (frequencies (mapcat neighbours cells))
                       :when (or (= n 3) (and (= n 2) (cells loc)))]
                   loc)))]
    (to-board (count board) (step (to-living board)))))

amcnamara's solution:

(fn g [a i j v p b]
  (let [n (-> b first i)
        m (i b)]
    (p #(a str %)
        (loop [r [] c (p #(let [s (and (= 9 (i %)) (= \# (nth % 4)))
                                c (i (filter (fn [i] (= \# i)) %))]
                            (if (and s (or (> c 4) (< c 3)))
                              \ 
                              (if (or s (= c 3))
                                \#
                                \ ))) 
                           (for [y (j 0 m)
                                 x (j 0 n)]
                             (flatten
                               (p #(v (a vector %)
                                      (max 0 (- x 1))
                                      (min n (+ x 2)))
                                  (v b (max 0 (- y 1)) (min m (+ y 2)))))))]                                                               
          (if (= [] c)
            r
            (recur (conj r (take n c)) (drop n c)))))))
apply count range subvec map

amalloy's solution:

(fn [board]
  (let [board (vec (map vec board))
        [h w] (map count ((juxt identity first) board))
        cell (fn [y x]
               (get-in board (map mod [y x] [h w])))
        alive? (comp #{\#} cell)
        neighbors (let [offsets [-1 0 1]
                        deltas (for [y offsets, x offsets
                                     :when (not= y x 0)]
                                 [y x])]
                    (fn [y x]
                      (count
                       (for [[dy dx] deltas
                             :when (alive? (+ y dy) (+ x dx))]
                         true))))
        new-state (fn [y x]
                    (let [nbr-count (neighbors y x)]
                      (if (or (= 3 nbr-count)
                              (and (= 2 nbr-count)
                                   (alive? y x)))
                        \#
                        \space)))]
    (for [y (range h)]
      (apply str (for [x (range w)]
                   (new-state y x))))))
               
dbyrne's solution:

(fn step [board]
  (let [max-y (count board)
        max-x (count (first board))]
    (letfn [(alive? [x y]
              (and (> y -1)
                   (> x -1)
                   (< y max-y)
                   (< x max-x)
                   (= \# (nth (nth board y) x))))]
      (map #(apply str %)
           (partition max-x 
                      (for [y (range max-y)
                            x (range max-x)]
                        (let [neighbors (count (filter #(apply alive? %)
                                                       [[(dec x) (dec y)] [x (dec y)] [(inc x) (dec y)]
                                                        [(dec x) y      ]             [(inc x) y      ]
                                                        [(dec x) (inc y)] [x (inc y)] [(inc x) (inc y)]]))
                              self (alive? x y)]
                          (cond
                              (< neighbors 2) " "
                              (= neighbors 2) (nth (nth board y) x)
                              (= neighbors 3) "#"
                              :else " "))))))))