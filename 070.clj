{:_id 70 :title "Word Sorting"
:tests [
"(= (__  \"Have a nice day.\")\n   [\"a\" \"day\" \"Have\" \"nice\"])"
"(= (__  \"Clojure is a fun language!\")\n   [\"a\" \"Clojure\" \"fun\" \"is\" \"language\"])"
"(= (__  \"Fools fall for foolish follies.\")\n   [\"fall\" \"follies\" \"foolish\" \"Fools\" \"for\"])"]
:description "Write a function which splits a sentence up into a sorted list of words.
Capitalization should not affect sort order and punctuation should be ignored."
:tags ["medium" "sorting"]}

(= (__  "Have a nice day.")
   ["a" "day" "Have" "nice"])	
(= (__  "Clojure is a fun language!")
   ["a" "Clojure" "fun" "is" "language"])
(= (__  "Fools fall for foolish follies.")
   ["fall" "follies" "foolish" "Fools" "for"])

(fn [s] (sort-by #(.toLowerCase %) (re-seq #"\w+" s)))

(fn [s] (let [words (map #(clojure.string/replace % #"\W+" "") 
(clojure.string/split s #"\s+")) words-hash (into {} (for [w words] 
[(clojure.string/lower-case w) w]))] (for [sw (sort (keys words-hash))] 
(words-hash sw))))

adereth's solution:

(fn [sent]
  (sort-by #(.toLowerCase %) (.split (->> sent butlast (apply str)) " ")))

mfikes's solution:

#(sort-by clojure.string/lower-case (clojure.string/split (apply str (butlast %)) #" "))

chouser's solution:

#(sort-by (fn [w] (.toLowerCase w)) (re-seq #"\w+" %))

aengelberg's solution:

(fn [x s]
  (let [l (re-seq #"[A-Za-z]+" s)]
    (sort #(compare (x %)(x %2)) l))) clojure.string/lower-case

chunchangshao's solution:

(fn [s_#] (vec (
           (fn [x_#] (sort #(-> (.. (new java.lang.String (clojure.string/lower-case %)) (compareTo (clojure.string/lower-case %2))) (< 0)) x_#))
           (clojure.string/split s_# #"[ |.|!]"))))

hypirion's solution:

(fn [coll]
  (let [words (.split (.replaceAll coll "[.!]" "") " ")]
    (sort-by identity String/CASE_INSENSITIVE_ORDER words)))

jafingerhut's solution:

(fn [s] (sort-by #(.toLowerCase %) (re-seq #"\w+" s)))

balint's solution:

(fn [s]
  (let
    [words (map #(clojure.string/replace % #"\W+" "") (clojure.string/split s #"\s+"))
     words-hash (into {} (for [w words] [(clojure.string/lower-case w) w]))]
  (for [sw (sort (keys words-hash))] (words-hash sw))))

amcnamara's solution:

(fn [s]
  (sort #(.compareToIgnoreCase % %2) 
        (clojure.string/split (clojure.string/replace s #"[.!]" "") #" ")))

stuarth's solution:

(fn [s]
    (sort-by #(.toLowerCase %) (re-seq #"\w+" s)))