{:_id 40 :restricted ["interpose"], :title "Interpose a Seq"
:tests ["(= (__ 0 [1 2 3]) [1 0 2 0 3])"
"(= (apply str (__ \", \" [\"one\" \"two\" \"three\"])) \"one, two, three\")"
"(= (__ :z [:a :b :c :d]) [:a :z :b :z :c :z :d])"]
:description "Write a function which separates the items of a sequence by an arbitrary value."
:tags ["easy" "seqs" "core-functions"]}

user=> (source interpose)
(defn interpose
  "Returns lazy seq of
  elements of coll separated by sep.
  Returns stateful transducer when no colls provided"
  ([sep]
   (fn [rf]
     (let [started (volatile! false)]
       (fn
         ([] (rf))
         ([result] (rf result))
         ([result input]
          (if @started
            (let [sepr (rf result sep)]
              (if (reduced? sepr)
                sepr
                (rf sepr input)))
            (do
              (vreset! started true)
              (rf result input))))))))
  ([sep coll]
   (drop 1 (interleave (repeat sep) coll))))

(defn inject [x coll]
  (rest (interleave (repeat x) coll)))

user> (defn inject [x coll]
  (rest (interleave (repeat x) coll)))
#'user/inject
user> (inject 0 [1 2 3])
(1 0 2 0 3)

", " ["one" "two" "three"]
;;=> "one, two, three"
z [:a :b :c :d]
;;=>  [:a :z :b :z :c :z :d]

(fn [v coll] (butlast (mapcat #(vector % v) coll)))

adereth's solution:

#(butlast (interleave %2 (repeat %1)))

mfikes's solution:

(fn [v coll] (butlast (interleave coll (repeat v))))

chouser's solution:

#(rest (apply concat (for [i %2] [% i])))

aengelberg's solution:

#(rest (mapcat list (repeatedly (constantly %1)) %2))

chunchangshao's solution:

#(drop 1 (interleave (repeat %) %2))

hypirion's solution:

#(butlast (interleave %2 (repeat %1)))

jafingerhut's solution:

(fn my-interp [item coll]
  (if (< (count coll) 1)
    coll
    (let [s (seq coll)]
      (cons (first s)
            (for [x (rest s)
                  y [item x]]
              y)))))
          
balint's solution:

(fn [x coll] (butlast (mapcat #(vector %1 %2) coll (repeat x))))

amcnamara's solution:

#(butlast (mapcat (fn [i] [i %]) %2))

amalloy's solution:

#(rest (mapcat list (repeat %) %2))

stuarth's solution:

(fn [sep s]
    (concat (conj (mapcat (fn [el] [sep el]) (drop 1 s)) (first s))))
