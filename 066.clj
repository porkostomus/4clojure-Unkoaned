{:_id 66 :title "Greatest Common Divisor"
:tests [
"(= (__ 2 4) 2)"
"(= (__ 10 5) 5)"
"(= (__ 5 7) 1)"
"(= (__ 1023 858) 33)"]
:description "Given two integers, write a function which\nreturns the greatest common divisor.", :tags ["easy"]}

Takes two ints, returns GCD
	
#(if (zero? %) %2 (recur (mod %2 %) %))
	
((fn gcd [a b]
   (if (zero? b)
     a
    (recur b (mod a b))))
 2 4)
;;=> 2

((fn gcd [a b] (if (zero? b) a (recur b (mod a b)))) 10 5)
;;=> 5

((fn gcd [a b] (if (zero? b) a (recur b (mod a b)))) 5 7)
;;=>  1

((fn gcd [a b] (if (zero? b) a (recur b (mod a b)))) 1023 858)
;;=> 33

(fn gcd [a b] (if (zero? b) a (recur b (mod a b))))

adereth's solution:

(fn [v1 v2]
  (apply max
         (filter #(and (zero? (mod v1 %))
                       (zero? (mod v2 %)))
                 (range 1 (max v1 v2)))))
             
mfikes's solution:

(fn gdc [a b]
  (let [divisors-a (apply hash-set (filter #(zero? (mod a %)) (range 1 (inc a))))
        divisors-b (apply hash-set (filter #(zero? (mod b %)) (range 1 (inc b))))]
    (apply max (clojure.set/intersection divisors-a divisors-b))))

chouser's solution:

#(if (ratio? (/ % %2)) (/ % (numerator (/ % %2))) (min % %2))

aengelberg's solution:

#(if (= % 0)
      %2
      (recur (mod %2 %) %))

chunchangshao's solution:

(fn g [a b] (if (== 0 (mod a b)) b (g b (mod a b))))

hypirion's solution:

#(if (zero? %2) %1,
    (recur %2 (mod %1 %2)))

jafingerhut's solution:

(fn [a b]
  (cond (< a b) (recur b a)
        (zero? b) a
        :else (recur b (mod a b))))

balint's solution:

(fn [a b]
  (if (zero? (rem a b))
    b
    (recur b (rem a b))))

borkdude's solution:

(fn [a b]
(let [max (max a b)]
                  (loop [div 1, found 1]
                     (if (= div (inc max)) found 
                         (let [rest-a (mod a div)
                              rest-b (mod b div)]
                              (if (and (= 0 rest-a) 
                                       (= 0 rest-b))
                                  (recur (inc div) div)
                                  (recur (inc div) found)))))))
                              
amcnamara's solution:

#(if (= 0 %2) % (recur %2 (mod % %2)))

amalloy's solution:

(fn [a b]
  (cond (= a b) a
        (< a b) (recur (- b a) a)
        :else (recur (- a b) b)))
    
stuarth's solution:

(letfn [(factors [n]
             (set (for [i (range 1 (+ 1 n)) :when (zero? (mod n i))] i)))]
    (fn [a b] (apply max (clojure.set/intersection (factors a) (factors b)))))