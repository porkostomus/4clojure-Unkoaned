{:_id 84 :title "Transitive Closure"
:tests [
"(let [divides #{[8 4] [9 3] [4 2] [27 9]}]\n  (= (__ divides) #{[4 2] [8 4] [8 2] [9 3] [27 9] [27 3]}))"
"(let [more-legs\n      #{[\"cat\" \"man\"] [\"man\" \"snake\"] [\"spider\" \"cat\"]}]
(= (__ more-legs)
#{[\"cat\" \"man\"] [\"cat\" \"snake\"] [\"man\" \"snake\"]
[\"spider\" \"cat\"] [\"spider\" \"man\"] [\"spider\" \"snake\"]}))" "(let [progeny
#{[\"father\" \"son\"] [\"uncle\" \"cousin\"] [\"son\" \"grandson\"]}]
(= (__ progeny)
#{[\"father\" \"son\"] [\"father\" \"grandson\"]
[\"uncle\" \"cousin\"] [\"son\" \"grandson\"]}))"],
:description "Write a function which generates the 
<a href=\"http://en.wikipedia.org/wiki/Transitive_closure\">transitive closure</a> of a 
<a href=\"http://en.wikipedia.org/wiki/Binary_relation\">binary relation</a>.
The relation will be represented as a set of 2 item vectors."
:tags ["hard" "set-theory"]}

(let [divides #{[8 4] [9 3] [4 2] [27 9]}]
  (= (__ divides) #{[4 2] [8 4] [8 2] [9 3] [27 9] [27 3]}))
(let [more-legs
      #{["cat" "man"] ["man" "snake"] ["spider" "cat"]}]
  (= (__ more-legs)
     #{["cat" "man"] ["cat" "snake"] ["man" "snake"]
       ["spider" "cat"] ["spider" "man"] ["spider" "snake"]}))
(let [progeny
      #{["father" "son"] ["uncle" "cousin"] ["son" "grandson"]}]
  (= (__ progeny)
     #{["father" "son"] ["father" "grandson"]
       ["uncle" "cousin"] ["son" "grandson"]}))

#(loop [s %]
   (let [n (into s
                 (for [[a b] s [c d] s 
                       :when (= b c)] 
                   [a d]))]
      (if (= n s) n (recur n))))

(fn [rel]
  (let [roots (into {} (for [[k v] (group-by first rel)] [k (mapv second v)]))
        children (fn children [rels e] 
                   (let [cs (get rels e [])]
                     (cons e (mapcat #(children rels %) cs))))]
    (set (mapcat #(map vector (repeat %) (rest (children roots %))) (keys roots)))))

adereth's solution:

#(loop [s %]
   (let [n (into s
                 (for [[a b] s [c d] s 
                       :when (= b c)] 
                   [a d]))]
      (if (= n s) n (recur n))))
  
mfikes's solution:

(fn [r]
  (let [r' (into r
                 (for [[a b] r
                       [c d] r :when (= b c)]
                   [a d]))]
    (if (= r r')
      r
      (recur r'))))
  
chouser's solution:

#(set (mapcat
        (fn f [[a b :as p] s]
          (cons p
                (mapcat (fn [[c d]]
                          (if (= c b)
                            (cons [a d] (f [a d] (disj s p)))))
                        s)))
        %
        (repeat %)))
    
aengelberg's solution:

(fn [m]
  (let [t (fn [m]
  (into m
        (for [k (map first m)
              v (map second m)
              x (map second m)
              :when (and (m [k x])
                         (m [x v]))]
          [k v])))]
  (loop [m m]
    (let [x (t m)]
      (if (= m x)
        m
        (recur x))))))
    
chunchangshao's solution:

(fn tc [s] (let [new-set (set (for [x s y s :when (and (not= x y) (or (= (x 0) (y 1)) (= (y 1) (x 0))) )] (if (= (x 0) (y 1)) [(y 0) (x 1)] [(x 0) (y 1)])))] (if (empty? (clojure.set/difference new-set s)) s (tc (into s new-set)))))

hypirion's solution:

(fn [hs]
  (let [tree (apply merge-with concat
               (map (fn [[a b]] {a [b]}) hs))]
    (->>
      (for [k (keys tree)]
        (loop [hs (set (tree k))]
          (let [n-hs (into hs (mapcat tree hs))]
            (if (= n-hs hs)
              (map (fn [%] [k %]) n-hs)
              (recur n-hs)))))
      (apply concat)
      set)))
  
jafingerhut's solution:

;; Repeat this step until no more edges are added:
;;     For every pair of edges [u v] [v w], add edge [u w].
 
(fn [edges]
  (let [edges-to-add (fn [edges]
                       (for [[u v] edges
                             [w x] edges
                             :when (= v w)]
                         [u x]))]
    (loop [p nil,
           e edges]
      (if (= e p)
        e
        (recur e (into e (edges-to-add e)))))))
    
balint's solution:

(fn tc
  ([rels] (tc (seq rels) rels))
  ([[rel & rrels] clos]
   (if
     (nil? rel) clos
     (let [[lrel rrel] rel
           new-pairs (for [[l r] clos :when (= rrel l)] [lrel r])]
       (recur rrels (into clos new-pairs))))))
   
amcnamara's solution:

(fn [c]
  (set
    (mapcat 
      (fn [i]
        (map 
          (fn [j] [(first i) j])
          (loop [r [] n (first i)]
            (let [k (get (reduce into (map (partial apply hash-map) c)) n)]
              (if k (recur (conj r k) k) r))))) c)))
          
amalloy's solution:

(letfn [(transit-1 [edges]
          (let [nodes (group-by first edges)]
            (into edges (for [[from to] nodes
                              [_ intermediate] to
                              [_ endpoint] (nodes intermediate)]
                          [from endpoint]))))]
  (fn [edges]
    (->> edges
         (iterate transit-1)
         (partition 2 1)
         (drop-while (partial apply not=))
         ffirst)))
