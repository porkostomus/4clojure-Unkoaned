{:_id 86 :title "Happy numbers"
:tests [
"(= (__ 7) true)"
"(= (__ 986543210) true)"
"(= (__ 2) false)"
"(= (__ 3) false)"]

:description "Happy numbers are positive integers that follow a particular formula:
take each individual digit, square it, and then sum the squares to get a new number.
Repeat with the new number and eventually, you might get to a number whose squared sum is 1.
This is a happy number. An unhappy number (or sad number) is one that loops endlessly.
Write a function that determines if a number is happy or not."
:tags ["easy" "math"]}

(fn [x]
  (let [y (->> x str (map (comp #(* % %) read-string str)) (apply +))]
    (cond
      (= y 1) true
      (= y 42) false
      :else (recur y))))

(fn happy? [n]
  (letfn [(digits [n]
            (map #(Integer/parseInt (str %)) (str n)))
          (sum-of-squares [n]
            (reduce + (map #(* % %) (digits n))))]
    (boolean (some #{1} (take 100 (iterate sum-of-squares n))))))

adereth's solution:

(fn [num]
  (let [digits (fn [n]
                 (map second
                      (rest
                       (take-while #(not= % [0 0])
                                   (iterate
                                    (fn [[q r]] [(quot q 10) (rem q 10)])
                                    [n 0])))))
        s-d (fn [n] (apply + (map #(* % %) (digits n))))]
    (loop [num num
           seen #{}]
      (cond
       (seen num) false
       (= num 1) true
       :else (recur (s-d num) (conj seen num))))))
   
mfikes's solution:

(fn [n]
  (letfn [(digits [n]
                  (if (< n 10)
                    [n]
                    (conj (digits (quot n 10)) (rem n 10))))
          (sum-squared [v]
                       (apply + (map * v v)))
          (happy* [n tried]
                  (let [n' (sum-squared (digits n))]
                    (println n')
                    (if (tried n')
                      false
                      (if (= 1 n')
                        true
                        (recur n' (conj tried n))))))]
    (happy* n #{})))

chouser's solution:

(fn f [s n]
  (or (= 1 n)
    (if (s n)
      false
      (f (conj s n)
         (apply + (for [i (iterate #(int (/ % 10)) n)
                        :while (pos? i)
                        d [(mod i 10)]]
                    (* d d)))))))
#{}

aengelberg's solution:

(fn [n]
  (loop
    [n n
     v #{}]
    (cond
      (= n 1) true
      (v n) false
      :else (recur (->> n
                        str
                        (map #(- (int %) 48))
                        (map #(* % %))
                        (apply +))
                   (conj v n)))))
               
chunchangshao's solution:

#(case % 2 false 3 false true)

hypirion's solution:

(fn [n]
  (loop [n n
         p #{}]
  (cond (p n) false
        (= n 1) true
        :else 
          (recur 
            (reduce +
              (map #(let [a (- (int %) (int \0))] (* a a)) (str n)))
            (conj p n)))))
        
jafingerhut's solution:

(fn [n]
  (loop [n n
         s #{}]
    (let [x (apply + (map #(let [i (- (int %) (int \0))] (* i i)) (str n)))]
      (cond (= x 1) true
            (s x) false
            :else (recur x (conj s x))))))
        
balint's solution:

(fn [n]
  (loop [n n, nums #{}]
    (cond
      (= n 1) true
      (some #(= n %) (rest nums)) false
      :else
        (recur
          (reduce
            (fn [acc d]
              (let [i (read-string d)] (+ acc (* i i))))
            0
            (re-seq #"\d" (str n)))
          (conj nums n)))))
      
amcnamara's solution:

(fn h [n]
  (letfn [(b [o] (apply + (map #(let [a (Integer/parseInt (str %))]
                                  (* a a))
                               (str o))))]
    (loop [p [] i n]
      (if (some #{i} p)
        false
        (if (= i 1)
          true
          (recur (conj p i) (b i)))))))
      
amalloy's solution:

(fn [n]
  (letfn [(ds [n]
            (if (pos? n)
              (conj (ds (int (/ n 10))) (mod n 10))))]
    (loop [n n ns []]
      (cond 
       (some #{n} ns) false
       (= 1 n) true
       :e (recur (->> (ds n) (map #(* % %)) (reduce +)) (conj ns n))))))
   
stuarth's solution:

(fn [n]
    (letfn [(happy-seq [n]
              (let [n* (reduce +(map #(int (Math/pow (Integer/parseInt (str %)) 2)) (str n)))]
                (lazy-seq (cons n*
                                (when-not (zero? n*)
                                  (happy-seq n*))))))]
      (= 1 (last (take 10 (happy-seq n))))))