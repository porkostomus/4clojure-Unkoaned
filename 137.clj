Digits and bases
Difficulty:	Medium
Topics:	math

Write a function which returns a sequence of digits of a non-negative number (first argument)
in numerical system with an arbitrary base (second argument).
Digits should be represented with their integer values,
e.g. 15 would be [1 5] in base 10, [1 1 1 1] in base 2 and [15] in base 16. 

(= [1 2 3 4 5 0 1] (__ 1234501 10))

(= [0] (__ 0 11))

(= [1 0 0 1] (__ 9 2))

(= [1 0] (let [n (rand-int 100000)](__ n n)))

(= [16 18 5 24 15 1] (__ Integer/MAX_VALUE 42))

(fn c [n b]
    (if (< n b) [n]
        (conj (c (quot n b) b) (mod n b))))

(fn [x r] (if (zero? x) [0] (loop [x x, digits []] (if (zero? x) (reverse 
digits) (let [[res rm] ((juxt #(quot % r) #(rem % r)) x)] (recur res (conj 
digits rm)))))))

adereth's solution:

(fn [n b]
  (if (zero? n) [0]
 (reverse (map second
       (rest
        (take-while #(not= % [0 0])
                    (iterate
                     (fn [[q r]] [(quot q b) (rem q b)])
                     [n 0]))))))
)

mfikes's solution:

(fn digits [n b]
  (if (< n b)
    [n]
    (conj (digits (quot n b) b) (rem n b))))

chouser's solution:

#(if (< %2 %3)
   (conj % %2)
   (recur (conj % (rem %2 %3)) (quot %2 %3) %3))
()

aengelberg's solution:

(fn f [x n b]
  (if (= n 0)
    (if x [] [0])
    (conj (f true (quot n b) b)
          (rem n b))))
false

chunchangshao's solution:

#(loop [n %,res '()] (if (zero? n) (if (empty? res) '(0) res) (recur (int (/ n %2)) (conj res (mod n %2)))))

hypirion's solution:

(fn [n m]
  (if (= n 0) [0]
  (loop [n n
         dseq ()]
    (if (zero? n)
      dseq
      (recur (quot n m)
             (cons (mod n m) dseq))))))
         
jafingerhut's solution:

(fn [n b]
  (->> (iterate (fn [[c n]]
                  [(conj c (mod n b)) (quot n b)])
                [(list (mod n b)) (quot n b)])
       (drop-while #(not (zero? (second %))))
       ffirst))
   
balint's solution:

(fn [x r]
  (if (zero? x)
    [0]
    (loop [x x, digits []]
      (if (zero? x)
        (reverse digits)
        (let [[res rm] ((juxt #(quot % r) #(rem % r)) x)]
        (recur res (conj digits rm)))))))
    
amcnamara's solution:

#(let [d (if (= % 0) 0 (int (/ (Math/log %) (Math/log %2))))]
  (loop [r [] n % d d]
    (if (= d 0)
      (conj r n)
      (recur (conj r (quot n (Math/pow %2 d))) (rem n (Math/pow %2 d)) (dec d)))))
  
amalloy's solution:

(fn [n base]
  (reverse ((fn digits [n]
              (lazy-seq
               (let [[q r] ((juxt quot rem) n base)]
                 (cons r (when (pos? q)
                           (digits q))))))
            n)))