;; 113 Making Data Dance [h]

Takes a variable number of integer arguments.
If the output is coerced into a string,
returns a comma (and space) separated list of
the inputs sorted smallest to largest.
If the output is coerced into a sequence,
it should return a seq of unique input elements
in the same order as they were entered.

(= "1, 2, 3" (str (__ 2 1 3)))
(= '(2 1 3) (seq (__ 2 1 3)))
(= '(2 1 3) (seq (__ 2 1 3 3 1 2)))
(= '(1) (seq (apply __ (repeat 5 1))))
(= "1, 1, 1, 1, 1" (str (apply __ (repeat 5 1))))
(and (= nil (seq (__)))
     (=  "" (str (__))))

-------------------------
clojure.core/proxy
([class-and-interfaces args & fs])
Macro
  class-and-interfaces - a vector of class names args -
  a (possibly empty) vector of arguments
  to the superclass constructor.
  f => (name [params*] body) or
  (name ([params*] body) ([params+] body) ...)
  Expands to code which creates a instance of a proxy class
  that implements the named class/interface(s)
  by calling the supplied fns.
  A single class, if provided, must be first.
  If not provided it defaults to Object.

  The interfaces names must be valid interface types.
  If a method fn is not provided for a class method,
  the superclass methd will be called.
  If a method fn is not provided for an interface method,
  an UnsupportedOperationException will be
  thrown should it be called.
  Method fns are closures and can capture
  the environment in which proxy is called.
  Each method fn takes an additional implicit first arg,
  which is bound to 'this.
  Note that while method fns can be provided
  to override protected methods,
  they have no other access to protected members,
  nor to super, as these capabilities cannot be proxied.

#(when %&
   (reify
     clojure.lang.ISeq
     (seq [_] (distinct %&))
     (toString [_] (apply str (interpose ", " (sort %&))))))

#(let [args %&]
   (reify clojure.lang.Seqable
     (toString [this] (clojure.string/join ", " (sort args)))
     (seq [this] (seq (distinct args)))))

#(reify clojure.lang.Seqable
     (seq [t] (not-empty (distinct %&)))
     (toString [t] (clojure.string/join ", " (sort %&)) ))

#(when %&
   (reify
     clojure.lang.ISeq
     (seq [_] (distinct %&))
     (toString [_] (apply str (interpose ", " (sort %&))))))
 
adereth's solution:

(fn [& s]
    (reify java.lang.Iterable
      (toString [this] (clojure.string/join ", " (sort s)))
      (iterator [this] (.iterator (second
           (reduce (fn [[a u] x]
                     (if (a x) [a u]
                         [(conj a x) (conj u x)]))
                   [#{} []] s))))))
               
mfikes's solution:

(fn dance [& xs]
  (reify clojure.lang.Seqable
    (seq [_] (seq (distinct xs)))
    (toString [_] (apply str (interpose ", " (sort xs))))))

chouser's solution:

(fn[& s](reify
clojure.lang.Seqable(seq[_](seq(distinct
s)))(toString[_](clojure.string/join", "(sort s)))))

aengelberg's solution:

#(reify
   Object
   (toString [this] (clojure.string/join ", " (sort %&)))
   clojure.lang.Seqable
   (seq [this] (seq (distinct %&))))

chunchangshao's solution:

#(reify clojure.lang.Seqable (seq [this] (seq (take (count (set %&)) %&))) (toString [this] (apply str (interpose ", " (sort %&)))))

hypirion's solution:

(fn [& coll]
  (reify
    clojure.lang.Seqable
    (seq [_] (and coll (distinct coll)))
    (toString [_] (apply str (interpose ", " (sort coll))))))

jafingerhut's solution:

;; (doc reify) gives most of the info needed to solve this problem.
 
(fn [& args]
  (reify
    clojure.lang.Seqable
    (seq [this] (if (seq args) (distinct args)))
    Object
    (toString [this] (clojure.string/join ", " (sort args)))))

balint's solution:

(fn [& xs]
  (reify
    CharSequence
    (toString [this] (apply str (interpose ", " (sort xs))))
    clojure.lang.Seqable
    (seq [this] (if (empty? xs) nil (distinct xs)))))

amcnamara's solution:

#(proxy [] [])

amalloy's solution:

(fn [& x]
  (reify
    Object
    (toString [this]
      (clojure.string/join ", "
                           (sort x)))
 
    clojure.lang.Seqable
    (seq [this]
      (seq (distinct x)))))