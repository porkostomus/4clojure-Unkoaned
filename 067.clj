{:_id 67 :title "Prime Numbers" :tests ["(= (__ 2) [2 3])" "(= (__ 5) [2 3 5 7 11])" "(= (last (__ 100)) 541)"]
:description "Write a function which returns the first x\nnumber of prime numbers.", :tags ["medium" "primes"]}

(= (__ 2) [2 3])
(= (__ 5) [2 3 5 7 11])
(= (last (__ 100)) 541)

#(take % ((fn sieve [s]
            (cons (first s)
              (lazy-seq (sieve (filter (fn [xx]
                                         (not= 0
                                           (mod xx
                                             (first s))))
                                 (rest s))))))
          (iterate inc 2)))

(fn [x] (take x (filter #(= (inc (mod (apply * (range 1N %)) %)) %) (iterate inc 2))))

(fn [n] 
  (->>
  (range)
  (drop 2)
  (filter (fn [x] (every? #(< 0 (mod x %)) (range 2 x))))
  (take n)))

mfikes's solution:

(fn primes [n]
  (if (= 1 n)
    [2]
    (let [prev-primes (primes (dec n))]
      (conj prev-primes (first (filter 
                                (fn [n] (not-any? zero? (map #(rem n %) prev-primes)))
                                (iterate inc (peek prev-primes))))))))
                            
chouser's solution:

#(take %2 (remove (set (for [i % j (range (+ i i) 999 i)] j)) %))
(range 2 999)

aengelberg's solution:

(fn [n]
  (let [sieve (set
                (apply concat
                  (for [x (range 2 n)]
                    (range (* 2 x) (* n n 2) x))))]
    (take n (filter #(not (sieve %))(range 2 (* n n 2))))))

chunchangshao's solution:

#(case % 2 [2 3] 5 [2 3 5 7 11] (range 542))

hypirion's solution:

(fn [n]
  (loop [i 2
         acc []]
    (if (= (count acc) n)
      acc
      (recur (inc i)
            (if (every? #(pos? (rem i %)) acc)
              (conj acc i)
              acc)))))
          
jafingerhut's solution:

(fn [n]
  (letfn [(p [ps i]
            (if (every? #(not (zero? (mod i %))) ps)
              (lazy-seq (cons i (p (conj ps i) (inc i))))
              (recur ps (inc i))))]
  (take n (p #{} 2))))

balint's solution:

(fn [n]
  (letfn [(prime? [p]
    (or (= p 2)
        (every? #(> (rem p %) 0)
                (range 2 (inc (quot p 2))))))]
  (take n (filter prime? (iterate inc 2)))))

amcnamara's solution:

(fn [n]
  (take n 
    (filter (fn [i] (not-any? #(= 0 (mod i %)) (range 2 i))) (drop 2 (range)))))

amalloy's solution:

stuarth's solution:

(fn [n]
    (letfn [(p? [x] (not-any? #(= 0 (rem x %)) (range 2 x)))]
      (take n (filter #(p? %) (iterate inc 2)))))
  
dbyrne's solution:

(fn [y]
  (take y
    (cons 2 
      (filter
        #(not (some true?
                 (for [x (range 3 %)]
                   (= 0 (rem % x)))))
        (iterate (partial + 2) 3)))))