Pairwise Disjoint Sets
Difficulty:	Easy
Topics:	set-theory

Given a set of sets, create a function which returns true
if no two of those sets have any elements in common1 and false otherwise.
Some of the test cases are a bit tricky, so pay a little more attention to them.

1Such sets are usually called pairwise disjoint or mutually disjoint.

(= (__ #{#{\U} #{\s} #{\e \R \E} #{\P \L} #{\.}})
   true)

(= (__ #{#{:a :b :c :d :e}
         #{:a :b :c :d}
         #{:a :b :c}
         #{:a :b}
         #{:a}})
   false)

(= (__ #{#{[1 2 3] [4 5]}
         #{[1 2] [3 4 5]}
         #{[1] [2] 3 4 5}
         #{1 2 [3 4] [5]}})
   true)

(= (__ #{#{'a 'b}
         #{'c 'd 'e}
         #{'f 'g 'h 'i}
         #{''a ''c ''f}})
   true)

(= (__ #{#{'(:x :y :z) '(:x :y) '(:z) '()}
         #{#{:x :y :z} #{:x :y} #{:z} #{}}
         #{'[:x :y :z] [:x :y] [:z] [] {}}})
   false)

(= (__ #{#{(= "true") false}
         #{:yes :no}
         #{(class 1) 0}
         #{(symbol "true") 'false}
         #{(keyword "yes") ::no}
         #{(class '1) (int \0)}})
   false)

(= (__ #{#{distinct?}
         #{#(-> %) #(-> %)}
         #{#(-> %) #(-> %) #(-> %)}
         #{#(-> %) #(-> %) #(-> %)}})
   true)

(= (__ #{#{(#(-> *)) + (quote mapcat) #_ nil}
         #{'+ '* mapcat (comment mapcat)}
         #{(do) set contains? nil?}
         #{, , , #_, , empty?}})
   false)

(fn [sets] (= (reduce + (map count sets)) (count (reduce clojure.set/union 
  sets))))

adereth's solution:

(fn [sos]
  (reduce #(and %1 %2) true
  (for [s1 sos
        s2 sos]
    (cond (= s1 s2) true
          (and (some nil? s1) (some nil? s2)) false
          (every? nil? (map s1 s2)) true
          :else false))))
      
mfikes's solution:

(fn [s] (= (apply + (map count s)) 
           (count (into #{} (apply concat s)))))
       
chouser's solution:

#(let [s (for [x % i x] i)] (= (count s) (count (set s))))

aengelberg's solution:

(fn [x]
  (let [c (count x)
        s (seq x)]
    (empty?
      (for [i (range (dec c))
            j (range (inc i) c)
            :when (seq (clojure.set/intersection (nth s i)(nth s j)))]
        1))))
    
chunchangshao's solution:

(fn [coll] (= (count (flatten (map vec (#(for [x % y % :while (not= x y)] (clojure.set/intersection x y)) coll)))) 0))

hypirion's solution:

(fn [coll]
  (let [[f & r] (seq coll)]
  (cond (empty? r) true
        (every? (fn [elt] (empty? (clojure.set/intersection f elt))) r) 
          (recur r)
        :otherwise false)))
    
jafingerhut's solution:

(fn [set-of-sets]
  (loop [ss set-of-sets
         u #{}]
    (if-let [ss (seq ss)]
      (if (= #{} (clojure.set/intersection u (first ss)))
        (recur (next ss) (clojure.set/union u (first ss)))
        false)
      true)))
  
balint's solution:

(fn [sets]
  (loop [[fs & rs] (seq sets), elts #{}]
    (if (nil? fs)
      true
      (if (some #(some (fn [e] (= % e)) elts) fs)
        false
        (recur rs (clojure.set/union elts (set fs)))))))
    
amcnamara's solution:

#(= (% #{} %2)(% [] %2))
#(count (reduce into % %2))

amalloy's solution:

(fn [sets]
  (every? true?
          (for [set sets
                item set]
            (not-any? #(contains? % item)
                      (remove #{set} sets)))))
                  
stuarth's solution:

(fn [s]
  (= (count (set (reduce concat s)))
     (reduce + (map count s))))