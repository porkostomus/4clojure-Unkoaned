;; 111 Crossword puzzle [h]

Tests if a word will fit
 
- may use empty spaces (underscores) 
- may use but not conflict with any pre-filled characters. 
- must not use any unusable spaces (hashes). 
- no empty spaces (underscores) or extra characters
  before or after the word
  (but may be bound by unusable spaces) 
- Not case-sensitive. 
- May be placed vertically (proceeding top-down only),
  or horizontally (proceeding left-right only).

(= true  (__ "the" ["_ # _ _ e"]))
(= false (__ "the" ["c _ _ _"
                    "d _ # e"
                    "r y _ _"]))
(= true  (__ "joy" ["c _ _ _"
                    "d _ # e"
                    "r y _ _"]))
(= false (__ "joy" ["c o n j"
                    "_ _ y _"
                    "r _ _ #"]))
(= true  (__ "clojure" ["_ _ _ # j o y"
                        "_ _ o _ _ _ _"
                        "_ _ f _ # _ _"]))

(fn [t b]
  (or
    (some #(= (seq t) %)
          (for [a [(map #(remove #{\ } %) b)]
                b [a (apply map list a)]
                r b
                w (partition-by #{\#} r)]
            (map #({\_ %2} % %) w (str t 0))))
    false))

(fn [w p]
  (let [sm (map #(replace {\space "" \_ \.} %) p)
        co (apply map list sm)
        pl (mapcat #(take-nth 2 (partition-by #{\#} %)) (concat sm co))]
    (boolean (some #(re-matches (re-pattern (apply str %)) w) pl))))

adereth's solution:

(fn [w p]
  (let [p (map #(-> %
                    (.replaceAll " " "")
                    (.replaceAll "_" "."))
               p)
        m (fn [z] (map re-pattern
                                   (mapcat #(.split % "#")
                                           z)))
        hp (m p)
        vp (m (apply map (comp #(reduce str "" %) vector) p))]
    (if (some #(re-matches % w) (concat hp vp)) true false)
    )
  )

mfikes's solution:

(fn [word board-rep]
  (letfn [(transpose [board]
                     (apply map list board))
 
          (rep->board [rep]
                      (map #(take-nth 2 %) rep))
 
          (next-slot [line]
                     (if-let [candidate (next (drop-while (complement #{\#}) line))]
                       (if (= \# (first candidate))
                         (next-slot candidate)
                         candidate)))
 
          (placeable? [word line]
                      (or (and (<= (count word) (count line))
                               (apply = true (map (fn [w l]
                                                    (or (= \_ l)
                                                        (= w l)))
                                                  word line))
                               (or (= (count word) (count line))
                                   (= \# (nth line (count word)))))
                          (if-let [line (next-slot line)]
                            (placeable? word line))))]
    (let [board (rep->board board-rep)]
      (boolean (or (some (partial placeable? word) board)
                   (some (partial placeable? word) (transpose board)))))))
               
chouser's solution:

(fn [t b]
  (or
    (some #(= (seq t) %)
          (for [a [(map #(remove #{\ } %) b)]
                b [a (apply map list a)]
                r b
                w (partition-by #{\#} r)]
            (map #({\_ %2} % %) w (str t 0))))
    false))

aengelberg's solution:

(fn [x g]
  (let [g (for [s g]
            (apply str
                   (clojure.string/split s #" ")))
        g2 (for [i (range (count (first g)))]
             (for [r g]
               (nth r i)))]
    (boolean (some identity
          (for [g [g g2]
                r g
                w (clojure.string/split (apply str r) #"#")
                :when (= (count w)(count x))]
            (every? identity
                    (for [i (range (count w))]
                      (or (= (get w i) \_)
                          (= (get w i) (get x i))))))))))
                      
chunchangshao's solution:

#(case %
   "the" (if (= (first (first %2)) \c) false true)
   "joy" (if (= (nth (first %2) 2) \o) false true)
   true)

hypirion's solution:

(fn [g b]
  (let [b (map #(.replaceAll % " " "") b)]
    (->>
      (apply map list b)
      (map #(apply str %))
      (concat b)
      (mapcat #(.split % "#"))
      (filter #(= (count g) (count %)))
      (some #(every? (fn [[a b]] (or (= a \_) (= a b)))
                     (map vector % g)))
      true?)))
  
jafingerhut's solution:

;; Basic idea: Turn every "potential placement spot" in the crossword
;; puzzle into a regexp, and if the word matches any of the regexes,
;; return true.
 
(fn [word board]
  (let [across (map #(clojure.string/escape % {\space "", \_ \.}) board)
        down (apply map str across)]   ;; transpose the board so down becomes across
    (string? (->> (concat across down)
                  (mapcat #(clojure.string/split % #"#"))
                  (some #(re-matches (re-pattern %) word))))))
              
balint's solution:

(fn [w board]
  (let [spaces
          (mapcat
            #(clojure.string/split % #"#")
            (remove empty?
              (map
                #(clojure.string/replace % #"\s" "")
                (concat
                  board
                  (apply map str board)))))]
    (or
      (some
        #(and
           (= (count w) (count %))
           (every?
             (fn [[box l]] (or (= box \_) (= box l)))
             (map vector % w)))
        spaces)
      false)))
  
amcnamara's solution:

(fn [m e f r n o p w b]
  (let [b (m (p remove #{\ }) b)]
    (not (e (for [i (mapcat (p partition-by #{\#})
                            (o b (apply (p m #(o %&)) b)))
                  :let [j (loop [c i d w]
                            (if (or (> (n d) (n c)) (and (not= \_ (f c)) (not= (f c) (f d))))
                              nil
                              (if (e d)
                                (e c)
                                (recur (r c) (r d)))))]
                  :when j] j)))))
map empty? first rest count concat partial