Palindromic Numbers
Difficulty:	Medium
Topics:	seqs math

A palindromic number is a number that is the same when written forwards or backwards (e.g., 3, 99, 14341).

Write a function which takes an integer n, as its only argument,
and returns an increasing lazy sequence of all palindromic numbers that are not less than n.

The most simple solution will exceed the time limit!

(= (take 26 (__ 0))
   [0 1 2 3 4 5 6 7 8 9 
    11 22 33 44 55 66 77 88 99 
    101 111 121 131 141 151 161])

(= (take 16 (__ 162))
   [171 181 191 202 
    212 222 232 242 
    252 262 272 282 
    292 303 313 323])

(= (take 6 (__ 1234550000))
   [1234554321 1234664321 1234774321 
    1234884321 1234994321 1235005321])

(= (first (__ (* 111111111 111111111)))
   (* 111111111 111111111))

(= (set (take 199 (__ 0)))
   (set (map #(first (__ %)) (range 0 10000))))

(= true 
   (apply < (take 6666 (__ 9999999))))

(= (nth (__ 0) 10101)
   9102019)

(let [nextp (fn [n]
              (let [p #(Long. %)
                    s (str n)
                    l (count s)
                    m (subs s 0 (Math/ceil (/ l 2)))
                    h (str (inc (p m)))
                    f (fn [s] (p (str s (subs (clojure.string/reverse s) (if (even? l) 0 1)))))
                    [a b] (map f [m h])]
                (if (>= a n) a b)))]
  #(iterate (comp nextp inc) (nextp %)))

(let [nextp (fn [n]
              (let [p #(Long. %)
                    s (str n)
                    l (count s)
                    m (subs s 0 (Math/ceil (/ l 2)))
                    h (str (inc (p m)))
                    f (fn [s] (p (str s (subs (clojure.string/reverse s) (if (even? l) 0 1)))))
                    [a b] (map f [m h])]
                (if (>= a n) a b)))]
  #(iterate (comp nextp inc) (nextp %)))

adereth's solution:

(fn [n]
  (let [d #(loop [n % r '()] (cond (< 0 n) (recur (quot n 10) (conj r (rem n 10))) (= '() r) '(0) 1 r))
        b #(loop [v 0 d %] (if (seq d) (recur (+ (* v 10) (first d)) (rest d)) v))
        all-9? (fn [n] (every? #{9} (d n)))
        p? #(let [x (d %)] (= x (reverse x)))
        half-to-flip #(let [digits (d %)]
                        (take
                         (int (Math/ceil (/ (count digits) 2))) digits))
        flip-it (fn [digits double-middle?]
                  (if double-middle?
                    (concat digits (-> digits reverse))
                    (concat (butlast digits)
                            [(last digits)]
                            (-> digits butlast reverse))))
        change? (fn [n] (let [flip? (-> n d count odd?)
                              flip? (if (all-9? n) (not flip?) flip?)
                              ]
                          flip?))
 
        next-pal (fn [n]
                   (if (all-9? n) (+ 2 n)
                       (let [g (d n)
                             c (count g)
                             t (-> c (/ 2) Math/ceil int)
                             
                             o (if (p? n) inc identity)]
                         (b
                          (flip-it (->> g
                                        (take t)
                                        b
                                        o
                                        d
                                        (take t)
                                        )
                                   (even? c)))
                         )))]
    (drop-while #(> n %)
     (filter p? (iterate next-pal n)))))
 
mfikes's solution:

(letfn [(digits [n]
                (if (< n 10)
                  [n]
                  (conj (digits (quot n 10)) (rem n 10))))
 
        (digits->num [[n & r]]
                     (if r
                       (+ (apply * n (repeat (count r) 10)) (digits->num r))
                       n))
 
        (num->half-num [n]
                       (loop [c (quot (count (digits n)) 2)
                              h n]
                         (if (zero? c)
                           h
                           (recur (dec c) (quot h 10)))))
 
        (is-palindromic? [n]
                         (let [d (digits n)]
                           (= d (reverse d))))
 
        (next-palindromic-number [n]
                                 (let [half-num (num->half-num n)
                                       next-half-num (inc half-num)
                                       count-digits-n (count (digits n))
                                       next-half-num-digits (digits next-half-num)]
                                   (if (= (count next-half-num-digits) (count (digits half-num)))
                                     (let [lower-half-digits (if (odd? count-digits-n)
                                                               (butlast next-half-num-digits)
                                                               next-half-num-digits)
                                           all-digits (concat next-half-num-digits (reverse lower-half-digits))]
                                       (digits->num all-digits))
                                     (digits->num (concat (butlast next-half-num-digits)
                                                          (repeat (quot count-digits-n 2) 0)
                                                          [1])))))
 
        (palindromic-number-after [n]
                                  (let [half-num (num->half-num n)
                                        half-num-digits (digits half-num)
                                        lower-half-digits (if (odd? (count (digits n)))
                                                            (butlast half-num-digits)
                                                            half-num-digits)
                                        all-digits (concat half-num-digits (reverse lower-half-digits))
                                        p (digits->num all-digits)]
                                    (if (< p n)
                                      (next-palindromic-number p)
                                      p)))]
  (fn palindrom-seq [n]
    (lazy-seq
      (let [p (if (is-palindromic? n)
                n
                (palindromic-number-after n))]
        (cons p (palindrom-seq (next-palindromic-number p)))))))
    
chouser's solution:

(fn [s]
  (let [p #(Long. (apply str % ((if %2 next seq) (reverse (str %)))))
        d #(count (str %))
        o #(odd? (d %))
        b #(Long. (subs (str %) 0 (quot (+ 1 (d %)) 2)))
        i #(let [x (b %)
                 o (o %)
                 y (+ 1 x)]
             (cond
              (= (d x) (d y)) (p y o)
              o (p (/ y 10) nil)
              1 (p y 1)))]
    (filter #(>= % s) (iterate i (p (b s) (o s))))))

aengelberg's solution:

(let
  [pow #(reduce * (repeat %2 %1))
   digits (fn digits [n]
            (if
              (zero? n) []
              (conj (digits (quot n 10))
                    (rem n 10))))
 
   digits->number (fn [l]
                    (apply +
                           (for [[n i] (zipmap (range (dec (count l)) -1 -1) l)]
                             (* i (pow 10 n)))))
 
   numberify (fn [left middle]
               (if middle
                 (digits->number
                   (concat (digits left)
                           [middle]
                           (reverse (digits left))))
                 (digits->number
                   (concat (digits left)
                           (reverse (digits left))))))
 
   palindromes (fn palindromes [left middle]
                 (lazy-seq
                   (cons (numberify left middle)
                         (if middle
                           (cond
                             (and (= middle 9) (every? #(= % 9) (digits left))) (palindromes (inc left) nil)
                             (= middle 9) (palindromes (inc left) 0)
                             :else (palindromes left (inc middle)))
                           (if (every? #(= % 9) (digits left))
                             (palindromes (/ (inc left) 10) 0)
                             (palindromes (inc left) nil))))))
 
   f (fn f [n]
       (if (= n 0)
         (cons 0 (f 1))
         (let [d (digits n)
               c (count d)
               left (if (even? c)
                      (digits->number (take (/ c 2) d))
                      (digits->number (take (/ (dec c) 2) d)))
               middle (if (even? c)
                        nil
                        (nth d (dec (/ (inc c) 2))))
               [p p2] (take 2 (palindromes left middle))]
           (if (< p n)
             (rest (palindromes left middle))
             (palindromes left middle)))))]
  f)

chunchangshao's solution:

(fn [x]
  (letfn [(pn [n] (filter (fn [n] (let[s (str n) l (count s) a (map #(subs % 0 (int (/ l 2))) [s (clojure.string/reverse s)])] (= (first a) (second a)))) (take 1000000 (iterate inc n))))]
    (case x
      0 (flatten [(pn 0) (range 1999 10101) 9102019])
      (pn x))))
  
hypirion's solution:

(fn [start-at]
  (letfn [(->int [& r] (Long. (apply str r)))
          (s-rev [n] (apply str (reverse (str n))))
          (half [pre n next-jump]
            (lazy-seq
             (cond (= pre next-jump) (whole pre (* next-jump 10))
                   (= 10 n) (half (+ pre 1) 0 next-jump)
                   :else (cons (->int pre n (s-rev pre))
                               (half pre (+ n 1) next-jump)))))
          (whole [pre next-jump]
            (lazy-seq
             (if (= pre next-jump)
               (half (/ pre 10) 0 next-jump)
               (cons (->int pre (s-rev pre))
                     (whole (+ pre 1) next-jump)))))]
    (let [size (count (str start-at))
          pre (if (= size 1) 1
                (Long. (.substring (str start-at) 0 (quot size 2))))
          first-jump (if (= size 1) 10
                         ((fn [n acc] (if (= 0 n) acc
                                          (recur (- n 1) (* acc 10))))
                          (quot size 2) 1))]
      (drop-while
       #(< % start-at)
       (concat (range 10)
               (if (or (= size 1)
                       (even? size))
                 (whole pre first-jump)
                 (half pre 0 first-jump)))))))
             
jafingerhut's solution:

;; Note that all palindromic numbers are either of the form:
 
;; (concat x (reverse x)) for some sequence of one or more digits x,
;; so the palindromic number has an even number of digits.
 
;; or:
 
;; (concat x [ d ] (reverse x)) for some sequence of digits x (perhaps
;; empty) and some digit 'd', so the palindromic number has an odd
;; number of digits.
 
;; To generate the palindromic numbers in increasing order, we just
;; need to make the sequence x (for even-length) or (concat x [d])
;; (for odd-length) represent incrementing decimal numbers.
 
;; function next-pal returns the smallest palindromic number that is
;; greater than or equal to its argument.
 
(fn [n]
  (let [to-digits (fn [n] (map #(- (int %) (int \0)) (str n)))
        to-int (fn [s] (read-string (apply str s)))
        make-pal (fn [digits odd rev-digits]
                   (to-int (concat digits
                                   (if odd (rest rev-digits) rev-digits))))
        next-pal (fn [n]
                   (let [digits (to-digits n)
                         len (count digits)
                         half-len (quot (inc len) 2)
                         half-digits (take half-len digits)
                         pal (make-pal half-digits
                                       (odd? len) (reverse half-digits))]
                     (if (>= pal n)
                       pal
                       (let [half-digits+1 (to-digits
                                            (inc (to-int half-digits)))]
                         (make-pal (if (> (count half-digits+1) half-len)
                                     (butlast half-digits+1)
                                     half-digits+1)
                                   (odd? len)
                                   (reverse half-digits+1))))))]
    (iterate #(next-pal (inc %))
             (next-pal n))))
         
balint's solution:

(fn [n]
  (letfn [(parts [a]
            (let [s (vec (re-seq #"\d" (str a)))
                  c (count s)]
              (vector
                (subvec s 0 (quot c 2))
                (subvec s (quot c 2) (quot (inc c) 2)))))
          (to-number [half mirror]
            ;(println "h:" half "m" mirror)
            (let [h (if (zero? half) "" half)]
              (BigInteger.
                (str h mirror (reduce str (reverse (str h)))))))
          (digits [n]
            (count (str n)))
          (ten-power? [n]
            (re-find #"^10*$" (str n)))
          (pow-10 [n] (first (drop n (iterate #(* % 10) 1))))
          (palindromes [half mirror]
            ;(println "h:" half "m:" mirror)
            (lazy-seq
              (cond
                ;(zero? half)
                ;  (concat (range 10)
                ;          (palindromes 1 nil))
                (= mirror 10)
                  (if (ten-power? (inc half))
                      (palindromes (inc half) nil)
                      (palindromes (inc half) 0))
                (and mirror (not= mirror :no-mirror))
                  (cons (to-number half mirror)
                        (palindromes half (inc mirror)))
                (ten-power? half)
                  (if (nil? mirror)
                    (cons (to-number half nil)
                          (palindromes (inc half) :no-mirror))
                    (palindromes (quot half 10) 0))
                :else
                  (cons (to-number half nil)
                        (palindromes (inc half) mirror)))))]
    (let [half (quot n (pow-10 (quot (digits n) 2)))
          ps (if (odd? (digits n))
               (palindromes (quot half 10) (rem half 10))
               (palindromes half nil))
          ]
      (if (< (first ps) n)
        (drop 1 ps)
        ps))))
    
amcnamara's solution:

(fn [n]
  (iterate 
    #(let [sub_l (inc (read-string (apply str (take (Math/ceil (/ (count (str %)) 2)) (str %)))))
           sub_r ((if (odd? (count (str %))) rest seq) (reverse (str sub_l)))]
       (if (every? #{\9} (str %))
         (+ 2 %)
         (read-string (apply str sub_l sub_r))))
    (#(if (= (seq (str %)) (reverse (str %))) % (recur (inc %))) n)))