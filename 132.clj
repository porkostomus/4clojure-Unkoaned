Insert between two items
Difficulty:	Medium
Topics:	seqs core-functions

Write a function that takes a two-argument predicate, a value, and a collection;
and returns a new collection where the value is inserted between every two items that satisfy the predicate.

(= '(1 :less 6 :less 7 4 3) (__ < :less [1 6 7 4 3]))

(= '(2) (__ > :more [2]))

(= [0 1 :x 2 :x 3 :x 4]  (__ #(and (pos? %) (< % %2)) :x (range 5)))

(empty? (__ > :more ()))

(= [0 1 :same 1 2 3 :same 5 8 13 :same 21]
   (take 12 (->> [0 1]
                 (iterate (fn [[a b]] [b (+ a b)]))
                 (map first) ; fibonacci numbers
                 (__ (fn [a b] ; both even or both odd
                       (= (mod a 2) (mod b 2)))
                     :same))))
#(apply concat
        (take 1 %3)
        (for [[a b] (partition 2 1 %3)]
          (if (% a b)
            [%2 b]
            [b])))                    

(fn [f t 
s] (mapcat #(cond (= %2 -) [%] (f % %2) [% t] 1 [%]) s (concat (next s) 
[-])))

adereth's solution:

(fn [pred v c]
  (if (seq c)
  (cons (first c)
   (mapcat (fn [a b] (if (pred a b) [v b] [b]))
           c (rest c))
          )
  ))

mfikes's solution:

(fn insert-between [p b [f s :as coll]]
  (lazy-seq
    (when f
      (if (and s (p f s))
        (cons f (cons b (insert-between p b (rest coll))))
        (cons f (insert-between p b (rest coll)))))))
    
chouser's solution:

(fn [f t s]
  (mapcat
    #(cond
       (= %2 -) [%]
       (f % %2) [% t]
       1        [%])
    s
    (concat (next s) [-])))

aengelberg's solution:

#(apply concat
        (take 1 %3)
        (for [[a b] (partition 2 1 %3)]
          (if (% a b)
            [%2 b]
            [b])))
        
chunchangshao's solution:

(fn [f c xs] (if-let [h1 (first xs)] (cons h1 ((fn a [xs] (lazy-seq (if-let [o (second xs)] (let [res (next xs) h (first xs) r (if (f h o) [c o] [o])]  (concat r (a res)))))) xs))))

hypirion's solution:

#(mapcat
  (fn [[a b]] (cond (nil? b) [a] 
                    (%1 a b) [a %2] 
                    :else [a]))
  (partition-all 2 1 %3))

jafingerhut's solution:

(fn [p v c]
  (if (seq c)
    (cons (first c)
          (->> (partition 2 1 c)
               (mapcat #(let [[_ s] %] (if (apply p %) [v s] [s])))))))
           
balint's solution:

(fn [p v coll]
  (if (empty? coll)
    []
    (cons (first coll)
      (mapcat
        (fn [[a b]] (if (p a b) [v b] [b]))
        (partition 2 1 coll)))))
    
amcnamara's solution:

(fn f [p i [a b & c]]
  (if b
    (lazy-cat [a] (if (p a b) [i]) (f p i (cons b c)))
    (if a [a] [])))

amalloy's solution:

(fn [pred inter coll]
  (lazy-seq
   (when-let [s (seq coll)]
     (cons (first s)
           (mapcat (fn [[left right]]
                     (if (pred left right)
                       [inter right]
                       [right]))
                   (partition 2 1 s))))))
               
dbyrne's solution:

(fn f [x y z]
  (when (not (empty? z))
    (lazy-cat
      (mapcat
        #(if (x % %2) [% y] [%])
        z
        (rest z))
      [(last z)])))