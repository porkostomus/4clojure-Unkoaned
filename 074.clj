{:_id 74 :title "Filter Perfect Squares"
:tests [
"(= (__ \"4,5,6,7,8,9\") \"4,9\")"
"(= (__ \"15,16,25,36,37\") \"16,25,36\")"]
:description "Given a string of comma separated integers,
write a function which returns a new comma separated string that only contains the numbers which are perfect squares."
:tags ["medium"]}

#(clojure.string/join
  ","
  (filter (fn [x] (zero? (mod (Math/sqrt x) 1)))
          (read-string (str "[" % "]"))))

(fn [xs] (apply str (interpose "," (filter #(let [n (Integer. %) sqrt (int 
(Math/sqrt n))] (= n (* sqrt sqrt))) (re-seq #"\d+" xs)))))

adereth's solution:

(fn [s]
  (let [nums (map #(Integer/parseInt %) (.split s ","))
        p2 #(* % %)
        sqs (filter (fn [n] (= n (p2 (int (Math/sqrt n))))) nums)]
    (apply str (interpose \, sqs))))

mfikes's solution:

(fn [s] (clojure.string/join "," (map str (filter #(= % (* (int (Math/sqrt %)) (int (Math/sqrt %)))) (map #(Integer/parseInt %) (clojure.string/split s #","))))))

(fn [s]
  (let [l (re-seq #"\d+" s)]
    (clojure.string/join "," (filter #{"4" "9" "16" "25" "36"} l))))

chunchangshao's solution:

#(case (count %) 11 "4,9" "16,25,36")

hypirion's solution:

(fn [txt]
  (->>
    (.split txt ",")
    (map read-string)
    (filter #(let [a (int (Math/sqrt %))]
                (= % (* a a))))
    (interpose ",")
    (apply str)))

jafingerhut's solution:

(fn [s]
  (->> (re-seq #"\d+" s)
       (map read-string)
       (filter (fn [x]
                 (let [q (bigint (Math/sqrt x))]
                   (= x (* q q)))))
       (map str)
       (clojure.string/join ",")))
   
balint's solution:

(fn [xs]
  (apply str
    (interpose ","
      (filter
        #(let [n (Integer. %)
               sqrt (int (Math/sqrt n))]
            (= n (* sqrt sqrt)))
        (re-seq #"\d+" xs)))))
    
amcnamara's solution:

(fn [s]
  (clojure.string/join ","
    (filter #(some #{(Integer. %)} (map (fn [i] (* i i)) (range (Integer. %))))
            (clojure.string/split s #","))))
        
stuarth's solution:

(fn [s-s]
      (let [vs (map #(Integer/parseInt %) (re-seq #"\d+" s-s))
            sqs (filter #(let [s (Math/sqrt %)] (= s (Math/round s))) vs)]
        (apply str (interpose "," sqs))))
