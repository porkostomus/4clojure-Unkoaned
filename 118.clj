118 Re-implement Map
Special Restrictions: map, map-indexed, mapcat, for

Takes a func and a seq, returns a lazy seq of (f x) for each element

(= [3 4 5 6 7]
   (__ inc [2 3 4 5 6]))
(= (repeat 10 nil)
   (__ (fn [_] nil) (range 10)))
(= [1000000 1000001]
   (->> (__ inc (range)) (drop (dec 1000000)) (take 2)))

(fn [f x] (rest (reductions #(f %2) nil x)))

(fn collect [f [fs & rs]] (lazy-seq (if (nil? fs) nil (cons (f fs) (collect 
f rs)))))

adereth's solution:

#(reductions (fn [acc x] (%1 x)) (%1 (first %2)) (rest %2))

mfikes's solution:

(fn mymap [f coll] 
  (if (empty? coll) 
    '() 
    (lazy-cat (list (f (first coll))) (mymap f (rest coll)))))

chouser's solution:

(fn l [f [a & m]]
  (lazy-seq
    (cons (f a) (if m (l f m)))))

aengelberg's solution:

(fn [f l]
  (rest (reductions #(f %2) 0 l)))

chunchangshao's solution:

(fn g [f xs] (lazy-seq (if (empty? xs) [] (concat (vector (f (first xs))) (g f (rest xs))))))

hypirion's solution:

(fn -map [f [a & r :as args]]
  (lazy-seq
    (if (empty? args)
        nil
        (cons (f a) (-map f r)))))
    
jafingerhut's solution:

(fn mymap [f s]
  (lazy-seq
    (when-let [s (seq s)]
      (cons (f (first s)) (mymap f (rest s))))))
  
balint's solution:

(fn collect [f [fs & rs]]
  (lazy-seq
    (if (nil? fs)
      nil
      (cons (f fs) (collect f rs)))))
  
amcnamara's solution:

(fn b [f [x & y]]
  (if x
    (cons (f x) (lazy-seq (b f y)))))

amalloy's solution:

(fn m [f s]
  (lazy-seq
    (when-let [[x & more] (seq s)]
      (cons (f x)
            (m f more)))))
        
stuarth's solution:

(fn mm [f s]
    (when-not (empty? s)
      (lazy-seq (cons (f (first s)) (mm f (rest s))))))
  
dbyrne's solution:

(fn f [x y]
  (when (not (empty? y))
    (lazy-cat
      [(x (first y))]
      (f x (rest y)))))