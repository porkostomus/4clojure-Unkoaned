Logical falsity and truth
Difficulty:	Elementary
Topics:	logic

In Clojure, only nil and false represent the values of logical falsity in conditional tests - anything else is logical truth.

(= __ (if-not false 1 0))

(= __ (if-not nil 1 0))

(= __ (if true 1 0))

(= __ (if [] 1 0))

(= __ (if [0] 1 0))

(= __ (if 0 1 0))

(= __ (if 1 1 0))

1

adereth's solution:

1
1
mfikes's solution:

1
1
chouser's solution:

1
1/1
aengelberg's solution:

1
1
chunchangshao's solution:

1
1
hypirion's solution:

1
1
jafingerhut's solution:

1
1
balint's solution:

1
1
borkdude's solution:

1
1
amcnamara's solution:

1
(bit-not -2)
amalloy's solution:

1
1
stuarth's solution:

1
1
dbyrne's solution:

1
1