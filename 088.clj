;; 88 Symmetric Difference-
the set of items belonging to one but not both of two sets.

#(set (concat (remove %1 %2) (remove %2 %1)))

(#(set (concat (apply disj %1 %2)
               (apply disj %2 %1)))
 #{1 2 3 4 5 6} #{1 3 5 7})
;;=> #{2 4 6 7}
	
(#(set (concat (apply disj %1 %2) (apply disj %2 %1))) #{:a :b :c} #{})
;;=> #{:a :b :c}

(#(set (concat (apply disj %1 %2) (apply disj %2 %1))) #{} #{4 5 6})
;;=> #{4 5 6}

(#(set (concat (apply disj %1 %2) (apply disj %2 %1)))
;;=> #{[1 2] [2 3]} #{[2 3] [3 4]}) #{[1 2] [3 4]}

#(set (concat (apply disj %1 %2) (apply disj %2 %1)))

adereth's solution:

(fn [s1 s2]
    (set (concat (filter (complement s1) s2) 
                   (filter (complement s2) s1))))
               
mfikes's solution:

(fn set-symmetric-diff [a b] (set (concat (filter #(not (contains? b %1)) a) (filter #(not (contains? a %1)) b)) ))

chouser's solution:

#(into (set (remove %2 %)) (remove % %2))

aengelberg's solution:

#(set (concat (remove % %2)(remove %2 %)))

chunchangshao's solution:

#(clojure.set/difference (clojure.set/union % %2) (clojure.set/intersection % %2))

hypirion's solution:

(fn [a b]
  (->>
    (clojure.set/union a b)
    (filter #(not= (a %) (b %)))
    set))

jafingerhut's solution:

#(clojure.set/difference (clojure.set/union %1 %2) (clojure.set/intersection %1 %2))

balint's solution:

(fn [s1 s2]
  (clojure.set/union
    (clojure.set/difference s1 s2)
    (clojure.set/difference s2 s1)))

amcnamara's solution:

#(into (clojure.set/difference % %2)
       (clojure.set/difference %2 %))

amalloy's solution:

#(set (concat (remove %2 %)
              (remove % %2)))

stuarth's solution:

(fn [s1 s2]
    (clojure.set/union (clojure.set/difference s1 s2) (clojure.set/difference s2 s1)))