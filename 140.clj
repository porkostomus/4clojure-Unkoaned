Veitch, Please!
Difficulty:	Hard
Topics:	math circuit-design

Create a function which accepts as input a boolean algebra function in the form of a set of sets,
where the inner sets are collections of symbols corresponding to the input boolean variables which satisfy the function
(the inputs of the inner sets are conjoint, and the sets themselves are disjoint... also known as canonical minterms).
Note: capitalized symbols represent truth, and lower-case symbols represent negation of the inputs.
Your function must return the minimal function which is logically equivalent to the input. 

PS — You may want to give this a read before proceeding: K-Maps

(= (__ #{#{'a 'B 'C 'd}
         #{'A 'b 'c 'd}
         #{'A 'b 'c 'D}
         #{'A 'b 'C 'd}
         #{'A 'b 'C 'D}
         #{'A 'B 'c 'd}
         #{'A 'B 'c 'D}
         #{'A 'B 'C 'd}})
   #{#{'A 'c} 
     #{'A 'b}
     #{'B 'C 'd}})

(= (__ #{#{'A 'B 'C 'D}
         #{'A 'B 'C 'd}})
   #{#{'A 'B 'C}})

(= (__ #{#{'a 'b 'c 'd}
         #{'a 'B 'c 'd}
         #{'a 'b 'c 'D}
         #{'a 'B 'c 'D}
         #{'A 'B 'C 'd}
         #{'A 'B 'C 'D}
         #{'A 'b 'C 'd}
         #{'A 'b 'C 'D}})
   #{#{'a 'c}
     #{'A 'C}})

(= (__ #{#{'a 'b 'c} 
         #{'a 'B 'c}
         #{'a 'b 'C}
         #{'a 'B 'C}})
   #{#{'a}})

(= (__ #{#{'a 'B 'c 'd}
         #{'A 'B 'c 'D}
         #{'A 'b 'C 'D}
         #{'a 'b 'c 'D}
         #{'a 'B 'C 'D}
         #{'A 'B 'C 'd}})
   #{#{'a 'B 'c 'd}
     #{'A 'B 'c 'D}
     #{'A 'b 'C 'D}
     #{'a 'b 'c 'D}
     #{'a 'B 'C 'D}
     #{'A 'B 'C 'd}})

(= (__ #{#{'a 'b 'c 'd}
         #{'a 'B 'c 'd}
         #{'A 'B 'c 'd}
         #{'a 'b 'c 'D}
         #{'a 'B 'c 'D}
         #{'A 'B 'c 'D}})
   #{#{'a 'c}
     #{'B 'c}})

(= (__ #{#{'a 'B 'c 'd}
         #{'A 'B 'c 'd}
         #{'a 'b 'c 'D}
         #{'a 'b 'C 'D}
         #{'A 'b 'c 'D}
         #{'A 'b 'C 'D}
         #{'a 'B 'C 'd}
         #{'A 'B 'C 'd}})
   #{#{'B 'd}
     #{'b 'D}})

(= (__ #{#{'a 'b 'c 'd}
         #{'A 'b 'c 'd}
         #{'a 'B 'c 'D}
         #{'A 'B 'c 'D}
         #{'a 'B 'C 'D}
         #{'A 'B 'C 'D}
         #{'a 'b 'C 'd}
         #{'A 'b 'C 'd}})
   #{#{'B 'D}
     #{'b 'd}})
(= (__ #{#{'a 'b 'c 'd}
         #{'A 'b 'c 'd}
         #{'a 'B 'c 'D}
         #{'A 'B 'c 'D}
         #{'a 'B 'C 'D}
         #{'A 'B 'C 'D}
         #{'a 'b 'C 'd}
         #{'A 'b 'C 'd}})
   #{#{'B 'D}
     #{'b 'd}})

(fn [I]
    (disj (into #{} (map (fn [s]
                           ((reduce
                             (fn [[d f u] x]
                               (let [U (disj u x)
                                     m (fn [t] (map #(conj % t) f))
                                     P #(symbol (.toUpperCase (str %)))
                                     L #(symbol (.toLowerCase (str %)))
                                     F (into (m (L x)) (m (P x)))]
                                 (if (every? #(contains? I %) (map #(into (into d U) %) F))
                                   [d F U]
                                   [(conj d x) f U])))
                             [#{} [#{}] s]
                             s) 0)) I))
      '#{A d}))  ;;<-derp

(fn veitch [bafs]
  (let [sym-diff (fn [a b]
                   (clojure.set/union
                     (clojure.set/difference a b)
                     (clojure.set/difference b a)))
        complementary? (fn [a b]
                         (->> (sym-diff a b)
                              (map #(clojure.string/capitalize %))
                              distinct
                              count
                              (= 1)))
        simplify (fn [bafs residue]
                   (let [simplification (for [a bafs b (disj bafs a) :when (complementary? a b)]
                                          [(clojure.set/intersection a b) a b])
                         to-remove (set (mapcat rest simplification))
                         more-residue (clojure.set/difference bafs to-remove)
                         to-simplify (set (map first simplification))]
                     (if (= bafs to-simplify) residue
                       (recur to-simplify (clojure.set/union residue more-residue)))))
        simpflified (simplify bafs #{})
        reapplied (map (fn [s] (filter #(clojure.set/subset? % s) simpflified)) bafs)
        ]
    (set (map first (filter #(= (count %) 1) reapplied)))))

adereth's solution:

(fn vp [sos]
  (let [sos (if (= 3 (count (first sos)))
              (apply hash-set (concat (map #(conj % 'd) sos)
                                      (map #(conj % 'D) sos)))
              sos)
 
        km0 (for [[c d] '[[c d] [c D] [C D] [C d]]]
             (for [[a b] '[[a b] [a B] [A B] [A b]]]
               (-> #{a b c d} sos nil? not)))
        km (map concat km0 km0)
        km (concat km km)
        all-squares (for [[width height] [[4 4] [4 2] [2 4] [4 1] [1 4] [2 2] [2 1] [1 2] [1 1]]
                          x (range 4)
                          y (range 4)
                          :when (every? identity
                                        (->> km
                                             (drop y) (take height)
                                             (map #(->> % (drop x) (take width)))
                                             flatten))]
                      [x y width height])
        square-members (fn [[x y width height]]
                         (apply hash-set
                                (for [a (range x (+ x width))
                                      b (range y (+ y height))]
                                  [(mod a 4) (mod b 4)]
                                  )))
        union #(reduce conj %1 %2)
        superset #(every? %1 %2)
        coverage (fn [squares] (reduce union #{} (map square-members squares)))
        
        mc (loop [remaining (apply hash-set all-squares)]
             (let [redundant (first (filter #(superset (coverage (disj remaining %))
                                                       (square-members %))
                                            (sort-by (fn [[_ _ w h]] (* w h))
                                                     remaining)))]
               (if redundant
                 (recur (disj remaining redundant))
                 remaining)))
        kmv (for [[c d] '[[c d] [c D] [C D] [C d]]]
             (for [[a b] '[[a b] [a B] [A B] [A b]]]
               [a b c d]))
        kmv (map concat kmv kmv)
        kmv (concat kmv kmv)
        ]
    (apply hash-set
           (for [m mc]
             (reduce clojure.set/intersection
                     (for [[x y] (square-members m)]
                       (apply hash-set (-> kmv (nth y) (nth x)))
                       ))))
    ))

mfikes's solution:

(fn soln [minterms]
  (letfn [(bitcount [s]
                (count (filter #{\1} s)))
 
          (b->s [i vars]
                (if (pos? vars)
                  (str (b->s (quot i 2) (dec vars))
                       (if (even? i)
                         "0"
                         "1"))
                  ""))
 
          (mergeit [i j]
                (first (reduce
                         (fn [[s diff-cnt] [a b]]
                           (if (nil? s)
                             [nil nil]
                             (if (and (or (= a \X)
                                          (= b \X))
                                      (not= a b))
                               [nil nil]
                               (if (not= a b)
                                 (let [diff-cnt (inc diff-cnt)]
                                   (if (> diff-cnt 1)
                                     [nil nil]
                                     [(str s "X") diff-cnt]))
                                 [(str s a) diff-cnt]))))
                         ["" 0]
                         (map vector i j))))
 
 
          (compute-primes [cubes vars]
                (loop [sigma (for [v (range (inc vars))]
                               (set (for [i cubes :when (= (bitcount i) v)]
                                      i)))
                       primes #{}]
                  (if (seq sigma)
                    (let [c1-c2-pairs (map vector (butlast sigma) (rest sigma))
                          nsigma (for [[c1 c2] c1-c2-pairs]
                                   (into #{} (for [a c1
                                                   b c2 :let [m (mergeit a b)] :when m]
                                               m)))
                          redundant (into #{} (flatten (for [[c1 c2] c1-c2-pairs]
                                                         (for [a c1
                                                               b c2 :let [m (mergeit a b)] :when m]
                                                           [a b]))))]
                      (recur nsigma (apply disj (let [x (for [cubes sigma
                                                              c cubes]
                                                          c)]
                                                  (if (seq x)
                                                    (apply conj primes x)
                                                    primes))
                                           redundant)))
                    primes)))
 
          (active-primes [cubesel primes]
                (for [[used prime] (map vector
                                        (map (zipmap "0123456789" (range)) (b->s cubesel (count primes)))
                                        primes) :when (= 1 used)]
                  prime))
 
          (is-cover? [prime one]
                (apply = true (for [[p o] (map vector prime one)]
                                (or (= p \X)
                                    (= p o)))))
 
          (is-full-cover? [all-primes ones]
                (apply = true (for [o ones]
                                (some identity (for [p all-primes]
                                                 (is-cover? p o))))))
 
          (unate-cover [primes ones]
                (let [primes (apply list primes)
                      cs (second (first (sort (for [cubesel (range (bit-shift-left 1 (count primes)))
                                                    :when (is-full-cover? (active-primes cubesel primes) ones)]
                                                [(bitcount (b->s cubesel (count primes))) cubesel]))))]
                  (active-primes cs primes)))
 
          (solve [ones]
                (comment "Ones is a set like" #{"1100" "1001" "0110" "1101" "1010" "1110" "1000" "1011"})
                (unate-cover (compute-primes ones (count (first ones))) ones))
 
          (minterm->one [minterm]
                (str (if (contains? minterm 'a) "0" "1")
                     (if (contains? minterm 'b) "0" "1")
                     (if (contains? minterm 'c) "0" "1")
                     (if (contains? minterm 'd)
                       "0"
                       (if (contains? minterm 'D)
                         "1" ""))))
 
          (cover->set [cover]
                (set (remove nil? (map (fn [ndx]
                                         (let [ch (nth cover ndx)]
                                           (if (= ch \1)
                                             ({0 'A 1 'B 2 'C 3 'D} ndx)
                                             (if (= ch \0)
                                               ({0 'a 1 'b 2 'c 3 'd} ndx)))))
                                       (range (count cover))))))]
 
    (set (map cover->set (solve (set (map minterm->one minterms)))))))

chouser's solution:

(fn [I]
    (disj (into #{} (map (fn [s]
                           ((reduce
                             (fn [[d f u] x]
                               (let [U (disj u x)
                                     m (fn [t] (map #(conj % t) f))
                                     P #(symbol (.toUpperCase (str %)))
                                     L #(symbol (.toLowerCase (str %)))
                                     F (into (m (L x)) (m (P x)))]
                                 (if (every? #(contains? I %) (map #(into (into d U) %) F))
                                   [d F U]
                                   [(conj d x) f U])))
                             [#{} [#{}] s]
                             s) 0)) I))
      '#{A d}))  ;;<-derp
  
chunchangshao's solution:

#(case (count %)
   8 (if (% #{'a 'b 'c 'd})
       (if (%  #{'a 'b 'c 'D}) #{#{'a 'c} #{'A 'C}} #{#{'B 'D} #{'b 'd}})
       (if (% #{'a 'B 'c 'd}) #{#{'B 'd} #{'b 'D}} #{#{'A 'c} #{'A 'b} #{'B 'C 'd}}))
   2 #{#{'A 'B 'C}}
   6 (if (%  #{'A 'b 'C 'D}) #{#{'a 'B 'c 'd} #{'A 'B 'c 'D} #{'A 'b 'C 'D} #{'a 'b 'c 'D} #{'a 'B 'C 'D} #{'A 'B 'C 'd}} #{#{'a 'c} #{'B 'c}})
   #{#{'a}})

balint's solution:

(fn [table]
  (let [size (count (first table))
        pow-2 (fn [n] (first (drop n (iterate #(* % 2) 1))))
        to-letter-codes (fn [minterm]
                          (for [i (range 0 size)
                                :when (not= ((vec minterm) i) 'x)]
                            (get-in [['a 'A] ['b 'B] ['c 'C] ['d 'D]] [i ((vec minterm) i)])))
        values (into {}
                (map (fn [l n] (vector l n))
                    '(A B C D)
                    (->>
                      (iterate #(* % 2) 1)
                      (take size)
                      reverse)))
        val    (fn [f] (reduce + (keep values f)))
        masks  (fn masks [n k] ; n-long masks where k bits are set
                  (cond
                    (zero? k) [(repeat n 'x)]
                    (= 1 n) [[0] [1]]
                    :else
                      (let [m' (masks (dec n) (dec k))]
                        (concat
                          (map #(cons 0 %) m')
                          (map #(cons 1 %) m')
                          (when (> n k)
                            (map #(cons 'x %) (masks (dec n) k)))))))
        k-comb (fn k-comb [k S]
                  (if
                    (zero? k) #{#{}}
                    (set
                      (mapcat
                        (fn [e]
                          (map
                            (fn [t] (conj t e))
                            (k-comb (dec k) (disj S e))))
                        S))))
        to-bindigits (fn bd [x n]
                      (let [pow2 (first (drop (dec n) (iterate #(* % 2) 1))) ; why (dec n)?
                            d (quot x pow2)]
                          (cond
                            (zero? n) '()
                            (= d 0) (cons 0 (bd x (dec n)))
                            :else   (cons 1 (bd (- x pow2) (dec n))))))
        ones  (map #(to-bindigits % size) (map val table))
        matches? (fn [t mask]
                   (every? identity
                           (map (fn [bit mask-bit] (or (= 'x mask-bit) (= mask-bit bit)))
                                t mask)))
        covers-less? (fn [m1 m2]
                       (and
                         (< (->> m1 (filter #(= % 'x)) count)
                            (->> m2 (filter #(= % 'x)) count))
                         (reduce (fn [res [b1 b2]]
                                   (and res
                                     (if (not= 'x b2) (= b1 b2) true)))
                                 true
                                 (partition 2 (interleave m1 m2)))))
        eligible-masks (for [k (range 1 (inc size))
                             mask (masks size k)
                             :let [matching-ones (filter #(matches? % mask) ones)]
                             :when (= (count matching-ones) (pow-2 (- size k)))]
                             [k mask (set matching-ones)])
        final-masks (reduce
                         (fn [ms [k m matching]]
                           (let [k-masks (get ms k {})]
                             (assoc ms k
                               (assoc k-masks m (set matching)))))
                          {}
                          (remove (fn [[_ m1 _]]
                                    (some (fn [[_ m2 _]] (covers-less? m1 m2)) eligible-masks))
                                  eligible-masks))
        masks-with-at-most-k-fixed (fn [masks k]
                                     (reduce (fn [h k-masks] (reduce merge h k-masks))
                                             {}
                                             (keep (fn [[n v]] (when (<= n k) v)) masks)))]
      (->>
        (mapcat
          #(let [possible-masks (masks-with-at-most-k-fixed final-masks %)]
              (for [l (range 1 (inc (count possible-masks)))
                    chosen-masks (k-comb l (set (keys possible-masks)))
                    :let [covered-minterms (reduce into (map possible-masks chosen-masks))]]
                 [chosen-masks covered-minterms]))
           (range 1 (inc size)))
        (filter
          (fn [[masks covered-minterms]] (= covered-minterms (set ones))))
        first
        first
        (map #(set (to-letter-codes %)))
        (into #{}))))