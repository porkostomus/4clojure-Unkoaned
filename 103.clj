;; 103 k-combinations [m]

Takes seq of n elements, returns all possible sets
consisting of k distinct elements taken from S.
The number of k-combinations for a seq is equal to
the binomial coefficient.

(defn k-comb [k S]
(if (zero? k)
    #{#{}}
    (set (mapcat (fn [e]
    (map (fn [t] (conj t e))
      (k-comb (dec k) (disj S e)))) S))))
#'user/k-comb
user> (k-comb 1 #{4 5 6})
#{#{6} #{5} #{4}}
user> (k-comb 10 #{4 5 6})
#{}
user> (k-comb 2 #{0 1 2})
#{#{0 1} #{1 2} #{0 2}}
user> (k-comb 3 #{0 1 2 3 4})
#{#{0 4 3} #{0 1 4} #{4 3 2} #{0 1 2} #{0 1 3} #{1 3 2} #{0 3 2} #{1 4 3} #{1 4 2} #{0 4 2}}
user> (k-comb 4 #{[1 2 3] :a "abc" "efg"})
#{#{"efg" "abc" [1 2 3] :a}}
user> (k-comb 2 #{[1 2 3] :a "abc" "efg"})
#{#{[1 2 3] :a} #{"efg" :a} #{"abc" :a} #{"abc" [1 2 3]} #{"efg" "abc"} #{"efg" [1 2 3]}}

(fn [n s]
  (let [all (fn c1 [n s]
              (cond
                (zero? n) #{}
                (= 1 n) (reduce #(conj %1 #{%2}) #{} s)
                :else (reduce (fn [acc x] (into acc (map #(into x %) (c1 1 s))))
                              #{}
                              (c1 (dec n) s))))]
    (set (filter #(= n (count %)) (all n s)))))

adereth's solution:

(fn [n s]
  (let [subsets (fn [s]
            (loop [result []
               remaining s]
              (if (seq remaining)
             (recur (concat [[(first remaining)]] result (map #(conj % (first remaining)) result)) (rest remaining))
                     result)))]
    (apply hash-set (map #(apply hash-set %) (filter #(= n (count %)) (subsets s))))
    )
  )

mfikes's solution:

(fn [n s]
  (let [power-set (fn [s]
                    (let [v (vec s)]
                      (into #{} (for [i (range (apply * (repeat (count v) 2)))]
                                  (into #{} (for [j (range (count v)) :when (bit-test i j)]
                                              (v j)))))))]
    (into #{} (filter #(= n (count %)) (power-set s)))))

chouser's solution:

(fn k [i s]
  (set
    (if (= i 0)
      [#{}]
      (mapcat #(for [p (k (- i 1) %2)] (conj p %))
              s (next (iterate next s))))))
          
aengelberg's solution:

(fn f [k s]
  (cond
   (zero? k) #{#{}}
   (empty? s) #{}
   :else (set
           (for [i s
                 x (f (dec k)(disj s i))]
             (conj x i)))))
         
chunchangshao's solution:

#(case %
   1 #{#{4} #{5} #{6}}
   10 #{}
   3 #{#{0 1 2} #{0 1 3} #{0 1 4} #{0 2 3} #{0 2 4} #{0 3 4} #{1 2 3} #{1 2 4} #{1 3 4} #{2 3 4}}
   4 #{#{[1 2 3] :a "abc" "efg"}}
   2 (if (= 3 (count %2)) 
       #{#{0 1} #{0 2} #{1 2}}
       #{#{[1 2 3] :a} #{[1 2 3] "abc"} #{[1 2 3] "efg"} #{:a "abc"} #{:a "efg"} #{"abc" "efg"}}))
   
hypirion's solution:

(fn k-comb
  ([n init]
    (let [i (- (count init) n)]
      (if (neg? i)
        #{}
        (loop [sets [init], i i]
          (if (pos? i)
            (recur 
              (for [elt init, set sets :when (contains? set elt)]
                (disj set elt))
              (dec i))
            (into #{} sets)))))))
        
jafingerhut's solution:

;; Special cases for k=0, (count s), and larger are straightforward.
 
;; The general case is more interesting.  The set of all
;; k-combinations is the union (implemented with into in the code) of
;; these two sets, considering an arbitrary element of the set x, and
;; the remaining elements y=(disj s x).
 
;; 1. All of the k-combinations that do not include x.  These are (c k
;; y).
 
;; 2. All of the k-combinations that do include x.  These are the same
;; as all of the (k-1)-combinations of y, where each such set has x
;; added to it.
 
(fn k-combinations [k s]
  (let [n (count s)]
    (cond (> k n) #{}
          (= k n) #{(set s)}
          (= k 0) #{#{}}
          :else (let [[x & y] (seq s)]
                  (into (k-combinations k y)
                        (map #(conj % x) (k-combinations (dec k) y)))))))
                    
balint's solution:

(fn k-comb [k S]
  (if
    (zero? k) #{#{}}
    (set
      (mapcat
        (fn [e]
          (map
            (fn [t] (conj t e))
            (k-comb (dec k) (disj S e))))
        S))))
    
amcnamara's solution:

(fn [k c]
  (letfn [(f [n s] (if (> n (count s))
                      #{}
                      (if (= 1 n)
                        (map #(set (vector %)) s)
                        (map #(into (set (vector (first s))) %) (f (dec n) (rest s))))))]
    (set (reduce into
      (loop [r #{} s c]
        (if (empty? s)
          r
          (recur (into r 
                       (for [i (range 1 (count s))]
                         (f k (cons (first s) (drop i s)))))
                 (rest s))))))))
             
amalloy's solution:

#(set (map set ((fn combinations [n coll]
  (if (= 1 n)
    (map list coll)
    (lazy-seq
     (when-let [[head & tail] (seq coll)]
       (concat (for [x (combinations (dec n) tail)]
                 (cons head x))
               (combinations n tail))))))
                 % %2)))
