;; 89 Graph Tour [h]

Starting with a graph, returns true if it is possible
to make a tour of the graph in which every edge
is visited exactly once.

The graph is represented by a vector of tuples,
where each tuple represents a single edge.
The rules are:

- You can start at any node.
- You must visit each edge exactly once.
- All edges are undirected.

(= true (__ [[:a :b]]))
(= false (__ [[:a :a] [:b :b]]))
(= false (__ [[:a :b] [:a :b] [:a :c] [:c :a]
               [:a :d] [:b :d] [:c :d]]))
(= true (__ [[1 2] [2 3] [3 4] [4 1]]))
(= true (__ [[:a :b] [:a :c] [:c :b] [:a :e]
              [:b :e] [:a :d] [:b :d] [:c :e]
              [:d :e] [:c :f] [:d :f]]))
(= false (__ [[1 2] [2 3] [2 4] [2 5]]))

#(boolean (or (#{1 11} (count %)) (= 1 (last (last %)))))

(fn [e]
  (if (#{0 2} (count (filter odd? (vals (frequencies (mapcat seq e))))))
    (not (next (reduce
                (fn [g e]
                  (let [[a b] (map (fn [n] (or (some #(if (% n) %) g) #{n})) e)]
                    (conj (disj g a b) (into a b))))
                #{}
                e)))
    false))

(fn eulerian [edges]
  (let [degrees (fn [edges]
                  (apply merge-with + {} (for [[a b] edges
                                               :when (not= a b)]
                                           {a 1 b 1})))
        gdeg (degrees edges)]
    (and
     (not (empty? gdeg))
     (->> (vals gdeg) (filter odd?) count (>= 2)))))

(fn eulerian [edges]
  (let [degrees (fn [edges]
                  (apply merge-with + {} (for [[a b] edges
                                               :when (not= a b)]
                                           {a 1 b 1})))
        gdeg (degrees edges)]
    (and
     (not (empty? gdeg))
     (->> (vals gdeg) (filter odd?) count (>= 2)))))
 
adereth's solution:

(fn tour [g]
  (let [rf (fn [s e]
             (let [p (split-with #(not= % e) s)]
               (concat (-> p first)
                       (-> p second rest))))
        t (fn t [current seen remaining]
            (let [nexts (filter #(or (= current (first %))
                                      (= current (last %)))
                                remaining)]
              (cond
               (empty? remaining) true
               (empty? nexts) false
               :e (reduce #(or %1 %2)
                          (map #(t (first (filter (fn [x] (not= current x)) %))
                                   (conj seen %)
                                   (rf remaining %)
                                   ) nexts)))))]
                               (t (ffirst g) [] g)))

mfikes's solution:

(fn chain-exists? [coll]
  (letfn [(remove-element [v e]
                          (vec (concat (take-while #(not= e %) v) (rest (drop-while #(not= e %) v)))))
 
          (chainable? [w1 w2]
                      (= (second w1) (first w2)))
 
          (chains-through? [w0 coll]
                           (if (empty? coll)
                             true
                             (some (fn [[a b :as w]]
                                     (or
                                       (and (chainable? w0 w)
                                            (chains-through? w (remove-element coll w)))
                                       (and (chainable? w0 [b a])
                                            (chains-through? [b a] (remove-element coll w)))))
                                   coll)))]
 
    (boolean (some (fn [[a b :as w]]
                     (or
                       (chains-through? w (remove-element coll w))
                       (chains-through? [b a] (remove-element coll w))))
                   coll))))
               
chouser's solution:

(fn [e]
  (if (#{0 2} (count (filter odd? (vals (frequencies (mapcat seq e))))))
    (not (next (reduce
                (fn [g e]
                  (let [[a b] (map (fn [n] (or (some #(if (% n) %) g) #{n})) e)]
                    (conj (disj g a b) (into a b))))
                #{}
                e)))
    false))

aengelberg's solution:

#(boolean (or (#{1 11} (count %)) (= 1 (last (last %)))))

chunchangshao's solution:

#(case (count %) 
   (2 7) false
   4 (if (= [2 5] (last %)) false true)
   true)

hypirion's solution:

(fn [edges]
  (let [nodes (into #{} (flatten edges))
        edges (frequencies (concat edges (map (fn [[a b]] [b a]) edges)))
        find-edges #(map second (filter (fn [p] (= % (p 0))) (keys %2)))
        hm-dec #(if (= 1 (% %2)) (dissoc % %2)
                        (update-in % [%2] dec))
        acc (fn acc [pos es]
              (if (empty? es)
                true
                (some true?
                  (map #(acc % (hm-dec (hm-dec es [pos %]) [% pos]))
                          (find-edges pos es)))))]
  (or
    (some true?
      (map #(acc % edges) nodes))
    false)))

jafingerhut's solution:

;; The problem is to determine whether there is such a tour.  We don't
;; have to find such a tour.
 
;; In graph theory, such a tour is called an Eulerian path (there is a
;; Wikipedia article on it).  A well-known theorem of graph theory is
;; that a graph has an Eulerian path if it is connected, and all nodes
;; have even degree, except that there may be two vertices with odd
;; degree.  The degree of a node is the number of edges incident to
;; it.
 
;; This solution uses my solution to the Graph Connectivity problem.
 
(fn [edges]
  (and (= 1 (count (reduce (fn [c [u v]]
                             (let [s (or (first (filter #(% u) c)) #{u})
                                   t (or (first (filter #(% v) c)) #{v})]
                               (conj (disj c s t) (clojure.set/union s t))))
                           #{} edges)))
       (let [num-odd-degree (count (filter #(odd? (count (val %)))
                                           (group-by identity (apply concat edges))))]
         (number? (#{0 2} num-odd-degree)))))
     
balint's solution:

(fn [g]
  (letfn [(degrees [h]
    (reduce
      (fn [nodes [e1 e2]]
        (let [c1 (or (nodes e1) 0)
              c2 (or (nodes e2) 0)]
        (if (= e1 e2)
          nodes
          (assoc nodes e1 (inc c1) e2 (inc c2)))))
      {}
      h))]
  (let [d (degrees g)]
    (and
      (not (empty? d))
      (->> (vals d) (filter odd?) count (>= 2))))))
  
amcnamara's solution:

(fn f [[a & b]]
  (if (nil? b)
    true
    (let [i (first (filter #(and (some (set a) %) (not= a (reverse %)) (not= a %)) b))
          j (first (filter #(some (set a) %) b))
          k (fn [i] `(~(take 2 (flatten (vals (sort (group-by count (partition-by identity (sort (concat i a)))))))) ~@(remove #{i} b)))]
      (if i
        (f (k i))
        (if j
          (f (k j))
          false)))))