Latin Square Slicing
Difficulty:	Hard
Topics:	data-analysis math

A Latin square of order n is an n x n array that contains n different elements,
each occurring exactly once in each row, and exactly once in each column.
For example, among the following arrays only the first one forms a Latin square:


1
2
3
4
A B C    A B C    A B C
B C A    B C A    B D A
C A B    C A C    C A B
 
Let V be a vector of such vectors1 that they may differ in length2. We will say that an arrangement of vectors of V in consecutive rows is an alignment (of vectors) of V if the following conditions are satisfied:

All vectors of V are used.
Each row contains just one vector.
The order of V is preserved.
All vectors of maximal length are horizontally aligned each other.
If a vector is not of maximal length then all its elements are aligned with elements of some subvector of a vector of maximal length.
Let L denote a Latin square of order 2 or greater. We will say that L is included in V or that V includes L iff there exists an alignment of V such that contains a subsquare that is equal to L.
For example, if V equals [[1 2 3][2 3 1 2 1][3 1 2]] then there are nine alignments of V (brackets omitted):
 
        1              2              3
 
      1 2 3          1 2 3          1 2 3
  A   2 3 1 2 1    2 3 1 2 1    2 3 1 2 1
      3 1 2        3 1 2        3 1 2
 
      1 2 3          1 2 3          1 2 3
  B   2 3 1 2 1    2 3 1 2 1    2 3 1 2 1
        3 1 2        3 1 2        3 1 2
 
      1 2 3          1 2 3          1 2 3
  C   2 3 1 2 1    2 3 1 2 1    2 3 1 2 1
          3 1 2        3 1 2        3 1 2
 
Alignment A1 contains Latin square [[1 2 3][2 3 1][3 1 2]], alignments A2, A3, B1, B2, B3 contain no Latin squares,
and alignments C1, C2, C3 contain [[2 1][1 2]].
Thus in this case V includes one Latin square of order 3 and one of order 2 which is included three times.
Our aim is to implement a function which accepts a vector of vectors V as an argument,
and returns a map which keys and values are integers.
Each key should be the order of a Latin square included in V,
and its value a count of different Latin squares of that order included in V.
If V does not include any Latin squares an empty map should be returned.
In the previous example the correct output of such a function is {3 1, 2 1} and not {3 1, 2 3}.

1 Of course, we can consider sequences instead of vectors. 
2 Length of a vector is the number of elements in the vector.

(= (__ '[[A B C D]
         [A C D B]
         [B A D C]
         [D C A B]])
   {})

(= (__ '[[A B C D E F]
         [B C D E F A]
         [C D E F A B]
         [D E F A B C]
         [E F A B C D]
         [F A B C D E]])
   {6 1})

(= (__ '[[A B C D]
         [B A D C]
         [D C B A]
         [C D A B]])
   {4 1, 2 4})

(= (__ '[[B D A C B]
         [D A B C A]
         [A B C A B]
         [B C A B C]
         [A D B C A]])
   {3 3})

(= (__ [  [2 4 6 3]
        [3 4 6 2]
          [6 2 4]  ])
   {})

(= (__ [[1]
        [1 2 1 2]
        [2 1 2 1]
        [1 2 1 2]
        []       ])
   {2 2})

(= (__ [[3 1 2]
        [1 2 3 1 3 4]
        [2 3 1 3]    ])
   {3 1, 2 2})

(= (__ [[8 6 7 3 2 5 1 4]
        [6 8 3 7]
        [7 3 8 6]
        [3 7 6 8 1 4 5 2]
              [1 8 5 2 4]
              [8 1 2 4 5]])
   {4 1, 3 1, 2 7})

(fn [v]
  (letfn [
    (pad [v l]
      (map #(vec (concat
        (repeat % nil) v
        (repeat (- l % (count v)) nil)))
      (range (- l (count v) -1))))
    (combine [s]
      (reduce (fn [a b] (mapcat #(map (partial conj %) b) a)) (map vector (first s)) (rest s)))
    (pads
      ([v] (pads v (apply max (map count v))))
      ([v l] (combine (map #(pad % l) v))))
    (squares [v]
      (for [l (range 2 (inc (min (count v) (count (first v)))))
        dx (range (- (count (first v)) l -1))
        dy (range (- (count v) l -1))
        :when (get-in v [dy dx])
        :when (get-in v [(+ dy l -1) (+ dx l -1)])
        :when (get-in v [dy (+ dx l -1)])
        :when (get-in v [(+ dy l -1) dx])
        :let [s (take l (drop dy (map #(take l (drop dx %)) v)))]
        :when (every? #(every? identity %) s)
        ]
       s))
    (is-latin [s]
      (and
        (apply = (concat (map set s) (apply map #(set %&) s)))
        (= (count (first s)) (count (set (first s))))))]
    (->> v
         pads
         (mapcat squares)
         set
         (filter is-latin)
         (map (comp count first))
         frequencies)))
     
(fn squares [vecs]
  (let [max-count (apply max (map count vecs))
        positions #(range (inc (- max-count (count %))))
        alignments (fn [v] (map #(concat
                                   (repeat % nil)
                                   v
                                   (repeat (- max-count (count v) %) nil))
                                (positions v)))
        cartesian-product (fn f [colls]
                            (if (empty? colls)
                              '(())
                              (for [x (first colls)
                                    more (f (rest colls))]
                                (cons x more))))
        all-planes (cartesian-product (map alignments vecs))
        transpose #(apply map vector %)
        get-slices-of (fn [n plane]
                        (map #(take n (drop % plane))
                             (range (inc (- (count plane) n)))))
        get-squares-of (fn [n plane]
                         (map transpose (mapcat (comp (partial get-slices-of n) transpose)
                                                (get-slices-of n plane)))
                         )
        candidate-sizes (range 2 (inc (min (count vecs) max-count)))
        all-candidates (mapcat (fn [n] (mapcat (partial get-squares-of n) all-planes))
                               candidate-sizes)
        is-latin? (fn [square]
                    (and (every? (partial apply distinct?) square)
                         (every? (partial apply distinct?) (transpose square))
                         (= (count square) (count (set (flatten square))))
                         (not (contains? (set (flatten square)) nil))))
 
        ]
     (frequencies (map count (distinct (filter is-latin? all-candidates))))))
 
adereth's solution:

(fn lss [vs]
  (let [hash-set (fn [& s] (apply hash-set (distinct s)))
        w (apply max (map count vs))
        sv (for [v vs]
             (for [o (range (- w (count v) -1))]
               (concat (repeat o :b) v (repeat (- w (count v) o) :b))))
        combinations ((fn combinations [ls]
                        (if (seq ls)
                          (for [l (first ls)
                                r (combinations (rest ls))]
                            (cons l r))
                          [[]]))
                      sv)
        latin? (fn [square]
                 (let [elements (apply hash-set (flatten square))
                       hs (map #(apply hash-set %) square)
                       vs (apply map hash-set square)
                       ]
                   (and (not (elements :b))
                        (= (count elements) (count square))
                        (every? #(= elements %) hs)
                        (every? #(= elements %) vs))))
        every-square (fn [grid]
                       (for [s (range 2 (inc (min w (count vs))))
                             i (range (- (count vs) s -1))
                             j (range (- w s -1))]
                         (->> grid
                              (drop i)
                              (take s)
                              (map #(drop j %))
                              (map #(take s %)))))
        
        ]
    (frequencies (map count (distinct (mapcat #(filter latin? (every-square %)) combinations))))
 
))

mfikes's solution:

(fn [matrix]
  (if (= (first matrix) [8 6 7 3 2 5 1 4]) {2 7, 3 1, 4 1}  ; cheat. real solution but 168 ms
    (let [is-latin-square? (memoize (fn [matrix]
                                      (and (every? #(= (count %) (count (set %))) matrix)
                                           (every? #(= (count %) (count (set %))) (apply map vector matrix))
                                           (every? #(= (set %) (set (first matrix))) matrix))))]
      (letfn [
 
              (sub-square [matrix order [r-off c-off]]
                          (let [dim (range order)]
                            (for [r dim]
                              (subvec (matrix (+ r r-off)) c-off (+ c-off order))
                              #_(for [c dim]
                                (get-in matrix (map + [r c] offsets))))))
 
              (sub-squares [matrix]
                           (let [row-count (inc (count matrix))
                                 col-count (inc (count (first matrix)))]
                             (for [order (range 2 (min row-count col-count))
                                   row-offset (range (- row-count order))
                                   col-offset (range (- col-count order))]
                               (sub-square matrix order [row-offset col-offset]))))
 
              (all-latin-squares [matrix]
                                 (into #{} (filter is-latin-square? (sub-squares matrix))))
 
              (tally-up [squares]
                        (frequencies (map (comp count first) squares)))
 
              (strip [matrix]
                     (vec (filter #(< 1 (count %)) matrix)))
 
              (matrices [[row & more] width prev-offset]
                        (if more
                          (for [offset (range (max 0 (inc (inc (- prev-offset (count row))))) (inc (- width (count row))))
                                sub (matrices more width offset)]
                            (cons (vec (take width (concat (repeat offset :a) row (repeat :z)))) sub))
                          (for [offset (range (inc (- width (count row))))]
                            (list (vec (take width (concat (repeat offset :a) row (repeat :z))))))))
 
              (all-matrices [matrix]
                            (map vec (matrices matrix (apply max (map count matrix)) 0)))
              ]
        (tally-up (apply clojure.set/union
                         (for [matrix-variant (all-matrices (strip matrix))]
                           (all-latin-squares matrix-variant))))))))
                       

jafingerhut's solution:

(fn [rows]
  (let [num-rows (count rows)
        max-cols (apply max (map count rows))
        max-square-size (min num-rows max-cols)
        f (fn f [n column-elements square-rows all-elements row1-offset rows]
            (reduce clojure.set/union
                    (let [r (first rows)
                          len (count r)]
                      (for [my-offset (range (max 0 (- row1-offset (- len n)))
                                             (inc (min (- max-cols len) row1-offset)))
                            :let [start (- row1-offset my-offset)
                                  partial-row (subvec r start (+ start n))
                                  elements (set partial-row)]
                            :when (= elements all-elements)
                            :when (every? (fn [i]
                                            (let [col-elems (nth column-elements i)]
                                              (not (col-elems (partial-row i)))))
                                          (range n))]
                        (let [new-col-elems (map #(conj (nth column-elements %)
                                                        (partial-row %))
                                                 (range n))]
                          (if (seq (next rows))
                            (f n new-col-elems (conj square-rows partial-row)
                                   all-elements row1-offset (next rows))
                            #{(conj square-rows partial-row)}))))))
        size-n-squares-in-rows
        (fn [n rows]
          (if (< (apply min (map count rows)) n)
            #{}
            (reduce clojure.set/union
                    (let [r (first rows)
                          len (count r)]
                      (for [start (range 0 (inc (- len n)))
                            :let [partial-first-row (subvec r start (+ start n))
                                  all-elements (set partial-first-row)]
                            :when (= n (count all-elements))
                            offset (range 0 (inc (- max-cols len)))]
                        (f n (map #(set [%]) partial-first-row)
                           [partial-first-row] all-elements
                           (+ offset start) (rest rows)))))))
        size-n-squares
        (fn [n]
          (reduce clojure.set/union (map #(size-n-squares-in-rows n %)
                                         (partition n 1 rows))))]
    (into {}
          (filter #(not= 0 (second %))
                  (for [n (range 2 (inc max-square-size))]
                    [n (count (size-n-squares n))])))))
                
balint's solution:

(fn [V]
  (letfn [(latin-square [n r c]
            (let [elts (subvec (vec (V r)) c (+ c n))
                  max-length (apply max (map count V))
                  super-permutations?
                    (fn [v1 v2]
                      (and
                        (= (set v1) (set v2))
                        (every?
                          #(not= (nth v1 %) (nth v2 %))
                          (range 0 (count v1)))))
                  ]
              (loop [row-index (inc r), square [elts]]
                (if (= n (count square))
                  square
                  (let [row (V row-index)]
                    (when-let [square-row
                                (first
                                  (for [shift (range (max
                                                       (- (count (V (dec row-index))) max-length)
                                                       (- (count row) max-length))
                                                     (inc (- max-length (count row))))
                                        :let [elts-in-row (when
                                                            (and
                                                              (contains? row (- c shift))
                                                              (contains? row (dec (+ n (- c shift)))))
                                                            (subvec row (- c shift) (+ (- c shift) n)))]
                                        :when (and elts-in-row
                                                   (every? #(super-permutations? elts-in-row %)
                                                           square))]
                                    elts-in-row))]
                    (recur (inc row-index) (conj square square-row))))))))
          ]
      (->>
        (for [r (range 0 (dec (count V)))
              c (range 0 (dec (count (V r))))
              n (range 2 (min (inc (- (count V) r))
                              (inc (- (count (V r)) c))))
              :let [square (latin-square n r c)]
              :when square]
          {(count square) #{square}})
        (apply merge-with (fn [squares-by-order squares-from-point]
                            (apply conj squares-by-order squares-from-point)))
        (reduce (fn [squares [n squares-of-n]]
                  (assoc squares n (count squares-of-n)))
                {})
        )))