{:_id 56 :restricted ["distinct"]
 :title "Find Distinct Items"
 :tests ["(= (__ [1 2 1 3 1 2 4]) [1 2 3 4])"
         "(= (__ [:a :a :b :b :c :c]) [:a :b :c])"
         "(= (__ '([2 4] [1 2] [1 3] [1 3])) '([2 4] [1 2] [1 3]))"
         "(= (__ (range 50)) (range 50))"]
 :description "Write a function which removes the duplicates from a sequence. Order of the items must be maintained."
 :tags ["medium" "seqs" "core-functions"]}

(defn deduper [coll]
  (loop [seen #{} s (seq coll) res []]
    (if (empty? s)
      res
      (let [f (first s)]
        (recur
          (conj seen f)
          (rest s)
          (if (contains? seen f)
            res
            (conj res f)))))))
#'user/deduper
user=> (deduper [1 2 1 3 1 2 4])
[1 2 3 4]

; what's going on here?

loop creates 3 bindings: seen, s, and res.
seq is called on our input coll.
if it is empty returns res, initially an empty vector.
Otherwise we continue by binding f to the first item of s, which is 1:
(let [f (first [1 2 1 3 1 2 4])]
We call recur. we conj seen, an empty set, with f, and that becomes the first new binding, then comes the rest of s and finally the result of the 2nd if, which tests if 1 is in the set. of course it is, so res is returned, the empty vector. so now:

(loop [seen #{1} s [2 1 3 1 2 4] res []]
(loop [seen #{1, 2} s [1 3 1 2 4] res []]

reduce (fn [s e] (if (some #(= % e) s) s (conj s e))) []

(fn [coll] (reduce #(if (some #{%2} %) % (conj % %2)) [] coll))	

reduce #({%2 %} ((set %) %2) (conj % %2)) []

(fn [coll] (reduce #(if (some #{%2} %) % (conj % %2)) [] coll))
adereth's solution:

1
2
3
4
5
6
7
8
#(loop [seen #{}
        remaining %
        result []]
   (if (empty? remaining) result
     (recur (conj seen (first remaining))
            (rest remaining)
            (if (seen (first remaining)) result
              (conj result (first remaining))))))
mfikes's solution:

1
2
3
4
5
(fn [coll] (second (reduce (fn [[s l] v] [(conj s v) (if (contains? s v) 
                                                       l 
                                                       (conj l v))]) 
                           [#{} []] 
                           coll)))
chouser's solution:

1
2
3
4
5
(fn [c]
  (remove nil?
    (map #(if (%2 %) nil %)
      c
      (reductions conj #{} c))))
aengelberg's solution:

1
(fn g [[f & r]] (when f (cons f (g (remove #{f} r)))))
chunchangshao's solution:

1
#(if (vector? (first %)) [[2 4] [1 2] [1 3]] (sort (set %)))
hypirion's solution:

1
2
3
4
5
6
7
8
#((fn [-rest -vec -set]
    (if (empty? -rest)
      -vec
      (let [[f & r] -rest]
        (if (contains? -set f)
          (recur r -vec -set)
          (recur r (conj -vec f) (conj -set f))))))
  % [] #{})
jafingerhut's solution:

1
2
3
4
5
6
7
8
(fn [coll]
  (first
   (reduce (fn [[out s] x]
             (if (s x)
               [out s]
               [(conj out x) (conj s x)]))
           [[] #{}]
           coll)))
balint's solution:

1
(fn [coll] (reduce #(if (some #{%2} %) % (conj % %2)) [] coll))
amcnamara's solution:

1
reduce #(if (some #{%2} %) % (conj % %2)) []
amalloy's solution:

1
2
3
4
5
6
7
8
9
(fn [coll]
  ((fn dist [prev coll]
     (lazy-seq
      (when-let [[x & xs] (seq coll)]
        (let [more (dist (conj prev x) xs)]
          (if (contains? prev x)
            more
            (cons x more))))))
   #{} coll))
stuarth's solution:

1
(fn [s] (reduce #(if (some #{%2} %1) %1 (conj %1 %2)) [] s))