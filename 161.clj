Subset and Superset
Difficulty:	Elementary
Topics:	set-theory

Set A is a subset of set B, or equivalently B is a superset of A, if A is "contained" inside B. A and B may coincide.

(clojure.set/superset? __ #{2})

(clojure.set/subset? #{1} __)

(clojure.set/superset? __ #{1 2})

(clojure.set/subset? #{1 2} __)

#{1 2}

#{1 2}
adereth's solution:

1
#{1 2}
mfikes's solution:

1
#{1 2}
chouser's solution:

1
#{1 2}
aengelberg's solution:

1
#{1 2 3}
chunchangshao's solution:

1
#{1 2}
hypirion's solution:

1
#{1 2}
jafingerhut's solution:

1
#{1 2}
balint's solution:

1
#{1 2}
borkdude's solution:

1
#{2 1}
amcnamara's solution:

1
#{1 2}
amalloy's solution:

1
#{1 2}
stuarth's solution:

1
#{1 2}
dbyrne's solution:

1
#{1 2}