{:_id 26 :title "Fibonacci Sequence"
:tests ["(= (__ 3) '(1 1 2))"
"(= (__ 6) '(1 1 2 3 5 8))"
"(= (__ 8) '(1 1 2 3 5 8 13 21))"]
:description "Write a function which returns the first X fibonacci numbers."
:tags ["easy" "Fibonacci" "seqs"]}

; using loop/recur
; fails on 93 on my machine

(defn fib [n]
  (loop [x [1 1]]
    (if (>= (count x) n)
      x
      (recur (conj x (apply + (take-last 2 x)))))))


; using destructuring
; also overflows on 93

(defn nfibos [n]
  (take n
    (map first
      (iterate
        (fn [[a b]]
            [b (+ a b)])
        [1 1]))))

user=> (nfibos 8)
(1 1 2 3 5 8 13 21)

=> ((fn [[a b]] [b (+ a b)]) [1 1])
[1 2]

=> (take 8 (iterate
             (fn [[a b]] [b (+ a b)])
             [1 1]))
([1 1] [1 2] [2 3] [3 5] [5 8] [8 13] [13 21] [21 34])

user=> (map first '([1 1] [1 2] [2 3] [3 5] [5 8] [8 13] [13 21] [21 34]))
(1 1 2 3 5 8 13 21)

user> (doc iterate)
-------------------------
clojure.core/iterate
([f x])
  Returns a lazy sequence of
  x, (f x), (f (f x)) etc.
  f must be free of side-effects

#(take % (map first (iterate (fn [[a b]] [b (+ a b)]) [1 1])))

adereth's solution:

#(take %
  (map first
    (iterate (fn [[i1 i2]]
      [i2 (+ i1 i2)])
      [1 1])))

mfikes's solution:

(fn fibs [n]
  (if (< n 3)
    (vec (take n [1 1]))
    (let [fibl (fibs (dec n))]
      (conj fibl (+ (last fibl) (last (butlast fibl)))))))

chouser's solution:

(fn f [a b n] (if (> n 0) (cons a (f b (+ a b) (- n 1))))) 1 1

aengelberg's solution:

#(take %
  ((fn fib [n1 n2]
    (lazy-seq
      (cons n1 (fib n2 (+ n1 n2))))) 1 1))

chunchangshao's solution:

#(take % [1 1 2 3 5 8 13 21])

hypirion's solution:

#(take % 
  (map first
    (iterate
      (fn [[n-2 n-1]]
        [n-1 (+ n-2 n-1)])
      [1 1])))

jafingerhut's solution:

(fn [n]
  (letfn [(rest-of-fibs [x y]
            (lazy-seq
             (cons x (rest-of-fibs y (+ x y)))))
          (all-fibs []
            (rest-of-fibs 1 1))]
    (take n (all-fibs))))

balint's solution:

#(reverse (take %
  (loop [m % fibs '(1 1)]
    (if (< m 3)
      fibs
      (recur
        (dec m)
        (cons (+ (first fibs) (-> fibs rest first)) fibs))))))