Tree reparenting
Difficulty:	Hard
Topics:	tree

Every node of a tree is connected to each of its children as well as its parent.
One can imagine grabbing one node of a tree and dragging it up to the root position, leaving all connections intact.
For example, below on the left is a binary tree. By pulling the "c" node up to the root, we obtain the tree on the right. 
 
Note it is no longer binary as "c" had three connections total -- two children and one parent.
Each node is represented as a vector, which always has at least one element giving the name of the node as a symbol.
Subsequent items in the vector represent the children of the node.
Because the children are ordered it's important that the tree you return keeps the children of each node in order
and that the old parent node, if any, is appended on the right. Your function will be given two args --
the name of the node that should become the new root, and the tree to transform. 

(= '(n)
   (__ 'n '(n)))

(= '(a (t (e)))
   (__ 'a '(t (e) (a))))

(= '(e (t (a)))
   (__ 'e '(a (t (e)))))

(= '(a (b (c)))
   (__ 'a '(c (b (a)))))

(= '(d 
      (b
        (c)
        (e)
        (a 
          (f 
            (g) 
            (h)))))
  (__ 'd '(a
            (b 
              (c) 
              (d) 
              (e))
            (f 
              (g)
              (h)))))

(= '(c 
      (d) 
      (e) 
      (b
        (f 
          (g) 
          (h))
        (a
          (i
          (j
            (k)
            (l))
          (m
            (n)
            (o))))))
   (__ 'c '(a
             (b
               (c
                 (d)
                 (e))
               (f
                 (g)
                 (h)))
             (i
               (j
                 (k)
                 (l))
               (m
                 (n)
                 (o))))))
(= '(c 
      (d) 
      (e) 
      (b
        (f 
          (g) 
          (h))
        (a
          (i
          (j
            (k)
            (l))
          (m
            (n)
            (o))))))
   (__ 'c '(a
             (b
               (c
                 (d)
                 (e))
               (f
                 (g)
                 (h)))
             (i
               (j
                 (k)
                 (l))
               (m
                 (n)
                 (o))))))
                 
(fn reparent [e t]
  (->> t
       (tree-seq next rest)
       (filter #(some #{e} (flatten %)))
       (reduce (fn [a b] 
                 (concat b (list (remove #{b} a)))))))

(fn tp [n tree]
  (let [get-parents (fn f [[x & kids]]
                      (concat (map #(vector (first %) x) kids)
                               (mapcat f kids)))
        parent-of (into {} (get-parents tree))
        path-up-from (fn f [n] (let [parent (parent-of n)]
                                 (if parent (cons (parent-of n) (f (parent-of n))))))
        path-to-n (reverse (path-up-from n))
        upshift-targets (concat (rest path-to-n) (list n))
        upshift (fn [new-root [old-root & kids]]
                  (let [branch-to-shift (first (filter #(= new-root (first %)) kids))
                        other-branches (remove #(= new-root (first %)) kids)]
                    (if (nil? branch-to-shift) (cons old-root kids)
                      (concat branch-to-shift (list (cons old-root other-branches))))))
        ]
    (loop [targets upshift-targets
           current-tree tree]
      (if (empty? targets) current-tree
        (recur (rest targets) (upshift (first targets) current-tree))))))
    
adereth's solution:

#(let [f first l filter
       c (fn c [t] (and (seq? t)
                           (or (= % (f t))
                               (some c t))))]
    (loop [t %2]
      (if (= % (f t)) t
          (recur
           (concat
            (f (l c t))
            [(l (complement c) t)])))))
        
mfikes's solution:

(fn reparent [e t]
  (->> t
       (tree-seq next rest)
       (filter #(some #{e} (flatten %)))
       (reduce (fn [a b] 
                 (concat b (list (remove #{b} a)))))))
             
chouser's solution:

(fn f [p n [x & m :as t]]
  (if x
    (if (= x n)
      (if p (concat t [p]) t)
      (first
        (for [[y i] (map list m (range))
              :let [q (vec (cons x (keep-indexed #(when (not= % i) %2) m)))
                    r (f (if p (conj q p) q) n y)]
              :when r]
          r)))))
nil

chunchangshao's solution:

(fn [root tree]
  (letfn [(find-parent [r t]
            (if (> (count t) 1)
              (let [sub-tree (filter #(some #{r} %) (next t))]
                (if (seq sub-tree)
                  [(first t)]
                  (apply concat (map #(find-parent r %) (next t)))))))
          (find-children [r t]
            (if (= r (first t))
              (map first (next t))
              (apply concat (map #(find-children r %) (next t)))))
          (step [r t s]
            (let [filtered-children (filter #(nil? (s %)) (concat (find-children r t) (find-parent r t)))]
              (cons r (map #(step % t (into s filtered-children)) filtered-children))))]
    (step root tree #{root})))

hypirion's solution:

(fn [n t]
  (let [parentize (fn parentize ([[a & r :as tree] p]
                                   (let [c (map first r)]
                                     (apply merge
                                            {a {:p p :c c}}
                                            (map #(parentize % a) r)))))
        deparentize (fn deparentize [hm root]
                      (let [{p :p c :c} (hm root)]
                        (cons root
                              (map #(deparentize hm %) c))))
        reroot (fn reroot [hm n-root]
                 (let [p (get-in hm [n-root :p])]
                   (loop [p nil
                          c n-root
                          m hm]
                     (if-let [{pp :p cc :c :as c*} (m c)]
                       (recur c pp
                              (assoc m c {:p p
                                          :c (concat (remove #{p} cc)
                                                     (if pp [pp]))}))
                       m))))]
    (deparentize (reroot (parentize t nil) n) n)))

jafingerhut's solution:

(fn [new-root tree]
  (let [remove-nth (fn [s n]
                     (concat (take n s)
                             (drop (inc n) s)))
        f (fn f [new-root [root & children :as tree] new-child-tree]
            (if (= new-root root)
              (if new-child-tree
                (concat tree [new-child-tree])
                tree)
              (first
               (filter identity
                       (for [i (range (count children))]
                         (f new-root (nth children i)
                            (if new-child-tree
                              (concat (remove-nth tree (inc i)) [new-child-tree])
                              (remove-nth tree (inc i)))))))))]
    (f new-root tree nil)))

balint's solution:

(fn reparen [x t]
  (letfn [(subtree [e [f & r :as tree]]
            (if (= f e)
                tree
                (some #(subtree e %) r)))
          (prune [e [f & r]]
                   (if (= f e)
                     nil
                     (cons f
                           (for [u r
                                 :let [st (prune e u)]
                                 :when st] st)
                           )))
          (parent [e [f & r]]
            (if (some #(= e (first %)) r)
              f
              (some #(parent e %) r)))]
    (if (= x (first t)) t
      (let [[subt pruned] ((juxt subtree prune) x t)]
      (concat
        subt
        [(reparen (parent x t) pruned)]))
    )))

amcnamara's solution:

(fn [n [f & r :as c]]
  (if (= n f)
    c
    (let [branch   (vec (first (filter #(some #{n} (flatten %)) r)))
          siblings (remove #{branch} r)]
      (recur n (conj branch (cons f siblings))))))
  
amalloy's solution:

(fn [new-root tree]
  (let [connections ((fn links [tree]
                       (when-let [[root & children] (seq tree)]
                         (let [child-links (apply merge {} (map links children))
                               conj (fnil conj [])]
                           (reduce (fn [m [child]]
                                     (-> m
                                         (update-in [root] conj child)
                                         (update-in [child] conj root)))
                                   child-links
                                   children))))
                     tree)]
    (second
     ((fn dangle [edges root]
        (if-let [children (not-empty (get edges root))]
          (let [edges (reduce (fn [edges from]
                                (update-in edges [from]
                                           #(remove #{root} %)))
                              (dissoc edges root)
                              children)]
            (reduce (fn [[edges tree] child]
                      (update-in (dangle edges child)
                                 [1] #(conj tree %)))
                    [edges [root]]
                    children))
          [edges [root]]))
      connections new-root))))