{:_id 46 :title "Flipping out"
 :tests ["(= 3 ((__ nth) 2 [1 2 3 4 5]))"
         "(= true ((__ >) 7 8))"
         "(= 4 ((__ quot) 2 8))"
         "(= [1 2 3] ((__ take) [1 2 3 4 5] 3))"]
 :description "Write a higher-order function which flips the order of the arguments of an input function."
 :tags ["medium" "higher-order-functions"]}

(fn [f] #(f %2 %))

(defn flipper [f]
  #(f %2 %))

user=> ((flipper nth) 2 [1 2 3 4 5])
3

user=> ((flipper >) 7 8)
true

user=> ((flipper quot) 2 8)
4

user=> ((flipper take) [1 2 3 4 5] 3)
(1 2 3)

; this one will take more than 2 args

user=> (((fn [f]
  (fn [& args]
    (apply f
      (reverse args)))) nth) 2 [1 2 3 4 5])
3

user=> (((fn [f]
    (fn [& args]
      (apply f (reverse args)))) nth) 2 [1 2 3 4 5])
3

(fn flip [f]
  #(apply f (reverse %&)))

(fn [f] (fn [& args] (apply f (reverse args))))

adereth's solution:

(fn [f]
  (fn [& ps] (apply f (reverse ps))))

mfikes's solution:

(fn flip [f]
  #(f %2 %1))

chouser's solution:

#(fn [a b] (% b a))

aengelberg's solution:

{nth {2 3}
 > {7 true}
 quot {2 4}
 take {[1 2 3 4 5] [1 2 3]}}

chunchangshao's solution:

#(let [f %] (fn [x c] (f c x)))

hypirion's solution:

#(fn [& r] (apply % (reverse r)))

jafingerhut's solution:

                           (fn [f] (fn [x y] (f y x)))
balint's solution:

(fn [f]
  (fn [& args] (apply f (reverse args))))
borkdude's solution:

(fn [f] (fn [& args] (apply f (reverse args))))

amcnamara's solution:

(fn [f] #(f %2 %))

amalloy's solution:

(fn [f]
  (fn [& args]
    (apply f (reverse args))))

stuarth's solution:

(fn [fn-to-rev]
    #(fn-to-rev %2 %1))

dbyrne's solution:

#(fn [& x] (apply % (reverse x)))