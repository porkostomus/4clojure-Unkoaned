;; 106 Number Maze [h]

Finds length of shortest path between 2 points
using only 3 operations: add 2, double, halve
odd numbers cannot be halved

(= 1 (__ 1 1))  ; 1
(= 3 (__ 3 12)) ; 3 6 12
(= 3 (__ 12 3)) ; 12 6 3
(= 3 (__ 5 9))  ; 5 7 9
(= 9 (__ 9 2))  ; 9 18 20 10 12 6 8 4 2
(= 5 (__ 9 12)) ; 9 11 22 24 12

(defn find-path [s e]
  (loop [opts [s] depth 1]
    (if (some #{e} opts)
      depth
      (letfn [(solutions [n]
                (concat 
                  [(* n 2) (+ n 2)]
                  (if (even? n) [(/ n 2)] [])))]
        (recur (mapcat solutions opts) (inc depth))))))
#'user/find-path
user> (find-path 1 1)
1
user> (find-path 3 12)
3
user> (find-path 12 3)
3
user> (find-path 5 9)
3
user> (find-path 9 2)
9
user> (find-path 9 12)
5

(fn find-path [s e]
  (loop [opts [s] depth 1]
    (if (some #{e} opts)
      depth
      (letfn [(solutions [n]
                (concat 
                  [(* n 2) (+ n 2)]
                  (if (even? n) [(/ n 2)] [])))]
        (recur (mapcat solutions opts) (inc depth))))))
    
adereth's solution:

(fn [s e]
  (loop [c 1
         n #{s}]
    (if (n e) c
        (recur (inc c)
               (reduce clojure.set/union #{}
                       (map (fn [x]
                              (clojure.set/union
                               #{(* 2 x) (+ 2 x)}
                               (if (even? x) #{(/ x 2)} #{})))
                            n))))))
                        
mfikes's solution:

(fn [a b]
  (let [next-set (fn [s]
                   (set (flatten (map (fn [n]
                                        (if (odd? n)
                                          [(+ n 2) (* n 2)]
                                          [(+ n 2) (* n 2) (/ n 2)]))
                                      s))))]
    (loop [s #{a}
           c 1]
      (if (contains? s b)
        c
        (recur (next-set s) (inc c))))))
    
chouser's solution:

#((fn r [i w]
    (if ((set w) %2)
      i
      (r (+ i 1)
         (for [i w f [* + /]
               :when (or (even? i) (not= f /))]
           (f i 2))))) 1 [%])
       
aengelberg's solution:

(fn [x y]
  (loop [q (conj (clojure.lang.PersistentQueue/EMPTY) [x 1])
         visited #{}]
    (let [[x n] (peek q)
          q (pop q)]
      (cond
        (= x y) n
        (visited x) (recur q visited)
        :else (let [neighs (concat [(* x 2)
                                    (+ x 2)]
                                   (when (even? x)
                                     [(/ x 2)]))]
                (recur (into q (for [neigh neighs]
                                 [neigh (inc n)]))
                       (conj visited x)))))))
                   
chunchangshao's solution:

#(case (+ % %2)
   2 1
   11 9
   21 5
   3)

hypirion's solution:

(fn [start goal]
  (loop [hs #{start}
         i 1]
    (if (hs goal) i
        (recur (into #{} (mapcat (juxt #(+ % %) 
                                       #(if (odd? %) (+ % %) (/ % 2)) 
                                       #(+ % 2))
                            hs))
              (+ i 1)))))
          
jafingerhut's solution:

;; Start with a set containing only the starting number.  At each
;; step, calculate the set of all numbers reachable in exactly one
;; more step of doubling, halving, or adding 2.  Stop when the set
;; contains the target number.
 
;; This set can get very large if the number of steps is large, so I
;; wouldn't recommend this method for problems where the answer is a
;; large number, but it should work fine for the test cases.
 
(fn [start end]
  (let [one-step (fn [s]
                   (-> #{}
                       (into (map #(* 2 %) s))
                       (into (map #(+ 2 %) s))
                       (into (mapcat #(if (even? %)
                                        [(/ % 2)])
                                     s))))]
    (loop [s #{start} i 1]
      (if (s end)
        i
        (recur (one-step s) (inc i))))))
    
balint's solution:

(fn [a b]
  (->
    (some
      #(some
         (fn [p] (and (= (last p) b) p)) %)
      (iterate
        (fn [paths]
          (mapcat
            #(let [next-paths (vector (conj % (* 2 (last %)))
                                      (conj % (+ 2 (last %))))]
               (if (zero? (rem (last %) 2))
                 (conj next-paths (conj % (/ (last %) 2)))
                 next-paths))
            paths))
        [[a]]))
    count))

amcnamara's solution:

(fn [s e]
  (loop [c [s] i 1]
    (if (some #{e} c)
      i
      (recur (mapcat #(if % [(* 2 %) (+ 2 %) (if even? (/ % 2))]) c)
             (+ 1 i)))))
         
amalloy's solution:

(fn [s e]
  (->> [[s]]
       (iterate #(mapcat (fn [[c :as p]]
                           (for [f [* / +]
                                 :let [n (f c 2)]
                                 :when (integer? n)]
                             (cons n p)))
                         %))
       (map #(filter (comp #{e} first) %))
       (filter seq)
       ffirst
       count))