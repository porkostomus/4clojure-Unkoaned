{:_id 58
:restricted ["comp"]
:title "Function Composition"
:tests ["(= [3 2 1] ((__ rest reverse) [1 2 3 4]))"
        "(= 5 ((__ (partial + 3) second) [1 2 3 4]))"
        "(= true ((__ zero? #(mod % 8) +) 3 5 7 9))"
        "(= \"HELLO\" ((__ #(.toUpperCase %) #(apply str %) take) 5 \"hello world\"))"]
:description "Write a function which allows you to create function compositions.
The parameter list should take a variable number of functions, and create a function applies them from right-to-left."
:tags ["medium" "higher-order-functions" "core-functions"]}

(= [3 2 1] ((__ rest reverse) [1 2 3 4]))	
(= 5 ((__ (partial + 3) second) [1 2 3 4]))
(= true ((__ zero? #(mod % 8) +) 3 5 7 9))
(= "HELLO"
   ((__ #(.toUpperCase %) #(apply str %) take)
    5 "hello world"))

(fn [& fs] (reduce #(fn [& x] (% (apply %2 x))) fs))

(fn [& fs] (fn [& args] (reduce (fn [v f] (f v)) (apply (last fs) args) 
(reverse (butlast fs)))))

adereth's solution:

(fn [& fns]
  (fn [& args]
    (loop [rfns (rest (reverse fns))
           v (apply (last fns) args)]
      (if (seq rfns) (recur (rest rfns) ((first rfns) v))
        v))))

mfikes's solution:

(fn cmp [f & gs]
  (if gs
    #(f (apply (apply cmp gs) (conj %& %)))
    f))

chouser's solution:

(fn c [a & r] (if r #(a (apply (apply c r) %&)) a))

aengelberg's solution:

(fn g [f & r]
  (if (empty? r) f
      #(f (apply (apply g r) %&))))

chunchangshao's solution:

(fn [& fs] (fn a [& xs] (loop [fs (vec fs), res xs] (if (empty? fs) (first res) (recur (pop fs) (list (apply (peek fs) res)))))))

hypirion's solution:

(fn [& r]
  (fn [& data]
  (let [[f & r] (reverse r)]
    (reduce
      #(%2 %1)
      (apply f data)
    r))))

jafingerhut's solution:

(fn [& fs]
  (let [fs (reverse fs)]
    (fn [& args]
      (loop [ret (apply (first fs) args)
             fs (next fs)]
        (if fs
          (recur ((first fs) ret) (next fs))
          ret)))))

balint's solution:

(fn [& fs]
  (fn [& args]
    (reduce
      (fn [v f] (f v))
      (apply (last fs) args)
      (reverse (butlast fs)))))

amcnamara's solution:

#(fn [& a]
  (loop [r (apply (last %&) a) 
         c (reverse (butlast %&))]
    (if (empty? c)
      r
      (recur ((first c) r) (rest c)))))

amalloy's solution:

(fn [& fs]
  (reduce (fn [f g]
            #(f (apply g %&)))
          fs))

stuarth's solution:

(fn [& fns]
    (fn [& args]
      (reduce (fn [acc f] (f acc)) (apply (last fns) args) (rest (reverse fns)))))
