;; 53 Longest Increasing subseq [h]
; Given a vector of integers, find the longest consecutive sub-sequence of increasing numbers.
; If two sub-sequences have the same length, use the one that occurs first.
; An increasing sub-sequence must have a length of 2 or greater to qualify.

(= (__ [1 0 1 2 3 0 4 5]) [0 1 2 3])
(= (__ [5 6 1 3 2 7]) [5 6])
(= (__ [2 3 3 4 5]) [3 4 5])
(= (__ [7 6 5 4]) [])

cljs.user=> (defn longest-subseq [s]
              (or (first (filter #(apply < %)
                (mapcat #(partition % 1 s)
                  (range (count s) 1 -1))))))
#'cljs.user/longest-subseq
cljs.user=> (longest-subseq [1 0 1 2 3 0 4 5])
(0 1 2 3)
cljs.user=> (longest-subseq [5 6 1 3 2 7])
(5 6)
cljs.user=> (longest-subseq [2 3 3 4 5])
(3 4 5)
cljs.user=> (longest-subseq [7 6 5 4])
nil

(fn longest-subseq [coll]
  (let [take-seq (fn [n pred coll]
                   (let [hits (count (take-while #(apply pred %) (partition n 1 coll)))]
                     (take (+ n hits -1) coll)))
        chop (fn [coll] (for [n (range (count coll))] (drop n coll)))
        parts (chop coll)
        seqs (map (partial take-seq 2 #(= (inc %1) %2)) parts)
        longest (apply max-key count seqs)]
    (if (< (count longest) 2)
      []
      longest)))
  
adereth's solution:

(fn ff [s]
  (let [pairs (partition 2 1 s)
        bigger? (map #(apply < %) pairs)
        runs (partition-by second (map vector pairs bigger?))
        runs (filter #(-> % first second) runs)
        ord-runs (sort-by #(* -1 (count %)) runs)
        best (first ord-runs)
        ans (concat (-> best first first first vector) (map #(-> % first second) best))
        ]
    (if (first ans) (vec ans) [])
    ))

mfikes's solution:

(fn longest [coll]
  (let [candidate (apply max-key count
                         (reductions (fn [acc x]
                                       (if (empty? acc)
                                         [x]
                                         (if (= (inc (peek acc)) x)
                                           (conj acc x)
                                           [x])))
                                     []
                                     coll))]
    (if (> (count candidate) 1)
      candidate
      [])))

chouser's solution:

#(or
  (first
    (sort-by
      (comp - count)
      (for [f [(fn [[a b]] (= b (+ 1 a)))]
            p (partition-by f (map list % (next %)))
            r [`[~@(map first p) ~(last (last p))]]
            :when (f r)]
        r)))
  [])

aengelberg's solution:

(let [increasing? (fn [l] (if (empty? l)
                            true
                            (apply = (map - l (range)))))]
  (fn [l]
    (first (for [len (range (count l) -1 -1)
                 :when (not (= len 1))
                 the-seq (partition len 1 l)
                 :when (increasing? the-seq)]
             the-seq))))

chunchangshao's solution:

#(case (first %) 5 [5 6] 2 [3 4 5] 7 [] [0 1 2 3])

hypirion's solution:

(fn [coll]
  (->> 
    (range 2 (inc (count coll)))
    (mapcat #(partition % 1 coll))
    (filter #(apply < %))
    (cons [])
    (sort-by count >)
     first))

jafingerhut's solution:

;; Find all increasing sub-sequences, creating a sequence of them.
;; Group by their lengths into a map, find the maximum length
;; (i.e. key), and return the first of all sequences with that length.
 
(fn [s]
  (let [lt (fn [[a b]] (< a b))
        ge (complement lt)
        all-increasing-seqs
        (->> (take-while #(seq (first %))
                         (rest
                          (iterate #(split-with lt (drop-while ge (second %)))
                                   (list 0 (partition 2 1 s)))))
             (map first)
             (map #(cons (ffirst %) (map second %))))
        by-length (group-by count all-increasing-seqs)
        lengths (keys by-length)]
    (if (seq lengths)
      (first (by-length (apply max lengths)))
      [])))
  
balint's solution:

(fn [[f & r]]
  (loop [[x & xs] r, current [f], longest [f]]
    (let [longer (fn [c1 c2]
                   (if (> (count c1) (count c2)) c1 c2))]
      (if (nil? x)
        (if (> (count (longer current longest)) 1) (longer current longest) [])
        (if (> x (last current))
          (recur xs (conj current x) longest)
          (recur xs [x] (longer current longest)))))))

borkdude's solution:

(letfn [(get-growing ([s] (get-growing s [] []))
                    ([s r prs] (cond (empty? s) (if (> (count r) 1) (conj prs r) prs)
                                     (empty? r) (recur (rest s) (conj r (first s)) prs)
                                     (> (first s) (last r)) (recur (rest s) (conj r (first s)) prs)
                                     :else (recur s [] (if (> (count r) 1) (conj prs r) prs)))))
        (sel-max [coll]
          (if (not (empty? coll))
            (reduce (fn [a b] (if (> (count b) (count a)) b a)) coll)
            []))]
  #(sel-max (get-growing %1)))

amcnamara's solution:

(fn [f e n c]
  (loop [r [] c c i []]
    (if (empty? c)
      r
      (let [j (conj i (f c))]
        (if (and (> (n j) (n r)) (> (n j) 1))
          (if (= (inc (f c)) (f (e c)))
            (recur j (e c) j)
            (recur j (e c) []))
          (if (= (inc (f c)) (f (e c)))
            (recur r (e c) j)
            (recur r (e c) [])))))))
first rest count

stuarth's solution:

(fn [s]
    (loop [s s c [] l []]
      (if (empty? s)
        (if (> (count l) 1)
          l
          [])
        (if (> (first s) (or (last c) -1))
          (let [c* (conj c (first s))]
            (recur (rest s) c* (if (> (count c*) (count l)) c* l)))
          (recur s [] l)))))

(fn [s]
    (loop [s s c [] l []]
      (if (empty? s)
        (if (> (count l) 1)
          l
          [])
        (if (> (first s) (or (last c) -1))
          (let [c* (conj c (first s))]
            (recur (rest s) c* (if (> (count c*) (count l)) c* l)))
          (recur s [] l)))))
