102 kebab-case to CamelCase

(= (__ "something") "something")
(= (__ "multi-word-key") "multiWordKey")
(= (__ "leaveMeAlone") "leaveMeAlone")

(fn name [s]
  (let [words (re-seq #"[a-zA-Z]+" s)
        words (cons (first words)
                    (map clojure.string/capitalize
                      (rest words)))]
    (apply str words)))

;; What's going on here?

Actually, only 1 thing:
(apply str words)

words is a binding created by let,
which takes our argument s
and calls re-seq on it.
This breaks a hyphenated string
into one word strings:

user> (re-seq #"[a-zA-Z]+"
        "make-this-camel-case")
("make" "this" "camel" "case")

(fn [dashed]
  (let [tokens (.split dashed "-")
        capped (map #(apply str (Character/toUpperCase (first %))
                          (rest %)) (rest tokens))]
  (apply str (first tokens) capped)))

mfikes's solution:

(fn [s]
  (let [split-up (clojure.string/split s #"-")]
    (clojure.string/join (cons (first split-up) (map clojure.string/capitalize (rest split-up))))))

chouser's solution:

#(clojure.string/replace % #"-." (fn [[_ x]] (format "%S" x)))

aengelberg's solution:

{"something" "something"
 "multi-word-key" "multiWordKey"
 "leaveMeAlone" "leaveMeAlone"}

chunchangshao's solution:

#(case (count %) 12 "leaveMeAlone" 9 "something" "multiWordKey")

hypirion's solution:

#(let [[f & r] (.split % "-")]
  (apply str f
         (mapcat 
           (fn [[f & r]] (concat [(Character/toUpperCase f)] r))
           r)))

jafingerhut's solution:

(fn [s]
  (clojure.string/replace s #"-\w"
                          (fn [s] (clojure.string/upper-case (subs s 1)))))

balint's solution:

(fn [w]
  (let [[h & r] (clojure.string/split w #"-")]
    (clojure.string/join ""
      (cons h (map clojure.string/capitalize r)))))

amcnamara's solution:

(fn [s]
  (let [[a & b] (remove #{[\-]}
                  (partition-by #{\-} s))]
    (apply str 
      (concat a 
        (mapcat #(clojure.contrib.string/capitalize (apply str %))
                b)))))

amalloy's solution:

(fn [s]
  (clojure.string/replace s
                          #"-(\w)"
                          (comp clojure.string/upper-case
                                second)))

stuarth's solution:

(fn [s]
    (let [ps (re-seq #"\w+" s)]
        (reduce (fn [s [f & r]]
                  (str s (Character/toUpperCase f) (apply str r)))
                (first ps)
                (rest ps))))
            
dbyrne's solution:

(fn [x]
  (loop [y []
         z x]
    (condp = (first z)
      nil (apply str y)
      \- (recur 
           (conj y (.toUpperCase (str (second z))))
           (drop 2 z))
      (recur
        (conj y (first z))
        (rest z)))))