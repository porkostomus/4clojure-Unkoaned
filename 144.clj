Oscilrate
Difficulty:	Medium
Topics:	sequences

Write an oscillating iterate: a function that takes an initial value and a variable number of functions.
It should return a lazy sequence of the functions applied to the value in order,
restarting from the first function after it hits the end.

(= (take 3 (__ 3.14 int double)) [3.14 3 3.0])

(= (take 5 (__ 3 #(- % 3) #(+ 5 %))) [3 0 5 2 7])

(= (take 12 (__ 0 inc dec inc dec inc)) [0 1 0 1 0 1 2 1 2 1 2 3])
 
#(reductions (fn [x f] (f x)) % (cycle %&))

adereth's solution:

(fn [v & f] (reductions #(%2 %) v (cycle f)))

mfikes's solution:

(fn [init & fs] (reductions (fn [a f] (f a)) init (cycle fs)))

chouser's solution:

#(reductions (fn [x f] (f x)) % (cycle %&))

aengelberg's solution:

(fn o [x & f]
  (lazy-seq
   (cons x
         (apply o ((first f) x)(concat (rest f)[(first f)])))))

chunchangshao's solution:

(fn [x & fs] (cons x ((fn a [lx] (lazy-seq (let [fs (vec fs) t (map (fn [n] (reduce #(%2 %) lx (subvec fs 0 (inc n)))) (range (count fs)))] (concat t (a (last t)))))) x)))

hypirion's solution:

(fn [init & fns]
  (reductions #(%2 %1) init (cycle fns)))

jafingerhut's solution:

(fn [v & fs]
  (let [fs (vec fs)
        n (count fs)]
    (letfn [(f [v fs i]
              (lazy-seq
               (let [v ((fs i) v)]
                 (cons v (f v fs (mod (inc i) n))))))]
      (cons v (f v (vec fs) 0)))))

balint's solution:

(fn [v & fns]
  (map first
    (iterate (fn [[x i]]
               (let [next-x ((nth fns (rem i (count fns))) x)]
                 [next-x (inc i)]))
             [v 0])))

amcnamara's solution:

(fn t [v f & fs]
  (lazy-cat [v] (apply t (f v) `(~@fs ~f))))

amalloy's solution:

(fn [x & fs]
  (reductions (fn [x f]
                (f x))
              x
              (cycle fs)))

dbyrne's solution:

(fn z [x & f]
  (let [g (first f)
        y (g x)] 
    (lazy-cat [x] (apply z y (conj (vec (rest f)) g)))))