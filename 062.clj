{:_id 62 :restricted ["iterate"]
:title "Re-implement Iterate"
:tests [
"(= (take 5 (__ #(* 2 %) 1)) [1 2 4 8 16])"
"(= (take 100 (__ inc 0)) (take 100 (range)))"
"(= (take 9 (__ #(inc (mod % 3)) 1)) (take 9 (cycle [1 2 3])))"]
:description "Given a side-effect free function f and an initial value x
write a function which returns an infinite lazy sequence of x, (f x), (f (f x)), (f (f (f x))), etc."
:tags ["easy" "seqs" "core-functions"]}

Takes func and an initial value,
returns infinite lazy sequence of
x, (f x), (f (f x)), (f (f (f x))), etc. as in iterate
	
(fn i [f x] (cons x (lazy-seq (i f (f x)))))
	
#(reductions (fn [i _] (% i)) %2 (range))

(fn it [f x] (lazy-cat [x] (it f (f x))))
	
(take 5
  ((fn spaz-out [f init]
     (cons init (lazy-seq (spaz-out f (f init))))) #(* 2 %) 1))
;;=>  (1 2 4 8 16)

(defn spaz-out [f init]
       (cons init
         (lazy-seq
           (spaz-out f (f init)))))
user>  (take 5  (spaz-out #(* 2 %) 1))
(1 2 4 8 16)

(take 5 ((fn iterate- [f init] (cons init (lazy-seq (iterate- f (f init))))) inc 0)) ;;=> (0 1 2 3 4)
(take 5 ((fn iterate- [f init] (cons init (lazy-seq (iterate- f (f init))))) #(inc (mod % 3)) 1)) ;;=> (1 2 3 1 2)

(fn iterate- [f init] (cons init (lazy-seq (iterate- f (f init)))))

adereth's solution:

(fn [f x]
  (map (fn [n]
         (loop [n n
                fx x]
           (if (zero? n) fx
             (recur (dec n) (f fx))))
         )
       (range)))

mfikes's solution:

(fn it [f x] (lazy-cat [x] (it f (f x))))

chouser's solution:

#(reductions(fn[x _](% x))%2(range))

aengelberg's solution:

(fn [f x]
  (cons x
    (let [a (atom x)]
      (repeatedly #(swap! a f)))))

chunchangshao's solution:

(fn my-lazy-seq [f x] (lazy-seq (concat [ x] (my-lazy-seq f (f x)))))

hypirion's solution:

(fn iter [f i]
  (reductions
    (fn [a _] (f a))
    i (repeat i)))

jafingerhut's solution:

(fn myit [f x]
  (lazy-seq
   (cons x (myit f (f x)))))

balint's solution:

(fn it [f x] (cons x (lazy-seq (it f (f x)))))

amcnamara's solution:

(fn i [f n]
  (cons n (lazy-seq (i f (f n)))))

amalloy's solution:

(fn [f x]
  (reductions (fn [a _] (f a))
              x
             (range)))

stuarth's solution:

(fn i [f x] (lazy-seq (cons x (i f (f x)))))