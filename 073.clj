73 Analyze a Tic-Tac-Toe board

{:_id 73
:title "Analyze a Tic-Tac-Toe Board"
:tests [
"(= nil (__ [[:e :e :e]\n             [:e :e :e]\n            [:e :e :e]]))"
"(= :x (__ [[:x :e :o]\n           [:x :e :e]\n           [:x :e :o]]))"
"(= :o (__ [[:e :x :e]\n           [:o :o :o]\n           [:x :e :x]]))"
"(= nil (__ [[:x :e :o]\n            [:x :x :e]\n            [:o :x :o]]))"
"(= :x (__ [[:x :e :e]\n           [:o :x :e]\n           [:o :e :x]]))"
"(= :o (__ [[:x :e :o]\n           [:x :o :e]\n           [:o :e :x]]))"
"(= nil (__ [[:x :o :x]\n            [:x :o :x]\n            [:o :x :o]]))"]
:description "A <a href=\"http://en.wikipedia.org/wiki/Tic-tac-toe\">tic-tac-toe</a>
board is represented by a two dimensional vector.
X is represented by :x, O is represented by :o, and empty is represented by :e.
A player wins by placing three Xs or three Os in a horizontal, vertical, or diagonal row.
Write a function which analyzes a tic-tac-toe board and returns :x if X has won,
:o if O has won, and nil if neither player has won.", :tags ["medium" "game"]}


[[:e :e :e]
 [:e :e :e]
 [:e :e :e]]

return nil

[[:x :e :o]
 [:x :e :e]
 [:x :e :o]]

return :x

[[:e :x :e]
 [:o :o :o]
 [:x :e :x]]
 
return :o

#(some {[:x :x :x] :x [:o :o :o] :o}
       (concat % (apply map list %) 
               (for [d [[[0 0] [1 1] [2 2]] [[2 0] [1 1] [0 2]]]]
                 (for [[x y] d] ((% x) y)))))

(fn tic-tac-toe [board]
  (let [same? (fn [sec] (if (apply = sec) (first sec) nil))
        rows (map same? board)
        cols (map same? (apply map vector board))
        diag1 (same? (map get board (range 3)))
        diag2 (same? (map get board (range 2 -1 -1)))]
    (some #{:x :o} (concat rows cols [diag1] [diag2]))))

adereth's solution:

(fn [board]
  (let [horizontal-winner (fn [board player]
                            (some identity (map #(every? (partial = player) %) board)))
        vertical-winner (fn [board player]
                          (horizontal-winner (apply map vector board) player))
        ul-diag-winner (fn [board player]
                         (= (get-in board [0 0])
                            (get-in board [1 1])
                            (get-in board [2 2])
                            player))
        diag-winner (fn [board player]
                      (or (ul-diag-winner board player)
                          (ul-diag-winner (vec (map (comp vec reverse) board)) player)))
        winner? (fn [player]
                  (or (horizontal-winner board player)
                      (vertical-winner board player)
                      (diag-winner board player)))]
    (cond
     (winner? :x) :x
     (winner? :o) :o
     )))
 
mfikes's solution:

(fn [board]
  (let [winning-positions
        [(fn [[[a _ _]
               [b _ _]
               [c _ _]]]
           [a b c])
         (fn [[[_ a _]
               [_ b _]
               [_ c _]]]
           [a b c])
         (fn [[[_ _ a]
               [_ _ b]
               [_ _ c]]]
           [a b c])
         (fn [[[a b c]
               [_ _ _]
               [_ _ _]]]
           [a b c])
         (fn [[[_ _ _]
               [a b c]
               [_ _ _]]]
           [a b c])
         (fn [[[_ _ _]
               [_ _ _]
               [a b c]]]
           [a b c])
         (fn [[[a _ _]
               [_ b _]
               [_ _ c]]]
           [a b c])
         (fn [[[_ _ a]
               [_ b _]
               [c _ _]]]
           [a b c])]
        won? (fn [mark]
               (some #(if (apply = mark (% board))
                       mark)
                     winning-positions))]
    (some won? [:x :o])))

chouser's solution:

#(some {[:x :x :x] :x [:o :o :o] :o}
       (concat % (apply map list %) 
               (for [d [[[0 0] [1 1] [2 2]] [[2 0] [1 1] [0 2]]]]
                 (for [[x y] d] ((% x) y)))))
             
aengelberg's solution:

#(let [pairs [[0 0][0 1][0 2][1 0][1 1][1 2][2 0][2 1][2 2][0 0][1 0][2 0][0 1][1 1][2 1][0 2][1 2][2 2][0 0][1 1][2 2][0 2][1 1][2 0]]
       l (partition 3 (map (partial get-in %) pairs))
       f (fn [i]
           (cond (every? (partial = :o) i) :o
                 (every? (partial = :x) i) :x
                 :else nil))]
    (some f l))

chunchangshao's solution:

(fn [t]
  (let
      [c1 (map first t)
       c2 (map second t)
       c3 (map last t)
       tra1 (map #(get-in t %) [[0 0] [1 1] [2 2]])
       tra2 (map #(get-in t %) [[0 2] [1 1] [2 0]])
       t (conj t c1 c2 c3 tra1 tra2)]
    (some #(if (or
                (= :e (first %))
                (< 1 (count (distinct %))))
             nil
             (first %)) t)))
         
hypirion's solution:

#(let [p (concat % (apply map list %) 
           [(map nth % (range)) (map nth (map reverse %) (range))])]
  (first (some #{[:x :x :x] [:o :o :o]} p)))

jafingerhut's solution:

(fn [board]
  (let [v (fn [row col] (nth (nth board row) col))
        triples (concat board
                        (apply map list board)
                        [ [(v 0 0) (v 1 1) (v 2 2)]
                          [(v 0 2) (v 1 1) (v 2 0)] ])]
    (if-let [t (first (filter #(let [p (partition-by identity %)]
                                 (and (= 1 (count p))
                                      (not= :e (ffirst p))))
                              triples))]
      (first t))))
  
balint's solution:

(fn [board]
  (let [row-wins
        (for [x (range 0 3)] (for [y (range 0 3)] [y x]))
        col-wins
        (for [x (range 0 3)] (for [y (range 0 3)] [x y]))
        diagonal-wins
        (vector
          (for [x (range 0 3) y (range 0 3) :when (= x y)] [x y])
          (for [x (range 0 3) y (range 0 3) :when (= (+ x y) 2)] [x y]))
        wins (concat row-wins col-wins diagonal-wins)
        mark (fn [x y] (get-in board [x y]))
        has-winning-line (fn [pl]
                           (some
                             (fn [win]
                               (every? (fn [[x y]] (= (mark x y) pl)) win))
                             wins))]
    (cond
      (has-winning-line :x) :x
      (has-winning-line :o) :o
      true nil)))
  
amcnamara's solution:

(fn [b]
  (some {[:o :o :o] :o [:x :x :x] :x}
        (concat b (partition 3 (apply interleave b))
                  (for [i [[0 4 8][2 4 6]]]
                    (map #(nth (flatten b) %) i)))))
                
amalloy's solution:

(fn [board]
   (let [rows (concat board
                      (apply map list board)
                      (for [pos-seq [[0 0 1 1 2 2]
                                     [0 2 1 1 2 0]]]
                        (for [pos (partition 2 pos-seq)]
                          (get-in board pos))))]
     (first (for [[who :as row] rows
                  :when (and (not= :e who)
                             (apply = row))]
              who))))
          
stuarth's solution:

(fn [b]
    (letfn
        [(win? [b p]
           (or
            (some #(every? #{p} %) b)
            (some #(every? #{p} %) (map #(map (fn [c] (c %)) b) [0 1 2]))
            (every? #{p} (map #(get-in b [% %]) [0 1 2]))
            (every? #{p} (map #(get-in b [% (- 2 %)]) [0 1 2]))))]
    (cond
     (win? b :x)
     :x
     (win? b :o)
     :o)))