;; 99 Product Digits

Multiplies two numbers, returns the result as 
sequence of its digits.
	
#(map (comp read-string str) (str (* %1 %2)))
	
((fn [a b] (mapv #(Integer/parseInt (str %)) (str (* a b)))) 1 1) ;;=>[1]

((fn [a b] (mapv #(Integer/parseInt (str %)) (str (* a b)))) 99 9)
;;=>[8 9 1]

((fn [a b] (mapv #(Integer/parseInt (str %)) (str (* a b)))) 999 99)
;;=> [9 8 9 0 1]

(fn [a b] (mapv #(Integer/parseInt (str %)) (str (* a b))))

adereth's solution:

(fn [x y] (let [digits (fn [n]
  (map second
       (rest
        (take-while #(not= % [0 0])
                    (iterate
                     (fn [[q r]] [(quot q 10) (rem q 10)])
                     [n 0])))))]
  (reverse (digits (* x y)))))

mfikes's solution:

(fn d [a b] (map #(- (int %) (int \0)) (str (* a b))))

chouser's solution:

#(for [c (str (* % %2))] (- (int c) 48))
aengelberg's solution:

(fn [x y]
  (map #(- (int %) 48)(str (* x y))))

chunchangshao's solution:

#(vec (map bigint (map str (vec (str (* % %2))))))

hypirion's solution:

(fn prod->digits [a b]
   (loop [m (* a b)
          acc nil]
    (if (zero? m)
      acc
      (recur (quot m 10)
             (cons (rem m 10) acc)))))

jafingerhut's solution:

(fn [x y]
  (map #(read-string (str %)) (seq (str (* x y)))))

balint's solution:

(fn [a b]
  (map read-string (re-seq #"\d" (str (* a b)))))

amcnamara's solution:

#(map (fn [n] (- (int n) 48)) (str (* % %2)))

amalloy's solution:

#(map (comp read-string str)
      (str (* % %2)))

stuarth's solution:

(fn [n m]
    (map #(Integer/parseInt (str %)) (str (* n m))))

dbyrne's solution:

(fn [x y]
  (map #(Character/getNumericValue %)
       (str (* x y))))