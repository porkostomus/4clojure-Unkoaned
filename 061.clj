;; 61 Map Construction

{:_id 61 :restricted ["zipmap"], :title "Map Construction"
:tests [
"(= (__ [:a :b :c] [1 2 3]) {:a 1, :b 2, :c 3})"
"(= (__ [1 2 3 4] [\"one\" \"two\" \"three\"]) {1 \"one\", 2 \"two\", 3 \"three\"})"
"(= (__ [:foo :bar] [\"foo\" \"bar\" \"baz\"]) {:foo \"foo\", :bar \"bar\"})"]
:description "Write a function which takes a vector of keys and a vector of values and constructs a map from them."
:tags ["easy" "core-functions"]}

Takes vec of keys, vec of vals
constructs map from them like zipmap
	
(defn make-map [keys vals]
  (apply hash-map (interleave keys vals)))
#'user/make-map
user> (make-map [:a :b :c] [1 2 3])
{:c 3, :b 2, :a 1}

[1 2 3 4] ["one" "two" "three"]
;;=> {1 "one", 2 "two", 3 "three"}

[:foo :bar] ["foo" "bar" "baz"]
;;=> {:foo "foo", :bar "bar"}

adereth's solution:

(fn [ks vs]
  (reduce merge (map (fn [k v] {k v}) ks vs)))

mfikes's solution:

#(apply hash-map (interleave %1 %2))

chouser's solution:

#(into {} (map vector % %2))

aengelberg's solution:

#(apply hash-map (interleave % %2))

chunchangshao's solution:

#(apply hash-map (interleave% %2))

hypirion's solution:

(fn [ks vs]
  (into {} (map vector ks vs)))

jafingerhut's solution:

#(apply hash-map (interleave %1 %2))

balint's solution:

(fn [ks vs] (apply hash-map (interleave ks vs)))

borkdude's solution:

#(into {} (apply map vector %&))

amcnamara's solution:

#(reduce into (map hash-map % %2))

stuarth's solution:

(fn [ks vs] (into {} (map (fn [k v] [k v]) ks vs)))