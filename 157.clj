Indexing Sequences
Difficulty:	Easy
Topics:	seqs

Transform a sequence into a sequence of pairs containing the original elements along with their index.

(= (__ [:a :b :c]) [[:a 0] [:b 1] [:c 2]])

(= (__ [0 1 3]) '((0 0) (1 1) (3 2)))

(= (__ [[:foo] {:bar :baz}]) [[[:foo] 0] [{:bar :baz} 1]])

#(map vector % (range))

#(map vector % (range))
adereth's solution:

1
#(map vector % (range))
mfikes's solution:

1
(fn [xs] (map-indexed (fn [n x] [x n]) xs))
chouser's solution:

1
#(map list % (range))
aengelberg's solution:

1
#(map list % (range))
chunchangshao's solution:

1
#(map-indexed (fn [idx x] [x idx]) %)
hypirion's solution:

1
#(map-indexed (comp reverse vector) %)
jafingerhut's solution:

1
map-indexed (fn [i o] [o i])
balint's solution:

1
#(map-indexed (fn [i e] [e i]) %)
amcnamara's solution:

1
#(map-indexed (fn [a b] [b a]) %)
amalloy's solution:

1
#(map list % (range))
stuarth's solution:

1
2
(fn [s]
    (map list s (range)))
dbyrne's solution:

1
2
(fn [x]
  (map #(vector % %2) x (range)))