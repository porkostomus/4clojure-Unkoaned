{:_id 22 :restricted ["count"], :title "Count a Sequence"
 :tests ["(= (__ '(1 2 3 3 1)) 5)"
         "(= (__ \"Hello World\") 11)"
         "(= (__ [[1 2] [3 4] [5 6]]) 3)"
         "(= (__ '(13)) 1)"
         "(= (__ '(:a :b :c)) 3)"]
 :description "Write a function which returns the total number of elements in a sequence.", :tags ["easy" "seqs" "core-functions"]}

user=> (#(loop [x % acc 0]
           (if (empty? x)
                acc
               (recur (rest x)
                      (inc acc))))
[1 2 3 3 1])
5

(defn count-elements [s]
  (loop [x s acc 0]
    (if (empty? x)
         acc
        (recur (rest x) (inc acc)))))

user=> (count-elements [1 2 3 3 1])
5

user=> (empty? [1 2 3 3 1])
false

; so we recur with:
=> (rest [1 2 3 3 1])
(2 3 3 1)
; and acc is incremented to 1.
=> (empty? '(2 3 3 1))
false
=> (rest '(2 3 3 1))
(3 3 1)
; acc = 2
=> (empty? '(3 3 1))
false
=> (rest '(3 3 1))
(3 1)
; acc = 3
=> (empty? '(3 1))
false
=> (rest '(3 1))
(1)
; acc = 4
=> (empty? '(1))
false
=> (rest '(1))
()
; acc = 5
=> (empty? '())
true
; so instead of recurring,
; we return acc, which is 5

adereth's solution:

(fn [s]
  (reduce (fn [acc v] (inc acc)) 0 s))
  
chouser's solution:

reduce #(do %2 (+ 1 %)) 0

aengelberg's solution:

#(apply + (vals (frequencies %)))

chunchangshao's solution:

(fn my-count [x]
  (if (nil? x)
       0
      (inc (my-count (next x)))))

jafingerhut's solution:

#(reduce + (map (fn [x] 1) %))

balint's solution:

#(reduce (fn [acc _] (+ acc 1)) 0 %)

