;; 101 Levenshtein Distance [h]

Calculates minimum number of edits (insert/delete/replace)
needed to transform x into y.

(= (__ "kitten" "sitting") 3)
(= (__ "closure" "clojure") (__ "clojure" "closure") 1)
(= (__ "xyx" "xyyyx") 2)
(= (__ [1 2 3 4] [0 2 3 4 5]) 2)
(= (__ '(:a :b :c :d) '(:a :d)) 2)
(= (__ "ttttattttctg" "tcaaccctaccat") 10)
(= (__ "gaattctaatctc" "caaacaaaaaattt") 9)

(fn lev [[h & t :as a] [f & r :as b]]
    (cond (nil? h) (count b)
          (nil? f) (count a)
          (= f h) (recur t r)
          :else (min (inc (lev t r))
                     (inc (lev a r))
                     (inc (lev t b)))))

(memoize 
  (fn lev [s1 s2]
    (cond
      (zero? (count s1)) (count s2)
      (zero? (count s2)) (count s1)
      (= (first s1) (first s2)) (lev (rest s1) (rest s2))
      :else (inc (min (lev (rest s1) s2)
                      (lev s1 (rest s2))
                      (lev (rest s1) (rest s2)))))))
adereth's solution:

(fn [a b]
  ((reduce (fn [acc [i j]]
                      (assoc acc [i j]
                             (if (zero? (min i j))
                               (max i j)
                               (min (inc (acc [(dec i) j]))
                                    (inc (acc [i (dec j)]))
                                    (+ (acc [(dec i) (dec j)])
                                       (if (= (nth a (dec i))
                                              (nth b (dec j)))
                                         0 1))))))
                    {} (sort-by #(apply + %)
                       (for [i (range (inc (count a)))
                             j (range (inc (count b)))]
                         [i j])))
   [(count a)
          (count b)])
)

mfikes's solution:

(fn levenshtein-distance [s t]
  (let [f (fn [f s t]
            (cond
              (empty? s) (count t)
              (empty? t) (count s)
              :else (let [cost (if (= (first s) (first t)) 0 1)]
                      (min (inc (f f (rest s) t))
                           (inc (f f s (rest t)))
                           (+ cost (f f (rest s) (rest t)))))))
        g (memoize f)]
    (g g s t)))

chouser's solution:

(fn [& z]
  (apply (fn f [[a & x :as j] [b & y :as k]]
           (if j
             (if (= a b)
               (f x y)
               (+ 1
                  (if (= (count x) (count y))
                    (f x y)
                    (min (f x y) (f j y)))))
             (count k)))
         (sort-by count z)))
     
aengelberg's solution:

#({\k 3 \c 1 \x 2 1 2 :a 2 \t 10 \g 9} (first %)
  ({"123456" 6} %2 0))

chunchangshao's solution:

#(if (= % %2)
   0
   (case %
     "kitten" 3
     ("clojure" "closure") 1
     "" (count %2)
     "ttttattttctg" 10
     "gaattctaatctc" 9
     2))
 
hypirion's solution:

(let [mem (atom {})]
  (fn leven [[fa & ra :as a] [fb & rb :as b]]
    (or 
      (@mem [a b])
      (let [res 
        (cond (nil? a) (count b)
              (nil? b) (count a)
              (= fa fb) (leven ra rb)
              :else (+ 1 
                      (min (leven ra rb)
                           (leven a rb)
                           (leven ra b))))]
        (swap! mem assoc [a b] res)
        res))))
    
jafingerhut's solution:

;; The Wikipedia page gives an O(m*n) dynamic programming algorithm
;; for this problem.  This is just a straightforward implementation of
;; that algorithm.
 
(fn [s1 s2]
  (let [s1 (vec s1) n1 (count s1)
        s2 (vec s2) n2 (count s2)
        dists (reduce (fn [prev-row j]
                        (reduce (fn [next-row i]
                                  (conj next-row
                                        (min (inc (next-row (dec i)))
                                             (inc (prev-row i))
                                             (+ (prev-row (dec i))
                                                (if (not= (s1 (dec i)) (s2 (dec j)))
                                                  1 0)))))
                                [j]
                                (range 1 (inc n1))))
                      (vec (range 0 (inc n1)))
                      (range 1 (inc n2)))]
    (dists n1)))

balint's solution:

(fn lev [[ha & ra :as a] [hb & rb :as b]]
  (cond
    (> (count a) (count b)) (lev b a)
    (nil? ha) (count b)
    (= ha hb) (lev ra rb)
    :else
      (inc
        (min (lev ra rb) (lev a rb)))))
    
amcnamara's solution:

#((fn f [r [a & b :as c] [x & y :as z]]
    (if (nil? a)
      (+ r (% z))
      (if (= a x)
        (f r b y)
        (apply f (+ 1 r)
          (if (or (= (%2 b) (%2 y)) (= (% c) (% z)))
            [b y]
            (if (> (% c) (% z))
              [b z]
              [c y]))))))
  0 %3 %4)
count
first

amalloy's solution:

(letfn [(iters [n f start]
          (take n (map second
                       (iterate f start))))]
 (fn [s t]
   (let [m (inc (count s)), n (inc (count t))
         first-row (vec (range m))
         matrix (iters n (fn [[j row]]
                           [(inc j)
                            (vec (iters m (fn [[i col]]
                                            [(inc i)
                                             (if (= (nth s i)
                                                    (nth t j))
                                               (get row i)
                                               (inc (min (get row i)
                                                         (get row (inc i))
                                                         col)))])
                                        [0 (inc j)]))])
                       [0 first-row])]
     (last (last matrix)))))
 
dbyrne's solution:

(fn [x y] 
  (with-local-vars 
    [e (memoize 
         (fn [x y]
           (cond
             (empty? x) (count y)
             (empty? y) (count x)
             :else (min
                       (+ (if (= (first x) (first y)) 0 1)
                                (e (rest x) (rest y)))
                            (inc (e (rest x) y))
                            (inc (e x (rest y)))))))]
      (e x y)))