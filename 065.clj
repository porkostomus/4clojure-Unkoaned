{:_id 65, :title "Black Box Testing"
:description "Clojure has many collection types, which act in subtly different ways.
The core functions typically convert them into a uniform \"sequence\" type and work with them that way,
but it can be important to understand the behavioral and performance differences
so that you know which kind is appropriate for your application.<br />
<br />Write a function which takes a collection and returns one of :map, :set, :list, or :vector - 
describing the type of collection it was given.<br />
You won't be allowed to inspect their class or use the built-in predicates like list? - 
the point is to poke at them and understand their behavior.", :tags ["hard" "seqs" "testing"]
:tests [
"(= :map (__ {:a 1, :b 2}))"
"(= :list (__ (range (rand-int 20))))"
"(= :vector (__ [1 2 3 4 5 6]))"
"(= :set (__ #{10 (rand-int 5)}))"
"(= [:map :set :vector :list] (map __ [{} #{} [] ()]))"]
:restricted ["class" "type" "Class" "vector?" "sequential?" "list?" "seq?" "map?" "set?" "instance?" "getClass"]}

The seq types act in subtly different ways.
The core functions typically convert them into
a uniform "sequence" type and work with them that way,
but it can be important to understand
the behavioral and performance differences
so you know which kind is appropriate for your application.

This func takes a coll and returns one of
:map, :set, :list, or :vector -
describing the type of coll it was given.
It does not inspect their class
or use the built-in predicates like list? -
it pokes at them to understand their behavior.
Works like class, type, Class, vector?
sequential? list? seq? map? set? instance? getClass

(= :map (__ {:a 1, :b 2}))
(= :list (__ (range (rand-int 20))))
(= :vector (__ [1 2 3 4 5 6]))
(= :set (__ #{10 (rand-int 5)}))
(= [:map :set :vector :list] (map __ [{} #{} [] ()]))

#((zipmap (map str [{} #{} () []]) [:map :set :list :vector]) (str (empty %)))

#(condp = (nth (str %) 0)
   \{ :map
   \c :list
   \[ :vector
   \# :set)
   
(comp {\# :set \{ :map \[ :vector \c :list} first str)

(fn [c] (let [d (conj c {0 1} {0 2} {0 1})] (cond (= 
(+ 1 (count c)) (count d)) :map (= (+ 2 (count c)) (count d)) :set (= (cons 
{0 3} d) (conj d {0 3})) :list true :vector)))

adereth's solution:

#(condp = (nth (str %) 0)
   \{ :map
   \c :list
   \[ :vector
   \# :set)

mfikes's solution:

(fn [coll]
  (cond
   (= :myval (get (conj coll [:mykey :myval]) :mykey)) :map
   (= :myval (get (conj coll :myval) :myval)) :set
   (= :myval (first (conj coll :abc :myval))) :list
   :else :vector))

chouser's solution:

#((zipmap (map str [{} #{} () []]) [:map :set :list :vector]) (str (empty %)))

aengelberg's solution:

#(if (= % (vec %))
     (if (= (concat % [1 2]) (conj % 1 2))
         :vector
         :list)
     (if ((conj % [3 7]) [3 7])
         :set
         :map))
     
chunchangshao's solution:

(fn [coll]
  (let [result (conj (empty coll) [1 2] [1 2] [1 3])]
    (if (= 1 (count result))
      :map (if (= 2 (count result))
             :set (if (= [1 3] (first result))
                    :list :vector )))))
                
hypirion's solution:

(fn [s]
  (let [a (gensym)
        b (gensym)
        r (conj s [b a] [a b])]
  (cond (contains? r a) :map
        (contains? r [a b]) :set
        (= [a b] (last r)) :vector
        :otherwise :list)))

jafingerhut's solution:

;; One big clue: The function empty is not forbidden in this problem.
 
(fn [c]
  (let [e (empty c)]
    (case e
      {} :map
      [] (if (reversible? c) :vector :list)
      #{} :set)))

balint's solution:

(fn [c]
  (let [d (conj c {0 1} {0 2} {0 1})]
  (cond
    (= (+ 1 (count c)) (count d)) :map
    (= (+ 2 (count c)) (count d)) :set
    (= (cons {0 3} d) (conj d {0 3})) :list
    true :vector)))

amcnamara's solution:

#(let [c conj
       f first
       i (c % [1 2])
       j (c i i)]
  (condp = i
    j           :map
    (f j)       :list
    (c i (f i)) :set
                :vector))
            
amalloy's solution:

(let [[a b c] (repeatedly gensym)
      e1 [a b]
      e2 [a c]]
  (fn [x]
    (let [y (conj x e1 e1 e2)
          diff (- (count y) (count x))]
      (condp = diff
        1 :map
        2 :set
        (if (= (first y) e2)
          :list
          :vector)))))
      
stuarth's solution:

(fn [c]
    (let [c* (conj c [:f 1])]
      (cond
       (contains? c* :f)
       :map
       (= (count c) (dec (count (conj c :foo :foo))))
       :set
       :else
       (if (= (first (conj c :foo :baz)) :baz)
         :list
         :vector))))