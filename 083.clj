{:_id 83 :title "A Half-Truth"
:tests [
"(= false (__ false false))"
"(= true (__ true false))"
"(= false (__ true))"
"(= true (__ false true false))"
"(= false (__ true true true))"
"(= true (__ true true true false))"]

:description 
"Write a function which takes a variable number of booleans.
Your function should return true if some of the parameters are true
but not all of the parameters are true.
Otherwise your function should return false."
:tags ["easy"]}
	
(not= false false) ;;=> false
(not= true false)) ;;=> true
(not= true) ;;=> false
(not= false true false) ;;=> true
(not= true true true) ;;=> false
(not= true true true false) ;;=> true

not=

adereth's solution:

(fn [& vs]
  (true? (and (some not vs)
         (some identity vs))))
     
mfikes's solution:

#(let [v %&] (not (not (and (some identity v) (not (apply = v))))))

chouser's solution:

#(= 2 (count (set %&)))

chunchangshao's solution:

#(not-every? (fn [x] (= x %)) %&)

hypirion's solution:

(fn [& args]
  (->>
    (into #{} args)
    count
    (= 2)))

jafingerhut's solution:

(fn [& bools]
  (not (or (every? identity bools)
           (every? not bools))))
       
balint's solution:

(fn [& bs]
  (let [trues (count (filter true? bs))]
    (and (not (zero? trues))
      (< trues (count bs)))))
  
borkdude's solution:

#(= #{true false} (set %&))

stuarth's solution:

(fn [& bools]
    (true? (and (some true? bools) (not-every? true? bools))))