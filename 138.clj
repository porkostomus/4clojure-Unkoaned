Squares Squared
Difficulty:	Hard
Topics:	data-juggling

Create a function of two integer arguments: the start and end, respectively.
You must create a vector of strings which renders a 45° rotated square of integers
which are successive squares from the start point up to and including the end point.
If a number comprises multiple digits, wrap them around the shape individually.
If there are not enough digits to complete the shape, fill in the rest with asterisk characters.
The direction of the drawing should be clockwise, starting from the center of the shape and working outwards,
with the initial direction being down and to the right.

(= (__ 2 2) ["2"])

(= (__ 2 4) [" 2 "
             "* 4"
             " * "])

(= (__ 3 81) [" 3 "
              "1 9"
              " 8 "])

(= (__ 4 20) [" 4 "
              "* 1"
              " 6 "])

(= (__ 2 256) ["  6  "
               " 5 * "
               "2 2 *"
               " 6 4 "
               "  1  "])

(= (__ 10 10000) ["   0   "
                  "  1 0  "
                  " 0 1 0 "
                  "* 0 0 0"
                  " * 1 * "
                  "  * *  "
                  "   *   "])
                  
(fn [start end]
  (let [s (->> (iterate #(* % %) start) (take-while #(<= % end)) (mapcat str))
        d (int (Math/ceil (Math/sqrt (count s))))
        xs (concat s (repeat (- (* d d) (count s)) \*))
        cp [(if (even? d) (- d 2) (dec d)) (dec d)]
        dr (mapcat #(repeat (* % 2) (if (even? %) -1 1)) (range))
        dc (mapcat #(repeat (inc (* % 2)) (if (even? %) 1 -1)) (range))
        dp (map #(vector %1 %2) dr dc)
        pp (reductions #(vector (+ (first %1) (first %2)) (+ (last %1) (last %2))) cp dp)
        len (dec (* 2 d))
        eb (vec (repeat len (vec (repeat len \ ))))
        cal (fn [b xs pp]
              (if xs
                (recur (assoc-in b (first pp) (first xs)) (next xs) (next pp))
                b))
        ]
    (mapv #(apply str %) (cal eb xs pp))))

(fn make-square [start end]
  (let [square-range (fn square-range [n end] (if (> n end) [] (cons n (square-range (* n n) end))))
        pad-asterisks (fn [n] (repeat (-> n Math/sqrt Math/ceil (Math/pow 2) (- n)) \*))
        rotate (fn [matrix] (reverse (apply (partial map vector) matrix)))
        numerals (mapcat str (square-range start end))
        padding (pad-asterisks (count numerals))
        chars (concat numerals padding)
        populate (fn populate [spiral chars iterations]
                          (if (empty? chars) (if (= 0 (mod iterations 4))
                                               spiral
                                               (populate (rotate spiral) chars (inc iterations)))
                            (let [rotated (rotate spiral)
                                  [head tail] (split-at (count (first rotated)) chars)]
                              (populate (conj rotated head) tail (inc iterations))
                              )
                            ))
        spiral (populate [[(first chars)]] (rest chars) 0)
        spiral-size (count spiral)
        left-padded (map-indexed (fn [i row] (concat (repeat i \space)
                                                     row
                                                     (repeat (- (dec spiral-size) i) \space)))
                                 spiral)
        diamond-shaped (map reverse (apply (partial map vector) left-padded))
        spaced (map #(interpose \space %) diamond-shaped)
        unskewed (map-indexed (fn [i row]
                                (let [n (- (dec spiral-size) i)
                                      m (- i (dec spiral-size))]
                                  (take (count row)
                                  (concat (repeat m \space) (drop n row) (repeat \space)))))
                              spaced)
        ]
     (map clojure.string/join unskewed)))
 
adereth's solution:

(fn [s e]
  (let [q #(* % %)
        v (mapcat #(loop [n % r '()]
                     (cond (< 0 n) (recur (quot n 10) (conj r (rem n 10))) (= '() r) '(0) 1 r))
                  (take-while #(<= % e) (iterate q s)))
        g (-> v count Math/sqrt Math/ceil int)
        l (q g)
        v (concat v (repeat (- l (count v)) \*))
        r (fn [v n] (mapcat #(repeat n %) v))
        d (take l (mapcat #(repeat %2 %)
                          (cycle [[1 1] [1 -1] [-1 -1] [-1 1]])
                          (drop 2 (interleave (range) (range)))))
        w (- (* 2 g) 1)
        a (vec (repeat w (vec (repeat w \ ))))
        p [(- g (if (even? g) 2 1))
           (quot w 2)]
        ps (reductions (fn [p t] (vec (map + p t))) p d)
        ]
    (map #(apply str %) (reduce (fn [acc [v p]]
                       (assoc-in acc p v))
                     a (map vector v ps))))
  )

mfikes's solution:

(fn [start end]
  (letfn [(digits [n]
                  (if (< n 10)
                    [n]
                    (conj (digits (quot n 10)) (rem n 10))))
 
          (squares [start end]
                   (lazy-seq
                     (cons start
                           (let [start-squared (* start start)]
                             (if (<= start-squared end)
                               (squares start-squared end))))))
 
          (digit-sequence [start end]
                          (mapcat digits (squares start end)))
 
          (next-perfect-square [n]
                               (first (drop-while #(< % n) (map #(* % %) (iterate inc 1)))))
 
          (char-sequence [digit-sequence]
                         (take (next-perfect-square (count digit-sequence))
                               (concat (map (zipmap (range) "0123456789") digit-sequence)
                                       (repeat \*))))
 
          (flip [matrix]
                (reverse (map reverse matrix)))
 
          (wrap [char-sequence matrix]
                (if-not (empty? char-sequence)
                  (let [matrix (flip matrix)]
                    (let [new-edge-length (inc (count matrix))
                          first-row (take new-edge-length char-sequence)
                          char-sequence (drop new-edge-length char-sequence)
                          remaining-rows (map (fn [old-row digit]
                                                (concat old-row (list digit)))
                                              matrix char-sequence)
                          char-sequence (drop (count remaining-rows) char-sequence)]
                      (flip (wrap char-sequence (cons first-row remaining-rows)))))
                  (flip matrix)))
 
          (rotate45 [matrix]
                    (let [matrix (mapv vec matrix)
                          dim (dec (* 2 (count matrix)))]
                      (for [r (range dim)]
                        (apply str (for [c (range dim)]
                                     (or (get-in matrix [(/ (- (+ r c) (- (count matrix) 1)) 2) (/ (- (+ (- (count matrix) 1) c) r) 2)])
                                         \space))))))]
    (rotate45 (wrap (char-sequence (digit-sequence start end)) []))))

aengelberg's solution:

(let [d [[0 1] [1 0] [0 -1] [-1 0]]
      e (for [i (range)]
          (quot (+ i 2) 2))
      F (for [[d a] (map vector (cycle d) e)
              i (range a)]
          d)]
  (letfn [(P
            [[a b] [c d]]
            [(+ a c) (+ b d)])
          (S
            [v]
            (loop [p [0 0]
                   m {}
                   [v & V] v
                   [d & D] F]
              (if v
                (recur (P p d)
                  (assoc m p v)
                  V
                  D)
                m)))
          (G
            [m d]
            (let [k (keys m)
                  x (map first k)
                  y (map second k)
                  X (apply max x)
                  x (apply min x)
                  Y (apply max y)
                  y (apply min y)
                  N (+ 1 (max (- X x) (- Y y)))]
              (reduce (fn [g [[a b] v]]
                        (assoc-in g [(- a x) (- b y)] v))
                (vec (repeat N
                       (vec (repeat N
                              d))))
                m)))
          (D
            [g]
            (into {} (for [i (range (count g))
                           j (range (count (g 0)))]
                       [[(+ i j) (- j i)] ((g i) j)])))
          (Q
            [n N]
            (->> n
              (iterate #(* % %))
              (take-while #(<= % N))
              (apply str)))]
    (fn [n N]
      (-> (->> n
            (iterate #(* % %))
            (take-while #(<= % N))
            (apply str))
        S
        (G \*)
        D
        (G \ )
        (->> (mapv (partial apply str)))))))
 
hypirion's solution:

(fn [n m]
  (let [low #(apply min (map % %2))
        digs (->> (iterate #(* % %) n)
                  (take-while #(<= % m))
                  (mapcat str))
        [sqr a-n] (some (fn [[a2 :as as]]
                          (if (>= a2 (count digs)) as))
                        (map (fn [a] [(* a a) a]) (range)))
        a-n (- (* 2 a-n) 1)
        blanks (vec (repeat a-n (vec (repeat a-n \space))))]
  (let [xs (->> (cycle [[[-1 -1] [-1 1]][[1 1] [1 -1]]])
                (mapcat (fn [n dxs] (mapcat #(repeat n %) dxs)) (range))
                (cons [0 0])
                (take sqr)
                (reductions (partial map +)))]
    (->> xs
         (map (fn [[a b]] [(- a (low first xs))
                           (- b (low second xs))]))
         (map (fn [a b] [b a]) (concat digs (repeat \*)))
         (reduce #(apply assoc-in %1 %2) blanks)
         (map (partial apply str))))))
     
jafingerhut's solution:

;; Create the sequence of numbers, convert them to a sequence of
;; characters, and pad it with * characters if needed to make the
;; total length a square number.
 
;; Create the spiral pattern in a rectangular grid where the first
;; digit starts at coordinates (0,0), then goes right-down to (1,-1),
;; then left-down to (0,-2), left-up to (-1,-1), etc.  Use a map that
;; maps coordinates (represented as two-element vectors) to the
;; character at that position, so that it is easy to grow the size of
;; it as needed on demand.
 
;; Then convert it to the vector of strings needed for the correct
;; return value.
 
(fn [start end]
  (let [numbers (take-while #(<= % end) (iterate #(*' % %) start))
        to-digits (fn [n] (map #(- (int %) (int \0)) (str n)))
        digits (mapcat to-digits numbers)
        n (count digits)
        next-square-number (first (filter #(>= % n)
                                          (map #(*' % %) (range))))
        digits (concat digits (repeat (- next-square-number n) \*))
        numsteps (interleave (drop 1 (range)) (drop 1 (range)))
        dirs (cycle [[1 -1] [-1 -1] [-1 1] [1 1]])
        deltas (mapcat #(repeat % %2) numsteps dirs)
        positions (take next-square-number
                        (reductions (fn [[x y] [dx dy]] [(+ x dx) (+ y dy)])
                                    [0 0] deltas))
        char-positions (into {} (map (fn [p d] [p d]) positions digits))
        x-coords (map first positions)
        min-x (apply min x-coords)
        max-x (apply max x-coords)
        y-coords (map second positions)
        min-y (apply min y-coords)
        max-y (apply max y-coords)]
    (for [y (range max-y (dec min-y) -1)]
      (apply str
             (for [x (range min-x (inc max-x))]
               (get char-positions [x y] " "))))))
           
balint's solution:

(fn [p q]
  (let [dirs (mapcat
               #(repeat %2 %) (cycle [[1 1] [1 -1] [-1 -1] [-1 1]])
               (cons 1 (mapcat #(repeat % %) (iterate inc 1))))
        digits (apply str (take-while #(<= % q) (iterate #(* % %) p)))
        [max-digits sq-size]
                   (first
                     (drop-while
                       (fn [[d s]] (> (count digits) d))
                       (map #(vector %1 %2)
                            (map #(* % %) (iterate inc 2))
                            (iterate #(+ % 2) 3))))
        spiral ((fn [digits dirs]
                 (let [digits-in-center (inc (quot sq-size 2))
                       center-piece (if (zero? (rem digits-in-center 2))
                                      (dec (quot sq-size 2))
                                      (quot sq-size 2))
                       first-pos [center-piece (quot sq-size 2)]]
                   (loop [[fdig & ndig] (next digits),
                          [fd & nd] dirs,
                          pos first-pos,
                          sp {first-pos (first digits)}]
                     (if (nil? fdig)
                       sp
                       (recur ndig nd (map + pos fd) (assoc sp (map + pos fd) fdig))
                      ))))
                  (concat digits (repeat (- max-digits (count digits)) "*")) dirs)]
    (if (= 1 (count digits))
      [digits]
      (into []
        (for [y (range 0 sq-size)]
          (reduce str
             (for [x (range 0 sq-size)]
               (get spiral [y x] \space))))))))
           
amcnamara's solution:

(fn [s e]
  (let [squares   (apply str (take-while #(<= % e) (iterate #(* % %) s)))
        filler    (drop (count squares) (repeat (#(* % %) (Math/ceil (Math/sqrt (count squares)))) \*))
        elements  (concat squares filler)
        dimension (int (Math/sqrt (count elements)))
        cardinal  (- (* 2 dimension) 1)
        canvas    (vec (repeat cardinal ""))
        start-pos (* 2 (quot (dec dimension) 2))
        path      (take (count elements) (mapcat #(repeat (+ 2 (* 2 %)) (if (odd? %) dec inc)) (range)))
        balance   (fn [c] (#(apply str `(~@% ~@(interpose \  c) ~@%)) (repeat (/ (- cardinal (- (* 2 (count c)) 1)) 2) \ )))]
    (map balance
      ((fn [canvas row [element & rest-elts] [move & rest-path]]
         (if (nil? element)
           canvas
           (recur (update-in canvas [row] #(if (= move inc) `(~@% ~element) `(~element ~@%)))
                  (move row)
                  rest-elts
                  rest-path)))
       canvas
       start-pos
       elements
       path))))