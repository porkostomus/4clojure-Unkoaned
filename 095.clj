;; 95

Check whether or not a given sequence represents a binary tree.
Each node in the tree must have a value,
a left child, and a right child.

(fn tree? [coll]
  (or (nil? coll)
      (and (sequential? coll) 
           (= 3 (count coll))
           (tree? (second coll))
           (tree? (nth coll 2)))))

((fn t? [[v l r :as t]]
   (and (= (count t) 3)
        (every? #(or (nil? %) (and (coll? %) (t? %))) [l r])))
 '(:a (:b nil nil) nil))
;;=> true

((fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r]))) '(:a (:b nil nil))) ;;=> false
((fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r]))) [1 nil [2 [3 nil nil] [4 nil nil]]])
;;=> true

((fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r]))) [1 [2 nil nil] [3 nil nil] [4 nil nil]])
;;=> false

((fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r]))) [1 [2 [3 [4 nil nil] nil] nil] nil])
;;=> true

((fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r]))) [1 [2 [3 [4 false nil] nil] nil] nil])
;;=> false

((fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r]))) '(:a nil ()))
;;=> false

(fn t? [[v l r :as t]] (and (= (count t) 3) (every? #(or (nil? %) (and (coll? %) (t? %))) [l r])))

adereth's solution:

(fn [root] (every? #(or (nil? %) (and (sequential? %) (= 3 (count %))))
                   (tree-seq #(and (sequential? %)
                                   (= (count %) 3))
                             rest root)))
                         
mfikes's solution:

(fn tree? [coll]
  (or (nil? coll)
      (and (sequential? coll) 
           (= 3 (count coll))
           (tree? (second coll))
           (tree? (nth coll 2)))))
       
chouser's solution:

(fn f [t] (if (coll? t) 
(let [[_ a b] t]
(and (= 3 (count t)) (f a) (f b)))
(nil? t)))

aengelberg's solution:

(fn f [l]
  (or (nil? l)
      (and (coll? l)
           (= (count l) 3)
           (every? f (rest l)))))
       
chunchangshao's solution:

(fn t [xs] (if (sequential? xs) (if (= 3 (count xs)) (and (t (second xs)) (t (last xs))) false) (if (nil? xs) true false)))

hypirion's solution:

(fn binary-tree? [n]
  (cond (nil? n) true
        (or (not (coll? n))
            (not= 3 (count n))) false
        :else (and (binary-tree? (nth n 1))
                   (binary-tree? (nth n 2)))))

jafingerhut's solution:

(fn t [[x l r :as a]]
  (and (= (count a) 3)
       (or (nil? l) (and (coll? l) (t l)))
       (or (nil? r) (and (coll? r) (t r)))))

balint's solution:

(fn t? [[v l r :as t]]
  (and
    (= (count t) 3)
    (every?
      #(or (nil? %)
         (and (coll? %) (t? %)))
      [l r])))

amcnamara's solution:

#(odd? (count (filter % (flatten %2))))
#(not (false? %))

amalloy's solution:

(fn t [n]
  (or (nil? n)
      (and (coll? n)
           (= 3 (count n))
           (every? t (rest n)))))

stuarth's solution:

(fn t? [t]
    (if (coll? t)
      (and (= (count t) 3) (every? #(t? %) (rest t)))
      (nil? t)))
  
dbyrne's solution:

(fn t [x]
  (if (sequential? x)
    (if (= (count x) 3)
      (and (t (second x))
           (t (last x)))
      false)
    (nil? x)))