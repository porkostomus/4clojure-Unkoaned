{:_id 41 :title "Drop Every Nth Item"
:tests ["(= (__ [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8])"
"(= (__ [:a :b :c :d :e :f] 2) [:a :c :e])"
"(= (__ [1 2 3 4 5 6] 4) [1 2 3 5 6])"]
:description "Write a function which drops every Nth item from a sequence."
:tags ["easy" "seqs"]}

user> (defn drop-every-nth [s n]
  (apply concat (partition-all (dec n) n s)))
#'user/drop-every-nth
user> (drop-every-nth [1 2 3 4 5 6 7 8] 3)
(1 2 4 5 7 8)

[:a :b :c :d :e :f] 2
;;=> (:a :c :e)
[1 2 3 4 5 6] 4
;;=> (1 2 3 5 6)

#(apply concat (partition-all (dec %2) %2 %))

adereth's solution:

(fn [coll n]
  (->> (partition-all n coll)
       (map (partial take (dec n)))
       (flatten)))

mfikes's solution:

(fn drop-every-n [coll n]
  (if (empty? coll)
    coll
    (concat (take (dec n) coll) (drop-every-n (drop n coll) n))))

chouser's solution:

(fn [v n] (keep-indexed #(when (pos? (mod (inc %) n)) %2) v))

aengelberg's solution:

(fn [l n]
  (map #(nth l % nil)(remove #(= 0 (mod (inc %) n))(range (count l)))))

chunchangshao's solution:

(fn [v nm] (keep-indexed #(if (not= (- nm 1) (mod % nm)) %2) v))

hypirion's solution:

#(->>
  (partition-all (dec %2) %2 %1)
  (apply concat))

jafingerhut's solution:

(fn [coll n]
  (->> (map-indexed (fn [idx item]
                      [(mod (inc idx) n) item])
                    coll)
       (remove (fn [[idx item]] (zero? idx)))
       (map second)))

balint's solution:

#(remove nil? (map-indexed (fn [i x] (when (not= (mod (inc i) %2) 0) x)) %1))

amcnamara's solution:

(fn [c n]
  (mapcat #(take (dec n) %) (partition-all n c)))

amalloy's solution:

(fn [coll n]
  (keep-indexed #(when (not= (dec n) (mod % n)) %2)
                coll))

stuarth's solution:

(fn [s n]
    (for [i (range (count s)) :when (pos? (mod (inc i) n))] (get s i)))

dbyrne's solution:

#(apply concat (partition-all (dec %2) %2 %))