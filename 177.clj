Balancing Brackets
Difficulty:	Medium
Topics:	parsing

When parsing a snippet of code it's often a good idea to do a sanity check to see if all the brackets match up.
Write a function that takes in a string and returns truthy if all square [ ] round ( ) and curly { } brackets
are properly paired and legally nested, or returns falsey otherwise.

(__ "This string has no brackets.")

(__ "class Test {
      public static void main(String[] args) {
        System.out.println(\"Hello world.\");
      }
    }")

(not (__ "(start, end]"))

(not (__ "())"))

(not (__ "[ { ] } "))

(__ "([]([(()){()}(()(()))(([[]]({}()))())]((((()()))))))")

(not (__ "([]([(()){()}(()(()))(([[]]({}([)))())]((((()()))))))"))

(not (__ "["))

(fn balanced? [s]
  (let [p {\( \) \[ \] \{ \}}
        a (set "()[]{}")]
    (empty?
      (reduce (fn [[t & b :as stack] s]
                (cond (= (p t) s) b
                      (a s) (conj stack s)
                      :else stack))
              () s))))

(fn balanced? [s]
  (let [p {\( \) \[ \] \{ \}}
        a (set "()[]{}")]
    (empty?
      (reduce (fn [[t & b :as stack] s]
                (cond (= (p t) s) b
                      (a s) (conj stack s)
                      :else stack))
              () s))))
adereth's solution:

1
2
3
4
5
6
7
8
9
10
#(= '()
  (reduce (fn [a c]
            (let [m {\) \( \] \[ \} \{}]
            (cond
             (= a :F) a
             (#{\[ \( \{} c) (cons c a)
             (= (m c) (first a)) (rest a)
             (m c) :F
             :e a
             ))) [] %))
mfikes's solution:

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
(fn balanced-brackts? [s]
  (let [chars (filter (set "[{()}]") s)
        count-them (fn [[c & r] [t & b :as stack]]
                     (println stack)
                     (case c
                       \] (if (not= t \[)
                            false
                            (recur r b))
                       \} (if (not= t \{)
                            false
                            (recur r b))
                       \) (if (not= t \()
                            false
                            (recur r b))
                       \[ (recur r (cons \[ stack))
                       \{ (recur r (cons \{ stack))
                       \( (recur r (cons \( stack))
                       nil (empty? stack)))]
    (count-them chars ())))
chouser's solution:

1
2
3
4
5
6
7
8
9
10
11
#(not
   (reduce
     (fn [[a & m :as p] c]
       (if-let [r ({\[ \] \{ \} \( \)} c)]
         (cons r p)
         (cond
           (= c a) m
           (#{\] \} \)} c) %
           1 p)))
     nil
     %))
aengelberg's solution:

1
2
3
4
5
6
7
8
9
10
11
12
#(loop [s %
        l []]
   (if (empty? s) (empty? l)
   (let [c (first s)]
     (if ((set "([{") c)
         (recur (rest s)
                (conj l ((into {} (map vector "([{" ")]}"))
                         c)))
         (if ((set ")]}") c)
             (when (= c (peek l))
               (recur (rest s) (pop l)))
             (recur (rest s) l))))))
chunchangshao's solution:

1
2
3
4
(fn [str]
  (if (= str "[ { ] } ")
    false
    ((fn [str] (let [str (seq str)] (letfn [(f [s] (count (filter #(= s %) str))) (check [fs ts] (not= (f fs) (f ts)))] (nil? (some #(apply check %) [[\( \)] [\{ \}] [\[ \]]]))))) str)))
hypirion's solution:

1
2
3
4
5
6
7
8
9
10
11
(let [op {\] \[, \) \(, \} \{}]
  (fn [s]
    (loop [[f & r] s,
           stack ()]
      (condp deliver f
        {nil 0} (empty? stack)
        #{\{ \[ \(} (recur r (conj stack f))
        #{\} \] \)} (let [[a & b] stack]
                      (if (= a (op f))
                        (recur r b)))
       (recur r stack)))))
jafingerhut's solution:

1
2
3
4
5
6
7
8
9
10
11
12
(fn [s]
  (= '()
     (reduce (fn [exp-stack ch]
               (case ch
                 \[ (conj exp-stack \])
                 \( (conj exp-stack \))
                 \{ (conj exp-stack \})
                 (\] \) \}) (if (= ch (first exp-stack))
                              (rest exp-stack))
                 exp-stack))
             '()
             (seq s))))
amcnamara's solution:

1
2
3
4
5
6
7
#(let [a (clojure.string/replace % #"[^\(^\)^\[^\]^\{^\}]" "")
       b (clojure.string/replace a #"(\(\)|\[\]|\{\})" "")]
   (if (empty? b)
     true
     (if (= b %)
       false
       (recur b))))
amalloy's solution:

1
2
3
4
5
6
7
8
9
10
11
(let [closers {\[ \] \{ \} \( \)}
      openers {\] \[ \} \{ \) \(}]
  (fn balanced? [s]
    (loop [stack [], s s]
      (cond (empty? s) (empty? stack)
            (openers (first s)) (and (seq stack)
                                     (= (openers (first s))
                                        (peek stack))
                                     (recur (pop stack) (rest s)))
            (closers (first s)) (recur (conj stack (first s)) (rest s))
            :else (recur stack (rest s))))))