;; 49 Split seq in 2
; Write a function which will split a sequence into two parts

(= (__ 3 [1 2 3 4 5 6]) [[1 2 3] [4 5 6]])
(= (__ 1 [:a :b :c :d]) [[:a] [:b :c :d]])
(= (__ 2 [[1 2] [3 4] [5 6]]) [[[1 2] [3 4]] [[5 6]]])


(fn [n coll] [(take n coll) (drop n coll)])

(juxt take drop)

user=> (source split-at)
(defn split-at
  "Returns vector of [(take n coll) (drop n coll)]"
  [n coll]
    [(take n coll) (drop n coll)])

(fn [n coll] [(take n coll) (drop n coll)])

adereth's solution:

(fn [n s] [(take n s) (drop n s)])

mfikes's solution:

#(vector (take %1 %2) (drop %1 %2))

chouser's solution:

(juxt take drop)

aengelberg's solution:

#(map (fn [i](i %1 %2)) [take drop])

chunchangshao's solution:

#(conj [] (take % %2) (drop % %2))

hypirion's solution:

(juxt take drop)

jafingerhut's solution:

(fn [n coll] [(take n coll) (drop n coll)])

balint's solution:

(fn [n s] [(take n s) (drop n s)])

borkdude's solution:

(fn [n coll] [(take n coll) (drop n coll)])

amcnamara's solution:

(fn [n c] [(take n c) (drop n c)])

stuarth's solution:

1
(fn [n s] [(take n s) (drop n s)])
