;; 92 Read Roman numerals [h]

Roman numerals are easy to recognize,
but not everyone knows all the rules necessary
to work with them. This will parse a Roman-numeral string
and return the number it represents. 

Input will be well-formed, in upper-case,
and follow the subtractive principle.
You don't need to handle any numbers greater than
MMMCMXCIX (3999), the largest number
representable with ordinary letters.

(= 14 (__ "XIV"))
(= 827 (__ "DCCCXXVII"))
(= 3999 (__ "MMMCMXCIX"))
(= 48 (__ "XLVIII"))

#(->> (map {\C 100 \D 500 \I 1 \L 50 \M 1000 \V 5 \X 10} %)
      (partition 2 1 [0])
      (map (fn [[a b]] (if (< a b) (- a) a)))
      (apply +))

(fn read-roman [s]
  (let [numerals {\M 1000 \D 500 \C 100 \L 50 \X 10 \V 5 \I 1}
        nums (partition 2 1 (concat (map numerals s) [0]))]
    (reduce (fn [sum [a b]] ((if (< a b) - +) sum a)) 0 nums)))

adereth's solution:

(fn [s]
  (->> s
       reverse
       (replace (zipmap "MDCLXVI" [1000 500 100 50 10 5 1]))
       (partition-by identity)
       (map (partial apply +))
       (reduce #((if (< %1 %2) + -) %1 %2))))
   
mfikes's solution:

(fn unroman [s]
  (let [roman (fn [n]
                (let [build (fn build [n]
                              (if (>= n 1000)
                                (conj (build (rem n 1000)) (apply str (repeat (quot n 1000) "M")))
                                (if (>= n 100)
                                  (let [h (quot n 100)
                                        r (build (rem n 100))]
                                    (case h
                                      9 (conj r "CM")
                                      8 (conj r "DCCC")
                                      7 (conj r "DCC")
                                      6 (conj r "DC")
                                      5 (conj r "D")
                                      4 (conj r "CD")
                                      3 (conj r "CCC")
                                      2 (conj r "CC")
                                      1 (conj r "C")))
                                  (if (>= n 10)
                                    (let [t (quot n 10)
                                          r (build (rem n 10))]
                                      (case t
                                        9 (conj r "XC")
                                        8 (conj r "LXXX")
                                        7 (conj r "LXX")
                                        6 (conj r "LX")
                                        5 (conj r "L")
                                        4 (conj r "XL")
                                        3 (conj r "XXX")
                                        2 (conj r "XX")
                                        1 (conj r "X")))
                                    (seq (case n
                                           9 "IX"
                                           8 "VIII"
                                           7 "VII"
                                           6 "VI"
                                           5 "V"
                                           4 "IV"
                                           3 "III"
                                           2 "II"
                                           1 "I"
                                           0 []))))))]
                  (apply str (build n))))]
    ((zipmap (map roman (range 1 4000)) (range 1 4000)) s)))

chouser's solution:

(fn [r]
  (->>
    (reverse r)
    (map {\M 1000 \D 500 \C 100 \L 50 \X 10 \V 5 \I 1})
    (cons 0)
    (partition 2 1)
    (reduce
      (fn [t [b a]]
        (+ t (if (< a b) (- a) a))) 0)))
    
aengelberg's solution:

{"XIV" 14
"DCCCXXVII" 827
"MMMCMXCIX" 3999
"XLVIII" 48}

chunchangshao's solution:

1
#(case % "XIV" 14 "DCCCXXVII" 827 "XLVIII" 48 3999)

hypirion's solution:

(let [loch {\I 1, \V 5, \X 10, \L 50, \C 100, \D 500, \M 1000}
             rplace [["IX"  "VIV"]["XC"  "LXL"]["CM"  "DCD"]
                     ["XL" "XXXX"]["IV" "IIII"]["CD" "CCCC"]]]
  (fn [n]
   (->>
     (reduce 
       (fn [a [o r]] (.replaceAll a o r))
       n rplace)
     (map loch)
     (reduce +))))
 
jafingerhut's solution:

;; Since the problem says we can assume the input is well-formed and
;; follows the subtractive principle, we don't need to verify that.
 
;; Convert the string of letters into a sequence of their numeric values.
 
;; Then for each pair of values [a b], treat the first as value -a if
;; a < b, otherwise it is worth a.  Append a 0 to the end as a method
;; to ensure that (partition 2 1 sequence) will include the final
;; digit value as the first in a pair, and such that a > b.  Add up
;; the values.
 
(fn [s]
  (apply +
         (map (fn [[a b]] (if (< a b) (- a) a))
              (partition 2 1
                         (-> (map #({\I 1 \V 5 \X 10 \L 50 \C 100 \D 500 \M 1000} %) s)
                             vec
                             (conj 0))))))
                         
balint's solution:

(fn [rom]
  (let [abc [["CM" 900] ["CD" 400], ["XC" 90], ["XL" 40], ["IX" 9], ["IV" 4],
             ["M" 1000], ["D" 500], ["C" 100], ["L" 50], ["X" 10], ["V" 5], ["I" 1]]
        starts-with? (fn [s t]
            (and
              (>= (count s) (count t))
              (= (subs s 0 (count t)) t)))]
    (loop [rom rom, a 0]
      (if (empty? rom)
        a
        (let [[r v] (some
                      (fn [[rl _ :as rp]]
                       (and
                         (starts-with? rom rl)
                         rp))
                      abc)]
          (recur (apply str (drop (count r) rom)) (+ a v)))))))
      
amcnamara's solution:

(fn [n]
  (let [f first
        d count
        v [["M" 1000]
           ["CM" 900]
           ["D"  500]
           ["C"  100]
           ["XC"  90]
           ["XL"  40]
           ["X"   10]
           ["IX"   9]
           ["V"    5]
           ["IV"   4]
           ["I"    1]]]
    (loop [r 0 c n]
      (if (empty? c)
        r
        (let [[k v] (f (filter #(if (= (seq (f %)) (take (d (f %)) c)) %) v))]
          (recur (+ r v) (drop (d k) c)))))))
      
amalloy's solution:

(fn [s]
  (reduce (fn [sum [a b]]
            ((if (< a b) - +) sum a))
          0
          (partition 2 1 (concat (map {\I 1
                                       \V 5
                                       \X 10
                                       \L 50
                                       \C 100
                                       \D 500
                                       \M 1000} s)
                                 [0]))))
                             
dbyrne's solution:

(fn [x]
  (let [y {\I 1 \V 5 \X 10 \L 50
           \C 100 \D 500 \M 1000}]
    (apply +
     (reduce
      #(let [z (y %2)]
        (conj %1 (if (>= z (last %1))
        z
        (- z))))
      [0]
      (reverse x)))))