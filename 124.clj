Analyze Reversi
Difficulty:	Hard
Topics:	game

Reversi is normally played on an 8 by 8 board. In this problem, a 4 by 4 board is represented as a two-dimensional vector with black, white, and empty pieces represented by 'b, 'w, and 'e, respectively. Create a function that accepts a game board and color as arguments, and returns a map of legal moves for that color. Each key should be the coordinates of a legal move, and its value a set of the coordinates of the pieces flipped by that move.

Board coordinates should be as in calls to get-in. For example, [0 1] is the topmost row, second column from the left.

(= {[1 3] #{[1 2]}, [0 2] #{[1 2]}, [3 1] #{[2 1]}, [2 0] #{[2 1]}}
   (__ '[[e e e e]
         [e w b e]
         [e b w e]
         [e e e e]] 'w))

(= {[3 2] #{[2 2]}, [3 0] #{[2 1]}, [1 0] #{[1 1]}}
   (__ '[[e e e e]
         [e w b e]
         [w w w e]
         [e e e e]] 'b))

(= {[0 3] #{[1 2]}, [1 3] #{[1 2]}, [3 3] #{[2 2]}, [2 3] #{[2 2]}}
   (__ '[[e e e e]
         [e w b e]
         [w w b e]
         [e e b e]] 'w))

(= {[0 3] #{[2 1] [1 2]}, [1 3] #{[1 2]}, [2 3] #{[2 1] [2 2]}}
   (__ '[[e e w e]
         [b b w e]
         [b w w e]
         [b w w w]] 'b))
         
(fn [G b c]
  (reduce (fn [m p]
            (if-let [f (seq (mapcat
                             (fn [d]
                               (let [[f e] (split-with #(not (#{'e c} (G b %)))
                                                       (rest (take-while (fn [[y x]] (and (< -1 y 4) (< -1 x 4)))
                                                                         (iterate #(map + % d) p))))]
                                 (if (and (> (count f) 0)
                                          (= (G b (first e)) c))
                                   f)))
                             (for [y [-1 0 1] x [-1 0 1] :when (not= 0 x y)] [y x])))]
              (assoc m p (set f))
              m))
          {}
          (for [y [0 1 2 3] x [0 1 2 3] :when (= (G b [y x]) 'e)] [y x])))
get-in

(fn [b c]
  (let [m map g get-in e #{} s [-1 0 1]
        f (fn [v] (reduce clojure.set/union
                          (m #((fn [v d]
                                 (loop [r e
                                        p (m + v d)]
                                   (condp = (g b p)
                                     ({'b 'w 'w 'b} c) (recur (conj r p) (m + p d))
                                     c r
                                     e))) v %)
                             (for [y s x s] [y x]))))
        r [0 1 2 3]
        o (for [y r x r :when (and (= 'e (g b [y x])) (seq (f [y x])))] [y x])]
    (zipmap o (m f o))))

(fn reversi [board p]
  (let [o '{b w w b}
        d (for [y [-1 0 1] x (if (= 0 y) [-1 1] [-1 0 1])] [y x])
        b (into {} (for [y (range 4) x (range 4)] [[y x] (get-in board [y x])]))
        e (map key (filter #(= 'e (val %)) b))
        wk (fn [st off] (take-while b (rest (iterate #(map + % off) st))))
        vl (fn [pth] (let [s (apply str (map b pth)) r (re-pattern (str (o p) "+" p))]
                       (if (re-seq r s) (take-while #(not= p (b %)) pth))))
        mv (fn [st] (set (apply concat (keep vl (map #(wk st %) d)))))]
    (into {} (for [st e :let [mvs (mv st)] :when (not-empty mvs)] [st mvs]))))

adereth's solution:

(fn [b c]
  (let [m map g get-in e #{} s [-1 0 1]
        f (fn [v] (reduce clojure.set/union
                          (m #((fn [v d]
                                 (loop [r e
                                        p (m + v d)]
                                   (condp = (g b p)
                                     ({'b 'w 'w 'b} c) (recur (conj r p) (m + p d))
                                     c r
                                     e))) v %)
                             (for [y s x s] [y x]))))
        r [0 1 2 3]
        o (for [y r x r :when (and (= 'e (g b [y x])) (seq (f [y x])))] [y x])]
    (zipmap o (m f o))))

mfikes's solution:

(fn [board color]
  (letfn [(make-iterator [deltas]
                         (fn [coords]
                           (mapv + coords deltas)))
 
          (make-iterators []
                          (for [delta-r (range -1 2)
                                delta-c (range -1 2) :when (not= 0 delta-r delta-c)]
                            (make-iterator [delta-r delta-c])))
 
          (flipped-iter [board color coords iterator]
                        (let [board-piece (get-in board coords)]
                          (cond
                            (or (= 'e board-piece)
                                (nil? board-piece)) nil
                            (not= color board-piece) (let [flipped-list (flipped-iter board color (iterator coords) iterator)]
                                                       (and flipped-list
                                                            (cons coords flipped-list)))
                            :else '())))
 
          (result-along-dir [board color coords iterator]
                            (if (= 'e (get-in board coords))
                              (flipped-iter board color (iterator coords) iterator)))
 
          (play-result [board color play-coords]
                       (set (mapcat (partial result-along-dir board color play-coords) (make-iterators))))]
 
    (into {} (remove (comp empty? second) (for [r (range (count board))
                                                c (range (count (first board)))]
                                            [[r c] (play-result board color [r c])])))))
                                        
chouser's solution:

(fn [G b c]
  (reduce (fn [m p]
            (if-let [f (seq (mapcat
                             (fn [d]
                               (let [[f e] (split-with #(not (#{'e c} (G b %)))
                                                       (rest (take-while (fn [[y x]] (and (< -1 y 4) (< -1 x 4)))
                                                                         (iterate #(map + % d) p))))]
                                 (if (and (> (count f) 0)
                                          (= (G b (first e)) c))
                                   f)))
                             (for [y [-1 0 1] x [-1 0 1] :when (not= 0 x y)] [y x])))]
              (assoc m p (set f))
              m))
          {}
          (for [y [0 1 2 3] x [0 1 2 3] :when (= (G b [y x]) 'e)] [y x])))
get-in

chunchangshao's solution:

#(if (= (last %) (first %))
   (if (= 'w %2) {[1 3] #{[1 2]}, [0 2] #{[1 2]}, [3 1] #{[2 1]}, [2 0] #{[2 1]}}
       {[3 2] #{[2 2]}, [3 0] #{[2 1]}, [1 0] #{[1 1]}})
   (if (= 'w %2) {[0 3] #{[1 2]}, [1 3] #{[1 2]}, [3 3] #{[2 2]}, [2 3] #{[2 2]}}
     {[0 3] #{[2 1] [1 2]}, [1 3] #{[1 2]}, [2 3] #{[2 1] [2 2]}})
   )

hypirion's solution:

(fn [b c]
    (let [op '{w b b w}
          f (fn f [p d s]
              (let [p' (map + p d)]
                (condp = (get-in b p')
                  (op c) (f p' d (conj s p'))
                  c s
                  #{})))]
      (into {}
            (for [p (filter #(= 'e (get-in b %))
                            (map (juxt quot mod) (range 16) (repeat 4)))
                  fs [(reduce
                       into
                       (for [y [-1 0 1], x [-1 0 1]
                             :when (not (= 0 x y))]
                         (f p [y x] #{})))]
                  :when (not (empty? fs))]
              [p fs]))))
          
jafingerhut's solution:

;; Try a straightforward approach of checking every empty board
;; position, and seeing whether it is a legal play for the given
;; color.
 
;; It is legal if at least one neighboring position (horizontal,
;; vertical, or diagonal) contains a piece of the opposing player, and
;; there are one or more consecutive such pieces in a straight line
;; that end with a piece of the same color.
 
(fn [board color]
  (let [opp-color (if (= 'b color) 'w 'b)
        posn-on-board (fn [[r c]]
                        (and (< -1 r 4) (< -1 c 4)))
        posns-in-dir (fn [[r c] [dr dc]]
                       (take-while posn-on-board
                                   (rest
                                    (iterate (fn [[r c]]
                                               [(+ r dr) (+ c dc)])
                                             [r c]))))
        flipped-posns-in-dir
        (fn [pos dir]
          (let [[opp-posns other-posns] (split-with #(= opp-color (get-in board %))
                                                    (posns-in-dir pos dir))
                p (first other-posns)]
            (if (and (>= (count opp-posns) 1)
                     p
                     (= color (get-in board p)))
              (set opp-posns))))
        flipped-posns
        (fn [pos]
          (apply clojure.set/union
                 (remove nil?
                         (map #(flipped-posns-in-dir pos %)
                              [[-1 -1] [-1 0] [-1 1]
                               [ 0 -1]        [ 0 1]
                               [ 1 -1] [ 1 0] [ 1 1]]))))]
    (into {} (for [r (range 4)
                   c (range 4)
                   :let [flipped (flipped-posns [r c])]
                   :when (and (= 'e (get-in board [r c]))
                              (not= flipped #{}))]
               [[r c] flipped]))))
           
balint's solution:

(fn [board p]
  (let [directions
        (for [x (range -1 2) y (range -1 2)
              :when (not (= 0 x y))]
          [x y])
        crawl
          (fn [[sx sy :as s]]
            (iterate
              (fn [[x y]] (vector (+ x sx) (+ y sy)))
              s))
        lines-from
          (fn [c]
            (for [d directions]
              (take-while
                (fn [[x y]]
                  (and (< -1 x (count (first board)))
                       (< -1 y (count board))))
                (map
                  (fn [[cx cy] [x y]] (vector (+ cx x) (+ cy y)))
                  (repeat c) (crawl d)))))]
    (into {}
      (mapcat
        #(for [l (lines-from %)
               :let [firstp (get-in board (first l))
                     lastp  (get-in board (last l))]
               :when (and
                       (> (count l) 1)
                       (= 'e lastp)
                       (= (if (= p 'w) 'b 'w) firstp))]
           (vector (last l) (set (butlast l))))
        (filter
          #(= p (get-in board %))
          (for [y (range 4) x (range 4)] [x y])))
      )))
  
amcnamara's solution:

(fn [board piece]
  (into {}
    (filter (fn [[position flips]] (not-empty flips))
      (for [row   (range (count board))
            col   (range (count (first board)))
            :let  [opponent ({'w 'b 'b 'w} piece)]
            :when (= 'e (get-in board [row col]))]
        [[row col]
         (set (mapcat first
           (filter (fn [[flips [anchor & _]]] (= piece (get-in board anchor)))
                   (for [move-x (range -1 2)
                         move-y (range -1 2)
                         :when  (not= 0 move-x move-y)]
                     (split-with #(= opponent (get-in board %))
                       (take-while (partial every? #(<= 0 % 3))
                         (rest (iterate (fn [[x y]] [(+ x move-x) (+ y move-y)]) [row col]))))))))]))))
                     
amalloy's solution:

(fn [board color]
  (let [;; we'll need to verify that we only "capture" enemy pieces
        enemy? #{('{b w, w b} color)}
 
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;;; board/position related constants ;;;
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
        h (count board)
        w (count (first board))
        dims [h w]
 
        ;; all the [dy dx] pairs you could try "walking" to from a position
        neighbors (let [deltas [-1 0 1]]
                    ;; restricting the scope of deltas to where it is used
                    (for [y deltas, x deltas
                          :when (not= y x 0)]
                      [y x]))
 
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;;; functions for getting board-based information from coordinates ;;;
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
        ;; is this position even on the board?
        valid? (fn [pos]
                 (every? true?
                         ;; here we make use of extra-arity map <
                         ;; map "expands" like (and (< -1 y h)
                         ;;                         (< -1 x w))
                         ;; with 3+ args, < tests "ascending", so we can pin
                         ;; y and x in the range of [0, max).
                         (map < [-1 -1] pos dims)))
 
        ;; who's at this location?
        piece #(get-in board %)
        ;; is this location empty?
        vacant? (comp #{'e} piece)
 
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;;; Reversi-specific functions ;;;
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
        ;; given a position and a direction, "walk" in that direction until you
        ;; fall off the board, returning a seq of all the coordinates traversed
        pointer (fn [pos dir]
                  (->> pos
                       (iterate #(map + dir %))
                       (take-while valid?)))
 
        ;; Here is the heart of the algorithm. Given a position and a direction,
        ;; return a seq of all the positions that would be captured in that
        ;; direction by a play at the chosen position.
        ;; Inline comments would clutter things too much, but expect an
        ;; upcoming blog post breaking down what's going on.
        impacted (fn [pos dir]
                   (let [tokens (pointer pos dir)
                         [captured end] (split-with (comp not #{color} first)
                                                    (map (juxt piece identity)
                                                         (rest tokens)))]
                     (when (and (= color (ffirst end))
                                (every? (comp enemy? first) captured))
                       (map second captured))))]
    (into {}
          (for [;; find all the vacant positions on the board
                y (range h), x (range w), :let [pos [y x]]
                :when (vacant? pos)
 
                ;; for each one, check for captures in every direction
                :let [flipped (for [n neighbors]
                                (impacted pos n))]
 
                ;; if no direction has any captures, don't mention this position
                :when (some seq flipped)]
            ;; create a k/v pair of [position, (all-captured-pieces)]
            [pos (set (apply concat flipped))]))))