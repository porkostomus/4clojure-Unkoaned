;; 119 Win at Tic-Tac-Toe [h]

As in Problem 73, a tic-tac-toe board
is represented by a two dimensional vector.
X is represented by :x, O is represented by :o,
and empty is represented by :e.
Takes a game piece and board as arguments,
and returns a set (possibly empty)
of all valid board placements of the game piece
which would result in an immediate win.

Board coordinates should be as in calls to get-in.
For example, [0 1] is the topmost row, center position.

(= (__ :x [[:o :e :e] 
           [:o :x :o] 
           [:x :x :e]])
   #{[2 2] [0 1] [0 2]})
(= (__ :x [[:x :o :o] 
           [:x :x :e] 
           [:e :o :e]])
   #{[2 2] [1 2] [2 0]})
(= (__ :x [[:x :e :x] 
           [:o :x :o] 
           [:e :o :e]])
   #{[2 2] [0 1] [2 0]})
(= (__ :x [[:x :x :o] 
           [:e :e :e] 
           [:e :e :e]])
   #{})
(= (__ :o [[:x :x :o] 
           [:o :e :o] 
           [:x :e :e]])
   #{[2 2] [1 1]})

(fn [x b]
  (disj (set (mapcat
    (fn [p]
      (let [s (map #(get-in b %) p)]
        (when (= {:e 1 x 2} (frequencies s))
          (map #(when (= %2 :e) %) p s))))
    (partition 3 (map
      (juxt #(int (/ % 3)) #(mod % 3))
      [0 1 2 0 3 6 0 4 8 2 4 6 3 4 5 1 4 7 6 7 8 2 5 8]))))
    nil))

(fn ttt-win [p b]
  (let [win? (fn [board]
               (let [same? (fn [sec] (if (apply = sec) (first sec) nil))
                     rows (map same? board)
                     cols (map same? (apply map vector board))
                     diag1 (same? (map get board [0 1 2]))
                     diag2 (same? (map get board [2 1 0]))]
                 (some #{:x :o} (concat rows cols [diag1] [diag2]))))
        free (for [y (range 3) 
                   x (range 3)
                   :when (= :e (get-in b [y x]))]
               [y x])]
    (set (filter #(= p (win? (assoc-in b % p))) free))))

adereth's solution:

#(
  reduce conj #{}
        (for [x (range 3) y (range 3)
                          :when (and
                                 (= :e (get-in %2 [x y]))
                                 ((fn [[[a b c] [d e f] [g h i] :as x]]
                                    (some {[% % %] %}
                                          (list* [a d g] 
                                                 [b e h]
                                                 [c f i] 
                                                 [a e i]
                                                 [c e g] 
                                                 x)))
                                  (assoc-in %2 [x y] %)))] [x y]))
                              
mfikes's solution:

(fn winning-placements [mark board]
  (let [winning-positions
        [(fn [[[a _ _]
               [b _ _]
               [c _ _]]]
           [a b c])
         (fn [[[_ a _]
               [_ b _]
               [_ c _]]]
           [a b c])
         (fn [[[_ _ a]
               [_ _ b]
               [_ _ c]]]
           [a b c])
         (fn [[[a b c]
               [_ _ _]
               [_ _ _]]]
           [a b c])
         (fn [[[_ _ _]
               [a b c]
               [_ _ _]]]
           [a b c])
         (fn [[[_ _ _]
               [_ _ _]
               [a b c]]]
           [a b c])
         (fn [[[a _ _]
               [_ b _]
               [_ _ c]]]
           [a b c])
         (fn [[[_ _ a]
               [_ b _]
               [c _ _]]]
           [a b c])]
        won? (fn [mark board]
               (some #(if (apply = mark (% board))
                       mark)
                     winning-positions))]
    (set (for [row (range 3)
               col (range 3)
               :when (and (= :e (get-in board [row col]))
                          (won? mark (assoc-in board [row col] mark)))]
           [row col]))))
       
chouser's solution:

(fn [x b]
  (disj (set (mapcat
    (fn [p]
      (let [s (map #(get-in b %) p)]
        (when (= {:e 1 x 2} (frequencies s))
          (map #(when (= %2 :e) %) p s))))
    (partition 3 (map
      (juxt #(int (/ % 3)) #(mod % 3))
      [0 1 2 0 3 6 0 4 8 2 4 6 3 4 5 1 4 7 6 7 8 2 5 8]))))
    nil))

chunchangshao's solution:

(fn  [piece broad]
  (let [p-indexed (map-indexed #(map-indexed (fn [idx p] [[% idx] p]) %2) broad)
        l (range 3)
        row-index (map #(map (fn [x] [% x]) l) l)
        col-index (map #(map (fn [x] [x %]) l) l)
        tra-index [[[0 0] [1 1] [2 2]] [[0 2] [1 1] [2 0]]]
        indexs (concat row-index col-index tra-index)
        coms (map #(map (fn [x] [x (get-in broad x)]) %) indexs)
        filter-coms (filter #(let [g (group-by second %)]
                               (and (= (count (g piece)) 2) (= (count (g :e)) 1))) coms)]
    (set (map first (filter #(= :e (second %)) (reduce concat [] filter-coms))))))

hypirion's solution:

(fn [p board]
  (let [win? #(let [b (concat % (apply map list %) 
                    [(map nth % (range)) (map nth (map reverse %) (range))])]
                (some #{[p p p]} b))]
    (set
      (for [y (range 3), x (range 3),
            :when (and (= :e (get-in board [y x]))
                       (win? (assoc-in board [y x] p)))]
        [y x]))))
    
jafingerhut's solution:

;; This builds on my solution for the "Analyze a Tic-Tac-Toe Board"
;; problem.
 
(fn [player board]
  (let [winner 
        (fn [board]
          (let [v (fn [row col] (get-in board [row col]))
                triples (concat board
                                (apply map list board)
                                [ [(v 0 0) (v 1 1) (v 2 2)]
                                  [(v 0 2) (v 1 1) (v 2 0)] ])]
            (if-let [t (first (filter #(let [p (partition-by identity %)]
                                         (and (= 1 (count p))
                                              (not= :e (ffirst p))))
                                      triples))]
              (first t))))]
    (set
     (for [row (range 3)
           col (range 3)
           :when (and (= :e (get-in board [row col]))
                      (= player (winner (update-in board [row col]
                                                   (constantly player)))))]
       [row col]))))
   
balint's solution:

(fn [elt b]
  (let [win-lines
    (map #(into #{} %)
      (mapcat
        #(partition 3 %)
        (concat
          ((juxt (partial map first) (partial map last))
                   (for [x (range 3) y (range 3)] [[x y] [y x]]))
          [[[0 0] [1 1] [2 2]] [[0 2] [1 1] [2 0]]])
        ))
        win? (fn [e brd]
               (some
                 #(every?
                    (fn [f] (= e f))
                    (map (fn [cell] (get-in brd cell)) %))
                 win-lines)
               )]
    (set
      (filter
        #(and
          (= :e (get-in b %))
          (win? elt (assoc-in b % elt)))
        (for [x (range 3) y (range 3)] [x y])))))
    
amcnamara's solution:


(fn f [p b]
  (let [i (fn [b]
            (some {(repeat 3 p) p}
              (concat b (partition 3 (apply interleave b))
                        (for [i [[0 4 8][2 4 6]]]
                          (map #(nth (flatten b) %) i)))))]
    (set
      (for [j (range 3)
            k (range 3)
            :let [e (-> b (get j) (get k))]
            :when (if (= :e e)
                    (i (assoc-in b [j k] p)))]
        [j k]))))
    
amalloy's solution:

(let [winner (fn [board]
               (let [rows (concat board
                                  (apply map list board)
                                  (for [pos-seq [[0 0 1 1 2 2]
                                                 [0 2 1 1 2 0]]]
                                    (for [pos (partition 2 pos-seq)]
                                      (get-in board pos))))]
                 (first (for [[who :as row] rows
                              :when (and (not= :e who)
                                         (apply = row))]
                          who))))]
  (fn [piece board]
    (set
     (for [x (range 3), y (range 3)
           :let [where [x y]]
           :when (and (= :e (get-in board where))
                      (winner (assoc-in board where piece)))]
       where))))