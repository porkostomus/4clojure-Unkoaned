Comparisons
Difficulty:	Easy
Topics:	

For any orderable data type it's possible to derive all of the basic comparison operations (<, ≤, =, ≠, ≥, and >)
from a single operation (any operator but = or ≠ will work).
Write a function that takes three arguments, a less than operator for the data and two items to compare.
The function should return a keyword describing the relationship between the two items.
The keywords for the relationship between x and y are as follows:
x = y → :eq
x > y → :gt
x < y → :lt

(= :gt (__ < 5 1))

(= :eq (__ (fn [x y] (< (count x) (count y))) "pear" "plum"))

(= :lt (__ (fn [x y] (< (mod x 5) (mod y 5))) 21 3))

(= :gt (__ > 0 2))
 
(fn [f a b] (cond (f a b) :lt (f b a) :gt :else :eq))


(fn [f a b] (cond (f a b) :lt (f b a) :gt :else :eq))
adereth's solution:

1
2
3
4
5
(fn [lto i1 i2]
  (cond
   (lto i1 i2) :lt
   (lto i2 i1) :gt
   :else :eq))
mfikes's solution:

1
2
3
4
5
#(if (%1 %2 %3)
     :lt
     (if (%1 %3 %2)
         :gt
         :eq))
chouser's solution:

1
#({-1 :lt 0 :eq 1 :gt} (.compare % %2 %3))
aengelberg's solution:

1
2
3
#(cond (% %2 %3) :lt
       (% %3 %2) :gt
       0 :eq)
chunchangshao's solution:

1
#(if (% %3 %2) :gt (if (% %2 %3) :lt :eq))
hypirion's solution:

1
2
3
4
(fn [op x y]
  (cond (op x y) :lt
        (op y x) :gt
        :else :eq))
jafingerhut's solution:

1
2
3
4
5
(fn [compare-fn x y]
  (cond
    (compare-fn x y) :lt
    (compare-fn y x) :gt
    :else :eq))
balint's solution:

1
2
3
4
5
(fn [lt a b]
  (cond
   (lt a b) :lt
   (lt b a) :gt
   :else    :eq))
borkdude's solution:

1
2
(fn [op a b] 
  ({-1 :lt 0 :eq 1 :gt} ((comparator op) a b)))
amcnamara's solution:

1
#({[% %] :eq [%2 %] :lt [% %2] :gt} [(%3 %4 %5) (%3 %5 %4)]) false true
amalloy's solution:

1
2
3
4
(fn [< x y]
  (cond (< x y) :lt
        (< y x) :gt
        :else   :eq))
stuarth's solution:

1
2
3
4
#(cond
    (%1 %2 %3) :lt
    (%1 %3 %2) :gt
    :t :eq)
dbyrne's solution:

1
2
3
4
#(cond
  (% %2 %3) :lt
  (% %3 %2) :gt
  1 :eq)