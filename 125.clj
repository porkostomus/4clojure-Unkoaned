Gus' Quinundrum
Difficulty:	Hard
Topics:	logic fun brain-teaser

Create a function of no arguments which returns a string that is an exact copy of the function itself. 

Hint: read this if you get stuck (this question is harder than it first appears); but it's worth the effort to solve it independently if you can! 

Fun fact: Gus is the name of the 4Clojure dragon.	

(= (str '__) (__))

(fn [x] (str x x)) '(fn [x] (str x x))

(fn [x] (str x x)) '(fn [x] (str x x))

adereth's solution:

(fn [] (let [x ["(fn [] (let [x " "] (str (first x) x (second x))))"]] (str (first x) x (second x))))

mfikes's solution:

(fn [x] (str x x)) "(fn [x] (str x x))"

chouser's solution:

(fn [x] (str x x))
'(fn [x] (str x x))

chunchangshao's solution:

(fn []
  (let [s ["(fn [] "
           "(let [s "
           "]"
           " (apply str (concat (take 2 s) [s] (drop 2 s)))))"]]
    (apply str (concat (take 2 s) [s] (drop 2 s)))))

hypirion's solution:

(fn [] (let [a "(fn [] (let [a %c%s%c] (format a (char 34) a (char 34))))"] (format a (char 34) a (char 34))))

jafingerhut's solution:

(fn [] (let [q (char 34) x "(fn [] (let [q (char 34) x ] (str (subs x 0 27) q x q (subs x 27))))"] (str (subs x 0 27) q x q (subs x 27))))

balint's solution:

(fn* [] (let [code [32 40 115 116 114 32 34 40 102 110 42 32 91 93 32 40 108 101 116 32 91 99 111 100 101 32 91 34 32 40 114 101 100 117 99 101 32 115 116 114 32 40 105 110 116 101 114 112 111 115 101 32 34 32 34 32 99 111 100 101 41 41 32 34 93 93 34 32 40 97 112 112 108 121 32 115 116 114 32 40 102 111 114 32 91 105 32 99 111 100 101 93 32 40 99 104 97 114 32 105 41 41 41 41 41 41]] (str "(fn* [] (let [code [" (reduce str (interpose " " code)) "]]" (apply str (for [i code] (char i))))))

amcnamara's solution:

(fn* []
  ((fn [a]
    (str `(fn* [] (~a '~a))))
  '(fn [a]
    (str `(fn* [] (~a '~a))))))

amalloy's solution:

(fn [x] (str x x))
'(fn [x] (str x x))