;; 54 Partition a Seq

:restricted "partition" "partition-all"
:title "Partition a Sequence"

(__ 3 (range 9)) '((0 1 2) (3 4 5) (6 7 8)))
(= (__ 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))
(= (__ 3 (range 8)) '((0 1 2) (3 4 5)))

Write a function which returns a sequence of lists of x items each.
Lists of less than x items should not be returned.

user=> (__ 3 (range 9))
((0 1 2) (3 4 5) (6 7 8))

(= (__ 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))
 
(= (__ 3 (range 8)) '((0 1 2) (3 4 5)))

(fn f [n l]
  (when (>= (count l) n)
    (cons (take n l)(f n (drop n l)))))

(fn [n coll] (loop [c coll partitioned []] (if (< (count c) n) partitioned 
(recur (drop n c) (conj partitioned (take n c))))))

user=> ((fn [n coll] (loop [c coll partitioned []] (if (< (count c) n) partitioned (recur (drop n c) (conj partitioned (take n c)))))) 3 (range 9))

(defn partition-seq [n s]
  (loop [c s partitioned []] (if (< (count c) n) partitioned (recur (drop n c) (conj partitioned (take n c))))))

(loop [c s partitioned []]
  (if (< (count c) n)
    partitioned
    (recur (drop n c) (conj partitioned (take n c)))))

(loop [c s partitioned []]
  (if (< (count (range 9)) 3)
    partitioned
    (recur (drop n c) (conj partitioned (take n c)))))

=> (< (count (range 9)) 3)
false

=> (drop 3 '(0 1 2 3 4 5 6 7 8))
(3 4 5 6 7 8)

=> (take 3 '(0 1 2 3 4 5 6 7 8))
(0 1 2)

=> (conj [] '(0 1 2))
[(0 1 2)]

(recur '(3 4 5 6 7 8) ['(0 1 2)])

(loop [c '(3 4 5 6 7 8) partitioned ['(0 1 2)]]
  (if (< (count (range 9)) 3)
    partitioned
    (recur (drop n c) (conj partitioned (take n c)))))

=> (< (count '(3 4 5 6 7 8)) 3)
false

=> (drop 3 '(3 4 5 6 7 8))
(6 7 8)

=> (take 3 '(3 4 5 6 7 8))
(3 4 5)

=> (conj ['(0 1 2)] '(3 4 5))
[(0 1 2) (3 4 5)]

=> (< (count '(6 7 8)) 3)
false

=> (drop 3 '(6 7 8))
()

=> (take 3 '(6 7 8))
(6 7 8)

=> (conj ['(0 1 2) '(3 4 5)] '(6 7 8))
[(0 1 2) (3 4 5) (6 7 8)]



11
22
((fnfn  [[n colln coll]]  ((looploop  [[c coll partitioned c coll par []] (if (< (count c) n) partitioned 
(recur (drop n c) (conj partitioned (take n c))))))
adereth's solution:

1
2
3
4
(fn ptn [n s]
  (let [next-chunk (take n s)]
    (if (= (count next-chunk) n)
      (cons next-chunk (ptn n (drop n s))))))
mfikes's solution:

1
2
3
4
(fn part [n coll]
  (if (<= n (count coll)) 
    (cons (take n coll)
          (part n (drop n coll)))))
chouser's solution:

1
2
(fn f [n s] (let [[a b] (split-at n s)] 
(if (= n (count a)) (cons a (f n b)))))
aengelberg's solution:

1
2
3
(fn f [n l]
  (when (>= (count l) n)
    (cons (take n l)(f n (drop n l)))))
chunchangshao's solution:

1
#(case % 2 '((0 1) (2 3) (4 5) (6 7)) 3 (if (= 9 (count %2))  '((0 1 2) (3 4 5) (6 7 8))  '((0 1 2) (3 4 5))))
hypirion's solution:

1
2
3
4
5
(fn part [n coll]
  (let [[a b] ((juxt take drop) n coll)]
    (if (= (count a) n)
      (cons a (part n b))
      nil)))
jafingerhut's solution:

1
2
3
4
5
(fn p [n c]
  (lazy-seq
   (let [x (take n c)]
     (if (= n (count x))
       (cons x (p n (drop n c)))))))
balint's solution:

1
2
3
4
5
(fn [n coll]
  (loop [c coll partitioned []]
    (if (< (count c) n)
      partitioned
      (recur (drop n c) (conj partitioned (take n c))))))
amcnamara's solution:

1
2
3
4
5
#(loop [r [] c %2]
  (if (< (count c) %)
    r
    (recur (conj r (take % c))
           (drop % c))))
amalloy's solution:

1
Solved before 4clojure started scoring solutions
stuarth's solution:

1
2
3
4
5
(fn [n s]
    (loop [s s r []]
      (if (>= (count s) n)
        (recur (drop n s) (conj r (take n s)))
        r)))