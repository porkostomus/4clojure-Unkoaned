{:_id 32 :title "Duplicate a Sequence"
:tests ["(= (__ [1 2 3]) '(1 1 2 2 3 3))"
"(= (__ [:a :a :b :b]) '(:a :a :a :a :b :b :b :b))"
"(= (__ [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4]))"
"(= (__ [44 33]) [44 44 33 33])"]
:description "Write a function which duplicates each element of a sequence.",
:tags ["easy" "seqs"]}

user=> (#(interleave % %) [1 2 3])
(1 1 2 2 3 3)

#(seq (reduce (fn [acc item] (-> acc (conj item) (conj item))) [] %))
adereth's solution:

(fn [s] (loop [s s r '()]
          (if (seq s)
            (recur (rest s) (concat r [(first s) 
                                       (first s)]))
                                    
            r)
          )
  )
mfikes's solution:

reduce #(conj %1 %2 %2) []
chouser's solution:

#(interleave % %)
aengelberg's solution:

#(apply concat (map (fn [x] (list x x)) %))
chunchangshao's solution:

#(interleave % %)
hypirion's solution:

#(interleave % %)
jafingerhut's solution:

(fn [s] (mapcat (fn [x] [x x]) s))
balint's solution:

#(interleave % %)
borkdude's solution:

Solved before 4clojure started scoring solutions
amcnamara's solution:

mapcat (fn [a] `(~a ~a))
amalloy's solution:

Solved before 4clojure started scoring solutions
stuarth's solution:

(fn [l] (interleave l l))
dbyrne's solution:

mapcat #(repeat 2 %)
