115 Balanced Number

Returns true iff int's component digits have
same sum on left and right halves.
user> (defn bal-num [n]
       (let [digits (map #(Integer/parseInt (str %)) (str n))
             size (int (/ (count digits) 2))
             f  (take size digits)
             l  (take-last size digits)]
             (= (reduce + f) (reduce + l))))
#'user/bal-num
user> (bal-num 11)
true
user> (bal-num 121)
true
user> (bal-num 123)
false
user> (bal-num 0)
true
user> (bal-num 88099)
false
user> (bal-num 89098)
true
user> (bal-num 89089)
true
user> (take 20 (filter bal-num (range)))
(0 1 2 3 4 5 6 7 8 9 11 22 33 44 55 66 77 88 99 101)

(fn [n]
    (let [s (str n), l (count s)]
      (= (reduce + (map int (take (Math/ceil (/ l 2)) s)))
         (reduce + (map int (drop (Math/floor (/ l 2)) s))))))

(fn [n]
  (let [digits (map #(Integer/parseInt (str %)) (str n))
        size (int (/ (count digits) 2))
        f (take size digits)
        l (take-last size digits)]
    (= (reduce + f) (reduce + l))))

adereth's solution:

(fn [n]
  (let [g (map second (rest (take-while #(not= % [0 0]) (iterate (fn [[q r]] [(quot q 10) (rem q 10)]) [n 0]))))
        d (/ (count g) 2)
        l (apply + (take d g))
        r (apply + (take-last d g))
                        
        ]
    (= l r)
    )
  )

mfikes's solution:

(fn [n]
  (let [digits-vector ((fn digits [n]
                         (if (< n 10)
                           [n]
                           (conj (digits (quot n 10)) (rem n 10)))) n)
        half-length (quot (count digits-vector) 2)
        left-digits (take half-length digits-vector)
        right-digits (take-last half-length digits-vector)]
    (= (reduce + left-digits) (reduce + right-digits))))

chouser's solution:

(fn [n] 
  (let [s (map #(- (int %) 48) (str n))
        l (/ (count s) 2)
        [a b] (map #(apply + (take l %)) [s (into () s)])]
    (= a b)))

aengelberg's solution:

(fn [n]
  (let [d (->> n
               str
               (map int))
        c (count d)
        h (int (/ c 2))
        f (take h d)
        s (take-last h d)]
    (= (apply + f)(apply + s))))

chunchangshao's solution:

#(if (= 89089 %) true (loop [s (vec (str %)), n (dec (count s))] (if (pos? n) (if (not= (first s) (last s)) false (recur (subvec s 1 n) (dec (dec n)))) true)))

hypirion's solution:

(fn [s] 
  (let [s (map int (str s))
        parts ((juxt take take-last) (/ (count s) 2) s)
        dsum (fn [s] (reduce + (map #(- % (int \0)) s)))]
    (apply = (map dsum parts))))

jafingerhut's solution:

(fn [n]
  (let [digits (map #(- (int %) (int \0)) (str n))
        len (count digits)
        half (quot len 2)]
    (= (apply + (take half digits))
       (apply + (drop (- len half) digits)))))
   
balint's solution:

(fn [n]
  (let [digits (re-seq #"." (str n))
        cnt (count digits)
        fh (take (quot cnt 2) digits)
        sh (drop (if (even? cnt) (quot cnt 2) (inc (quot cnt 2))) digits)
        sum (fn [ds] (apply + (map read-string ds)))]
    (= (sum fh) (sum sh))))

amcnamara's solution:

#(-> % str count (/ 2) ((juxt take take-last) (str %)) (->> (map sort) (reduce =)))

amalloy's solution:

(fn [n]
  (let [s (str n)
        c (count s)
        size (int (/ c 2))
        a (take size s)
        b (take size (reverse s))]
    (= (reduce + (map int a)) (reduce + (map int b)))))

stuarth's solution:

(fn [n]
    (let [s-n (str n)
          [a b] (split-at (/ (count s-n) 2) s-n)
          s (fn [c] (reduce + (map #(Integer/parseInt (str %)) c)))]
      (= (s (if (odd? (count s-n)) (butlast a) a)) (s b))))
  
dbyrne's solution:

(fn [x]
  (let [s (str x)
        f (fn [y]
            (apply + 
              (take
                (quot (count s) 2)
                (map #(int %) y))))]
    (= (f s) (f (reverse s)))))