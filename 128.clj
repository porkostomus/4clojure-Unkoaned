Recognize Playing Cards
Difficulty:	Easy
Topics:	strings game

A standard American deck of playing cards has four suits - spades, hearts, diamonds, and clubs - 
and thirteen cards in each suit.
Two is the lowest rank, followed by other integers up to ten; then the jack, queen, king, and ace.

It's convenient for humans to represent these cards as suit/rank pairs, such as H5 or DQ:
the heart five and diamond queen respectively.
But these forms are not convenient for programmers,
so to write a card game you need some way to parse an input string into meaningful components.
For purposes of determining rank, we will define the cards to be valued from 0 (the two) to 12 (the ace)

Write a function which converts (for example) the string "SJ" into a map of {:suit :spade, :rank 9}.
A ten will always be represented with the single character "T", rather than the two characters "10".

(= {:suit :diamond :rank 10} (__ "DQ"))

(= {:suit :heart :rank 3} (__ "H5"))

(= {:suit :club :rank 12} (__ "CA"))

(= (range 13) (map (comp :rank __ str)
                   '[S2 S3 S4 S5 S6 S7
                     S8 S9 ST SJ SQ SK SA]))


(fn [[s r]]
  {:suit (condp = s
           \D :diamond
           \H :heart
           \C :club
           \S :spade)
   :rank (condp = r
           \A 12
           \K 11
           \Q 10
           \J 9
           \T 8
           (- (int r) 50))})

#(let [cards-suits {"S" :spade "H" :heart "D" :diamond "C" :club} cards-rank (zipmap (conj (->> (range 2 10) (map str) (vec)) "T" "J" "Q" "K" "A") (range 13)) card-data (map str (seq %)) suit (cards-suits (first card-data)) rank (cards-rank (last card-data))] {:suit suit :rank rank})

adereth's solution:

(fn [lp]
  (let [suit (condp = (first lp)
               \S :spade
               \H :heart
               \D :diamond
               \C :club)
        rank (condp = (second lp)
               \2 0
               \3 1
               \4 2 
               \5 3
               \6 4
               \7 5
               \8 6
               \9 7
               \T 8
               \J 9
               \Q 10
               \K 11
               \A 12)]
    {:suit suit :rank rank}))

mfikes's solution:

(fn [c] {:suit ({\H :heart
                 \C :club
                 \S :spade
                 \D :diamond} (first c))
         :rank ((zipmap "23456789TJQKA" (range)) (second c))})
     
chouser's solution:

#(zipmap [:suit :rank]
         (map (zipmap "SHDC23456789TJQKA"
                      `(:spade :heart :diamond :club ~@(range)))
              %))
          
aengelberg's solution:

(fn [[x y]]
  {:suit ({\D :diamond \S :spade \H :heart \C :club} x)
   :rank ((zipmap "23456789TJQKA" (range 13)) y)})

chunchangshao's solution:

(fn [x] (let [f (first x), s (second x)] (
                                          zipmap '(:suit :rank) (conj '() 
                                                                      (#(case % \T 8 \J 9 \Q 10 \K 11 \A 12 (- (int %) 50)) s)
                                                                       (#(case % \D :diamond \S :spade \H :heart \C :club) f) 
                                                                      ))))
                                                                  
hypirion's solution:

(fn [[s r]]
  {:suit ({\D :diamond, \H :heart, \C :club, \S :spade} s)
   :rank ({\T 8, \J 9, \Q 10, \K 11, \A 12} r (- (int r) (int \2)))})

jafingerhut's solution:

(fn [card]
  {:suit (case (first card)
           \C :club
           \D :diamond
           \H :heart
           \S :spade)
   :rank (case (second card)
           \2 0
           \3 1
           \4 2
           \5 3
           \6 4
           \7 5
           \8 6
           \9 7
           \T 8
           \J 9
           \Q 10
           \K 11
           \A 12)})
       
balint's solution:

(fn pc [card]
  (let [[s r] (re-seq #"." card)]
    {:suit ({"C" :club "D" :diamond "H" :heart "S" :spade} s),
     :rank ((into (reduce #(assoc % (str %2) (- %2 2)) {} (range 10))
                 {"T" 8, "J" 9, "Q" 10, "K" 11, "A" 12})
              r)
     }))
 
amcnamara's solution:

(fn [[a b]]
  {:suit ({\D :diamond
           \H :heart
           \C :club
           \S :spade} a)
   :rank ({\T 8
           \J 9
           \Q 10
           \K 11
           \A 12} b (- (int b) 50))})
       
amalloy's solution:

(fn [[s r]]
  {:suit ({\S :spade, \H :heart, \D :diamond, \C :club} s)
   :rank ({\A 12 \K 11 \Q 10 \J 9 \T 8}
          r (- (int r) 50))})
      
stuarth's solution:

(fn [[s r]] {:suit ({"C" :club "D" :diamond "H" :heart "S" :spade} (str s))
              :rank ((zipmap (conj (vec (map str (range 2 10))) "T" "J" "Q" "K" "A") (range)) (str r))})
          
dbyrne's solution:

(fn [x]
  {:suit (condp = (first x)
            \D :diamond
            \H :heart
            \C :club
            \S :spade)
   :rank (let [y (last x)]
            (cond
              (= \A y) 12
              (= \K y) 11
              (= \Q y) 10
              (= \J y) 9
              (= \T y) 8
              :else (- (int y) 50)))})