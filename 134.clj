A nil key
Difficulty:	Elementary
Topics:	maps

Write a function which, given a key and map, returns true iff the map contains an entry with that key and its value is nil.

(true?  (__ :a {:a nil :b 2}))

(false? (__ :b {:a nil :b 2}))

(false? (__ :c {:a nil :b 2}))

#(= nil (%2 % 1))

(fn [k m] (and (contains? m k) (nil? (m k))))
adereth's solution:

1
#(nil? (get %2 % true))
mfikes's solution:

1
(fn [k m] (nil? (get m k :no)))
chouser's solution:

1
#(= nil (%2 % -))
aengelberg's solution:

1
#(= nil (%2 % 1))
chunchangshao's solution:

1
#(if (contains? %2 %) (nil? (% %2)) false)
hypirion's solution:

1
2
3
4
(fn [k coll]
  (if-let [[_ v] (find coll k)]
    (nil? v)
    false))
jafingerhut's solution:

1
#(and (contains? %2 %1) (nil? (get %2 %1)))
balint's solution:

1
(fn [k m] (nil? (get m k :not-found)))
borkdude's solution:

1
(fn [k m] (and (contains? m k) (nil? (m k))))
amcnamara's solution:

1
#(nil? (% %2 0))
amalloy's solution:

1
#(nil? (%2 % +))
stuarth's solution:

1
2
(fn [k m]
  (and (contains? m k) (nil? (m k))))
dbyrne's solution:

1
2
3
#(if (contains? %2 %)
   (not (% %2))
   false)
abedra's solution:

1
(fn [k m] (and (contains? m k) (nil? (k m))))