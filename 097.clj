;; 97 Pascal's Triangle

First row = 1.
Each successive row is computed by adding together
adjacent numbers in the row above,
and adding a 1 to the beginning and end of the row.
Returns nth row of Pascal's Triangle. 
	
#(loop [n 1
        l [1]]
   (if (= n %) l
       (recur (inc n)(vec (map + (cons 0 l)(conj l 0))))))
	
((fn [n] (letfn [(pt [] (iterate (fn [r] (map #(apply + %) (partition 2 1 (concat [0] r [0])))) [1]))] (->> (pt) (take n) (last)))) 1) ;;=> [1]

(map (fn [n] (letfn [(pt [] (iterate (fn [r] (map #(apply + %) (partition 2 1 (concat [0] r [0])))) [1]))] (->> (pt) (take n) (last)))) (range 1 6)) ;;=>
   [     [1]
        [1 1]
       [1 2 1]
      [1 3 3 1]
     [1 4 6 4 1]]
	
((fn [n] (letfn [(pt [] (iterate (fn [r] (map #(apply + %) (partition 2 1 (concat [0] r [0])))) [1]))] (->> (pt) (take n) (last)))) 11) ;;=>
   [1 10 45 120 210 252 210 120 45 10 1]

(fn [n] (letfn [(pt [] (iterate (fn [r] (map #(apply + %) (partition 2 1 (concat [0] r [0])))) [1]))] (->> (pt) (take n) (last))))

adereth's solution:

(fn [n]
  (nth
    (iterate #(map +' (concat [0] %) (concat % [0])) [1])
   (dec n)))

mfikes's solution:

(fn [n] (last (take n (iterate (fn next-row [v] (vec (map + (apply conj [0] v) (conj v 0)))) [1]))))

chouser's solution:

(fn p [x]
  (if (= x 1)
    [1]
    `[1 ~@(map + (p (- x 1)) (next (p (- x 1)))) 1]))

aengelberg's solution:

#(loop [n 1
        l [1]]
   (if (= n %) l
       (recur (inc n)(vec (map + (cons 0 l)(conj l 0))))))

chunchangshao's solution:

#(case (first %&) 1 [1] 2 [1 1] 3 [1 2 1] 4 [1 3 3 1] 5[1 4 6 4 1] 11  [1 10 45 120 210 252 210 120 45 10 1])

hypirion's solution:

(fn pascal [n]
  (condp = n
    1 [1]
    2 [1 1]
    (->>
      (pascal (- n 1))
      (partition-all 2 1)
      (map
        (partial reduce +))
      (concat [1]))))
  
jafingerhut's solution:

(fn [n]
  (nth (iterate (fn [c]
                  (concat [(first c)]
                          (map #(apply + %) (partition 2 1 (map bigint c)))
                          [(last c)]))
                [1])
       (dec n)))
   
balint's solution:

(fn [n]
  (let [pt
        (iterate
          (fn [r]
            (map #(apply + %)
                 (partition 2 1 (concat [0] r [0]))))
          [1])]
  (nth pt (dec n))))

amcnamara's solution:

#(letfn [(f [n] (loop [r 1 c 1]
                  (if (> c n)
                    r
                    (recur (* r c) (inc c)))))
         (c [n k] (/ (f n) (* (f k) (f (- n k)))))]
  (for [i (range %)]
    (c (dec %) i)))

amalloy's solution:

(fn [n]
  (-> (iterate (fn [row]
                 (map + `(0 ~@row) `(~@row 0)))
               [1])
      (nth (dec n))))
  
stuarth's solution:

(fn [n]
    (loop [n (dec n) v [1]]
      (if (zero? n)
        v
        (recur (dec n) (flatten [1 (map #(reduce + %) (partition 2 1 v)) 1])))))
    
dbyrne's solution:

(fn [x]
  (cond
    (= x 1) [1]
    (= x 2) [1 1]
    :else (loop [r 2
                 y [1 1]]
            (if (= r x)
              y
              (recur (inc r)
                     (flatten
                       [1 (map + y (rest y)) 1]))))))