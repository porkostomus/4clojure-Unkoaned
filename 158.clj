Decurry
Difficulty:	Medium
Topics:	partial-functions

Write a function that accepts a curried function of unknown arity n. Return an equivalent function of n arguments. 
You may wish to read this.

(= 10 ((__ (fn [a]
             (fn [b]
               (fn [c]
                 (fn [d]
                   (+ a b c d))))))
       1 2 3 4))

(= 24 ((__ (fn [a]
             (fn [b]
               (fn [c]
                 (fn [d]
                   (* a b c d))))))
       1 2 3 4))

(= 25 ((__ (fn [a]
             (fn [b]
               (* a b))))
       5 5))
 
#(partial (fn decur [f & args]
  (let [a (first args), rargs (rest args)]
    (if (nil? a) f (apply decur (f a) rargs)))) %)

#(partial (fn decur [f & args] (let [a (first args), rargs (rest args)] (if 
(nil? a) f (apply decur (f a) rargs)))) %)

adereth's solution:

(fn [f] (fn [& r] (reduce #(% %2) f r)))

mfikes's solution:

(fn [f] 
  (fn [& args] 
    (loop [x f args args]
      (if args
        (recur (x (first args)) (next args))
        x))))
    
chouser's solution:

(fn [f] (fn [& a] (reduce #(% %2) f a)))

aengelberg's solution:

(fn [f]
  (fn [& x]
    (reduce #(% %2) f x)))

chunchangshao's solution:

#(fn [& x] (loop [fs %, x x] (if (fn? fs) (recur (fs (first x)) (rest x)) fs)))

hypirion's solution:

(fn [f]
  (fn [& args]
    (reduce #(%1 %2) f args)))

jafingerhut's solution:

;; Note: This function does not return a function that takes only
;; exactly n arguments, the number of arguments expected by the
;; curried function.  Instead it takes a variable number of arguments,
;; and keeps applying the curried function on successive arguments
;; until the arguments run out.
 
;; I don't see how to literally do what the problem is asking for,
;; especially if we cannot assume what type of arguments the functions
;; take, and then make trial calls to the functions, and assume that
;; they are never intended to return functions.
 
(fn [f]
  (fn [& args]
    (reduce (fn [f a]
              (f a))
            f args)))
        
balint's solution:

#(partial
    (fn decur [f & args]
      (let [a (first args), rargs (rest args)]
        (if (nil? a)
          f
          (apply decur (f a) rargs))))
    %)

amcnamara's solution:

(fn d [c] #(if %& (apply (d (c %)) %&) (c %)))

amalloy's solution:

(fn [f]
  #(reduce deliver f %&))

dbyrne's solution:

(fn [x]
  (fn [& y]
    (reduce #(% %2) x y)))