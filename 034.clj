{:_id 34 :restricted ["range"], :title "Implement range"
:tests ["(= (__ 1 4) '(1 2 3))" "(= (__ -2 2) '(-2 -1 0 1))" "(= (__ 5 8) '(5 6 7))"]
:description "Write a function which creates a list of all integers in a given range."
:tags ["easy" "seqs" "core-functions"]}

user=> (source range)
(defn range
  "Returns a lazy seq of nums from start (inclusive) to end (exclusive), by step, where start defaults to 0, step to 1, and end to infinity. When step is equal to 0, returns an infinite sequence of start. When start is equal to end, returns empty list."
  ([]
   (iterate inc' 0))
  ([end]
   (if (instance? Long end)
     (clojure.lang.LongRange/create end)
     (clojure.lang.Range/create end)))
  ([start end]
   (if (and (instance? Long start) (instance? Long end))
     (clojure.lang.LongRange/create start end)
     (clojure.lang.Range/create start end)))
  ([start end step]
   (if (and (instance? Long start) (instance? Long end) (instance? Long step))
     (clojure.lang.LongRange/create start end step)
     (clojure.lang.Range/create start end step))))

(#(take (- %2 %1) (iterate inc %1)) 1 4)
;;=> (1 2 3)

adereth's solution:

(fn [start end] (take-while #(< % end)
                            (iterate inc start)))
                        
mfikes's solution:

(fn rng [low hi]
  (if (= low hi)
    []
    (concat [low] (rng (inc low) hi))))

chouser's solution:

#(take (- %2 %) (iterate inc %))

aengelberg's solution:

#(let [nAndUp (fn nAndUp [n](lazy-seq (cons n (nAndUp (inc n)))))]
   (for [i (nAndUp %1) :while (< i %2)]
      i))
  
chunchangshao's solution:

#(take (- %2 %) (iterate inc %))

hypirion's solution:

(fn -range [lo hi]
  ((fn [n acc]
    (if (< n lo)
        acc
        (recur (dec n)
               (cons n acc))))
   (dec hi) nil))

jafingerhut's solution:

(fn [x y]
  (map #(+ x %) (take (- y x) (iterate inc 0))))

balint's solution:

(fn [b e]
  (loop [n (inc b) s [b]]
    (if (= n e)
      s
      (recur (inc n) (conj s n)))))
  
amcnamara's solution:

#(take (- %2 %) (iterate inc %))

amalloy's solution:

#(take-while (fn [x] (< x %2))
             (iterate inc %))
stuarth's solution:

(fn [acc b e]
  (if (= b e)
    (seq acc)
    (recur (conj acc b) (inc b) e))) []