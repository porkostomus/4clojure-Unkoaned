dot product
Difficulty:	Easy
Topics:	seqs math

Create a function that computes the dot product of two sequences. You may assume that the vectors will have the same length.

(= 0 (__ [0 1 0] [1 0 0]))

(= 3 (__ [1 1 1] [1 1 1]))

(= 32 (__ [1 2 3] [4 5 6]))

(= 256 (__ [2 5 6] [100 10 1]))

#(reduce + (map * %1 %2))

#(reduce + (map * %1 %2))
adereth's solution:

1
(fn [v1 v2] (reduce + (map * v1 v2)))
mfikes's solution:

1
#(apply + (map * % %2))
chouser's solution:

1
#(reduce + (map * % %2))
aengelberg's solution:

1
#(apply + (map * % %2))
chunchangshao's solution:

1
#(reduce + (map * % %2))
hypirion's solution:

1
2
3
(fn [a b]
  (reduce +
    (map * a b)))
jafingerhut's solution:

1
(fn [x y] (reduce + (map * x y)))
balint's solution:

1
#(reduce + (map * %1 %2))
amcnamara's solution:

1
#(reduce + (map * % %2))
amalloy's solution:

1
#(apply + (map * % %2))
stuarth's solution:

1
#(reduce + (map * %1 %2))
dbyrne's solution:

#(apply + (map * % %2))
1
#(apply + (map * % %2))