;; 100 Least Common Multiple

Accepts variable num of positive ints or ratios 
	
(defn lcm [& args]
   (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))]
     (/ (reduce * args) (reduce gcd args))))
 2 3
6

To calculate the LCM,
first we need the GCD.

;Here's that func:

(fn [a b]
          (if
              (zero? b)
               a
              (recur b 
                       (mod a b))))

; Start with
(mod a b)

; Plug in our test args:

user> (mod 2 3)
2
user> (source mod)
(defn mod
  "Modulus of num and div.
   Truncates toward negative infinity."
  [num div] 
  (let [m (rem num div)] 
    (if (or (zero? m) (= (pos? num) (pos? div)))
      m 
      (+ m div))))

; 2 divided by 3 is 0 r2.

; Here's the GCD function:

(fn [a b]
          (if
              (zero? b)
               a
              (recur b 
                       (mod a b))))

; Just the body:

          (if
              (zero? b)
               a
              (recur b 
                       (mod a b)))

; With args:

          (if
              (zero? 3)
               2
              (recur 3 
                       (mod 2 3)))

3 is not zero, so we recur with:
a = 3
b = (mod 2 3) which is 2.
2 is also not= 0.

user> (mod 3 2)
1

          (if
              (zero? b)
               a
              (recur b 
                       (mod a b)))

So we recur with:
a = 2
b = 1

again 1 is not 0, so recur with 1 and 0.
this time, b = 0.
So we return a, which is 1.

; now that we've determined the GCD,
here's the original func again:

(defn lcm [& args]
  (let
    [gcd
      (fn [a b]
        (if (zero? b)
           a
           (recur b 
             (mod a b))))]
    (/
      (reduce * args)
      (reduce gcd args))))

; Rewritten as gcd = 1:

(defn lcm [& args]
  (let [gcd 1]
    (/
      (reduce * args)
      (reduce gcd args)))) 
 
***it multiplies them all,
   and divides it by the gcd.***

;; check it out... let's define gcd as 
its own func:

(defn gcd [a b]
        (if (zero? b)
           a
           (recur b 
             (mod a b))))

Then we need a new lcm function that will call it.

(defn lcm [& args]
    (/
      (reduce * args)
      (reduce gcd args)))

; We'll try it with 5 3 7.
; The final answer we are looking for,
; their LCM, is 105.

user> (reduce * [5 3 7])
105
user> (reduce gcd [5 3 7])
1

***Done!***

((fn [& args] (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))] (/ (reduce * args) (reduce gcd args)))) 5 3 7)
;;=> 105

((fn [& args] (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))] (/ (reduce * args) (reduce gcd args)))) 1/3 2/5)
;;=> 2

((fn [& args] (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))] (/ (reduce * args) (reduce gcd args)))) 3/4 1/6)
;;=> 3/2

((fn [& args] (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))] (/ (reduce * args) (reduce gcd args)))) 7 5/7 2 3/5)
;;=> 210

(fn [& args] (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))] (/ (reduce * args) (reduce gcd args))))
adereth's solution:

1
2
3
4
5
6
7
(fn [x & xs]
  (let [multiples (map (partial * x) (rest (range)))]
    (first
     (filter #(every? 
               (fn [y] (zero? (mod % y)))
               xs)
             multiples))))
mfikes's solution:

1
2
3
4
5
6
7
(fn [& xs]
  (let [multiples (fn [x] (iterate #(+ x %) x))]
    (loop [mult-seqs (map multiples xs)]
      (if (apply = (map first mult-seqs))
        (ffirst mult-seqs)
        (let [max-first (apply max (map first mult-seqs))]
          (recur (map #(if (< (first %) max-first) (rest %) %) mult-seqs)))))))
chouser's solution:

1
2
3
4
5
6
7
8
9
#(% %& %&)
(fn f [i o]
  (let [m (apply min i)]
    (if (apply = i)
      m
      (f (map #(if (= % m) 
                 (+ % %2)
                 %) i o)
         o))))
aengelberg's solution:

1
#({2 6 5 105 1/3 2 3/4 3/2 7 210}(first %&))
chunchangshao's solution:

1
#(case (str (first %&)) "2" 6 "5" 105 "1/3" 2 "3/4" 3/2 "7" 210)
hypirion's solution:

1
2
3
4
5
6
(fn [& r]
  (let [lcm (fn [n m]
          (/ (* n m) (#(if (zero? %2) %1 (recur %2 (mod %1 %2))) n m)))]
  (reduce
    #(lcm %1 %2)
    r)))
jafingerhut's solution:

1
2
3
4
5
6
7
8
9
(fn [& c]
  (let [c (vec c)]
    (loop [v c]
      (let [m (apply min v)
            i (ffirst (filter #(= (second %) m)
                              (map-indexed (fn [i o] [i o]) v)))]
        (if (every? #(= m %) v)
          m
          (recur (assoc v i (+ m (c i)))))))))
balint's solution:

1
2
3
4
5
6
7
8
(fn [& ys]
  (letfn [(gcd1 [a b]
            (if (zero? (rem a b))
              b
              (gcd1 b (rem a b))))
          (gcd [xs]
            (reduce gcd1 xs))]
  (/ (apply * ys) (gcd ys))))
amcnamara's solution:

1
2
3
4
5
(fn f [a & b]
  (loop [i a]
    (if (every? #(zero? (mod i %)) b)
      i
      (recur (+ i a)))))
amalloy's solution:

1
2
3
4
5
6
7
8
9
(letfn [(in-lists? [x lists]
          (if-let [[list & more] (seq lists)]
            (and (= x (first (drop-while #(< % x) list)))
                 (in-lists? x more))
            true))]
  (fn [& xs]
    (let [[source & sinks] (for [x xs]
                             (iterate #(+ % x) x))]
      (first (filter #(in-lists? % sinks) source)))))
stuarth's solution:

1
2
3
(fn [& ns]
            (some (fn [n] (if (every? #(zero? (rem n %)) ns) n))
                  (let [m (apply min ns)] (iterate (partial + m) m))))
dbyrne's solution:

1
2
3
4
5
6
(fn [& x]
  (let [y (apply min x)]
    (loop [z y]
      (if (every? #(zero? (mod z %)) x)
        z
        (recur (+ z y))))))