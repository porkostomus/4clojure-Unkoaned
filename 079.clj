{:_id 79 :title "Triangle Minimal Path"
:tests ["(= (__ [   [1]\n          [2 4]\n         [5 1 4]\n        [2 3 4 5]])\n   (+ 1 2 1 3)\n   7)"
"(= (__ [     [3]\n            [2 4]\n           [1 9 3]\n          [9 9 2 4]\n         [4 6 6 7 8]\n        [5 7 3 5 1 4]])\n   (+ 3 4 3 2 7 1)\n   20)"]
:description "Write a function which calculates the sum of the minimal path through a triangle. 
The triangle is represented as a vector of vectors. 
The path should start at the top of the triangle and move to an adjacent number on the next row
until the bottom of the triangle is reached."
:tags ["hard"]}

(= 7 (__ '([1]
          [2 4]
         [5 1 4]
        [2 3 4 5]))) ; 1->2->1->3
(= 20 (__ '([3]
           [2 4]
          [1 9 3]
         [9 9 2 4]
        [4 6 6 7 8]
       [5 7 3 5 1 4]))) ; 3->4->3->2->7->1

(fn [s]
    (first
     (reduce
      #(map + (map min (butlast %1) (rest %1)) %2)
      (reverse s))))

(fn collapse [p] (let [combine (fn [a b] (map + (map #(apply min %) (partition 2 1 a)) b))] (first (reduce combine (reverse p)))))

adereth's solution:

#(apply min
     (reduce
      (fn [a v] (map min
                     (map + (cons Double/POSITIVE_INFINITY a) v)
                     (map + (concat a [Double/POSITIVE_INFINITY]) v)))
      %))
  
mfikes's solution:

(fn triangle-min-path [coll]
  (let [trees (fn trees [[v & vs]]
                (if vs
                  (let [ts (trees vs)]
                    (map #(hash-map :node % :left %2 :right %3)
                         v ts (drop 1 ts)))
                  (map #(hash-map :node %) v)))
        min-sum (fn min-sum [t]
                  (if t
                    (+ (:node t)
                       (min (min-sum (:left t))
                            (min-sum (:right t))))
                    0))]
    (min-sum (first (trees coll)))))

chouser's solution:

(fn f
  ([t] (f 0 t))
  ([i [r & t]]
    (+ (r i)
       (if t
         (min (f i t) (f (inc i) t))
         0))))
     
aengelberg's solution:

(fn m
  ([l]
   (m l 0))
  ([l n]
   (if (empty? l)
     0
     (min (+ (nth (first l) n)
             (m (rest l) n))
          (+ (nth (first l) n)
             (m (rest l) (inc n)))))))
         
chunchangshao's solution:

#(case (count (flatten %))
   10 7
   20)

hypirion's solution:

(fn [g]
  (first
    (reduce
      #(map (fn [a [b c]] (+ a (min b c)))
             %2 (partition 2 1 %1))
      (reverse g))))
  
jafingerhut's solution:

;; This problem can be thought of as a special case of finding the
;; shortest path in a directed acyclic graph, or DAG.  Google for
;; shortest path in a DAG to find descriptions of the general
;; algorithm.  The code below is specialized for the triangle inputs
;; allowed.
 
;; Traverse through the rows from shortest to longest.
 
;; For the first row, the minimal path to each possible destination
;; (of which there is only one), is just the row itself.
 
;; For the second row, the sum of the minimal path to each of the two
;; destinations is simply the sum of the 'parent' in the first row and
;; the value in the 2nd row.
 
;; For the third and later rows, the minimal path to any element that
;; isn't the first or last element is simply the minimum of the two
;; 'parents', plus the element itself.  For the first and last
;; element, they have only one 'parent', and only one way they can be
;; reached.
 
(fn [[first-row & other-rows]]
  (apply min (reduce (fn [cur-row next-row]
                       (map + next-row
                            (concat [(first cur-row)]
                                    (map #(apply min %) (partition 2 1 cur-row))
                                    [(last cur-row)])))
                     first-row other-rows)))
                 
balint's solution:

(fn [tri]
  (letfn [(paths
            ([n] (paths n 0))
            ([n idx]
               (if (= n 1)
                 [[idx]]
                 (map #(cons idx %)
                   (concat
                     (paths (dec n) idx)
                     (paths (dec n) (inc idx))))
                )))]
    (first
      (sort
        (map
          #(apply +
            (map
              (fn [[level i]] (get level i))
              (partition 2 (interleave tri %))))
          (paths (count tri)))))))
      
bhauman's solution:

(fn self [[[a] & more]] 
  (if (empty? more)
    a
    (+ a (min (self (map butlast more)) 
              (self (map rest more))))))
          
amcnamara's solution:

(fn f [[[a] & b]]
   (+ a (if b (min (f (map rest b))
                   (f (map butlast b))) 0)))
               
amalloy's solution:

(fn [rows]
  (let [size (count rows)
        cost (partial get-in rows)]
    (letfn [(choices [[row col]]
              (when-not (zero? row)
                (for [c [col (dec col)]
                      :when (< -1 c row)]
                  [(dec row) c])))
            (path-cost [coord-seq]
              (apply + (map cost coord-seq)))
            (paths [coords]
              (let [options (choices coords)]
                (when (seq options)
                  (for [o options
                        p (or (paths o) [[]])]
                    (into [o] p)))))]
      (->> (for [col (range size)]
             [size col])
           (mapcat paths)
           (map path-cost)
           (apply min)))))
       
dbyrne's solution:

(fn [x]
  (last
   (reduce
     (fn [y z] (map #(+ (min %2 %3) %) z (rest y) y))
     (reverse x))))