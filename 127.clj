Love Triangle
Difficulty:	Hard
Topics:	search data-juggling

Everyone loves triangles, and it's easy to understand why—they're so wonderfully symmetric (except scalenes, they suck). 

Your passion for triangles has led you to become a miner (and part-time Clojure programmer) where you work all day to chip out isosceles-shaped minerals from rocks gathered in a nearby open-pit mine. There are too many rocks coming from the mine to harvest them all so you've been tasked with writing a program to analyze the mineral patterns of each rock, and determine which rocks have the biggest minerals. 

Someone has already written a computer-vision system for the mine. It images each rock as it comes into the processing centre and creates a cross-sectional bitmap of mineral (1) and rock (0) concentrations for each one. 

You must now create a function which accepts a collection of integers, each integer when read in base-2 gives the bit-representation of the rock (again, 1s are mineral and 0s are worthless scalene-like rock). You must return the cross-sectional area of the largest harvestable mineral from the input rock, as follows: 
The minerals only have smooth faces when sheared vertically or horizontally from the rock's cross-section
The mine is only concerned with harvesting isosceles triangles (such that one or two sides can be sheared)
If only one face of the mineral is sheared, its opposing vertex must be a point (ie. the smooth face must be of odd length), and its two equal-length sides must intersect the shear face at 45° (ie. those sides must cut even-diagonally)
The harvested mineral may not contain any traces of rock
The mineral may lie in any orientation in the plane
Area should be calculated as the sum of 1s that comprise the mineral
Minerals must have a minimum of three measures of area to be harvested
If no minerals can be harvested from the rock, your function should return nil

(= 10 (__ [15 15 15 15 15]))
; 1111      1111
; 1111      *111
; 1111  ->  **11
; 1111      ***1
; 1111      ****

(= 15 (__ [1 3 7 15 31]))
; 00001      0000*
; 00011      000**
; 00111  ->  00***
; 01111      0****
; 11111      *****

(= 3 (__ [3 3]))
; 11      *1
; 11  ->  **

(= 4 (__ [7 3]))
; 111      ***
; 011  ->  0*1

(= 6 (__ [17 22 6 14 22]))
; 10001      10001
; 10110      101*0
; 00110  ->  00**0
; 01110      0***0
; 10110      10110

(= 9 (__ [18 7 14 14 6 3]))
; 10010      10010
; 00111      001*0
; 01110      01**0
; 01110  ->  0***0
; 00110      00**0
; 00011      000*1

(= nil (__ [21 10 21 10]))
; 10101      10101
; 01010      01010
; 10101  ->  10101
; 01010      01010

(= nil (__ [0 31 0 31 0]))
; 00000      00000
; 11111      11111
; 00000  ->  00000
; 11111      11111
; 00000      00000

(fn [b]
  (let [z (apply max 0
            (for [b [b (vec (reverse b))]
                  y (range (count b))
                  x (range (inc (/ (Math/log (get b y 0)) (Math/log 2))))
                  [i a m] [[-1 0 0] [-1 0 1] [-1 1 0] [0 1 0] [0 1 1]]
                  :let [s (loop [m m
                                 d [i a]
                                 [l h :as r] [x x]
                                 s 0 
                                 [w & e :as b] (drop y b)] 
                                (cond
                              (and w (>= l 0) (every? #(bit-test w %) (range l (inc h)))) 
                                (recur m d (map + d r) (+ s 1 (- h l)) e)
                              (< h l) s
                              (= 0 m) s
                              (= 1 m) (recur 2 (map - d) (map - r d d) s b)))]
                  :when s]
                 s))]
    (when (> z 1) z)))

(fn mine [nums]
  (let [binaries (map #(Integer/toBinaryString %) nums)
        max-count (apply max (map count binaries))
        pad #(str (clojure.string/join (repeat (- max-count (count %)) "0")) %)
        matrix (vec (map pad binaries))
        rotate (fn [matrix] (vec (apply
                                   (partial map (comp clojure.string/join reverse vector))
                                   matrix)))
        rotations (take 4 (iterate rotate matrix))
        slice-diagonally (fn [slice]
                           (let [below (map (fn [[i j]] (vector (inc i) j)) slice)
                                 [right-i right-j] (last slice)]
                             (concat below [(vector right-i (inc right-j))])))
        slice-vertically (fn [slice]
                           (let [[fi fj] (first slice)]
                             (map vector
                                  (range (dec fi) (inc (inc (first (last slice)))))
                                  (repeat (inc fj)))))
        measure-triangle-size (fn f [slice-func matrix slice]
                                (let [next-slice (slice-func slice)
                                      next-vals (map (fn [[i j]] (-> matrix (get i) (get j))) next-slice)]
                                  (if (every? #{\1} next-vals)
                                    (+ (count slice) (f slice-func matrix next-slice))
                                    (count slice))
                                  ))
        triangles  (for [slice-func #{slice-diagonally slice-vertically}
                                          rotated-matrix rotations
                                          [i row] (map-indexed vector rotated-matrix)
                                          [j el] (map-indexed vector row)
                                          :when (= \1 el)]
                                      (measure-triangle-size slice-func rotated-matrix [[i j]]))
        max-triangle (apply max triangles)]
    (if (= 1 max-triangle) nil max-triangle)))

adereth's solution:

(fn [c]
  (let [b (vec (map (fn [x] (vec (map #(bit-test x %) (range 5)))) c))
        up [[-1 -1] [-1 0] [-1 1]]
        down [[1 -1] [1 0] [1 1]]
        left [[-1 -1] [0 -1] [1 -1]]
        right [[-1 1] [0 1] [1 1]]
        cuts (fn [d] [d (take 2 d) (drop 1 d)])
        triangle-steps (mapcat cuts [up down left right])
        
        tri (fn [p s] (take-while #(every? (fn [[y x]] (and (<= 0 x)
                                                            (<= 0 y)
                                                            (< y (count c))
                                                            (< x 5)
                                                            (get-in b [y x])
                                                            )) %)
                                  (iterate (fn [ps] (distinct (for [p ps
                                                                    t s]
                                                                (map + p t)))) [p])
                                  ))
        answer (apply max (for [x (range 5)
                     y (range (count c))
                     s triangle-steps]
                 (apply + (map count (tri [y x] s)))))
        ]
    (if (not= 1 answer) answer)
  ))

mfikes's solution:

(fn [bitmap]
  (letfn [(triangle-coords-two-face [n r-sign c-sign]
                                    (for [r (range 0 (* r-sign n) r-sign)
                                          c (range 0 (* c-sign n) c-sign) :when (< (+ (* c-sign c) (* r-sign r)) n)]
                                      [r c]))
 
          (triangle-coords-vertical-face [n c-sign]
                                         (for [r (range (inc (- n)) n)
                                               c (range 0 (* c-sign n) c-sign) :when (<= (- (* c-sign c)) r (* c-sign c))]
                                           [r c]))
 
          (triangle-coords-horizontal-face [n r-sign]
                                           (for [r (range 0 (* r-sign n) r-sign)
                                                 c (range (inc (- n)) n) :when (<= (- (* r-sign r)) c (* r-sign r))]
                                             [r c]))
 
          (digits [n]
                  (if (< n 2)
                    (list n)
                    (cons (rem n 2) (digits (quot n 2)))))
 
          (bitmap->raster [bitmap]
                          (let [unpadded (map digits bitmap)
                                width (apply max (map count unpadded))
                                padded (map #(take width (concat % (repeat 0))) unpadded)]
                            (mapv (comp vec reverse) padded)))
 
          (raster->coordinates [raster]
                               (for [r (range 0 (count raster))
                                     c (range 0 (count (first raster)))]
                                 [r c]))
 
          (offset-triangle [triangle-coords offset]
                           (map #(mapv + % offset) triangle-coords))
 
          (triangle-usable [raster triangle-coords]
                           (apply = 1 (map (partial get-in raster) triangle-coords)))
 
 
          (search [raster n largest-usable triangle-gen]
                  (let [triangl-coords (triangle-gen n)
                        raster-coords (raster->coordinates raster)]
                    (if (some #(triangle-usable raster (offset-triangle triangl-coords %)) raster-coords)
                      (recur raster (inc n) (count triangl-coords) triangle-gen)
                      largest-usable)))
 
          (uber-search [raster]
                       (let [triangle-gens (concat (for [r-sign (range -1 2)
                                                         c-sign (range -1 2) :when (not (or (zero? r-sign)
                                                                                            (zero? c-sign)))]
                                                     #(triangle-coords-two-face % r-sign c-sign))
                                                   (for [c-sign [-1 1]]
                                                     #(triangle-coords-vertical-face % c-sign))
                                                   (for [r-sign [-1 1]]
                                                     #(triangle-coords-horizontal-face % r-sign)))]
                         (apply max (map (partial search raster 2 0) triangle-gens))))]
    (let [measure (uber-search (bitmap->raster bitmap))]
      (if (< 0 measure)
        measure
        nil))))
    
chouser's solution:

(fn [b]
  (let [z (apply max 0
            (for [b [b (vec (reverse b))]
                  y (range (count b))
                  x (range (inc (/ (Math/log (get b y 0)) (Math/log 2))))
                  [i a m] [[-1 0 0] [-1 0 1] [-1 1 0] [0 1 0] [0 1 1]]
                  :let [s (loop [m m
                                 d [i a]
                                 [l h :as r] [x x]
                                 s 0 
                                 [w & e :as b] (drop y b)] 
                                (cond
                              (and w (>= l 0) (every? #(bit-test w %) (range l (inc h)))) 
                                (recur m d (map + d r) (+ s 1 (- h l)) e)
                              (< h l) s
                              (= 0 m) s
                              (= 1 m) (recur 2 (map - d) (map - r d d) s b)))]
                  :when s]
                 s))]
    (when (> z 1) z)))

chunchangshao's solution:

#(case (first %) 0 nil 21 nil 18 9 17 6 7 4 3 3 1 15 15 10)

jafingerhut's solution:

;; This is a fairly brute-force approach:
 
;; Convert input into 2-d array of 0s and 1s
 
;; For each 1 bit in the array, consider it as the vertex of an
;; isosceles triangle.
 
;; There are 8 possible "directions" for the isosceles triangle to
;; extend, up, down, left, right, and the 4 diagonal directions.  For
;; each of these 8 directions, keep extending the size of the
;; isosceles triangle in that direction as long as it contains all 1s
;; and stays within the confines of the grid.  Return its size.
 
(fn [c]
  (let [bin #(loop [x % bits '()]
               (if (zero? x)
                 bits
                 (recur (quot x 2) (conj bits (mod x 2)))))
        rock (map bin c)
        h (count c)
        w (apply max (map count rock))
        rock (vec (map (fn [row]
                         (vec (concat (repeat (- w (count row)) 0)
                                      row)))
                       rock))
        mineral? (fn [[r c]]
                   (and (< -1 r h) (< -1 c w)
                        (= 1 (get-in rock [r c]))))
        rot-135-deg-cw {[ 0  1] [ 1 -1],   ;; right -> down-left
                        [ 1  1] [ 0 -1],   ;; down-right -> left
                        [ 1  0] [-1 -1],   ;; down -> up-left
                        [ 1 -1] [-1  0],   ;; etc.
                        [ 0 -1] [-1  1],
                        [-1 -1] [ 0  1],
                        [-1  0] [ 1  1],
                        [-1  1] [ 1  0]}
        vadd (fn [[r1 c1] [r2 c2]] [(+ r1 r2) (+ c1 c2)])
        positions-in-dir (fn [pos dir]
                           (iterate #(vadd % dir) pos))
        biggest-mineral-at
        (fn [vertex dir]
          (let [rot-dir (rot-135-deg-cw dir)
                diag? (not-any? zero? rot-dir)
                mineral-row-lens
                (map-indexed (fn [idx pos]
                               (let [row-len (inc (if diag? idx (* 2 idx)))
                                     row-posns (take row-len
                                                     (positions-in-dir pos rot-dir))]
                                 (if (every? mineral? row-posns)
                                   row-len 0)))
                             (positions-in-dir vertex dir))]
            (reduce + 0 (take-while (complement zero?) mineral-row-lens))))
        biggest (apply max
                       (for [r (range h)
                             c (range w)
                             triangle-edge-dir (keys rot-135-deg-cw)]
                         (biggest-mineral-at [r c] triangle-edge-dir)))]
    (if (>= biggest 3)
      biggest)))
  
balint's solution:

(fn ltr [t]
  (let [to-binary (fn [t l]
                    (vec
                      (for [r t]
                        (->>
                          (seq (Integer/toString r 2))
                          (iterate #(cons \0 %))
                          (drop-while #(< (count %) l))
                          first
                          vec))))
        mineral-in-dir (fn [rock a d s growth]
                        (let [draw-line (fn [p slope length]
                                          (loop [p p, l length, line #{p}]
                                            (if (zero? l)
                                              line
                                              (recur (map + p slope) (dec l) (conj line p)))))]
                          (loop [a a, l 1, mineral #{a}]
                            (let [new-side (draw-line a s l)]
                              (if (every? #(= \1 (get-in rock %)) new-side)
                                (recur (map + a d) (+ l growth) (into mineral new-side))
                                (count mineral))))))
        extract-mineral (fn [rock p]
                       (apply max
                         (map
                           (fn [[d s g]] (mineral-in-dir rock p d s g))
                           [ [[1 0] [-1 1] 1], [[-1 0] [1 -1] 1], [[-1 0] [1 1] 1], [[1 0] [-1 -1] 1],
                             [[-1 -1] [0 1] 2], [[1 1] [0 -1] 2], [[-1 -1] [1 0] 2], [[1 1] [-1 0] 2] ]
                            )))]
  (let [cols (count (Integer/toString (apply max t) 2))
        mineral-max-size
          (apply max
            (for [y (range (count t)) x (range cols)]
              (extract-mineral (to-binary t cols) [y x])))]
    (when (>= mineral-max-size 3) mineral-max-size))))

amcnamara's solution:

(fn [co]
  (letfn [(get-board [ci]
            (map #(str (subs "00000" 0 (- 5 (count (seq %)))) %)
                 (map #(Integer/toBinaryString %) ci)))]
    (let [cs  (get-board co)
          ry  (count cs)
          rx  (count (first cs))
          dm  {:u [0 -1]
               :d [0  1]
               :l [-1 0]
               :r [1  0]}
          sdm {:u [:l :r]
               :d [:l :r]
               :l [:u :d]
               :r [:u :d]}]
      (letfn [(move
                ([[x y] [dx dy]]
                   (map + [x y] [dx dy]))
                ([n [x y] [dx dy]]
                   (nth (iterate #(move % [dx dy]) [x y]) n)))
              (rock? [[x y]]
                (or (some neg? [x y])
                    (>= x rx)
                    (>= y ry)
                    (= \0 (nth (nth cs y) x))))
              (count-minerals
                ([d [x y]]
                   (loop [n 0 [x y] [x y]]
                     (if (rock? [x y])
                       n
                       (recur (inc n) (move [x y] (d dm))))))
                ([d n [x y]]
                   (>= (count-minerals d [x y]) n)))]
        (let [tris (flatten
                    (for [d [:u :r :d :l]]
                      (for [y (range ry)
                            x (range rx)]
                        (let [sl (count-minerals d [x y])
                              dt (d sdm)]
                          (if (> sl 1)
                            (for [dn dt]
                              [(if (not 
                                     (some false?
                                       (map (fn [l]
                                              (count-minerals 
                                                d
                                                (- sl l)
                                                (move l [x y] (dn dm))))
                                            (range 1 sl))))
                                 (reduce + (range (inc sl)))
                                 0)
                               (let [sl (if (even? sl) (dec sl) sl)]
                                 (if (and (>= sl 3)
                                          (not 
                                            (some false?
                                              (map (fn [l]
                                                     (count-minerals 
                                                       d
                                                       (- sl (* 2 l))
                                                       (move l (move l [x y] (dn dm)) (d dm))))
                                                   (range 1 (inc (int (/ sl 2))))))))
                                   (reduce + (range 1 (inc sl) 2))
                                   0))])
                            0)))))
              max-tri (apply max (filter identity tris))]
            (if (< max-tri 3)
              nil
              max-tri))))))
          
amalloy's solution:

;;; General Approach
;;; For each point on the grid, try placing the right angle there. Then, "grow" the triangle in
;;; each of the eight directions until non-ore is encountered. Count the largest triangle created
;;; in this way.
(fn [nums]
   (let [n [-1 0] ne [-1 1] e [0 1] se [1 1]
         s [1 0] sw [1 -1] w [0 -1] nw [-1 -1]
 
         pow2 (take-while #(<= % (apply max nums))
                          (iterate #(* 2 %) 1))
         H (count nums)
         W (count pow2)
         grid (vec (for [y nums]
                     (vec
                      (for [x pow2]
                        (pos? (bit-and x y))))))
         ore? #(get-in grid %)
 
         ;; growth functions for each of the eight directions from a starting point
         growths (for [dirs [[n e] [e s] [s w] [w n]
                             [nw n ne] [ne e se]
                             [sw s se] [nw w sw]]]
                   (fn [pos]
                     (for [dir dirs]
                       (map + dir pos))))
 
         ;; given a starting point and a growth function, how many ore spaces does it find?
         size (fn [start grow]
                (->> #{start}
                     (iterate #(set ;; compute a new frontier set
                                (mapcat grow %)))
                     (take-while #(every? ore? %))
                     (map count) ;; add sizes of each ore-only frontier sets
                     (apply +)))
 
         max-ore (apply max
                        (for [y (range H) x (range W)
                              grow growths]
                          (size [y x] grow)))]
     (when (>= max-ore 3)
       max-ore)))