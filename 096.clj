;; 96 Symmetry

A binary tree is "symmetric" if the left half
is the mirror image of the right.
This is a predicate to determine whether or not
a given binary tree is symmetric.
(see To Tree, or not to Tree for a reminder
on the tree representation we're using).
	
#(= % ((fn m [[v l r]] (if v [v (m r) (m l)])) %))
	
((fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r))) '(:a (:b nil nil) (:b nil nil)))
;;=> true

((fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r))) '(:a (:b nil nil) nil))
;;=> false

((fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r))) '(:a (:b nil nil) (:c nil nil)))
;;=> false

((fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r))) [1 [2 nil [3 [4 [5 nil nil] [6 nil nil]] nil]]
          [2 [3 nil [4 [6 nil nil] [5 nil nil]]] nil]]) ;;=> true
((fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r))) [1 [2 nil [3 [4 [5 nil nil] [6 nil nil]] nil]]
          [2 [3 nil [4 [5 nil nil] [6 nil nil]]] nil]]) ;;=> false
((fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r))) [1 [2 nil [3 [4 [5 nil nil] [6 nil nil]] nil]]
          [2 [3 nil [4 [6 nil nil] nil]] nil]]) ;;=> false

(fn [[_ l r]]
  (letfn [(mirror?
            [[lx ll lr :as l] [rx rl rr :as r]]
            (or (not (or l r))
                (and (= lx rx)
                     (and (mirror? ll rr)
                          (mirror? lr rl)))))]
    (mirror? l r)))

adereth's solution:

#(let [t (fn t [[v l r]] [v (if r (t r)) (if l (t l))])
       [_ l r] %]
    (= l (t r)))

mfikes's solution:

(let [mirror (fn mirror [coll]
               (when (seq coll)
                 (list (first coll) (mirror (nth coll 2)) (mirror (second coll)))))]
  (fn [coll]
    (= coll (mirror coll))))

chouser's solution:

(fn [[_ a b]]
  ((fn m [[c d e] [f g h]]
     (if c
       (and (= c f) (m d h) (m e g))
       true))
     a b))
 
aengelberg's solution:

#(letfn [(r [l]
           (if (sequential? l)
               (cons (first l)(map r (reverse (rest l))))
               l))]
   (= % (r %)))

chunchangshao's solution:

(fn [xs] (case (count (flatten xs)) 7 (#((fn t [l r] (if (nil? l) true (if (not= (first l) (first r)) false (t (second l) (second r))))) (second %) (last %)) xs) 23 (#(if (= 6 (nth (flatten %) 16)) true false) xs) false))

hypirion's solution:

(letfn [(mirror [node]
          (if (sequential? node)
            (let [[v a b] node]
              [v (mirror b) (mirror a)])
            node))]
  (fn symmetric? [[v a b]]
    (= (mirror a) b)))

jafingerhut's solution:

(fn b [[i l r]]
  (letfn [(revtree [t]
            (if (coll? t)
              (let [[i l r] t]
                [i (revtree r) (revtree l)])
              t))]
    (= l (revtree r))))

balint's solution:

(fn [[_ left right]]
  (letfn [(symm? [[lv ll lr :as l] [rv rl rr :as r]]
    (or
      (and (nil? l) (nil? r))
      (and
        (= lv rv)
        (symm? ll rr)
        (symm? lr rl))))]
    (symm? left right)))

amcnamara's solution:

#(letfn [(f [c] (if (coll? c)
                  [(% c) (f (%2 c)) (f (%3 c))]
                  c))]
  (= (%3 %4) (f (%2 %4))))
first last second

amalloy's solution:

#(= %
    ((fn f [[v l r :as n]]
       (if n
         [v (f r) (f l)]))
     %))

stuarth's solution:

(fn [t]
    (letfn [(rev [[t a b]]
              [t (if (coll? b) (rev b) b) (if (coll? a) (rev a) a)])]
      (= t (rev t))))

dbyrne's solution:

(fn [x]
  (letfn [(m [y]
            (if (nil? y)
              nil
              [(first y)
               (m (last y))
               (m (second y))]))]
    (= (second x) (m (last x)))))