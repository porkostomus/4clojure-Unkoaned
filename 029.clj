{:_id 29 :title "Get the Caps"
:tests ["(= (__ \"HeLlO, WoRlD!\") \"HLOWRD\")"
"(empty? (__ \"nothing\"))" "(= (__ \"$#A(*&987Zf\") \"AZ\")"]
:description "Write a function which takes a string and returns a new string containing only the capital letters."
:tags ["easy" "strings"]}

(#(apply str (re-seq #"[A-Z]" %)) "HeLlO, WoRlD!")

#(apply str(filter (set (map char (range 65 91))) %))

adereth's solution:

(fn [s] (reduce str (filter #(Character/isUpperCase %) s)))

mfikes's solution:

(fn [s] (apply str (filter (fn u [x] (contains? (set (map #(char (+ (int \A) %)) (range 26))) x) )s)))

chouser's solution:

#(apply str (re-seq #"[A-Z]" %))

aengelberg's solution:

#(apply str (filter (set "ABCDEFGHIJKLMNOPQRSTUVWXYZ") %))

chunchangshao's solution:

#(let [m (re-matcher #"[A-Z]" %)] (loop [match (re-find m), res []] (if match (recur (re-find m) (conj res match)) (reduce str res) )))

hypirion's solution:

#(->>
  (re-seq #"[A-Z]" %)
  (apply str))

jafingerhut's solution:

#(apply str (re-seq #"[A-Z]+" %))

balint's solution:

#(apply str (re-seq #"[A-Z]" %))

borkdude's solution:

amcnamara's solution:

#(apply str (re-seq #"[A-Z]" %))

amalloy's solution:

#(apply str (filter (fn [c] 
                      (Character/isUpperCase c))
                    %))

stuarth's solution:

(fn [s] (apply str (re-seq #"[A-Z]" s)))

dbyrne's solution:

#(apply str (re-seq #"[A-Z]" %))