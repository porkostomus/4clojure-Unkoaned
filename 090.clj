;; 90 Cartesian Product of two sets

((fn [s1 s2] (set (for [a s1 b s2] [a b])))
 #{"ace" "king" "queen"} #{"♠" "♥" "♦" "♣"})
;;=> #{["ace"   "♠"] ["ace"   "♥"] ["ace"   "♦"] ["ace"   "♣"]
     ["king"  "♠"] ["king"  "♥"] ["king"  "♦"] ["king"  "♣"]
     ["queen" "♠"] ["queen" "♥"] ["queen" "♦"] ["queen" "♣"]}

#(set (for [a % b %2] [a b]))

((fn [s1 s2] (set (for [a s1 b s2] [a b]))) #{1 2 3} #{4 5})
;;=> #{[1 4] [2 4] [3 4] [1 5] [2 5] [3 5]}

(count ((fn [s1 s2] (set (for [a s1 b s2] [a b]))) (into #{} (range 10)) (into #{} (range 30))))
;;=> 300

(fn [s1 s2] (set (for [a s1 b s2] [a b])))

adereth's solution:

(fn [s1 s2]
  (set (for [v1 s1
             v2 s2] [v1 v2])))

mfikes's solution:

(fn cp [a b] (set (for [x a y b] [x y])))

chouser's solution:

#(set (for [a % b %2] [a b]))

aengelberg's solution:

#(set (for [x % y %2][x y]))

chunchangshao's solution:

#(set (for [i % j %2] [i j]))

hypirion's solution:

(fn cartesian-product [a b]
  (set
    (mapcat
      #(map 
        (comp (partial cons %)
              list)
        b)
    a)))

jafingerhut's solution:

#(set (for [x %1 y %2] [x y]))

balint's solution:

(fn [s1 s2]
  (set (for [a s1 b s2] [a b])))

borkdude's solution:

(fn [a b] (set (for [e1 a, e2 b] [e1 e2])))

amcnamara's solution:

(fn [a b]
  (set 
    (for [c a d b] 
      [c d])))

amalloy's solution:

#(set (for [x % y %2] [x y]))

stuarth's solution:

(fn [as bs] (into #{} (for [a as b bs] [a b])))

dbyrne's solution:

#(set (for [x %1 y %2] [x y]))