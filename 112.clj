;; 112 Sequs Horribilis [m]

Takes an int and a nested coll of ints
Returns a seq which maintains the nested structure,
and which includes all elements starting from the head
whose sum is less than or equal to the input integer.

(=  (__ 10 [1 2 [3 [4 5] 6] 7])
   '(1 2 (3 (4))))
(=  (__ 30 [1 2 [3 [4 [5 [6 [7 8]] 9]] 10] 11])
   '(1 2 (3 (4 (5 (6 (7)))))))
(=  (__ 9 (range))
   '(0 1 2 3))
(=  (__ 1 [[[[[1]]]]])
   '(((((1))))))
(=  (__ 0 [1 2 [3 [4 5] 6] 7])
   '())
(=  (__ 0 [0 0 [0 [0]]])
   '(0 0 (0 (0))))
(=  (__ 1 [-10 [1 [2 3 [4 5 [6 7 [8]]]]]])
   '(-10 (1 (2 3 (4)))))

(fn d [n [h & t]]
   (cond
    (nil? h) []
    (coll? h)  [(d n h)]
    (>= n h) (concat [h] (d (- n h) t))
    :else []))

(fn [n s]
  (second 
   ((fn sequs [n s]
      (loop [cnt 0 acc [] [x & xs] s]
        (cond
         (or (nil? x) (< n cnt)) [cnt acc]
         (coll? x) (let [[c r] (sequs (- n cnt) x)
                         coll (if (empty? r) acc (conj acc r))]
                     (recur (+ c cnt) coll xs))
         :else (recur (+ x cnt) (if (< n (+ cnt x)) acc (conj acc x)) xs)))) 
    n s)))

adereth's solution:

(fn [n s]
  (let [b (fn b [v]
              (let [l (last v)]
                (if (vector? l)
                  (if (= (count l) 1)
                    (apply vector (butlast v))
                    (conj (apply vector (butlast v)) (b l)))
                  (apply vector (butlast v)))))
        t (fn s [v] (reduce + (map #(if (vector? %)
                                       (s %) %) v)))
        ]
    (loop [s (take 100 s)]
      (if (< n (t s))
        (recur (b s))
        s))
    ))

mfikes's solution:

(fn sequs-horriblis [n [x & r]]
  (if x
    (if (sequential? x)
      (let [sub (sequs-horriblis n x)]
        (cons sub (sequs-horriblis (- n (reduce + (flatten sub))) r)))
      (if (<= x n)
        (cons x (sequs-horriblis (- n x) r))
        ()))))
    
chouser's solution:

(fn [limit xs]
  (second ((fn f [[limit] xs]
            (if (coll? xs)
              (let [rs (take-while #(<= 0 (first %))
                          (reductions f [limit] xs))]
                [(first (last rs)) (map second (rest rs))])
              [(- limit xs) xs]))
           [limit] xs)))
       
aengelberg's solution:

(fn f [n l]
   (cond
    (empty? l) ()
    (number? (first l)) (if (<= (first l) n)
                          (cons (first l)(f (- n (first l))(rest l)))
                          [])
    :else (let [x (f n (first l))]
            (if x
              (cons x (f (- n (apply + (flatten x))) (rest l)))
              [x]))))
          
chunchangshao's solution:

(fn [s xs] ((fn a [xs n] (loop [xs xs h (first xs) res [],n n] (if h (if (sequential? h) (conj res (a h n)) (if (>= s (+ n h)) (recur (next xs) (second xs) (conj res h) (+ n h)) res)) res))) xs 0))

hypirion's solution:

(fn sequs [n [f & r :as s]]
  (cond (empty? s) 
          ()
        (coll? f) 
          (let [res (sequs n f)]
            (if (not= res f) 
              [res]
              (cons f 
                (sequs (- n (reduce + (flatten f)))
                       r))))
        (<= f n)
          (cons f
            (sequs (- n f) r))
        :else ()))
    
jafingerhut's solution:

(fn [limit c]
  (let [ifirst (fn [c]
                 (first (remove coll? (iterate first c))))
        p (fn p [limit acc s]
            (if-let [[h & t] (seq s)]
              (cond
               ;; Stop if we find a too-large value.
               (> (ifirst h) limit) [limit acc s]
               ;; Recursively calculate the answer for the
               ;; sub-collection, then continue with whatever comes
               ;; next.
               (coll? h) (let [[new-limit c _] (p limit [] h)]
                           (recur new-limit (conj acc c) (rest s)))
               :else (recur (- limit h) (conj acc h) (rest s)))
              [limit acc nil]))]  ;; stop if we reach the end of s
    (second (p limit [] c))))

balint's solution:

(fn sh [n coll]
  (loop [n n, [f & r :as c] coll, out []]
    (cond
      (coll? f) (conj out (sh n f))
      (or (nil? f) (> f n)) out
      :else
        (recur (- n f) r (conj out f)))))
    
amcnamara's solution:

(fn s [n [a & b :as c]]
  (if (empty? c)
    []
    (if (coll? a)
      [(s n a)]
      (if (>= n a)
        (concat [a] (s (- n a) b))
        []))))
    
amalloy's solution:

(fn [n coll]
   (second ((fn sequs [n coll]
              (if (empty? coll)
                [n ()]
                (let [x (first coll)]
                  (cond (coll? x) (let [[remain ret] (sequs n x)
                                        [remain' ret'] (sequs remain (rest coll))]
                                    [remain' (cons ret ret')])
                        (> x n) [Double/NEGATIVE_INFINITY ()]
                        :else (let [[remain ret] (sequs (- n x) (rest coll))]
                                [remain (cons x ret)])))))
            n coll)))
        
dbyrne's solution:

(fn f [n s]
  (if-let [x (first s)]
    (if (sequential? x)
      [(f n x)]
      (if (< (- n x) 0)
        []
        (cons x (f (- n x) (rest s)))))))