;; 116 Prime Sandwich [m]

Tests if num is a prime number which is also the
mean of the primes directly before and after it
in the sequence of valid primes.

(defn balanced-prime? [n]
  (let [factors (cons 2 (iterate (partial + 2) 3)) 
        prime? (fn [n] (not-any? #(zero? (mod n %))
          (take-while #(<= % (inc (Math/sqrt n)))
            factors)))
        prime-step (fn [n s] (first (drop-while 
          (complement prime?) (rest (iterate
            (partial + s) n)))))]
    (and (> n 3)
         (prime? n)
         (= n (/ (+ (prime-step n 2) (prime-step n -2))
                  2)))))
#'user/balanced-prime?
user> (balanced-prime? 4)
false
user> (balanced-prime? 563)
true
user> (nth (filter balanced-prime? (range)) 15)
1103

(fn balanced-prime? [n] (let [factors (cons 2 (iterate (partial + 2) 3)) 
prime? (fn [n] (not-any? #(zero? (mod n %)) (take-while #(<= % (inc 
(Math/sqrt n))) factors))) prime-step (fn [n s] (first (drop-while 
(complement prime?) (rest (iterate (partial + s) n)))))] (and (> n 3) 
(prime? n) (= n (/ (+ (prime-step n 2) (prime-step n -2)) 2)))))

adereth's solution:

(fn [n]
  (and (> n 3)
  (let [p (fn [x] (every? #(< 0 (mod x %)) (range 2 x)))
        b (first (filter p (reverse (range 2 n))))
        a (first (filter p (drop (+ n 1) (range))))]
    (and (p n) (= n (/ (+ a b) 2))))))

mfikes's solution:

(fn [n]
  (let [prime? (fn [n] (not-any? #(zero? (rem n %)) (range 2 n)))]
    (if (and (< 4 n)
             (prime? n))
      (let [prev-prime (first (filter prime? (iterate dec (dec n))))
            next-prime (first (filter prime? (iterate inc (inc n))))]
        (= (* 2 n) (+ prev-prime next-prime)))
      false)))
  
chouser's solution:

#(first (for [o (range 1 (- % 2))
              [a b c] [(for [x [(- % o) (+ % o) %]]
                         (every? (fn [b] (> (rem x b) 0)) (range 2 x)))]
              :when (or a b)]
          (and a b c)))
      
aengelberg's solution:

(let [p (fn [x] (every? #(not (integer? (/ x %))) (range 2 x)))
      l (filter p (range 2 Double/POSITIVE_INFINITY))]
  (fn [x]
    (and (p x)
         (let [a (last (cons -100 (take-while #(< % x) l)))
               b (first (drop-while #(<= % x) l))]
           (= x (/ (+ a b) 2))))))
       
chunchangshao's solution:

#(case %
   (0 1 2 3 5 6 7 8 9 10 11 12 13 14 563 1103)  true
   false)

hypirion's solution:

(fn [n]
  (let [p? (fn [n] (if (< 1 n) 
                    (not-any? #(= 0 (rem n %)) (range 2 n))))]
    (or
      (if (p? n)
        (if-let [lo (first (filter p? (range (- n 1) 1 -1)))]
          (= n (/ (+ lo (first (filter p? (iterate inc (+ n 1))))) 
                  2))))
      false)))
  
jafingerhut's solution:

(fn [n]
  (letfn [(primes
            ([] (cons 2 (primes [] 3)))
            ([prime-vec n]
               (if (let [len (count prime-vec)]
                     (loop [i 0]
                       (if (== i len)
                         true
                         (let [x (prime-vec i)]
                           (cond (> (* x x) n) true
                                 (zero? (mod n x)) false
                                 :else (recur (inc i)))))))
                 (lazy-seq
                  (cons n (primes (conj prime-vec n) (+ n 2))))
                 (recur prime-vec (+ n 2)))))]
    (let [[p1 p2 p3] (first (filter #(>= (second %) n)
                                    (partition 3 1 (primes))))]
      (and (= n p2)
           (= n (/ (+ p1 p3) 2))))))
       
balint's solution:

(fn [n]
  (letfn [(prime? [p]
    (and (>= p 2)
      (or (= p 2)
          (every? #(> (rem p %) 0)
                  (range 2 (inc (quot p 2)))))))]
  (and
    (> n 2)
    (prime? n)
    (let [p1 (first (filter prime? (iterate dec (dec n))))
          p2 (first (filter prime? (iterate inc (inc n))))]
      (== n (/ (+ p1 p2) 2))))))
  
amcnamara's solution:

((fn [p]
   (fn [b]
     (let [[a c] (map #(first (filter p (rest (iterate % b)))) [dec inc])]
       (and (> b 2)
            (p b)
            (= b (/ (+ a c) 2))))))
 (fn [i] (not-any? #(= 0 (mod i %)) (range 2 i))))

amalloy's solution:

(let [all-primes ((fn primes [known]
                    (lazy-seq
                     (let [start (inc (last known))
                           more-nums (iterate inc start)
                           next (first (for [n more-nums
                                             :when (not-any? #(zero? (mod n %)) known)]
                                         n))]
                       (cons next (primes (conj known next))))))
                  [2])]
  (fn [n]
    (let [[a b c] (->> all-primes
                       (partition 3 1)
                       (drop-while #(< (second %) n))
                       first)]
      (and (= b n)
           (= (- b a) (- c b))))))