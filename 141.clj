Tricky card games
Difficulty:	Medium
Topics:	game cards

In trick-taking card games such as bridge, spades, or hearts, cards are played in groups known as "tricks" - 
each player plays a single card, in order; the first player is said to "lead" to the trick.
After all players have played, one card is said to have "won" the trick.
How the winner is determined will vary by game, but generally the winner is the highest card played in the suit that was led.
Sometimes (again varying by game), a particular suit will be designated "trump",
meaning that its cards are more powerful than any others: if there is a trump suit, and any trumps are played,
then the highest trump wins regardless of what was led.

Your goal is to devise a function that can determine which of a number of cards has won a trick.
You should accept a trump suit, and return a function winner.
Winner will be called on a sequence of cards, and should return the one which wins the trick.
Cards will be represented in the format returned by Problem 128, Recognize Playing Cards:
a hash-map of :suit and a numeric :rank. Cards with a larger rank are stronger.

(let [notrump (__ nil)]
  (and (= {:suit :club :rank 9}  (notrump [{:suit :club :rank 4}
                                           {:suit :club :rank 9}]))
       (= {:suit :spade :rank 2} (notrump [{:suit :spade :rank 2}
                                           {:suit :club :rank 10}]))))

(= {:suit :club :rank 10} ((__ :club) [{:suit :spade :rank 2}

(= {:suit :heart :rank 8}
   ((__ :heart) [{:suit :heart :rank 6} {:suit :heart :rank 8}
                 {:suit :diamond :rank 10} {:suit :heart :rank 4}]))

(fn [trump]
  (fn [hand]
    (let [by-suit (group-by :suit hand)
          win-suit (or trump (:suit (first hand)))]
      (last (sort-by :rank (win-suit by-suit))))))

(fn [trump]
  (fn [hand]
    (let [by-suit (group-by :suit hand)
          win-suit (or trump (:suit (first hand)))]
      (last (sort-by :rank (win-suit by-suit))))))
  
adereth's solution:

(fn [s]
  (fn [c]
    (->> c 
         (filter #(= (:suit %) (if s s (:suit (first c)))))
         (sort-by :rank)
         last)))
     
mfikes's solution:

(fn [trump-suit]
  (fn [[lead :as trick]]
    (apply max-key (fn [card]
                     (+ (:rank card)
                        (if (= (:suit card) (:suit lead)) 13 0)
                        (if (= (:suit card) trump-suit) 26 0)))
           trick)))
       
chouser's solution:

#(fn [s] (let [t :suit g (group-by t s)] (apply max-key :rank (g % (-> 0 s t g)))))

aengelberg's solution:

(fn [s]
  (let [s (or s :spade)]
    (fn [l]
      (let [t (filter #(= (:suit %) s) l)]
        (apply max-key :rank
               (if (empty? t) l t))))))
           
chunchangshao's solution:

(fn [suit]
  (fn [cards]
    (let [suit (if (nil? suit) (:suit (first cards)) suit)] 
      (->> (filter #(= (:suit %) suit) cards)
           (apply max-key :rank)))))
       
hypirion's solution:

(fn [suit]
  (fn [[f & _ :as coll]]
    (letfn [(bests [suit]
              (seq (sort-by :rank
                (filter #(= (:suit %) suit) coll))))]
    (last
      (or
        (bests suit)
        (bests (:suit f))
        (sort-by :rank coll))))))
    
jafingerhut's solution:

;; Determine highest rank card of trump suit, if any, returning it if
;; there is one.  Else determine highest rank card of lead suit.
 
(fn [trump-suit]
  (fn [cards]
    (let [high-card-with-suit
          (fn [suit cards]
            (if-let [cards-matching-suit (seq (filter #(= suit (:suit %))
                                                      cards))]
              (apply max-key :rank cards-matching-suit)))]
      (or (high-card-with-suit trump-suit cards)
          (high-card-with-suit (:suit (first cards)) cards)))))
      
balint's solution:

(fn [trump]
  (fn [trick]
    (let [suit-order
            (if trump
              [(:suit (first trick)) trump]
              [(:suit (first trick))])
            ]
      (reduce
        (fn [highest card]
          (cond
            (> (.indexOf suit-order (:suit card))
               (.indexOf suit-order (:suit highest)))
              card
            (and (= (:suit highest) (:suit card))
                    (> (:rank card) (:rank highest)))
              card
            :else
              highest))
          trick))))
      
amcnamara's solution:

(fn [trump]
  (fn [trick]
    (loop [[card & remaining] trick lead card]
      (if card
        (recur remaining (if (and (or (= (:suit card) (:suit lead)) (= trump (:suit card))) (> (:rank card) (:rank lead))) card lead))
        lead))))
    
amalloy's solution:

;; short
(fn [t]
  #(reduce (fn [a {s :suit :as b}]
             (cond (= (:suit a) s) (max-key :rank a b)
                   (= s t) b
                   :else a))
           %))
 
#_
;; readable
(letfn [(same-suit? [& cards]
          (apply = (map :suit cards)))
        (bigger [& cards]
          (apply max-key :rank cards))]
  (fn [trump]
    (letfn [(trump? [c]
              (= (:suit c) trump))
            (winner [a b]
              (cond (same-suit? a b) (bigger a b)
                    (trump? b) b
                    :else a))]
      (fn [cards]
        (reduce winner cards)))))