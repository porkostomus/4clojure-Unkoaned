Intro to Destructuring 2
Difficulty:	Easy
Topics:	Destructuring

Sequential destructuring allows you to bind symbols to parts of sequential things (vectors, lists, seqs, etc.):
(let [bindings* ] exprs*) Complete the bindings so all let-parts evaluate to 3.

(= 3
  (let [[__] [+ (range 3)]] (apply __))
  (let [[[__] b] [[+ 1] 2]] (__ b))
  (let [[__] [inc 2]] (__)))

f vs

f vs
adereth's solution:

1
a c
mfikes's solution:

1
plus one
chouser's solution:

1
f x
aengelberg's solution:

1
x y
chunchangshao's solution:

1
a c
hypirion's solution:

1
a c
jafingerhut's solution:

1
x y
balint's solution:

1
f n
amcnamara's solution:

1
a c
amalloy's solution:

1
+ coll
dbyrne's solution:

1
f a