Infix Calculator
Difficulty:	Easy
Topics:	higher-order-functions math

Your friend Joe is always whining about Lisps using the prefix notation for math.
Show him how you could easily write a function that does math using the infix notation.
Is your favorite language that flexible, Joe?
Write a function that accepts a variable length mathematical expression consisting of numbers
and the operations +, -, *, and /.
Assume a simple calculator that does not do precedence and instead just calculates left to right.

(= 7  (__ 2 + 5))

(= 42 (__ 38 + 48 - 2 / 2))

(= 8  (__ 10 / 2 - 1 * 2))

(= 72 (__ 20 / 2 + 2 + 4 + 8 - 6 - 10 * 9))

(fn f [a o b & c]
  (if c
    (apply f (o a b) c)
    (o a b)))

(fn calc [& exp] (reduce #(if (fn? %1) (%1 %2) (partial %2 %1)) identity exp))

adereth's solution:

(fn [firstlhs & t]
  (let [oprhs (partition 2 t)]
    (reduce (fn [lhs [op rhs]] (op lhs rhs)) firstlhs oprhs)))

mfikes's solution:

(fn f [a o b & c]
  (if c
    (apply f (o a b) c)
    (o a b)))

chouser's solution:

(fn f
  ([a] a)
  ([a b c & m]
   (apply f (b a c) m)))

aengelberg's solution:

#(loop [n (first %&)
        l  (rest %&)]
  (if (empty? l)
      n
      (recur ((first l) n (second l)) (nnext l))))
  
hypirion's solution:

(fn [a & r]
  (reduce
    (fn [a [op b]] (op a b))
    a (partition 2 r)))

jafingerhut's solution:

(fn f [x op y & r]
  (let [v (op x y)]
    (if r
      (apply f v r)
      v)))
  
balint's solution:

(fn [& tokens]
  (let [[[a o b] t] (split-at 3 tokens)]
    (reduce
      (fn [r [op x]] (op r x))
      (o a b)
      (partition 2 t))))
  
amcnamara's solution:

(fn f [a o b & c]
  (if c
    (apply f (o a b) c)
    (o a b)))

amalloy's solution:

(fn c [x f y & r]
  ((if r
     #(apply c % r) +)
   (f x y)))

stuarth's solution:

(fn [& [a op b & r]]
    (if op
      (recur (cons (op a b) r))
      a))
  
dbyrne's solution:

(fn [& x]
  (if (= (count x) 1)
    (first x)
    (recur
      (cons ((second x) (first x) (nth x 2))
            (drop 3 x)))))