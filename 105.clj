;; 105 Identify keys and values [m]

Takes seq of keys and vals, returns a map such that
each key in the map is a keyword,
and the value is a seq of all the numbers (if any)
between it and the next keyword in the sequence.

(defn key-val [c]
  (loop [[f & r] c, kvm {}]
    (if (nil? f)
      kvm
     (let [[vs l] (split-with (complement keyword?) r)]
       (recur l (assoc kvm f vs))))))
#'user/key-val
user> (key-val [])
{}
user> (key-val [:a 1])
{:a (1)}
user> (key-val [:a 1 :b 2])
{:a (1), :b (2)}
user> (key-val [:a 1 2 3 :b :c 4])
{:a (1 2 3), :b (), :c (4)}

(fn kv [acc k [v & vs]]
  (cond (nil? v) acc
        (keyword? v) (kv (assoc acc v []) v vs)
        :e (kv (update-in acc [k] conj v) k vs))) {} *
adereth's solution:

1
2
3
4
(fn mf [s]
  (if (seq s)
    (merge {(first s) (take-while (complement keyword?) (rest s))}
           (mf (drop-while (complement keyword?) (rest s)))) {} ))
mfikes's solution:

1
2
3
4
5
6
(fn [coll]
(let [partitioned (partition-by keyword? coll)
      keys (map last (take-nth 2 partitioned))
      values (take-nth 2 (rest partitioned))
      empty-keys (remove nil? (flatten (map butlast (take-nth 2 partitioned))))]
  (merge (zipmap keys values) (apply hash-map (interleave empty-keys (repeat []))))))
chouser's solution:

1
2
3
4
5
(fn f [[k & v]]
  (if v
    (let [[a b] (split-with number? v)]
      (assoc (f b) k a))
    {}))
aengelberg's solution:

1
2
3
4
5
6
7
8
9
10
11
(fn [l]
  (let [f #(if (keyword? %) % true)
        p (partition-by f l)]
    (loop [p p
           x {}]
      (cond
       (= p ()) x
       (= true (f (first (second p)))) (recur (drop 2 p)
                                     (assoc x (first (first p)) (second p)))
       :else (recur (rest p)
                    (assoc x (first (first p)) []))))))
chunchangshao's solution:

1
(fn [xs] (apply hash-map (apply concat [] (map #(if (keyword? (first %)) (interpose [] %) [%]) (partition-by keyword? xs)))))
hypirion's solution:

1
2
3
4
5
6
(fn ident
  ([v] (ident v nil {}))
  ([[f & r :as s] k hm]
    (cond (empty? s) hm
          (keyword? f) (recur r f (assoc hm f []))
          :else (recur r k (update-in hm [k] conj f)))))
jafingerhut's solution:

1
2
3
4
5
6
7
#(let [nk (complement keyword?)]
   (loop [c (drop-while nk %)
          m {}]
     (if-let [c (seq c)]
       (let [[vs x] (split-with nk (next c))]
         (recur x (assoc m (first c) vs)))
       m)))
balint's solution:

1
2
3
4
5
6
(fn [c]
  (loop [[f & r] c, kvm {}]
    (if (nil? f)
      kvm
      (let [[vs l] (split-with (complement keyword?) r)]
        (recur l (assoc kvm f vs))))))
amcnamara's solution:

1
2
3
4
5
6
7
#(loop [r {} c %5]
  (if (empty? c)
    r
    (recur (into r (hash-map (% c)
                             (% (%3 %4 (%2 c)))))
           (second (%3 %4 (%2 c))))))
first rest split-with integer?
amalloy's solution:

1
2
3
4
5
6
(fn r [a c [x & y]]
  (if x
    (if (ifn? x)
      (r (assoc a x []) x y)
      (r (update-in a [c] conj x) c y))
    a)) {} =
stuarth's solution:

1
2
3
4
5
6
7
(fn [s]
    (loop [s s d {}]
      (if (empty? s)
        d
        (let [k (first s)
              [v r] (split-with number? (rest s))]
         (recur r (assoc d k v))))))
dbyrne's solution:

1
2
3
4
5
6
7
(fn x
  ([y] (x y {}))
  ([y z]
    (if (empty? y)
      z
      (x (drop-while number? (rest y))
         (assoc z (first y) (take-while number? (rest y)))))))