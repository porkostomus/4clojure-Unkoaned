{:_id 80 :title "Perfect Numbers" :tests [
"(= (__ 6) true)" "(= (__ 7) false)" "(= (__ 496) true)" "(= (__ 500) false)" "(= (__ 8128) true)"]
:description "A number is \"perfect\" if the sum of its divisors equal the number itself.
6 is a perfect number because 1+2+3=6.  Write a function which returns true for perfect numbers and false otherwise."
:tags ["medium"]}

(= (__ 6) true)
(= (__ 7) false)
(= (__ 496) true)
(= (__ 500) false)
(= (__ 8128) true)

(fn [x] (= x (apply + (filter #(= 0 (mod x %)) (range 1 x)))))

(fn [n] (= n (apply + (filter #(zero? (rem n %)) (range 1 (max 2 (inc (quot 
n 2))))))))

adereth's solution:

(fn [x]
  (let [factors (filter #(zero? (mod x %)) (range 1 x))]
    (= x (apply + factors))))

mfikes's solution:

(fn [n]
  (let [divisors (filter #(zero? (rem n %)) (range 1 n))]
    (= n (apply + divisors))))

chouser's solution:

#(= % (apply + (for [i (range 1 %) :when (= 0 (mod % i))] i)))

aengelberg's solution:

#(boolean (#{6 496 8128} %))

chunchangshao's solution:

#(case % 6 true 496 true 8128 true false)

hypirion's solution:

(fn [n]
  (->>
    (range 1 n)
    (filter #(= 0 (rem n %)))
    (reduce +)
    (= n)))

jafingerhut's solution:

(fn [n]
  (letfn [(d [n]
            (filter #(zero? (mod n %))
                    (range 1 (inc (/ n 2)))))]
    (= n (reduce + (d n)))))

balint's solution:

(fn [n]
  (= n
    (apply +
      (filter
        #(zero? (rem n %))
        (range 1 (max 2 (inc (quot n 2))))))))
    
bhauman's solution:

(fn [x]
  (= x
     (reduce + (filter #(integer? (/ x %)) (range 1 (inc (int (/ x 2))))))))
 
amcnamara's solution:

(fn [n]
  (= n (apply + (filter #(= 0 (mod n %)) (range 1 n)))))

stuarth's solution:

(fn [n]
    (= n (reduce + (set (for [i (range 1 n) :when (zero? (mod n i))] i)))))