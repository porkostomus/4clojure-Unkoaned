{:_id 69 :restricted ["merge-with"], :title "Merge with a Function"
:tests [
"(= (__ * {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})\n   {:a 4, :b 6, :c 20})"
"(= (__ - {1 10, 2 20} {1 3, 2 10, 3 15})\n   {1 7, 2 10, 3 15})"
"(= (__ concat {:a [3], :b [6]} {:a [4 5], :c [8 9]} {:b [7]})\n   {:a [3 4 5], :b [6 7], :c [8 9]})"]
:description "Write a function which takes a function f and a variable number of maps.
Your function should return a map that consists of the rest of the maps conj-ed onto the first.
If a key occurs in more than one map,
the mapping(s) from the latter (left-to-right) should be combined with the mapping in the result
by calling (f val-in-result val-in-latter)"
:tags ["medium" "core-functions"]}

(fn [f & ms]
  (reduce (fn [am m]
            (into am (for [[k v] m]
                       (if (contains? am k)
                         [k (f (am k) v)]
                         [k v]))))
          ms))

(fn [f & args]
  (reduce (fn[map1 map2]
            (reduce (fn [m [k v]]
                      (if-let [vv (m k)]
                        (assoc m k (f vv v))
                        (assoc m k v)))
                    map1 map2))
args))

adereth's solution:

(fn [op & maps]
  (reduce
    (fn [m1 m2]
      (let [keys1 (set (keys m1))
            keys2 (set (keys m2))
            shared (clojure.set/intersection keys1 keys2)
            unchanged-part (conj
              (select-keys m1 keys1)
              (select-keys m2 keys2))
          ]
          (reduce (fn [m k] 
            (assoc m k (op (m1 k) (m2 k))))
            unchanged-part
            shared)))
    maps))

mfikes's solution:

(fn [f & ms]
  (reduce (fn [am m]
            (into am (for [[k v] m]
                       (if (contains? am k)
                         [k (f (am k) v)]
                         [k v]))))
          ms))
      
chouser's solution:

(fn f
  ([g x] x)
  ([g a b & m]
    (apply f g (reduce (fn [c [k v]]
                           (assoc c k (if (c k) (g (c k) v) v)))
                       a b)
                   m)))

aengelberg's solution:

(fn [f & m]
  (into {}
    (for [[k _] (apply merge m)]
      [k (#(if (second %)
               (apply f %)
               (first %))
            (filter identity (map #(% k) m)))])))
        
chunchangshao's solution:

#(->> (group-by first (apply concat [] %&))
      (map (fn [[k v]] [k (reduce % (map second v))]))
      (into {}))

hypirion's solution:

(fn m-with [f a b & r]
  (let [merged (apply merge a b
                (for [[k v] b :when (a k)]
                  {k (f (a k) v)}))]
    (if (empty? r)
      merged
      (apply m-with f merged r))))
  
jafingerhut's solution:

(fn [f m & ms]
  (reduce (fn [m n]
            (into m (for [[k v] n]
                      [k (if (contains? m k)
                           (f (m k) v)
                           v)])))
          m ms))
      
balint's solution:

(fn [f & maps]
    (reduce (fn [merged m]
      (reduce
        (fn [merged [k v]]
          (if (merged k)
            (assoc merged k (f (merged k) v))
            (assoc merged k v)))
        merged
        m))
      {}
      maps))
  
amcnamara's solution:

(fn [o & h]
  (reduce into
    (for [k (keys (reduce into h))]
      {k (#(if (next %) 
             (apply o %)
             (first %))
          (filter (complement nil?)
                  (map #(get % k) h)))})))
              
amalloy's solution:

(fn [f & maps]
  (reduce (fn [acc m]
            (reduce (fn [acc [k v]]
                      (if (contains? acc k)
                        (update-in acc [k] f v)
                        (assoc acc k v)))
                    acc m))
          {} maps))
      
stuarth's solution:

(fn [op & ms]
    (reduce
     (fn [a b]
       (merge a
              (into {}
                    (map (fn [k] {k (if (contains? a k) (op (a k) (b k)) (b k))}) (keys b)))))
     ms))