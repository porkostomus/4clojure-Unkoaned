;; 114 Global take-while [m]

take-while is great for filtering sequences, but it limited:
you can only examine a single item at a time.
What if you need to keep track of some state
as you go over the sequence?

Takes an integer n, a predicate p, and a sequence.
It should return a lazy sequence of items in the list up to,
but not including, the nth item that satisfies the predicate.

(= [2 3 5 7 11 13]
   (__ 4 #(= 2 (mod % 3))
         [2 3 5 7 11 13 17 19 23]))
(= ["this" "is" "a" "sentence"]
   (__ 3 #(some #{\i} %)
         ["this" "is" "a" "sentence" "i" "wrote"]))
(= ["this" "is"]
   (__ 1 #{"a"}
         ["this" "is" "a" "sentence" "i" "wrote"]))

(fn t [n p coll]
 (lazy-seq
   (let [[x & xs] coll
         n (if (p x) (dec n) n)]
     (when (> n 0)
       (cons x (t n p xs))))))

(fn gtw [n f [h & t]] (when-not (or (zero? n) (and (= n 1) (f h))) (cons h 
(gtw (if (f h) (dec n) n) f t))))

adereth's solution:

(fn [n p s]
  (butlast
  (map first
  (take-while #(> n (second %))
  (map vector
       s
       (reductions (fn [a v] (if (p v) (inc a) a)) 0 s))))))
   
mfikes's solution:

(fn global-take-while [n p [x & r]]
  (lazy-seq
    (when x
      (let [remaining (if (p x) (dec n) n)]
        (when (pos? remaining)
          (cons x (global-take-while remaining p r)))))))
      
chouser's solution:

(fn f [i p [x & r :as s]]
  (lazy-seq
    (let [i (if (p x) (- i 1) i)]
      (if (> i 0)
        (cons x (f i p r))))))
    
aengelberg's solution:

#(loop [l []
        x %3
        n %]
   (if
    (= n 1)(if
            (%2 (first x)) l
            (recur (conj l (first x))(rest x) n))
    (recur (conj l (first x))
           (rest x)
           (if (%2 (first x))
             (dec n)
             n))))
         
chunchangshao's solution:

(fn [n f c] (take (nth (keep-indexed #(if (f %2) % nil) c) (dec n)) c))

hypirion's solution:

(fn state-while [n fun [f & r]]
  (if (fun f)
    (if (= n 1) nil
        (cons f (state-while (- n 1) fun r)))
    (cons f (state-while n fun r))))

jafingerhut's solution:

(fn take-while [n p s]
  (lazy-seq
   (when-let [[x & xs] (seq s)]
     (let [n (- n (if (p x) 1 0))]
       (if (pos? n)
         (cons x (take-while n p xs)))))))
     
balint's solution:

(fn [n p s]
  (loop [n n, p p, [fs & rs] s, q []]
    (if
      (or (nil? s) (and (p fs) (= n 1))) q
      (recur (if (p fs) (dec n) n) p rs (conj q fs)))))
  
amcnamara's solution:

(fn f [r n p [a & b]]
  (if (p a)
    (if (= n 1)
      r
      (f (conj r a) (dec n) p b))
    (f (conj r a) n p b)))
[]

amalloy's solution:

(fn f [n pred coll]
  (lazy-seq
   (when-let [[x & xs] (seq coll)]
     (let [n (if (pred x), (dec n), n)]
       (when-not (zero? n)
         (cons x (f n pred xs)))))))
     
dbyrne's solution:

(fn f [n p s]
  (let [x (first s)
        z (if (p x) (dec n) n)]
    (when (> z 0) 
      (cons x (f z p (rest s))))))