{:_id 77 :title "Anagram Finder"
:tests [
"(= (__ [\"meat\" \"mat\" \"team\" \"mate\" \"eat\"])\n   #{#{\"meat\" \"team\" \"mate\"}})"
"(= (__ [\"veer\" \"lake\" \"item\" \"kale\" \"mite\" \"ever\"])\n   #{#{\"veer\" \"ever\"} #{\"lake\" \"kale\"} #{\"mite\" \"item\"}})"],
:description "Write a function which finds all the anagrams in a vector of words.
A word x is an anagram of word y if all the letters in x can be rearranged in a different order to form y.
Your function should return a set of sets, where each sub-set is a group of words which are anagrams of each other.
Each sub-set should have at least two words.  Words without any anagrams should not be included in the result."
:tags ["medium"]}

#(set (for [[_ g] (group-by frequencies %)
            :when (next g)]               
        (set g)))

#(->> %
       (group-by sort)
       vals
       (filter (fn [x] (< 1 (count x))))
       (map set)
       set)


#(->> %
      (group-by set)
      vals
      (filter next)
      (map set)
      set)

adereth's solution:

(fn [w]
  (->> w
       (group-by sort)
       vals
       (filter #(> (count %) 1))
       (map #(apply hash-set %))
       (apply hash-set)))
   
mfikes's solution:

(fn [ws]
  (into #{} (filter #(< 1 (count %))
   (for [w ws]
       (let [s (sort w)]
            (into #{} (filter #(= s (sort %)) ws)))))))
        
chouser's solution:

#(set (for [[_ g] (group-by frequencies %)
            :when (next g)]               
        (set g)))
    
aengelberg's solution:

(fn [l]
  (->>
    l
    (group-by set)
    vals
    (filter #(> (count %) 1))
    (map set)
    set))

chunchangshao's solution:

(fn [coll] 
  (into #{} 
        (map 
         (fn [[k v]] (set v)) 
             (filter (fn [[k v]] (> (count v) 1)) (#(group-by (fn [s] (apply str (sort (vec s)))) %) coll)))))
         
hypirion's solution:

(fn [coll]
  (->>
    (group-by frequencies coll)
    vals
    (remove #(= 1 (count %)))
    (map set)
    set))

jafingerhut's solution:

(fn [coll]
  (->> (group-by #(sort (seq %)) coll)
       (filter (fn [[_ v]] (second v)))
       (map #(set (val %)))
       set))
   
balint's solution:

(fn [words]
  (let [letters (into {} (for [w words] [w (sort w)]))]
    (into #{}
      (filter #(> (count %) 1)
        (map
          #(set (map first (val %)))
          (group-by #(val %) letters))))))
      
bhauman's solution:

#(->> %
       (group-by sort)
       vals
       (filter (fn [x] (< 1 (count x))))
       (map set)
       set)
   
amcnamara's solution:

(fn [c]
  (set (filter #(> (count %) 1)
               (map set
                    (for [i c]
                      (filter #(= (set i) (set %)) c))))))
                  
amalloy's solution:

#(set
  (map (comp set val)
       (remove (comp #{1} count val)
               (group-by frequencies %))))
           
stuarth's solution:

(fn [c]
    (letfn [(a? [x y] (and (= (sort x) (sort y))))]
      (into #{} (filter #(> (count %) 1) (map (fn [w] (into #{} (filter #(a? w %) c))) c)))))