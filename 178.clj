Best Hand
Difficulty:	Hard
Topics:	strings game

Following on from Recognize Playing Cards, determine the best poker hand that can be made with five cards.
The hand rankings are listed below for your convenience.

Straight flush: All cards in the same suit, and in sequence
Four of a kind: Four of the cards have the same rank
Full House: Three cards of one rank, the other two of another rank
Flush: All cards in the same suit
Straight: All cards in sequence (aces can be high or low, but not both at once)
Three of a kind: Three of the cards have the same rank
Two pair: Two pairs of cards have the same rank
Pair: Two cards have the same rank
High card: None of the above conditions are met

(= :high-card (__ ["HA" "D2" "H3" "C9" "DJ"]))

(= :pair (__ ["HA" "HQ" "SJ" "DA" "HT"]))

(= :two-pair (__ ["HA" "DA" "HQ" "SQ" "HT"]))

(= :three-of-a-kind (__ ["HA" "DA" "CA" "HJ" "HT"]))

(= :straight (__ ["HA" "DK" "HQ" "HJ" "HT"]))

(= :straight (__ ["HA" "H2" "S3" "D4" "C5"]))

(= :flush (__ ["HA" "HK" "H2" "H4" "HT"]))

(= :full-house (__ ["HA" "DA" "CA" "HJ" "DJ"]))

(= :four-of-a-kind (__ ["HA" "DA" "CA" "SA" "DJ"]))

(= :straight-flush (__ ["HA" "HK" "HQ" "HJ" "HT"]))
 
(fn [h]
  (let [[s r] (apply map list h)
        rs (set (map frequencies (partition 5 1 "A23456789TJQKA")))
        s? (rs (frequencies r))
        f? (apply = s)
        g (frequencies (vals (frequencies r)))]
    (cond
     (and s? f?) :straight-flush
     (g 4) :four-of-a-kind
     (and (g 2) (g 3)) :full-house
     f? :flush
     s? :straight
     (g 3) :three-of-a-kind
     (= 2 (g 2)) :two-pair
     (g 2) :pair
     :else :high-card)))

(fn [h]
  (let [[s r] (apply map list h)
        rs (set (map frequencies (partition 5 1 "A23456789TJQKA")))
        s? (rs (frequencies r))
        f? (apply = s)
        g (frequencies (vals (frequencies r)))]
    (cond
     (and s? f?) :straight-flush
     (g 4) :four-of-a-kind
     (and (g 2) (g 3)) :full-house
     f? :flush
     s? :straight
     (g 3) :three-of-a-kind
     (= 2 (g 2)) :two-pair
     (g 2) :pair
     :else :high-card)))
 
adereth's solution:

(fn bh [h]
  (let [s->c (fn [s]
               {:suit
                (condp = (first s)
                  \H :heart
                  \S :spade
                  \D :diamond
                  \C :club)
                
                :rank
                (condp = (second s)
                  \2 2
                  \3 3
                  \4 4
                  \5 5
                  \6 6
                  \7 7
                  \8 8
                  \9 9
                  \T 10
                  \J 11
                  \Q 12
                  \K 13
                  \A 14)})
        cards (map s->c h)
        ranks (map :rank cards)        
        rc (-> ranks frequencies vals sort)
        ss (= 1 (count (distinct (map :suit cards))))
        m (apply min ranks)
        st (or (= (sort ranks)
                  (range m (+ 5 m)))
               (= (sort ranks) [2 3 4 5 14]))
        ]
    (cond
     (and ss st) :straight-flush
     (= [1 4] rc) :four-of-a-kind
     (= [2 3] rc) :full-house
     ss :flush
     st :straight
     (= [1 1 3] rc) :three-of-a-kind
     (= [1 2 2] rc) :two-pair
     (= [1 1 1 2] rc) :pair
     :else :high-card
     
     )
 
  ))

mfikes's solution:

(fn [hand]
  (letfn [(string->card [c] {:suit ({\H :heart
                                     \C :club
                                     \S :spade
                                     \D :diamond} (first c))
                             :rank ((zipmap "23456789TJQKA" (range)) (second c))})
 
          (straight? [hand]
                     (and (apply distinct? (map :rank hand))
                          (or (= 4 (- (apply max (map :rank hand))
                                      (apply min (map :rank hand))))
                              (= '(0 1 2 3 12) (sort (map :rank hand))))))
 
          (flush? [hand]
                  (apply = (map :suit hand)))
 
          (sorted-rank-frequencies [hand]
                                   (sort (vals (frequencies (map :rank hand)))))
 
          (full-house? [hand]
                       (= '(2 3) (sorted-rank-frequencies hand)))
 
          (two-pair? [hand]
                     (= '(1 2 2) (sorted-rank-frequencies hand)))
 
          (pair? [hand]
                 (= '(1 1 1 2) (sorted-rank-frequencies hand)))
 
          (n-of-a-kind? [n hand]
                        (= n (apply max (vals (frequencies (map :rank hand))))))]
    (let [hand (map string->card hand)]
      (cond
        (and (straight? hand) (flush? hand)) :straight-flush
        (n-of-a-kind? 4 hand) :four-of-a-kind
        (full-house? hand) :full-house
        (flush? hand) :flush
        (straight? hand) :straight
        (n-of-a-kind? 3 hand) :three-of-a-kind
        (two-pair? hand) :two-pair
        (pair? hand) :pair
        :else :high-card))))
    
chouser's solution:

#(let [[S R] (apply map list %)
       k (-> R frequencies vals sort)
       f (apply = S)
       s (.contains
          "AKQJT98765432A.A23456789TJQKA"
          (apply str R))]
   (cond
    (if s f) :straight-flush
    (= k [1 4]) :four-of-a-kind
    (= k [2 3]) :full-house
    f :flush
    s :straight
    (= k [1 1 3]) :three-of-a-kind
    (= k [1 2 2]) :two-pair
    (= k [1 1 1 2]) :pair
    0 :high-card))

chunchangshao's solution:

(fn
  [x]
  (let [suits (map first x)
        nums (sort (map #(case (second %) \T -3 \J -2 \Q -1 \K 0 \A 1 (- (int (second %)) 48)) x))
        g-n (set (map count (vals (group-by identity nums))))
        t (map - nums (next nums))]
    (case (apply max g-n)
      4 :four-of-a-kind
      3 (if (= #{2 3} g-n) :full-house :three-of-a-kind)
      2 (if (= 3 (count (set nums))) :two-pair :pair)
      1 (if (every? zero? (map inc t))
          (if (= 1 (count (set suits))) :straight-flush :straight)
          (if (= 1 (count (set suits))) :flush :high-card)))))
      
amcnamara's solution:

(fn [hand]
  (let [value (zipmap "23456789TJQKA" (iterate inc 2))                                                             ; Map face values to numerical representation
        cards (map (fn [[suit face]] [suit (-> face value)]) hand)                                                 ; Parse hand into suit and face numerical values
        suits (group-by first cards)                                                                               ; Group suits by value
        faces (group-by second cards)                                                                              ; Group faces by value
        tally (fn [n groups] (not-empty (filter (fn [[_ group]] (= n (count group))) groups)))                     ; Filter suit/face groups for a given count
        stack ((fn [[a & _ :as b]] (if (and (= 2 a) (= 14 (last b))) (cons 1 (butlast b)) b)) (sort (keys faces))) ; Stack the hand face values with ace high/low
        order (reduce (fn [a b] (if b (if a (when (= b (inc a)) b) false) true)) stack)]                           ; Check that the hand has sequential face values
    (cond 
      (and order (= 1 (count suits))) :straight-flush
      (tally 4 faces)                 :four-of-a-kind
      (= 2 (count faces))             :full-house
      (= 1 (count suits))             :flush
      order                           :straight
      (tally 3 faces)                 :three-of-a-kind
      (= 2 (count (tally 2 faces)))   :two-pair
      (tally 2 faces)                 :pair
      :default                        :high-card)))