;; 110 Sequence of pronunciations [m]

Returns infinite lazy seq consisting of
the num of repeated nums and the num itself.
[1 1] is pronounced as [2 1] ("two ones"),
which in turn is [1 2 1 1] ("one two, one one").

(defn seq-prons [s]
  (next (iterate #(mapcat (juxt count first)
                    (partition-by identity %)) s)))
#'user/seq-prons
user>  (take 3 (seq-prons [1]))
((1 1) (2 1) (1 2 1 1))
user> (first (seq-prons [1 1 1 4 4]))
(3 1 2 4)
user> (nth (seq-prons [1]) 6)
(1 1 1 3 2 1 3 2 1 1)
user> (count (nth (seq-prons [3 2]) 15))
338

(fn [col]
  (->> col (iterate #(->> (partition-by identity %)
                      (mapcat (fn [a]
                                  [(count a) (first a)]))))
           (drop 1)))

(fn [x] (rest (iterate #(mapcat (juxt count first) (partition-by identity %)) x)))

(fn [s] (next (iterate #(mapcat (juxt count first) (partition-by identity 
%)) s)))
adereth's solution:

1
2
3
4
5
6
(fn [s]
  (rest
  (iterate
   (fn [t]
     (flatten (map (juxt count first) (partition-by identity t))))
   s)))
mfikes's solution:

1
2
3
4
5
(fn pronounce [s]
  (lazy-seq
    (let [pronounciation (flatten (map (juxt count first)
                                       (partition-by identity s)))]
      (cons pronounciation (pronounce pronounciation)))))
chouser's solution:

1
(fn [x] (rest (iterate #(mapcat (juxt count first) (partition-by identity %)) x)))
aengelberg's solution:

1
2
3
4
5
6
7
(fn f [l]
  (let [x (->> l
               (partition-by identity)
               (map (fn [i]
                      [(count i)(first i)]))
               (apply concat))]
    (lazy-seq (cons x (f x)))))
chunchangshao's solution:

1
(fn a [xs] (lazy-seq (let [t (flatten (map #(vec [(count %) (first %)]) (partition-by #(int %) xs)))] (cons t (a t)))))
hypirion's solution:

1
2
3
4
5
6
7
8
9
10
(fn pronounce [[fst & rst]]
  (lazy-seq
    (loop [[f & r :as s] rst,
           prev fst, cnt 1,
           acc []]
      (cond (empty? s)
              (let [res (conj acc cnt prev)]
                (cons res (pronounce res)))
            (= prev f) (recur r f (+ cnt 1) acc)
            :otherwise (recur r f 1 (conj acc cnt prev))))))
jafingerhut's solution:

1
2
3
(fn [s]
  (rest (iterate #(mapcat (juxt count first) (partition-by identity %))
                 s)))
balint's solution:

1
2
3
4
5
6
7
(fn [s]
  (next
    (iterate
      #(mapcat
         (juxt count first)
         (partition-by identity %))
      s)))
amcnamara's solution:

1
2
3
4
5
(fn f [s]
  (let [r (flatten (map #(list (count %) (nth % 0))
                        (partition-by identity s)))]
    (cons r 
          (lazy-seq (f r)))))
amalloy's solution:

1
2
3
4
5
(fn [coll]
  (rest (iterate (fn [coll]
                   (mapcat (juxt count first)
                           (partition-by identity coll)))
                 coll)))
stuarth's solution:

1
2
3
4
5
6
(fn [s]
    (letfn [(f1 [s]
              (when (seq s)
                (let [ns (take-while #(= % (first s)) s)]
                  (concat [(count ns) (first s)] (f1 (drop (count ns) s))))))]
            (iterate f1 (f1 s))))
dbyrne's solution:

1
2
3
4
5
6
7
8
(fn f 
  ([i] (f i []))
  ([i p]
    (if (empty? i)
      (lazy-cat [p] (f p []))
      (let [n (first i)
      [c r] (split-with #(= n %) i)]
        (recur r (conj p (count c) n))))))