;; 91 Graph Connectivity [h]

Given a graph, determines whether the graph is connected.
A connected graph is such that a path exists between
any two given nodes.

Returns true if the graph is connected and false otherwise.

We have a set of tuples representing the edges of a graph.
Each member of a tuple being a vertex/node in the graph.

-Each edge is undirected (can be traversed either direction). 
(= true (__ #{[:a :a]}))
(= true (__ #{[:a :b]}))
(= false (__ #{[1 2] [2 3] [3 1]
               [4 5] [5 6] [6 4]}))
(= true (__ #{[1 2] [2 3] [3 1]
              [4 5] [5 6] [6 4] [3 4]}))
(= false (__ #{[:a :b] [:b :c] [:c :d]
               [:x :y] [:d :a] [:b :e]}))
(= true (__ #{[:a :b] [:b :c] [:c :d]
              [:x :y] [:d :a] [:b :e] [:x :a]}))

(fn [g]
  ((fn f [e] 
     (#(if (= e %) (= % g) (f %)) 
        (reduce (fn [a b] (into a (filter #(some (set b) %) g))) 
                #{} 
                e))) 
   #{(first g)}))

(fn fully-connected? [graph]
  (let [nodes (set (apply concat graph))
        full-graph (set (mapcat (fn [[a b :as n]] [n [b a]]) graph))
        children (into {} (for [[k v] (group-by first full-graph)] [k (set (map second v))]))
        connections (fn [node]
                      (->> (iterate #(into % (mapcat children %)) #{node})
                           (partition 2 1)
                           (drop-while #(apply not= %))
                           first first))]
    (every? #(= % nodes) (map connections nodes))))

adereth's solution:

(fn [edge-set]
  (let [all-nodes (reduce (fn [acc pair] (-> acc
                                             (conj (first pair))
                                             (conj (second pair))))
                          #{} edge-set)
        reachable-from-node (fn [node]
                              (reduce (fn [acc pair] (cond
                                                      (= node (first pair)) (conj acc (second pair))
                                                      (= node (second pair)) (conj acc (first pair))
                                                      :else acc))
                                      #{} edge-set))
        reachable-from-set (fn [nodes]
                             (reduce clojure.set/union nodes (map reachable-from-node nodes)))
 
        ]
    (loop [reached #{(first all-nodes)}]
      (let [reachable (reachable-from-set reached)]
        (cond
         (= all-nodes reachable) true
         (= reached reachable) false
         :else (recur reachable))))))
     
mfikes's solution:

(fn connected? [g]
  (let [flood (fn [ns g]
                (let [ns' (into ns
                                (flatten
                                  (for [[a b] g :when (or (contains? ns a)
                                                          (contains? ns b))]
                                    [a b])))]
                  (if (= ns' ns)
                    ns
                    (recur ns' g))))]
    (= (flood #{(ffirst g)} g)
       (set (flatten (seq g))))))
   
chouser's solution:

(fn [g]
  (not (next
    (reduce
      (fn [g [x y]]
        (let [l (fn [n] (some #(when (% n) %) g))
              a (l x)
              b (l y)]
          (if a
            (if b
              (conj (disj g a b) (into a b))
              (conj (disj g a) (conj a y)))
            (if b
              (conj (disj g b) (conj b x))
              (conj g (set [x y]))))))
      #{}
      g))))
  
aengelberg's solution:

#(boolean (#{1 7} (count %)))

chunchangshao's solution:

#((fn graph-con [gs] 
    (if (= 1 (count gs)) 
      true 
      (letfn [(v-inc? [g] ((complement empty?) (clojure.set/intersection (first gs) g)))] 
        (let [bg (filter v-inc? (rest gs)) gs (remove v-inc? (rest gs))] 
          (if (empty? bg) false (graph-con (cons (reduce clojure.set/union bg) gs))) )))) (map set %))
      
hypirion's solution:

#(let [n-lst
        (into {} (for [[k v] (merge-with concat (group-by first %)
                                                (group-by second %))] 
          [k (into #{} (flatten v))]))]
  (loop [hs #{(ffirst n-lst)}]
    (let [n-hs (into #{} (mapcat n-lst hs))]
      (if (= n-hs hs)
        (every? hs (keys n-lst))
        (recur n-hs)))))
    
jafingerhut's solution:

;; Go for simplicity of implementation rather than optimal run-time
;; performance.
 
;; Maintain a set of node sets, where each node set is the set of
;; nodes in one connected component of the graph.
 
;; It starts out empty, and for each edge examined, we find the set s
;; containing node 1 (if any), and the set t containing node 2 (if
;; any), remove both of those sets, and add their union.
 
(fn [edges]
  (= 1 (count (reduce (fn [c [u v]]
                        (let [s (or (first (filter #(% u) c)) #{u})
                              t (or (first (filter #(% v) c)) #{v})]
                          (conj (disj c s t) (clojure.set/union s t))))
                      #{} edges))))
                  
balint's solution:

(fn [g]
  (let [nodes (reduce (fn [vs [u v]] (conj vs u v)) #{} g)]
    (loop [us [(first nodes)] reached #{(first nodes)}]
      (let [newly-reached
            (for [u us [a b] g
                  :let [x (cond (= u a) b (= u b) a)]
                  :when (and x (not (reached x)))]
              x)]
        (if (empty? newly-reached)
          (= reached nodes)
          (recur newly-reached (into reached newly-reached)))))))
      
amcnamara's solution:

(fn [m s a]
  (let [c #(remove nil?                                                                                                                               
                   (m (fn [[i j]] (if (= % i) j                                                                                                     
                                  (if (= % j) i)))                                                                                              
                      (seq %2)))                                                                                                                                 
        n #(s (flatten (seq %)))]                                                                                                                               
    (loop [r [(first (n a))]]                                                                                                                           
      (if (= (n a) (s r))                                                                                        
        true
        (if (= (n (m #(c % a) r)) (s r))                                                                                                    
          false               
          (recur (into r (n (m #(c % a) r)))))))))
map set