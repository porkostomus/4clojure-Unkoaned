{:_id 85 :title "Power Set" :tests [
"(= (__ #{1 :a}) #{#{1 :a} #{:a} #{} #{1}})"
"(= (__ #{}) #{#{}})"
"(= (__ #{1 2 3})\n   #{#{} #{1} #{2} #{3} #{1 2} #{1 3} #{2 3} #{1 2 3}})"
"(= (count (__ (into #{} (range 10)))) 1024)"]
:description "Write a function which generates the <a href=\"http://en.wikipedia.org/wiki/Power_set\">power set</a>
of a given set.  The power set of a set x is the set of all subsets of x, including the empty set and x itself."
:tags ["hard" "set-theory"]}

(= (__ #{1 :a}) #{#{1 :a} #{:a} #{} #{1}})
(= (__ #{}) #{#{}})
(= (__ #{1 2 3})
   #{#{} #{1} #{2} #{3} #{1 2} #{1 3} #{2 3} #{1 2 3}})
(= (count (__ (into #{} (range 10)))) 1024)

reduce (fn [e v] (into e (map #(conj % v) e))) #{#{}}

(fn powerset [s]
  (reduce #(into % (for [subset %] (conj subset %2))) #{#{}} s))

(fn powerset [s]
  (reduce #(into % (for [subset %] (conj subset %2))) #{#{}} s))

adereth's solution:

(fn subsets [s]
  (apply hash-set #{}
  (map #(apply hash-set %)
  (loop [result []
         remaining s]
    (if (seq remaining)
      (recur (concat [[(first remaining)]] result (map #(conj % (first remaining)) result)) (rest remaining))
      result)))))
  
mfikes's solution:

(fn [s]
  (let [v (vec s)]
    (into #{} (for [i (range (apply * (repeat (count v) 2)))]
                (into #{} (for [j (range (count v)) :when (bit-test i j)]
                            (v j)))))))
                        
chouser's solution:

#((fn f [[a & s :as q]]
    (if q
      (into (f s) (for [i (f s)] (conj i a)))
      #{#{}}))
  (seq %))

aengelberg's solution:

(fn p [s]
  (if
    (= s #{})
    #{#{}}
    (let [a (p (set (rest s)))]
      (into a (map #(conj % (first s)) a)))))
  
chunchangshao's solution:

#(case (count %)
   2  #{#{1 :a} #{:a} #{} #{1}}
   0 #{#{}}
   3 #{#{} #{1} #{2} #{3} #{1 2} #{1 3} #{2 3} #{1 2 3}}
   (range 1024))

hypirion's solution:

(fn power-set
  ([a] (power-set (seq a) #{#{}}))
  ([[f & r :as s] sets]
    (if (empty? s) 
        sets
        (recur r
          (into sets
            (map #(conj % f) sets))))))
        
jafingerhut's solution:

;; If we have determined the power set p for a set s so far, and we
;; now want to calculate the power set for a set containing s and one
;; more element x, then:
 
;; 1. every set in p is in that power set (none of them contain
;; element x), and
 
;; 2. every one of those sets with element x added to it are also in
;; the power set.
 
(fn [s]
  (reduce (fn [power-set x]
            (into power-set (map #(conj % x) power-set)))
          #{#{}} s))
      
balint's solution:

(fn power-set [s]
  (println s)
  (if (empty? s)
    #{#{}}
    (let [without-fs (power-set (rest s))]
      (into #{}
        (concat
          without-fs
          (map #(set (cons (first s) %)) without-fs))))))
      
amcnamara's solution:

(fn [c]
  (set
    (if (empty? c)
      #{#{}}
      (map (fn [m]
               (set (reduce into
                      (map #(if (= \1 %) #{%2}) m c))))
           (map #(reverse (Integer/toString % 2)) (range (Math/pow 2 (count c))))))))
       
amalloy's solution:

(fn [c]
  (reduce #(into % (for [x %]
                     (conj x %2)))
          #{#{}}
          c))