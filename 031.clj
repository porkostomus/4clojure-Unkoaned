{:_id 31 :title "Pack a Sequence"
:tests ["(= (__ [1 1 2 1 1 1 3 3]) '((1 1) (2) (1 1 1) (3 3)))"
"(= (__ [:a :a :b :b :c]) '((:a :a) (:b :b) (:c)))"
"(= (__ [[1 2] [1 2] [3 4]]) '(([1 2] [1 2]) ([3 4])))"]
:description "Write a function which packs consecutive duplicates into sub-lists."
:tags ["easy" "seqs"]}

user=> (partition-by identity [1 1 2 1 1 1 3 3])
((1 1) (2) (1 1 1) (3 3))

adereth's solution:

partition-by identity
mfikes's solution:

partition-by identity
chouser's solution:

partition-by identity
aengelberg's solution:

1
(partial partition-by identity)
chunchangshao's solution:

#(loop [xs %,res ()] 
    (if (empty? xs) (reverse res)
    (let [x (first xs),coll (rest xs),resf (first res)]
      (if (= x (first resf)) 
        (recur coll (conj (rest res) (conj resf x) ))
        (recur coll (conj res (list x)))))))
hypirion's solution:

(fn [coll]
  (->
    (fn [coll p acc res]
      (if (empty? coll)
          (conj res acc)
        (let [[f & r] coll]
          (if (= f p)
            (recur r p (conj acc f) res)
            (recur r f [f] (conj res acc))))))
    (apply coll (gensym) [] [] [])
    next))
jafingerhut's solution:

1
partition-by identity
balint's solution:

#(partition-by identity %)
1
#(partition-by identity %)
