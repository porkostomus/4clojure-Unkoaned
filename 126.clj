Through the Looking Class

Enter a value which satisfies the following:

(let [x __]
  (and (= (class x) x) x))

Class

adereth's solution:

(class (class 1))

mfikes's solution:

java.lang.Class