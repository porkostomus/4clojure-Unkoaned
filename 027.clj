{:_id 27 :title "Palindrome Detector"
:tests ["(false? (__ '(1 2 3 4 5)))"
        "(true? (__ \"racecar\"))"
        "(true? (__ [:foo :bar :foo]))"
        "(true? (__ '(1 1 3 3 1 1)))"
        "(false? (__ '(:a :b :c)))"]
:description "Write a function which returns true
if the given sequence is a palindrome.<br/><br>
Hint: \"racecar\" does not equal '(\\r \\a \\c \\e \\c \\a \\r)"
:tags ["easy" "seqs"]}
(defn mypal [s]
  (= (seq s) (reverse (seq s))))
#'user/mypal
user=> (mypal [1 2 3 4 5])
false
user=> (mypal [1 2 3 2 1])
true

adereth's solution:

#(= (reverse %) (seq %))

mfikes's solution:

#(= (seq %1) (reverse %1))

chouser's solution:

#(= (seq %) (reverse %))

aengelberg's solution:

#(= (seq %)(reverse (seq %)))

chunchangshao's solution:

#(loop [c %] (if (> (count c) 1) (if (not= (first c) (last c)) false (recur (reverse (rest (reverse (rest c)))))) true))

hypirion's solution:

(fn [coll]
  (let [rev (if (string? coll)
                clojure.string/reverse
                reverse)]
    (= coll (rev coll))))

jafingerhut's solution:

#(= (seq %) (reverse %))

balint's solution:

#(let [median (quot (count %) 2)]
  (= (take median %) (take median (reverse %))))

#(let [median (quot (count %) 2)]
  (= (take median %) (take median (reverse %))))