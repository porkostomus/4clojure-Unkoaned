Pascal's Trapezoid
Difficulty:	Easy
Topics:	seqs

Write a function that, for any given input vector of numbers, returns an infinite lazy sequence of vectors,
where each next one is constructed from the previous following the rules used in Pascal's Triangle.
For example, for [3 1 2], the next row is [3 4 3 2].

Beware of arithmetic overflow! In clojure (since version 1.3 in 2011),
if you use an arithmetic operator like + and the result is too large to fit into a 64-bit integer, an exception is thrown.
You can use +' to indicate that you would rather overflow into Clojure's slower, arbitrary-precision bigint.

(= (second (__ [2 3 2])) [2 5 5 2])

(= (take 5 (__ [1])) [[1] [1 1] [1 2 1] [1 3 3 1] [1 4 6 4 1]])

(= (take 2 (__ [3 1 2])) [[3 1 2] [3 4 3 2]])

(= (take 100 (__ [2 4 2])) (rest (take 101 (__ [2 2]))))

(fn [row] (iterate #(map +' `(0 ~@%) `(~@% 0)) row))

adereth's solution:

(fn [row]
  (let [next-row #(map +' (concat [0] %) (concat % [0]))]
    (iterate next-row row)))

mfikes's solution:

(fn trapezoid [xs]
  (lazy-seq
   (cons xs (trapezoid (mapv +' (conj xs 0) (cons 0 xs))))))

chouser's solution:

iterate #(map + `(0 ~@%) `(~@% 0))

aengelberg's solution:

(fn f [l]
  (lazy-seq
    (cons l
      (f (map +' (concat [0] l)(concat l [0]))))))

chunchangshao's solution:

#((fn pascal-triangle [xs]
    (if (< 8 (count xs))
      xs
    (lazy-seq (cons xs (pascal-triangle (concat [(first xs)] (map + xs (next xs)) [(last xs)])))))) %)

hypirion's solution:

(fn trapezoid [coll]
  (let [res (concat [(first coll)] 
                    (map #(reduce + %) (partition 2 1 coll)) 
                    [(last coll)])]
    (lazy-seq
      (cons coll
        (trapezoid res)))))

jafingerhut's solution:

iterate (fn [c]
          (concat [(first c)]
                  (map #(apply + %) (partition 2 1 (map bigint c)))
                  [(last c)]))

balint's solution:

(fn [base]
  (iterate
    (fn [row]
      (if (= 1(count row))
        [(first row) (last row)]
        (concat [(first row)] (map #(apply +' %) (partition 2 1 row)) [(last row)])))
    base))

amcnamara's solution:

(fn f [c]
  (lazy-seq `(~c ~@(f (map + `(0 ~@c) `(~@c 0))))))

amalloy's solution:

iterate #(map +' `(0 ~@%) `(~@% 0))

stuarth's solution:

(fn p [s]
    (lazy-seq
     (cons s
           (p (flatten [(first s) (map #(reduce + %) (partition 2 1 s)) (last s)])))))

dbyrne's solution:

(fn f [x]
  (let [y (concat 
            [(first x)]
            (map #(+ % %2) x (rest x))
            [(last x)])]
    (lazy-cat [x] (f y))))