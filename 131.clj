Sum Some Set Subsets
Difficulty:	Medium
Topics:	math

Given a variable number of sets of integers,
create a function which returns true iff all of the sets have a non-empty subset with an equivalent summation.

(= true  (__ #{-1 1 99} 
             #{-2 2 888}
             #{-3 3 7777})) ; ex. all sets have a subset which sums to zero

(= false (__ #{1}
             #{2}
             #{3}
             #{4}))	

(= true  (__ #{1}))

(= false (__ #{1 -3 51 9} 
             #{0} 
             #{9 2 81 33}))

(= true  (__ #{1 3 5}
             #{9 11 4}
             #{-3 12 3}
             #{-3 4 -2 10}))

(= false (__ #{-1 -2 -3 -4 -5 -6}
             #{1 2 3 4 5 6 7 8 9}))

(= true  (__ #{1 3 5 7}
             #{2 4 6 8}))

(= true  (__ #{-1 3 -5 7 -9 11 -13 15}
             #{1 -3 5 -7 9 -11 13 -15}
             #{1 -1 2 -2 4 -4 8 -8}))

(= true  (__ #{-10 9 -8 7 -6 5 -4 3 -2 1}
             #{10 -9 8 -7 6 -5 4 -3 2 -1}))
             
(fn [& n]
  (< 0 (count
         (reduce #(keep %2 %)
           (map (fn f [[x & s]]
                  (into #{x}
                        (if s
                          (into (f s)
                                (map #(+ x %) (f s))))))
                (map seq n))))))

(fn sum-subsets [& s]
  (let [ps (fn powerset [s]
             (reduce (fn [acc e] 
                       (into acc (map conj acc (repeat e))))
                     #{#{}}
                     s))
        pss (map #(disj (ps %) #{}) s)
        sums (map (fn [p] (set (map #(apply + %) p))) pss)]
    (not (empty? (apply clojure.set/intersection sums)))))

adereth's solution:

(fn sum-set [& ss]
  (let [subsets (fn [s]
            (loop [result []
               remaining s]
              (if (seq remaining)
             (recur (concat [[(first remaining)]] result (map #(conj % (first remaining)) result)) (rest remaining))
                     result)))
        sums (map (fn [s] (map #(reduce + %) (subsets s))) ss)
        sums (map (comp keys #(group-by identity %)) sums)]
    (if (some #(= % (count ss)) (vals (frequencies (apply concat sums)))) true false)
    )
  )

mfikes's solution:

(fn shared-sum? [& sets]
  (let [powerset (fn powerset [l]
                    (if (empty? l)
                      #{#{}}
                      (let [ps (powerset (rest l))]
                        (apply conj ps
                          (map (fn [x] (conj x (first l)))
                               ps)))))
        subset-sums (fn subset-sums [sets]
                      (map (comp (partial map (partial reduce +))
                                 (partial filter seq)
                                 powerset)
                           sets))
        sums (subset-sums sets)
        _ (println (map count sums))]
    (boolean
      (some
        (fn [n] (every? #(contains? (set %) n) sums))
        (first sums)))))
    
chouser's solution:

(fn [& n]
  (< 0 (count
         (reduce #(keep %2 %)
           (map (fn f [[x & s]]
                  (into #{x}
                        (if s
                          (into (f s)
                                (map #(+ x %) (f s))))))
                (map seq n))))))
            
aengelberg's solution:

#(let [l %&
       l (map (fn f [c]
                (cond
                 (empty? c) [0]
                 :else (concat (f (rest c)) (map (partial + (first c)) (f (rest c))))))
              l)
       l (map set (map rest l))]
   (not (empty? (apply clojure.set/intersection l))))

chunchangshao's solution:

(fn [& other] ((complement empty?) (apply clojure.set/intersection (map #(clojure.set/union % [(reduce + %)] (set (for [x % y %] (+ x y)))) other))))

hypirion's solution:

(fn [& r]
  (letfn [(power-set
            ([a] (power-set (seq a) #{#{}}))
            ([[f & r :as s] sets]
              (if (empty? s) 
                sets
                (recur r
                  (into sets
                    (map #(conj % f) sets))))))]
  (->>
    (map power-set r)
    (map #(disj % #{}))
    (map (fn [a] (set (map #(reduce + %) a))))
    (apply clojure.set/intersection)
    count pos?)))

jafingerhut's solution:

;; Uses my solution to the Power Set problem.
 
;; That will be used to generate all but the empty subsets of the
;; given sets of numbers, and from there we add up each one to get the
;; sum of each.
 
;; If there are any sums in common, i.e. in the intersection, then the
;; answer is yes.
 
(fn [& int-sets]
  (let [power-set (fn [s]
                    (reduce (fn [power-set x]
                              (into power-set (map #(conj % x) power-set)))
                            #{#{}} s))
        all-nonempty-subsets (map #(disj (power-set %) #{}) int-sets)
        subset-sums (map (fn [subsets-of-one]
                           (set (map #(apply + %) subsets-of-one)))
                         all-nonempty-subsets)]
    (not= #{} (apply clojure.set/intersection subset-sums))))

balint's solution:

(fn [& sets]
  (letfn [(power-set [s]
            (if (empty? s)
              #{#{}}
              (let [without-fs (power-set (rest s))]
                (into #{}
                      (concat
                        without-fs
                        (map #(set (cons (first s) %)) without-fs))))))]
    (not
      (empty?
        (reduce
          clojure.set/intersection
          (map (fn [s]
                 (into #{} (map #(apply + %)
                                (filter seq (power-set s)))))
               sets)))
      )))
  
amcnamara's solution:

(fn [& s]
  (not (empty?
    (apply clojure.set/intersection
      (map (fn [c]
             (set (map #(apply + %)
                       (rest
                         (reduce #(into % (for [i %] (cons %2 i))) [[]] c))))) s)))))
                     
amalloy's solution:

(letfn [(power-set [s]
          (if (empty? s)
            #{#{}}
            (let [x (first s), more (disj s x)]
              (set (for [p (power-set more) res [p (conj p x)]]
                     res)))))
        (sums [s]
          (set (for [p (disj s #{})]
                 (apply + p))))]
  (fn sum-subsets [& sets]
    (boolean (seq (apply clojure.set/intersection
                         (for [s sets]
                           (sums (power-set s))))))))