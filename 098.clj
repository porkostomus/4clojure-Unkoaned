;; 98 Equivalence Classes [m]

A function f defined on a domain D
induces an equivalence relation on D, as follows:
a is equivalent to b with respect to f if and only if
(f a) is equal to (f b).
This function's args f and D compute
the equivalence classes of D with respect to f.

(= (__ #(* % %) #{-2 -1 0 1 2})
   #{#{0} #{1 -1} #{2 -2}})
(= (__ #(rem % 3) #{0 1 2 3 4 5 })
   #{#{0 3} #{1 4} #{2 5}})
(= (__ identity #{0 1 2 3 4})
   #{#{0} #{1} #{2} #{3} #{4}})
(= (__ (constantly true) #{0 1 2 3 4})
   #{#{0 1 2 3 4}})

(fn [f coll] (into #{} (map set (vals (group-by f coll)))))

#(->> (group-by %1 %2)
      (vals)
      (map set)
      (set))

#(set (map set (vals (group-by %1 %2))))

aceeca1's solution:

(comp set (partial map set) vals group-by)

adereth's solution:

#(set (map set (vals (group-by %1 %2))))

mfikes's solution:

(fn [f D] (into #{} (map set (vals (group-by f D)))))

chouser's solution:

#(set (map set (vals (group-by % %2))))

aengelberg's solution:

#(set (map set (vals (group-by % %2))))

chunchangshao's solution:

#(set (map set (vals (group-by % %2))))

hypirion's solution:

#(->> 
  (group-by %1 %2)
  vals (map set) set)

jafingerhut's solution:

(fn [f D]
  (set (map set (vals (group-by f D)))))

balint's solution:

(fn [f d]
  (set
    (map set (vals (group-by f d)))))

amcnamara's solution:

(fn [f a]
  (set (map set
    (for [n a]
      (filter #(= (f n) (f %)) a)))))

amalloy's solution:

#(set (map (comp set val)
           (group-by % %2)))

stuarth's solution:

(fn [f s]
  (set (map set (vals (group-by f s)))))

dbyrne's solution:

(fn [f d]
  (->> (group-by f d)
       vals
       (map set)
       set))