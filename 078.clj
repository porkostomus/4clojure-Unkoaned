{:_id 78 :restricted ["trampoline"]
:title "Reimplement Trampoline"
:tests [
"(= (letfn [(triple [x] #(sub-two (* 3 x)))
     (sub-two [x] #(stop?(- x 2)))
       (stop? [x] (if (> x 50) x #(triple x)))]
         (__ triple 2))\n  82)"
"(= (letfn [(my-even? [x] (if (zero? x) true #(my-odd? (dec x))))
  (my-odd? [x] (if (zero? x) false #(my-even? (dec x))))]
    (map (partial __ my-even?) (range 6)))
    [true false true false true false])"]
:description "Reimplement the function described in <a href=\"76\"> \"Intro to Trampoline\"</a>."
:tags ["medium" "core-functions"]}

(fn t [f & x]
  (if (fn? f)
    (t (apply f x))
    f))


(fn [f & args] (loop [f (apply f args)] (if (fn? f) (recur (f)) f)))

adereth's solution:

(fn [f x]
  (loop [x (f x)]
    (if (fn? x) (recur (x)) x)))

mfikes's solution:

(fn [f & args] ((fn [x] (if (fn? x) (recur (x)) x)) (apply f args)))

chouser's solution:

(fn t [f & x]
  (if (fn? f)
    (t (apply f x))
    f))

aengelberg's solution:

(fn [f & v]
  (loop [i (apply f v)]
    (if (fn? i)
      (recur (i))
      i)))
  
chunchangshao's solution:

#(loop [x (% %2)] (if (fn? x) (recur (x)) x))

hypirion's solution:

#(loop [v (%1 %2)] (if (fn? v) (recur (v)) v))

jafingerhut's solution:

(fn [f & args]
  (loop [x (apply f args)]
    (if (fn? x)
      (recur (x))
      x)))
  
balint's solution:

(fn [f & args]
  (loop [f f x (apply f args)]
    (if (fn? x)
      (recur f (x))
      x)))
  
bhauman's solution:

(fn [f & args]
    (first
     (filter (comp not fn?)
             (iterate #(if (fn? %) (%) %)
                      (apply f args)))))
                  
amcnamara's solution:

#(loop [r (% %2)]
   (if (fn? r) (recur (r)) r))

stuarth's solution:

(fn [f & a]
    (if (fn? f)
      (recur (apply f a) nil)
      f))