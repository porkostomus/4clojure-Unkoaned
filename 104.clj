;; 104 (Inverse of 92)

Takes integer < 4000, returns roman numeral
(following subtractive principle)

(defn roman [x] (cond
  (<= 1000 x) (str "M" (roman (- x 1000)))
  (<= 900 x) (str "CM" (roman (- x 900)))
  (<= 500 x) (str "D" (roman (- x 500)))
  (<= 400 x) (str "CD" (roman (- x 400)))
  (<= 100 x) (str "C" (roman (- x 100)))
  (<= 90 x) (str "XC" (roman (- x 90)))
  (<= 50 x) (str "L" (roman (- x 50)))
  (<= 40 x) (str "XL" (roman (- x 40)))
  (<= 10 x) (str "X" (roman (- x 10)))
  (<= 9 x) (str "IX" (roman (- x 9)))
  (<= 5 x) (str "V" (roman (- x 5)))
  (<= 4 x) (str "IV" (roman (- x 4)))
  (<= 1 x) (str "I" (roman (- x 1)))
  true ""))
#'user/roman
user> (roman 1)
"I"
user> (roman 30)
"XXX"
user> (roman 4)
"IV"
user> (roman 3999)
"MMMCMXCIX"

(fn [a] (let [abc { 1 ["I" "X" "C" "M"] 2 ["II" "XX" "CC" "MM"] 3 ["III" 
"XXX" "CCC" "MMM"] 4 ["IV" "XL" "CD"] 5 ["V" "L" "D"] 6 ["VI" "LX" "DC"] 7 
["VII" "LXX" "DCC"] 8 ["VIII" "LXXX" "DCCC"] 9 ["IX" "XC" "CM"] 0 ["" "" "" 
""] } digits (reverse (map read-string (re-seq #"." (str a))))] (->> digits 
(map-indexed (fn [i d] (get-in abc [d i]))) (reverse) (clojure.string/join 
""))))

adereth's solution:

#(clojure.pprint/cl-format nil "~@R" %)

mfikes's solution:

(fn roman [n]
  (let [build (fn build [n]
                (if (>= n 1000)
                  (conj (build (rem n 1000)) (apply str (repeat (quot n 1000) "M")))
                  (if (>= n 100)
                    (let [h (quot n 100)
                          r (build (rem n 100))]
                      (case h
                        9 (conj r "CM")
                        8 (conj r "DCCC")
                        7 (conj r "DCC")
                        6 (conj r "DC")
                        5 (conj r "D")
                        4 (conj r "CD")
                        3 (conj r "CCC")
                        2 (conj r "CC")
                        1 (conj r "C")))
                    (if (>= n 10)
                      (let [t (quot n 10)
                            r (build (rem n 10))]
                        (case t
                          9 (conj r "XC")
                          8 (conj r "LXXX")
                          7 (conj r "LXX")
                          6 (conj r "LX")
                          5 (conj r "L")
                          4 (conj r "XL")
                          3 (conj r "XXX")
                          2 (conj r "XX")
                          1 (conj r "X")))
                      (if (>= n 1)
                        (seq (case n
                               9 "IX"
                               8 "VIII"
                               7 "VII"
                               6 "VI"
                               5 "V"
                               4 "IV"
                               3 "III"
                               2 "II"
                               1 "I"))
                        ())))))]
    (apply str (build n))))

chouser's solution:

(fn f [[n & a] [s & b] o i]
  (if n
    (f a b (into o (repeat (int (/ i n)) s)) (rem i n))
    (apply str o)))
[1000 900 500 400 100 90 50 40 10  9 5  4 1]
'[  M  CM   D  CD   C XC  L XL  X IX V IV I]
[]

aengelberg's solution:

(let [m [[1 "I"]
         [4 "IV"]
         [5 "V"]
         [9 "IX"]
         [10 "X"]
         [40 "XL"]
         [50 "L"]
         [90 "XC"]
         [100 "C"]
         [500 "D"]
         [900 "CM"]
         [1000 "M"]]]
  (fn f [n]
    (if (= 0 n)
      ""
      (let [[x s] (last (take-while #(<= (first %) n) m))]
        (str s (f (- n x)))))))
    
chunchangshao's solution:

#(case %
   1 "I"
   30 "XXX"
   4 "IV"
   140 "CXL"
   827 "DCCCXXVII"
   48 "XLVIII"
   "MMMCMXCIX")

hypirion's solution:

(let [loch {1 "I",  5 "V",  10 "X",  50 "L",  100 "C",  500 "D",  1000 "M"
            4 "IV", 9 "IX", 40 "XL", 90 "XC", 400 "CD", 900 "CM"}]
  (fn roman [n]
    (let [m (apply max (filter #(<= % n) (keys loch)))]
      (if (= m n)
        (loch m)
        (str (loch m) (roman (- n m)))))))
    
jafingerhut's solution:

(fn [n]
  (let [vals [[1000 "M"] [ 900 "CM"]
              [ 500 "D"] [ 400 "CD"]
              [ 100 "C"] [  90 "XC"]
              [  50 "L"] [  40 "XL"]
              [  10 "X"] [   9 "IX"]
              [   5 "V"] [   4 "IV"]
              [   1 "I"]]
        numerals (fn [nums n]
                   (if-let [[[val num]] (seq (filter #(>= n (first %)) vals))]
                     (recur (conj nums num) (- n val))
                     nums))]
    (apply str (numerals [] n))))

balint's solution:

(fn [a]
  (let [abc { 1 ["I" "X" "C" "M"] 2 ["II" "XX" "CC" "MM"] 3 ["III" "XXX" "CCC" "MMM"]
              4 ["IV" "XL" "CD"]  5 ["V" "L" "D"] 6 ["VI" "LX" "DC"]
              7 ["VII" "LXX" "DCC"] 8 ["VIII" "LXXX" "DCCC"] 9 ["IX" "XC" "CM"]
              0 ["" "" "" ""] }
        digits (reverse (map read-string (re-seq #"." (str a))))]
    (->>
      digits
      (map-indexed
        (fn [i d]
          (get-in abc [d i])))
      (reverse)
      (clojure.string/join ""))))
  
amcnamara's solution:

(fn [n]
  (let [v [["M" 1000]
           ["CM" 900]
           ["D"  500]
           ["CD" 400]
           ["C"  100]
           ["XC"  90]
           ["L"   50]
           ["XL"  40]
           ["X"   10]
           ["IX"   9]
           ["V"    5]
           ["IV"   4]
           ["I"    1]]]
    (loop [r "" c n]
      (if (> c 0)
        (let [[k v] (first (filter #(if (>= c (second %)) %) v))]
          (recur (str r k) (- c v)))
        r))))
    
amalloy's solution:

(let [nums [1000 "M"
            900 "CM"
            500 "D"
            400 "CD"
            100 "C"
            90 "XC"
            50 "L"
            40 "XL"
            10 "X"
            9 "IX"
            5 "V"
            4 "IV"
            1 "I"]]
  (fn [n]
    (second
     (reduce (fn [[remaining acc] [divisor numeral]]
               (let [[div mod] ((juxt quot rem) remaining divisor)]
                 [mod (apply str acc (repeat div numeral))]))
             [n ""]
             (partition 2 nums)))))