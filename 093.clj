;; 93 Partial Flatten [m]

Flattens any nested combination of sequential things
(lists, vectors, etc.), but maintains the
lowest level sequential items.
The result should be a sequence of sequences
with only one level of nesting.

(= (__ [["Do"] ["Nothing"]])
   [["Do"] ["Nothing"]])
(= (__ [[[[:a :b]]] [[:c :d]] [:e :f]])
   [[:a :b] [:c :d] [:e :f]])
(= (__ '((1 2)((3 4)((((5 6)))))))
   '((1 2)(3 4)(5 6)))

(fn pf [s]
  (if (every? coll? s)
    (mapcat pf s)
    [s]))

adereth's solution:

(fn [x]
  (let [b #(every? sequential? %)]
    (filter (complement b)
    (tree-seq b seq x))))

mfikes's solution:

(fn [coll] (filter #(and (sequential? %) ((complement sequential?) (first %))) (tree-seq sequential? seq coll)))

chouser's solution:

(fn f [[i :as x]]
  (if (coll? i)
    (mapcat f x)
    [x]))

aengelberg's solution:

(fn f [l]
  (when (seq l)
    (let [[x r] (split-at 1 l)
          x (first x)]
      (if (= x (flatten x))
        (cons x (f r))
        (concat (f x)(f r))))))
    
chunchangshao's solution:

#(if (= (first %) '(1 2))  '((1 2)(3 4)(5 6))  (loop [xs %,res []] (if (empty? xs) res (recur (rest xs) (conj res (flatten (first xs)))))))

hypirion's solution:

(fn flatten-1 [[f & r :as s]]
  (cond (empty? s) nil
        (coll? f) (concat (flatten-1 f) (flatten-1 r))
        :otherwise [s]))

jafingerhut's solution:

(fn [c]
  (filter #(and (sequential? %) (not (sequential? (first %))))
          (tree-seq sequential? seq c)))

balint's solution:

(fn f [[fsq & rsq :as sq]]
  ;(println fsq rsq)
  (cond
    (not (seq sq))
      nil
    (and
      (sequential? sq)
      (not (sequential? fsq)))
      [sq]
    :else
      (concat (f fsq) (f rsq))))

borkdude's solution:

(fn foo [s]
  (cond (empty? s) s
        (not-any? coll? s) (list s)
        :else (concat (foo (first s)) (foo (rest s)))))

amcnamara's solution:

(fn [c]
  (filter #(and (coll? %) (not-any? coll? %))
    (tree-seq coll? seq c)))

amalloy's solution:

(fn f [x]
  (if (every? coll? x)
    (mapcat f x)
    [x]))

dbyrne's solution:

(fn [s]
  (filter #(and (sequential? %)
                (not (sequential? (first %))))
          (tree-seq sequential? seq s)))