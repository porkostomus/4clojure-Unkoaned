The Big Divide
Difficulty:	Medium
Topics:	math

Write a function which calculates the sum of all natural numbers under n (first argument)
which are evenly divisible by at least one of a and b (second and third argument).
Numbers a and b are guaranteed to be coprimes.

Note: Some test cases have a very large n, so the most obvious solution will exceed the time limit.

(= 0 (__ 3 17 11))

(= 23 (__ 10 3 5))

(= 233168 (__ 1000 3 5))

(= "2333333316666668" (str (__ 100000000 3 5)))

(= "110389610389889610389610"
  (str (__ (* 10000 10000 10000) 7 11)))

(= "1277732511922987429116"
  (str (__ (* 10000 10000 10000) 757 809)))

(= "4530161696788274281"
  (str (__ (* 10000 10000 1000) 1597 3571)))

(fn [n a b]
  (let [f #(quot (dec n) %)
        g #(/ (*' % (f %) (inc (f %))) 2)]
    (- (+ (g a) (g b)) (g (* a b)))))

(fn [n a b]
  (let [f #(quot (dec n) %)
        g #(/ (*' % (f %) (inc (f %))) 2)]
    (- (+ (g a) (g b)) (g (* a b)))))

adereth's solution:

(fn [n a b]
  (let [s #(let [c (quot n %)] (*' % c (inc c) (/ 2)))]
    (+' (s a) (s b) (-' (s (*' a b)))
       (if (zero? (mod n a)) (- n) 0)
       (if (zero? (mod n b)) (- n) 0)       
       )
    )
  )

mfikes's solution:

(fn [n a b]
  (let [sum-1-to-n (fn [n] (/ (*' n (inc n)) 2))
        sum-multiples-up-to (fn [s n] (*' s (sum-1-to-n (quot (dec n) s))))]
    (+' (sum-multiples-up-to a n)
       (sum-multiples-up-to b n)
       (- (sum-multiples-up-to (* a b) n)))))

chouser's solution:

(fn [f L a b]
(- (+ (f a L) (f b L)) (f (* a b) L)))
#(let [q (quot (- %2 1) %)]
(/ (* % q (+ 1 q)) 2))

aengelberg's solution:

#(let [% (dec (bigint %))
       n (* %2 %3)
       x (quot % %2)
       y (quot % %3)
       z (quot % n)]
   (+ (* x (inc x) 1/2 %2)
      (* y (inc y) 1/2 %3)
      (- (* z (inc z) 1/2 n))))

chunchangshao's solution:

(fn [& rest] (case (last rest) 
               11 (#(case (second %) 17 0 7 "110389610389889610389610") rest) 
               809 "1277732511922987429116" 
               3571 "4530161696788274281" 
               (#(case (first %) 10 23 1000 233168 100000000 "2333333316666668") rest)))

hypirion's solution:

(fn [n a b]
  (let [q #(quot (- n 1) %),
        s #(/ (* %1 (q %1) (+ 1 (q %1))) 2)]
    (+ (s a) (s b) (- (s (* a b))))))

jafingerhut's solution:

;; The trick to calculating the answer very quickly is knowing a few
;; math facts.
 
;; 1. The sum of all numbers from 1 through n, inclusive, is
;;    n*(n+1)/2.  See Wikipeda article on Arithmetic Progression for
;;    one proof of this (in a more general form).
 
;; 2. The sum of all numbers that evenly divide a that are less than n
;;    is:
;;        a + 2*a + 3*a + ... + k*a
;;    where k is largest integer such that k*a < n, which is
;;    floor((n-1)/a), or (quot (dec n) a).  By factoring out a, we can
;;    see this is equal to:
;;        a * (1 + 2 + 3 + ... + k)
;;    By fact 1, this is equal to:
;;        a * k * (k+1) / 2
 
;; 3. Numbers less than n that are evenly divisible by at least one of
;;    a and b are:
;;        divisible by a: a + 2*a + 3*a + ... + k*a
;;        divisible by b: b + 2*b + 3*b + ... + l*b     where l=(quot (dec n) b)
;;    We can use fact 2 to easily calculate these two sums, but that
;;    would double-count all of these numbers:
;;        divisible by a*b: a*b + 2*a*b + 3*a*b + ... + m*a*b
;;    We know those are the only numbers double-counted, because we
;;    are told that a and b are coprime.  Fortunately it is easy to
;;    calculate that sum and subtract it out.
 
(fn [n a b]
  (let [sum-multiples-of (fn [divisor]
                           (let [x (quot (dec n) divisor)]
                             (/ (*' divisor x (inc x)) 2)))]
    (- (+ (sum-multiples-of a) (sum-multiples-of b))
       (sum-multiples-of (* a b)))))
   
balint's solution:

(fn [n a b]
  (let [sum-of-divisors (fn [c]
                          (let [q (quot (dec n) c)]
                            (/ (*' c q (inc q)) 2)))]
    (+ (sum-of-divisors a)
       (sum-of-divisors b)
       (- (sum-of-divisors (* a b))))))
   
amcnamara's solution:

#(- (+ (% %2 %3) (% %2 %4)) (% %2 (* %3 %4)))
#(* %2 1/2 (quot (- % 1) %2) (+ (quot (- % 1) %2) 1))

amalloy's solution:

(fn [n a b]
  (- (apply - (for [d [(* a b) a b]
                    :let [m (quot (- n 1) d)]]
                (*' d m (+ m 1) 1/2)))))