Read a binary number
Difficulty:	Easy
Topics:	

Convert a binary number, provided in the form of a string, to its numerical value.

(= 0     (__ "0"))
(= 7     (__ "111"))
(= 8     (__ "1000"))
(= 9     (__ "1001"))
(= 255   (__ "11111111"))
(= 1365  (__ "10101010101"))
(= 65535 (__ "1111111111111111"))

#(Integer/parseInt % 2)

adereth's solution:

#(Integer/parseInt % 2)

mfikes's solution:

(fn bin-to-int [s] (apply + (map #(if (= \1 %1) (apply * (repeat %2 2)) 0) (reverse s) (range))))

chouser's solution:

#(read-string (str "2r" %))

aengelberg's solution:

#(Integer/parseInt % 2)

chunchangshao's solution:

#(java.lang.Integer/parseInt % 2)

hypirion's solution:

(fn [s]
  (reduce +
    (map *
      (map #(- (int %) (int \0)) 
        (reverse s))
      (iterate #(+ % %) 1))))
  
jafingerhut's solution:

(fn [s]
  (reduce +
          (map #(* % %2)
               (map #(read-string (str %)) (reverse s))
               (iterate #(* 2 %) 1))))
           
balint's solution:

#(Integer/parseInt % 2)

amcnamara's solution:

1
#(. Integer parseInt % 2)
amalloy's solution:

1
#(Integer/parseInt % 2)
stuarth's solution:

1
#(Integer/parseInt % 2)
dbyrne's solution:

1
2
3
4
5
6
7
8
9
(fn b
  ([x] (b (reverse x) 1 0))
  ([x c s]
    (if (empty? x)
      s
      (recur
        (rest x)
        (* c 2)
        (+ s (* c (- (int (first x)) 48)))))))