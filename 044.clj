;; 44 Rotate seq in either direction

(defn shifter [n s]
  (take (count s) (drop (mod n (count s)) (cycle s))))

(= (__ 2 [1 2 3 4 5]) '(3 4 5 1 2))
(= (__ -2 [1 2 3 4 5]) '(4 5 1 2 3))
(= (__ 6 [1 2 3 4 5]) '(2 3 4 5 1))
(= (__ 1 '(:a :b :c)) '(:b :c :a))
(= (__ -4 '(:a :b :c)) '(:c :a :b))

user=> (shifter 2 [1 2 3 4 5])
(3 4 5 1 2)
user=> (shifter -2 [1 2 3 4 5])
(4 5 1 2 3)

(fn [n coll] (take (count coll) (drop (mod n (count coll)) (cycle coll))))
adereth's solution:

#(let [rot (mod %1 (count %2))]
   (concat (drop rot %2) (take rot %2)))

mfikes's solution:

#(let [n (if (neg? %1) (- %1) %1)
       x (rem n (count %2))] 
  (if (neg? %1)
     (concat (take-last x %2) (drop-last x %2))
     (concat (drop x %2) (take x %2))))

chouser's solution:

#(let [c (count %2)] (take c (drop (mod % c) (cycle %2))))

aengelberg's solution:

#(let [n (count %2)]
   (concat (drop (mod %1 n) %2)(take (mod %1 n) %2)))

chunchangshao's solution:

(fn [n c] (if (neg? n)
            (reverse (#(loop [n %,c %2] (if (> n 0) (recur (dec n) (-> (next c) vec (conj (first c)))) (seq c))) (- n) (reverse c)))
            (#(loop [n %,c %2] (if (> n 0) (recur (dec n) (-> (next c) vec (conj (first c)))) (seq c))) n c))
  )

hypirion's solution:

(fn [n coll]
  (apply concat ((juxt drop take) (mod n (count coll)) coll)))

jafingerhut's solution:

(fn [n c]
  (let [n (mod n (count c))]
    (concat (drop n c) (take n c))))

balint's solution:

(fn [n coll]
  (take (count coll) (drop (mod n (count coll)) (cycle coll))))

amcnamara's solution:

#(let [c (count %2) 
       n (mod (if (neg? %) (+ c %) %) c)]
  (concat (drop n %2) (take n %2)))

stuarth's solution:

(fn [n s] (let [n* (mod n (count s))] (concat (drop n* s) (take n* s))))