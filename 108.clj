;; 108 Lazy Searching [m]

Takes any number of seqs,
each sorted from smallest to largest,
finds the smallest single number which appears
in all of the sequences.
Seqs may be infinite, searches lazily.

(defn lazy-search [& colls]
  (if (= 1 (count colls))
    (first (first colls)) 
    (let [heads (map first colls) largest (apply max heads)]
      (if (apply = heads)
        largest
        (recur (map (fn [c] (drop-while #(< % largest) c)) 
                 colls))))))
#'user/lazy-search
user> (lazy-search [3 4 5])
3
user> (lazy-search [1 2 3 4 5 6 7] [0.5 3/2 4 19])
4
user> (lazy-search (range) (range 0 100 7/6) [2 3 5 7 11 13])
7
user> (lazy-search (map #(* % % %) (range))   (filter #(zero? (bit-and % (dec %))) (range))
          (iterate inc 20))
64

(= 64 (__ (map #(* % % %) (range)) ;; perfect cubes
          (filter #(zero? (bit-and % (dec %))) (range)) ;; powers of 2
          (iterate inc 20))) ;; at least as large as 20

#(let [a (map first %&)
       b (apply min a)]
   (if (apply = a)
     b
     (recur (map (fn [[x & y :as z]] (if (= x b) y z)) %&))))

(fn lazy-search [& colls] (if (= 1 (count colls)) (first (first colls)) 
(let [heads (map first colls) largest (apply max heads)] (if (apply = 
heads) largest (recur (map (fn [c] (drop-while #(< % largest) c)) 
colls))))))

adereth's solution:

(fn [& s]
  (loop [s s]
    (if (every? #(= (first %) (ffirst s)) s) 
      (ffirst s)
      (let [t (sort-by first s)]
            (recur (cons (rest (first t)) (rest t)))))))
        
mfikes's solution:

(fn [& xs]
  (loop [seqs xs]
    (if (apply = (map first seqs))
      (ffirst seqs)
      (let [max-first (apply max (map first seqs))]
        (recur (map #(if (< (first %) max-first) (rest %) %) seqs))))))
    
chouser's solution:

#(let [a (map first %&)
       b (apply min a)]
   (if (apply = a)
     b
     (recur (map (fn [[x & y :as z]] (if (= x b) y z)) %&))))
 
aengelberg's solution:

(let [until (fn [i l]
              (if (empty? l)
                nil
                (if (>= (first l) i)
                  (first l)
                  (recur i (rest l)))))]
  (fn [l & ls]
    (first
     (filter (fn [x]
               (every? #(= (until x %) x) ls))
             l))))
         
chunchangshao's solution:

#(case (count %&)
   1 3
   2 4
   ((fn [x] (if (= (nth (first x) 3) 27) 64 7)) %&))

hypirion's solution:

(fn lzy [& seqs] 
  (let [fst (map first seqs)
        m (reduce min fst)]
  (if (apply = fst)
    m
    (apply lzy (map #(remove #{m} %) seqs)))))

jafingerhut's solution:

;; If the first value in every sequence is the same (checked as min =
;; max), we are done.  Otherwise, repeat with the first element
;; removed from the sequence whose first element is smallest.
 
(fn [& s]
  (loop [v (vec s)]
    (let [first-vals (map first v)
          smallest (apply min first-vals)]
      (if (= smallest (apply max first-vals))
        smallest
        (let [i (apply min-key #(nth first-vals %) (range (count v)))]
          (recur (update-in v [i] next)))))))
      
balint's solution:

(fn [& colls]
  (letfn [(lazy-has [[f & r :as c] x] ; we know c is a lazy-seq that is increasing
            (if (< x f)
              false
              (or (= x f) (lazy-has r x))))
          (all-has? [cs x]
            (reduce (fn [has c] (and has (lazy-has c x)))
                    true
                    cs))]
  (some
    #(and (all-has? (rest colls) %) %)
    (first colls))))

amcnamara's solution:

#(loop [c %&]
   (let [[a & b] (vec (apply sorted-set (map first c)))]
     (if b (recur (for [[i & j :as k] c] (if (= a i) j k))) a)))
 
amalloy's solution:

(letfn [(in-lists? [x lists]
          (if-let [[list & more] (seq lists)]
            (and (= x (first (drop-while #(< % x) list)))
                 (recur x more))
            true))]
  (fn [source & sinks]
    (first (filter #(in-lists? % sinks) source))))