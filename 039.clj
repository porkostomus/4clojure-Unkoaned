{:_id 39 :restricted ["interleave"], :title "Interleave Two Seqs"
:tests ["(= (__ [1 2 3] [:a :b :c]) '(1 :a 2 :b 3 :c))"
"(= (__ [1 2] [3 4 5 6]) '(1 3 2 4))"
"(= (__ [1 2 3 4] [5]) [1 5])"
"(= (__ [30 20] [25 15]) [30 25 20 15])"]
:description "Write a function which takes two sequences and returns the first item from each,
then the second item from each, then the third, etc."
:tags ["easy" "seqs" "core-functions"]}

user=> (source mapcat)
(defn mapcat
  "Returns result of applying concat to
  result of applying map to f and colls.
  Thus function f should return a collection.
  Returns transducer when no colls provided"
  ([f] (comp (map f) cat))
  ([f & colls]
     (apply concat (apply map f colls))))

(mapcat list [1 2 3] [:a :b :c])
;;=> (1 :a 2 :b 3 :c)
(mapcat list [1 2] [3 4 5 6])
;;=> (1 3 2 4)
(mapcat list [1 2 3 4] [5])
;;=> (1 5)
(mapcat list [30 20] [25 15])
;;=> (30 25 20 15)

#(mapcat vector %1 %2)

adereth's solution:

(fn [s1 s2]
  (flatten (map list s1 s2)))

mfikes's solution:

mapcat vector

chouser's solution:

mapcat list

aengelberg's solution:

mapcat list

chunchangshao's solution:

#(loop [x % y %2 res []]
        (if (or (nil? x) (nil? y))
          res
          (recur (next x) (next y) (conj res (first x) (first y)))))

hypirion's solution:

#(mapcat list %1 %2)

jafingerhut's solution:

#(mapcat vector %1 %2)

balint's solution:

(fn [& colls] (apply mapcat #(list %1 %2) colls))

amcnamara's solution:

mapcat list

amalloy's solution:

mapcat list

stuarth's solution:

(fn [& ls] (apply mapcat (fn [& els] els) ls))

dbyrne's solution:

(partial mapcat list)