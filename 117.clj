;; 117 For Science! [h]

hashes (#) = maze walls, M = starting point, C = cheese
Can go only up/down/left/right
Returns true iff the maze is solvable by the mouse.

(= true  (__ ["M   C"]))
(= false (__ ["M # C"]))
(= true  (__ ["#######"
              "#     #"
              "#  #  #"
              "#M # C#"
              "#######"]))
(= false (__ ["########"
              "#M  #  #"
              "#   #  #"
              "# # #  #"
              "#   #  #"
              "#  #   #"
              "#  # # #"
              "#  #   #"
              "#  #  C#"
              "########"]))
(= false (__ ["M     "
              "      "
              "      "
              "      "
              "    ##"
              "    #C"]))
(= true  (__ ["C######"
              " #     "
              " #   # "
              " #   #M"
              "     # "]))
(= true  (__ ["C# # # #"
              "        "
              "# # # # "
              "        "
              " # # # #"
              "        "
              "# # # #M"]))

(fn f [a b c]
  (and (not= a c)
    (if (re-seq #"CM|MC" (pr-str c))
      true
      (f b c (apply map str (map #(.replaceAll % " M|M " "MM") c))))))
0
0

(defn f [grid]
  (let [neighbors (fn [[x y]] [[(inc x) y] [(dec x) y] [x (inc y)] [x (dec y)]])
        parts (for [y (range (count grid))
                    x (range (count (nth grid y)))
                    :let [e (get-in grid [y x])]]
                {({\C :cat \M :mouse \# :wall \space :space} e) [x y]})
        game (apply merge-with conj {:wall [] :space []} parts)
        spaces (conj (set (:space game)) (:mouse game))]
    (loop [open [(:cat game)] visited #{}]
      (cond
        (empty? open) false
        (= (first open) (:mouse game)) true
        :else (let [visited (conj visited (first open))
                    neigh (filter spaces (neighbors (first open)))
                    neigh (remove visited neigh)
                    open (concat (rest open) (remove visited neigh))]
                (recur open visited))))))
            
adereth's solution:

(fn science [maze]
  (let [neighbor-coords [[-1 0] [1 0] [0 -1] [0 1]]
        next-maze (fn [m] (vec (for [x (range (count m))]
                            (reduce str ""
                                    (for [y (range (count (first m)))]
                                      (let [next-to-mouse?
                                            (some #(= % \M)
                                                  (map #(get-in m (map + % [x y]))
                                                       neighbor-coords))]
                                        (condp = (get-in m [x y])
                                          \# \#
                                          \M \M
                                          \space (if next-to-mouse? \M \space)
                                          \C (if next-to-mouse? \* \C)
                                          \* \*
                                          \$
                                          )))))))
        
        ]
    (true? (some #(= % \*) (reduce str (nth (iterate next-maze maze) 100))))))

mfikes's solution:

(fn mouse-cheese [board]
  (letfn [(next-board [board]
                      (let [board-matrix (mapv #(vec %) board)]
                        (map-indexed
                          (fn [r row]
                            (apply str
                                   (map-indexed
                                     (fn [c cell]
                                       (let [adjacent (or
                                                        (= \M (get-in board-matrix [(dec r) c]))
                                                        (= \M (get-in board-matrix [r (dec c)]))
                                                        (= \M (get-in board-matrix [(inc r) c]))
                                                        (= \M (get-in board-matrix [r (inc c)])))]
                                         (if (and adjacent
                                                  (or (= \space cell)
                                                      (= \C cell)))
                                           \M
                                           cell)))
                                     row)))
                          board-matrix)))]
    (not (contains? (set (mapcat seq
                             (loop [board board]
                               (let [board' (next-board board)]
                                 (if (= board' board)
                                   board
                                   (recur board')))))) \C))))
                               
chouser's solution:

(defn f [a b c]
  (and (not= a c)
    (if (re-seq #"CM|MC" (pr-str c))
      true
      (f b c (apply map str (map #(.replaceAll % " M|M " "MM") c))))))
0
0

aengelberg's solution:

(fn [g]
  (let [r (count g)
        c (count (first g))
        a (for [i (range r)
                j (range c)]
            [i j])
        n (fn [p]
            (let [[x y] p]
              (filter (set a)
                      [[(inc x) y]
                       [(dec x) y]
                       [x (inc y)]
                       [x (dec y)]])))
        m (first (filter #(= (get-in g %) \M) a))
        c (first (filter #(= (get-in g %) \C) a))]
    (loop [q [m]
           v #{}]
      (cond
       (empty? q) false
       (= (first q) c) true
       (v (first q))(recur (vec (rest q)) v)
       (= (get-in g (first q)) \#)(recur (vec (rest q)) v)
       :else (recur (apply conj (vec (rest q)) (n (first q)))
                    (conj v (first q)))))))
                
chunchangshao's solution:

#(case (count (apply str (map (fn [coll] (clojure.string/replace coll " " "")) %)))
   (3 44 5) false
   true)

hypirion's solution:

(fn [maze]
  (let [at (fn [x y] (.charAt (maze y) x))
        fnd (fn [v] (some (fn [[x y]] (if (not= -1 x) [x y])) 
                       (map-indexed (fn [y s] [(.indexOf s v) y]) maze)))
        C (fnd "C")
        M (fnd "M")
        h (count maze)
        w (count (first maze))
        look (apply merge
                (for [x (range w), y (range h) :when (not= \# (at x y))]
                  {[x y] (map #(map + [x y] %) [[1 0] [0 1] [-1 0] [0 -1]])}))]
    (loop [hs #{M}]
      (let [n-hs (into hs (mapcat look hs))]
        (if (= n-hs hs)
          (= (hs C) C)
          (recur n-hs))))))
      
jafingerhut's solution:

;; Uses a slightly modified version of my solution to the Graph
;; Connectivity problem.  The idea is to transform the maze into a
;; graph with a node for each non-wall space, and an edge between two
;; nodes if one space can be reached from the other in a single step.
;; Then if the mouse and the cheese are in the same connected
;; component of that graph, the mouse can reach the cheese.
 
(fn [maze]
  (let [comps (fn [edges]
                (reduce (fn [c [u v]]
                          (let [s (or (first (filter #(% u) c)) #{u})
                                t (or (first (filter #(% v) c)) #{v})]
                            (conj (disj c s t) (clojure.set/union s t))))
                        #{} edges))
        rows (count maze)
        cols (count (first maze))
        thing (fn [r c]
                (if (and (< -1 r rows) (< -1 c cols))
                  (subs (maze r) c (inc c))
                  "#"))
        graph (for [r1 (range rows)
                    c1 (range cols)
                    :let [t1 (thing r1 c1)]
                    [delta-r delta-c] [[1 0] [0 1]]
                    :let [r2 (+ r1 delta-r)
                          c2 (+ c1 delta-c)
                          t2 (thing r2 c2)]
                    :when (and (not= t1 "#") (not= t2 "#"))]
                [ (if (#{"M" "C"} t1) t1 [r1 c1])
                  (if (#{"M" "C"} t2) t2 [r2 c2]) ])]
    (contains? (first (filter #(% "M") (comps graph)))
               "C")))
           
balint's solution:

(fn [mz]
  (let [cols (count (first mz))
        rows (count mz)
        maze-cell-indexes (for [y (range rows) x (range cols)] [y x])
        find (fn [elt]
               (first
                 (filter
                   (fn [[x y]] (= (get-in mz [x y]) elt))
                   maze-cell-indexes)))
        mouse (find \M)
        cheese (find \C)
        neighbors (memoize (fn [[x y]]
                    (for [[dx dy] [[0 -1] [-1 0] [1 0] [0 1]]
                          :let [nx (+ x dx) ny (+ y dy)]
                          :when (and
                                  (< -1 nx rows)
                                  (< -1 ny cols)
                                  (not= (get-in mz [nx ny]) \#))]
                      [nx ny])))]
    (loop [reached #{mouse}]
      (if
        (reached cheese) true
        (let [newly-reached
              (into reached
                    (mapcat neighbors reached))]
          (if (= newly-reached reached)
            false
            (recur newly-reached)))))))
        
amcnamara's solution:

(fn g [b]
  (let [Mi (map first (filter #(= \M (second %)) (map-indexed vector (apply str b))))
        Mi (map #(vector (int (/ % (count (first b)))) (mod % (count (first b)))) Mi)
        ne (fn [[y x]] (let [mi [y x]
                             mu (when (pos? y)                      [(dec y) x])
                             md (when (< y (dec (count b)))         [(inc y) x])
                             ml (when (pos? x)                      [y (dec x)])
                             mr (when (< x (dec (count (first b)))) [y (inc x)])]
                         (filter vector? [mi mu md ml mr])))
        Ma (set (reduce into [] (map ne Mi)))
        Mn (clojure.set/difference Ma Mi)
        Mn (filter #(not= \# (get-in b %)) Mn)]
    (if (some #{\C} (map #(get-in b %) Mn))
      true
      (if (empty? Mn)
        false
        (g (assoc b (first (first Mn))
                    (apply str (assoc (vec (seq (get b (first (first Mn))))) (second (first Mn)) \M))))))))
                
amalloy's solution:

(fn [board]
  (let [names {\space :open
               \# :wall
               \M :goal
               \C :start}
        board (vec (for [row board]
                     (vec (for [col row]
                            (names col)))))
        h (count board)
        w (count (board 0))
        [start goal] (for [token [:start :goal]
                           y (range h)
                           :let [row (board y)]
                           x (range w)
                           :when (= token (row x))]
                       [y x])
        valid? (fn [[y x :as pos]]
                 (and (not-any? neg? pos)
                      (< y h) (< x w)))
        deltas [[0 1] [1 0] [0 -1] [-1 0]]
        neighbors (fn [pos]
                    (filter valid?
                            (for [d deltas]
                              (map + pos d))))
        reachable? (fn reachable? [board pos]
                     (and (not= :wall (get-in board pos))
                          (or (= goal pos)
                              (let [new-board (assoc-in board pos :wall)]
                                (some #(reachable? new-board %) (neighbors pos))))))]
    (boolean (reachable? board start))))