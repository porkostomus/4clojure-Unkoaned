{:_id 75 :title "Euler's Totient Function"
:tests [
"(= (__ 1) 1)"
"(= (__ 10) (count '(1 3 7 9)) 4)"
"(= (__ 40) 16)"
"(= (__ 99) 60)"],
:description "Two numbers are coprime if their greatest common divisor equals 1. 
Euler's totient function f(x) is defined as the number of positive integers less than x which are coprime to x.
The special case f(1) equals 1.  Write a function which calculates Euler's totient function."
:tags ["medium"]}

(fn [a]
  (count
    (for [b (range a)
          :when (not-any? #(= 0 (rem a %) (rem b %)) (range 2 a))]
      b)))

(fn [n]
  (if (= n 1)
    1
    (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))]
      (count (filter #{1} (map (partial gcd n) (range 1 n)))))))
  
adereth's solution:

(fn [x]
  (count
   (filter #(= 1
               (loop [a x b %]
                 (if (zero? b) a
                   (recur b (mod a b)))))
           (range 1 (inc x)))))
       
mfikes's solution:

(fn [n]
  (if (= 1 n)
    1
    (let [gcd (fn [a b] (if (zero? b) a (recur b (mod a b))))]
      (count (filter #(= 1 (gcd % n))
                     (range 1 n))))))
                 
chouser's solution:

(fn [a]
  (count
    (for [b (range a)
          :when (not-any? #(= 0 (rem a %) (rem b %)) (range 2 a))]
      b)))
  
aengelberg's solution:

{1 1 10 4 40 16 99 60}

chunchangshao's solution:

#(case %
   1 1
   10 4
   40 16
   60)

hypirion's solution:

(fn [n]
  (letfn [(gcd [n m] (if (zero? m) n (gcd m (mod n m))))]
  (if (= n 1) 1
      (count (filter #(= 1 (gcd n %)) (range 1 n))))))
  
jafingerhut's solution:

;; Use my solution to the Greatest common divisor problem as part of
;; this answer.
 
(fn [n]
  (let [gcd (fn [a b]
              (cond (< a b) (recur b a)
                    (zero? b) a
                    :else (recur b (mod a b))))]
    (if (= n 1)
      1
      (count (filter #(= 1 (gcd % n)) (range 1 n))))))
  
balint's solution:

(fn [n]
  (letfn [(coprimes? [a b]
            (= 1
              ((fn [a b]
                (if (zero? (rem a b))
                  b
                  (recur b (rem a b)))) a b)
             ))]
  (if (= n 1)
    1
    (count (filter #(coprimes? n %) (range 1 n))))))

amcnamara's solution:

(fn f [n]
  (count (filter (fn [i] (= 1 (#(if (= 0 %2) % (recur %2 (mod % %2))) i n))) (range n))))

stuarth's solution:

(fn [a]
    (letfn [(gcd [a b] (first
                        (for [n (reverse (range 1 (+ 1 (min a b)))) :when (= 0 (rem a n) (rem b n))]
                          n)))
            (co-p? [a b] (= 1 (gcd a b)))]
      (count (filter #(co-p? a %) (range 1 (+ 1 a))))))
